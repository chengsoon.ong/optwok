"""
A container for the movielens dataset

MovieLens data sets were collected by the GroupLens Research Project
at the University of Minnesota.

Herlocker, J., Konstan, J., Borchers, A., Riedl, J.. An Algorithmic
Framework for Performing Collaborative Filtering. Proceedings of the
1999 Conference on Research and Development in Information
Retrieval. Aug. 1999.
"""

from numpy.random import rand
from numpy import zeros
import numpy
import tarfile
import os
import numpy

from optwok.named_array import NArray
from coord_descent import reg_matrix_factorization, nmae, rmse

class DataMovieLens(object):
    """
    A container for the movielens dataset
    """
    def __init__(self, filename, num_row=0, num_col=0):
        """Load the archive into memory"""
        self.source_file = tarfile.open(filename,'r')
        self.perf = NArray(['Split','Perf'])
        self.perf.add_keys(['RMSE','NMAE'], 'Perf')
        self.num_row = num_row
        self.num_col = num_col

    def get_standard_split(self):
        """Load the standard split, for movielens 100K"""
        self.data_list = ['u1','u2','u3','u4','u5','ua','ub']
        self.perf.add_keys(self.data_list, 'Split')
        base_dir = self.source_file.getnames()[0] + '/'
        self.get_info(base_dir)

        self.train_data = []
        for data in self.data_list:
            self.train_data.append(self.get_matrix(base_dir, data+'.base'))

        self.test_data = []
        for data in self.data_list:
            self.test_data.append(self.get_matrix(base_dir, data+'.test'))

    def get_random_split(self, frac_train=0.5, sep='::'):
        """Load all the data, and split randomly"""
        self.data_list = ['r1']
        self.perf.add_keys(self.data_list, 'Split')
        train_data = zeros((self.num_row, self.num_col))
        test_data = zeros((self.num_row, self.num_col))
        contents = self.source_file.getnames()
        if 'ml-data/u.data' in contents:
            cur_file = self.source_file.extractfile('ml-data/u.data')
        elif 'ratings.dat' in contents:
            cur_file = self.source_file.extractfile('ratings.dat')
        else:
            print "Data file not found"
            print contents
        for line in cur_file:
            user_id, item_id, rating, timestamp = line.split(sep)
            if rand() < frac_train:
                train_data[int(user_id)-1, int(item_id)-1] = float(rating)
            else:
                test_data[int(user_id)-1, int(item_id)-1] = float(rating)
        cur_file.close()
        self.train_data = []
        self.test_data = []
        self.train_data.append(train_data)
        self.test_data.append(test_data)

    def get_info(self, base_dir):
        """Parse u.info to obtain the number of rows, columns and ratings"""
        info_file = self.source_file.extractfile(base_dir+'u.info')
        line1 = info_file.readline()
        line2 = info_file.readline()
        line3 = info_file.readline()
        info_file.close()
        self.num_row = int(line1.split(' ')[0])
        self.num_col = int(line2.split(' ')[0])
        self.num_rating = int(line3.split(' ')[0])

    def get_matrix(self, base_dir, part_name):
        """Parse the part in the tar source into a matrix"""
        print 'Parsing %s' % part_name
        mat = zeros((self.num_row, self.num_col))
        cur_file = self.source_file.extractfile(base_dir+part_name)
        for line in cur_file:
            user_id, item_id, rating, timestamp = line.split()
            mat[int(user_id)-1, int(item_id)-1] = float(rating)
        cur_file.close()
        return mat

    def matrix_fac(self, reg_param):
        """Train and test on all splits"""
        for ix in range(len(self.data_list)):
            print 'Matrix factorization on split %s' % self.data_list[ix]
            Y_train = self.train_data[ix]
            mask_train = Y_train > 0
            X_pred = reg_matrix_factorization(Y_train, mask_train, reg_param)

            Y_test = self.test_data[ix]
            mask_test = Y_test > 0
            abs_error = nmae(numpy.round(X_pred), Y_test, mask_test)
            sqr_error = rmse(numpy.round(X_pred), Y_test, mask_test)
            print 'RMSE = %f, NMAE = %f' % (sqr_error, abs_error)
            self.perf.fill_value(abs_error, [self.data_list[ix], 'NMAE'],
                                 ['Split','Perf'])
            self.perf.fill_value(sqr_error, [self.data_list[ix], 'RMSE'],
                                 ['Split','Perf'])

            out_file = self.data_list[ix] + '.pred'
            write_pred(numpy.round(X_pred), mask_test, out_file)

def write_pred(pred, mask, out_file_name):
    """Write the predictions to file"""
    out_file = open(out_file_name,'w')
    (rows, cols) = numpy.nonzero(mask)
    for ix in range(len(rows)):
        out_file.write('\t'.join([str(rows[ix]+1), str(cols[ix]+1),
                                  str(int(pred[rows[ix],cols[ix]]))])
                       + '\n')
    out_file.close()


def movielens100K_standard():
    """Experiment with the standard movielens 100K split"""
    ml100K = DataMovieLens(os.path.expanduser('~')+'/Data/movielens/ml-data.tar__0.gz')
    ml100K.get_standard_split()
    ml100K.matrix_fac(10000.0)
    print ml100K.perf.keys('Split')
    print ml100K.perf.keys('Perf')
    print ml100K.perf.content

def movielens100K_random():
    """Experiment with random 80% movielens 100K split"""
    ml_random = DataMovieLens(os.path.expanduser('~')+'/Data/movielens/ml-data.tar__0.gz',
                              943, 1682)
    ml_random.get_random_split(0.8, '\t')
    ml_random.matrix_fac(10000.0)
    print ml_random.perf.content


if __name__ == '__main__':
    ml_random = DataMovieLens(os.path.expanduser('~')+'/Data/movielens/million-ml-data.tar__0.gz',
                              6040, 4000)
    ml_random.get_random_split(0.5)
    ml_random.matrix_fac(40000.0)
    print ml_random.perf.content

