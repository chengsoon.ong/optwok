"""
Implements a version of Hazan's algorithm based on
Jaggi and Sulovsky, 'A simple algorithm for nuclear norm
regularized problems', ICML 2010
"""
import numpy
from numpy import outer, sort, dot, ceil, sqrt, trace
from numpy import zeros, concatenate
from numpy.random import rand
from numpy.linalg import norm
from scipy.sparse.linalg import eigsh
from scipy.sparse.linalg.eigen.arpack.arpack import ArpackNoConvergence

def reg_matrix_factorization(label, mask, reg_param):
    """Regularized matrix factorization.
    Squared loss with nuclear norm constraint
    """
    X = coord_descent(label/reg_param, mask, squared_loss_grad, 1.0)
    X *= reg_param
    return X

def coord_descent(label, mask, grad_func, convex_curv, target_acc=0.05, progress=10):
    """Implements Hazan's algorithm
    Elad Hazan, 'Sparse approximate solutions to semidefinite programs'
    LATIN pp. 306--316, 2008
    """
    #print trace(label)
    #print label[:10,:10]
    (num_row, num_col) = label.shape
    num_dim = num_row + num_col
    Y = zeros((num_dim,num_dim))
    Y[:num_row,-num_col:] = label
    Y[-num_col:,:num_row] = label.T
    v0 = rand(num_dim)
    v0 /= norm(v0)
    Z = outer(v0,v0)
    max_iter = int(ceil(4.0*convex_curv/target_acc))
    #max_iter = 7
    for k in range(1,max_iter):
        X = Z[:num_row,-num_col:]
        if k%progress == progress-1:
            print 'Iteration %d of %d' % (k, max_iter-1)
            print rmse(X, label, mask)
        grad = -grad_func(X, label, mask)
        v = approx_ev( grad, convex_curv/(1.0*k*k))
        alpha = 1.0/k
        Z += alpha * (outer(v,v) - Z)
    X = Z[:num_row,-num_col:].copy()
    return X

def squared_loss_grad_big(Z, Y, mask):
    """Gradient of the squared loss, only in the masked region"""
    (num_row, num_col) = mask.shape
    num_dim = num_row + num_col
    big_grad = zeros((num_dim,num_dim))
    grad = Z[:num_row,-num_col:]-Y[:num_row,-num_col:]
    grad[~mask] = 0.0
    big_grad[:num_row,-num_col:] = grad
    big_grad[-num_col:,:num_row] = grad.T
    return big_grad

def approx_ev_full(M, approx):
    """Return the maximum eigenvector up to approximation error approx"""
    try:
        (val, vec) = eigsh(M, k=1, which='LA', tol=approx)
    except ArpackNoConvergence:
        print 'Eigenvalue approximation did not converge'
        return zeros(M.shape[0])
    return vec

def squared_loss_grad(Z, Y, mask):
    """Gradient of the squared loss, only in the masked region"""
    grad = Z - Y
    grad[~mask] = 0.0
    return grad

def approx_ev_MMT(M, approx):
    """Return the maximum eigenvector up to approximation error approx.
    Use the fact that matrix is bipartite
    """
    MMT = dot(M,M.T)
    #print 'Solving %d by %d eigenvalue problem' % MMT.shape
    (val, vec1) = eigsh(MMT, k=1, which='LA', tol=approx)
    vec2 = dot(M.T, vec1)
    vec2 /= norm(vec2)
    vec = concatenate([vec1,vec2])/sqrt(2)
    return vec

def approx_ev_MTM(M, approx):
    """Return the maximum eigenvector up to approximation error approx.
    Use the fact that matrix is bipartite
    """
    MTM = dot(M.T,M)
    #print 'Solving %d by %d eigenvalue problem' % MTM.shape
    (val, vec2) = eigsh(MTM, k=1, which='LA', tol=approx)
    vec1 = dot(M, vec2)
    vec1 /= norm(vec1)
    vec = concatenate([vec1,vec2])/sqrt(2)
    return vec

def approx_ev(M, approx):
    """Return the maximum eigenvector up to approximation error approx.
    Use the fact that matrix is bipartite
    """
    num_row, num_col = M.shape
    if num_row > num_col:
        return approx_ev_MTM(M, approx)
    else:
        return approx_ev_MMT(M, approx)

def rmse(pred, label, mask=None):
    """Root mean squared error for matrices"""
    norm_fac = 1.0
    diff = label - pred
    if mask is not None:
        diff[~mask] = 0.0
        norm_fac = numpy.sum(mask)
    tot = sqrt(numpy.sum(diff*diff))
    return tot/norm_fac

def nmae(pred, label, mask=None):
    """Normalized mean absolute error"""
    norm_fac = 1.0
    diff = label - pred
    if mask is not None:
        diff[~mask] = 0.0
        norm_fac = numpy.sum(mask)
    tot = 0.25*abs(numpy.sum(diff))
    return tot/norm_fac

def create_recommend(num_row=500, num_col=300, frac_observed=0.5,
                     num_class_row=5, num_class_col=11, num_score=5):
    """
    Create a toy matrix mimicing a recommender system matrix

    returns a full matrix and a mask denoting observed values.
    """
    # The full matrix
    tot_rank = num_class_row * num_class_col
    U = rand(num_row, tot_rank)
    V = rand(tot_rank, num_col)
    X = dot(U,V)
    X_norm = numpy.round(num_score*(X-numpy.min(X))/(0.1+numpy.max(X)-numpy.min(X)) + 0.51)

    # The mask of observed values
    vals = rand(num_row, num_col)
    sorted_vals = sort(vals.flatten())
    cutoff = sorted_vals[round((1-frac_observed)*num_row*num_col)]
    mask = vals > cutoff
    
    return X_norm, mask

def test_approx_ev():
    """Check that the bipartite version is the same as the full version"""
    num_row = 23
    num_col = 17
    num_dim = num_row + num_col
    tol = 1e-3
    X, mask = create_recommend(num_row, num_col, 0.8,1,1)
    Z = zeros((num_dim, num_dim))
    Z[:num_row,-num_col:] = X
    Z[-num_col:,:num_row] = X.T

    vec_MMT = approx_ev_MMT(X, tol)
    vec_MTM = approx_ev_MTM(X, tol)
    vec_full = approx_ev_full(Z, tol)
    print numpy.hstack([vec_MMT, vec_MTM, vec_full])
    print 'Diff MMT: %f, Diff MTM: %f' % (norm(vec_MMT-vec_full), norm(vec_MTM-vec_full))
    print 'Diff MMT: %f, Diff MTM: %f' % (norm(vec_MMT+vec_full), norm(vec_MTM+vec_full))

def demo_recommend():
    """Small dataset for sanity check"""
    print 'Creating data'
    X_full, mask = create_recommend(9,13,0.8,1,1)
    Y = X_full.copy()
    Y[~mask] = numpy.NaN

    print 'Running matrix completion'
    X_pred = reg_matrix_factorization(Y, mask, 1000.0)
    error = rmse(numpy.round(X_pred), X_full)

    print 'True data'
    print X_full

    print 'Training data'
    print Y

    print 'Completed matrix'
    print numpy.round(X_pred)
    
    print 'RMSE = %f' % error

def demo_movielens(num_row, num_col, frac_observed):
    """Simulate movielens data"""
    print 'Simulate %d rows, %d columns, %f observed' % (num_row, num_col, frac_observed)
    
    print 'Creating data'
    X_full, mask = create_recommend(num_row=num_row, num_col=num_col,
                                    frac_observed=frac_observed)
    Y = X_full.copy()
    Y[~mask] = numpy.NaN

    print 'Running matrix completion'
    X_pred = reg_matrix_factorization(Y, mask, 1000.0)
    X_pred[mask] = Y[mask]
    error = rmse(numpy.round(X_pred), X_full)

    #print 'True data'
    #print X_full[:10,:13]

    #print 'Training data'
    #print Y[:10,:13]

    #print 'Completed matrix'
    #print numpy.round(X_pred)[:10,:13]

    print 'RMSE = %f' % error
    print 'Normalized RMSE = %f' % (error/numpy.sum(mask))    

if __name__ == '__main__':
    #test_approx_ev()
    #demo_recommend()
    demo_movielens(943, 1682, 0.063)
    #demo_movielens(6040, 3706, 0.045)
    #demo_movielens(69878, 10677, 0.013)
