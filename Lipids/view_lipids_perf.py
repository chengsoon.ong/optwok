"""Investigate the performance per lipid"""

import sys
from os.path import isfile
import numpy
from numpy import zeros, eye, array
import matplotlib as mpl
import matplotlib.pyplot as plt
from optwok.io_pickle import load, save
from optwok.evaluation import r2, spearman
import expttools
import safhs_data

def group_boxplot(data):
    """Plot a 3D array as groups of boxplots
    Dimension 0 is the data in a particular box. 
    Dimension 1 is the x-axis.
    Dimension 2 provides the groups
    """
    (num_split, num_task, num_group) = data.shape
    num_bar = num_task*(num_group+1) + 1
    locs = array(range(1, num_bar, num_bar/num_task))
    fig = plt.figure(figsize=(30,5))
    ax = fig.add_subplot(111)
    for idx_group in range(num_group):
        cur_locs = array(locs) + idx_group - num_group/2
        ax.boxplot(data[:,:,idx_group], positions=cur_locs)
        plt.hold(True)

def diff_boxplot(data):
    """Plot the difference between the 2 groups"""
    fig = plt.figure(figsize=(30,11))
    ax = fig.add_subplot(211)
    ax.boxplot(data[:,:,0])
    ax.plot([0,323],[0,0],'k:',alpha=0.5)
    ax.set_xticklabels([])
    ax.set_ylabel('Correlation (higher is better)')
    ax.set_title('Performance from learning task similarity')
    ax = fig.add_subplot(212)
    ax.boxplot(data[:,:,0]-data[:,:,1])
    ax.plot([0,323],[0,0],'k:',alpha=0.5)
    ax.set_xticklabels([])
    ax.set_ylabel('Increased correlation')
    ax.set_xlabel('Lipids')
    ax.set_title('Improvement over task independent setting')

def predict(data, res_dir, split_idx, chrom):
    """Prediction for all values of the regularization parameter"""
    okl_name = safhs_data.kern_mach_name(res_dir, chrom, split_idx)
    print('Loading %s' % okl_name)
    kern_mach = load(okl_name)
    num_param = len(kern_mach.all_reg_param)
    test_ex = data.kernel_test(split_idx)
    test_lab = data.labels_test(split_idx)

    print('Predicting split %d' % split_idx)
    Ky_preds = kern_mach.predict_reg_path(test_ex)
    eye_preds = kern_mach.predict_reg_path(test_ex, ident_Ky=True)
    Ky_perf = zeros((num_param, data.num_class))
    eye_perf = zeros((num_param, data.num_class))
    for ixp in range(num_param):
        for ixt in range(data.num_class):
            Ky_perf[ixp, ixt] = r2(Ky_preds[ixt,:,ixp], test_lab[ixt,:]) 
            eye_perf[ixp, ixt] = r2(eye_preds[ixt,:,ixp], test_lab[ixt,:]) 
    return Ky_perf, eye_perf

def compare_average(conf, res_dir, ktype, chrom):
    """Plot the performance for each regularization parameter"""
    print('Comparing learned vs identity kernel')
    kern_mach = load(safhs_data.kern_mach_name(res_dir, chrom, 0))
    num_param = len(kern_mach.all_reg_param)
    data = safhs_data.DatasetKernel(conf['data_dir'], kernel_file=chrom, label_file=conf['labels'],
                                    kernel_type=ktype)
    Ky_perf = zeros((conf['num_split'], num_param, data.num_class))
    eye_perf = zeros((conf['num_split'], num_param, data.num_class))

    for split_idx in range(conf['num_split']):
        Ky_perf[split_idx,:,:], eye_perf[split_idx,:,:] = predict(data, res_dir, split_idx, chrom)
    save('perf.pkl',{'Ky_perf': Ky_perf, 'eye_perf': eye_perf})
        
    Ky_max = numpy.max(Ky_perf, axis=2)
    eye_max = numpy.max(eye_perf, axis=2) 
    Ky_mean = numpy.mean(Ky_perf, axis=2) 
    eye_mean = numpy.mean(eye_perf, axis=2) 

    print('Plot and save')
    expttools.plot_corr('SAFHS: lipids', kern_mach.all_reg_param, Ky_max, eye_max, 
                        '%s/lipids_corr_max_%s.pdf' % (res_dir, chrom))
    expttools.plot_corr('SAFHS: lipids', kern_mach.all_reg_param, Ky_mean, eye_mean, 
                        '%s/lipids_corr_mean_%s.pdf' % (res_dir, chrom))

    
def show_performance(conf, ktype, chrom):
    res_dir = conf['result_dir'] + ktype + '/'
    compare_average(conf, res_dir, ktype, chrom)

    all_perf = load(safhs_data.perf_name(res_dir, chrom))
    #group_boxplot(all_perf)
    diff_boxplot(all_perf)
    plt.savefig('%s/performance_pertask_%s.pdf' % (res_dir, chrom))

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: python %s configure.py ktype chrom' % sys.argv[0])
        exit(1)
    conf = safhs_data.configure_from_file(sys.argv[1])
    ktype = sys.argv[2]
    chrom = sys.argv[3]
    if ktype not in conf['ktypes']:
        print('Unknown kernel type %s' % ktype)
        print(conf['ktypes'])
        exit(1)
    if chrom not in conf['datasets']:
        print('Unknown data name %s' % chrom)
        print('Data names in configuration are:')
        print(conf['datasets'])
        exit(1)
    show_performance(conf, ktype, chrom)

