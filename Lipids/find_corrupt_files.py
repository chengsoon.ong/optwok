"""Find corrupt files due to incomplete write"""

import sys
import gzip
import ujson
from optwok.io_pickle import load
import safhs_data as sd

def check_all(conf):
    for ktype in conf['ktypes']:
        res_dir = conf['result_dir']+ktype
        for data_name in conf['datasets']:
            check(sd.perf_name(res_dir, data_name))
            check(sd.export_perf_name(res_dir, 'json', data_name))
            for split in range(conf['num_split']):
                check(sd.kern_mach_name(res_dir, data_name, split))
                check(sd.pred_name(res_dir, data_name, split))
                check(sd.export_pred_name(res_dir, 'json', data_name, split))
                for reg_idx in range(len(conf['all_reg_param'])):
                    check(sd.export_ky_name(res_dir, 'json', data_name, split, reg_idx))


def check(file_name, error_io=True):
    try:
        if file_name.endswith('json.gz'):
            dummy = ujson.load(gzip.open(file_name, 'r'))
        elif file_name.endswith('pkl'):
            dummy = load(file_name, raise_error=True)
    except EOFError:
        print('%s corrupted' % file_name)
    except IOError:
        if error_io:
            print('%s not found' % file_name)
        
                
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    conf = sd.configure_from_file(filename)
    check_all(conf)
