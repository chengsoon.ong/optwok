#!/bin/sh
#python train_lipids.py config_genotype.py
#python predict_lipids.py config_genotype.py
#python export_lipid_results.py config_genotype.py

#python train_lipids.py config_nofamily.py
#python predict_lipids.py config_nofamily.py
#python export_lipid_results.py config_nofamily.py

#python train_lipids.py config_diff.py
#python predict_lipids.py config_diff.py
#python export_lipid_results.py config_diff.py

python train_lipids.py config_lp_geno.py
python predict_lipids.py config_lp_geno.py
python export_lipid_results.py config_lp_geno.py

#python train_lipids.py config_mt_genotype.py
#python predict_lipids.py config_mt_genotype.py
#python export_lipid_results.py config_mt_genotype.py

