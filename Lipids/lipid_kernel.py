"""Compute a lipid-lipid similarity"""

import numpy as np
import safhs_data as sd
from optwok.kernel import LinearKernel, GaussKernel
from optwok.kernel import sqr_dist, normalize_unit_diag
from optwok.io_pickle import save
import matplotlib.pyplot as plt

def compute_kernel(lipids):
    print('%d lipids %d individuals' % lipids.shape)
    klin = LinearKernel()
    klin.compute(lipids.T)
    dist = sqr_dist(lipids.T, lipids.T)
    avg = np.median(dist)
    krbf = GaussKernel(gamma=1./avg)
    krbf.compute(lipids.T)
    return normalize_unit_diag(klin.kmat), krbf.kmat

if __name__ == '__main__':
    data_dir = '/Users/cong/Data/SAFHS/lipids/'
    data = sd.DatasetKernel(data_dir, kernel_file='wholegenome')
    lin, rbf = compute_kernel(data.labels)
    save('%s/output_kernel_linear.pkl' % data_dir, lin)
    save('%s/output_kernel_rbf.pkl' % data_dir, rbf)
    plt.matshow(lin)
    plt.colorbar()
    plt.matshow(rbf)
    plt.colorbar()
    plt.show()
