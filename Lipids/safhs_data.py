"""Container for the San Antonio Family Heart Study data"""

import ujson
from os.path import isfile
import numpy
from numpy import array, loadtxt, ix_
from numpy import mean, std
from numpy.random import permutation
from optwok.kernel import normalize_unit_diag, center
from optwok.ranks import score2rank
import scipy.stats
from quantile_normalise import quantile_normalise

class DatasetKernel(object):
    """A wrapper around data and permutation"""
    def __init__(self, data_dir, prop_train=0.8, kernel_file='kernel', label_file='lipids',
                 kernel_type='linear', verbose=True):
        self.verbose = verbose
        self.data_dir = data_dir
        self.kernel_file = '%s/%s.json' % (data_dir, kernel_file)
        self.label_file = '%s/%s.json' % (data_dir, label_file)
        self.perm_file = '%s/lipids_perm.txt' % data_dir
        self.kernel_type = kernel_type
        self.kernel = self.load_snps()
        self.labels = self.load_lipids()
        self.num_ex = self.kernel.shape[0]
        self.num_class = self.labels.shape[0]
        assert(self.num_ex == self.kernel.shape[0])
        assert(self.num_ex == self.labels.shape[1])
        if self.verbose:
            print('Read %d examples and %d tasks' % (self.num_ex, self.num_class))
        self.num_train = int(prop_train*self.num_ex)
        if not isfile(self.perm_file):
            self.generate_perm()
        self.perms = loadtxt(self.perm_file, dtype=int, delimiter=' ')
        

    def kernel_train(self, perm_idx):
        """Return the training data split for permutation perm_idx"""
        perm = self.perms[perm_idx]
        idx = perm[:self.num_train]
        return self.kernel[ix_(idx,idx)]
        
    def labels_train(self, perm_idx):
        """Return the training labels split for permutation perm_idx"""
        perm = self.perms[perm_idx]
        idx = perm[:self.num_train]
        return self.labels[:,idx]
        
    def kernel_test(self, perm_idx):
        """Return the test data split for permutation perm_idx"""
        perm = self.perms[perm_idx]
        idx_train = perm[:self.num_train]
        idx_test = perm[self.num_train:]
        return self.kernel[ix_(idx_train,idx_test)]
        
    def labels_test(self, perm_idx):
        """Return the test label split for permutation perm_idx"""
        perm = self.perms[perm_idx]
        idx_test = perm[self.num_train:]
        return self.labels[:,idx_test]
        
    def load_lipids(self, normalise='norm'):
        """Load the lipid data"""
        lipids = array(ujson.load(open(self.label_file, 'r')))
        if normalise == 'quant':
            lipids = quantile_normalise(numpy.log(lipids+0.01))

        for idx_lip in range(lipids.shape[0]):
            if normalise == 'rank':
                lipids[idx_lip,:] = (1.-score2rank(lipids[idx_lip,:])) - 0.5
            elif normalise in ['z', 'quant']:
                mu = mean(lipids[idx_lip,:])
                sigma = std(lipids[idx_lip,:])
                assert(sigma > 0.0)
                lipids[idx_lip,:] = (lipids[idx_lip,:]-mu)/sigma 
            elif normalise == 'boxcox':
                res = boxcox_auto(lipids[idx_lip,:])
                mu = mean(res['bc_data'])
                sigma = std(res['bc_data'])
                lipids[idx_lip,:] = (res['bc_data']-mu)/sigma
            elif normalise == 'norm':
                X = 1.-score2rank(lipids[idx_lip,:])
                D = scipy.stats.norm()
                lipids[idx_lip,:] = D.ppf(X)
            else:
                raise NotImplementedError
        return lipids

    def load_snps(self):
        """Load the linear kernel on the SNP data"""
        data = ujson.load(open(self.kernel_file, 'r'))
        if type(data) == type({}):
            base_kernel = array(normalize_unit_diag(center(array(data['K']))))
        elif type(data) == type([]):
            base_kernel = array(normalize_unit_diag(center(array(data))))

        if self.verbose:
            print('%s kernel' % self.kernel_type)
        if self.kernel_type == 'linear':
            kernel = base_kernel
        elif self.kernel_type == 'p2h':
            kernel = self.kernelise(base_kernel, 2, False)
        elif self.kernel_type == 'p2i':
            kernel = self.kernelise(base_kernel, 2, True)
        elif self.kernel_type == 'p3h':
            kernel = self.kernelise(base_kernel, 3, False)
        elif self.kernel_type == 'p3i':
            kernel = self.kernelise(base_kernel, 3, True)
        elif self.kernel_type == 'p4h':
            kernel = self.kernelise(base_kernel, 4, False)
        elif self.kernel_type == 'p4i':
            kernel = self.kernelise(base_kernel, 4, True)
        else:
            print('Unknown kernel')
        return kernel

    def kernelise(self, kernel, degree=4, inhom=True):
        """Return the polynomial kernel"""
        if inhom:
            kernel = kernel+1
        if degree == 2:
            kernel = kernel*kernel
        elif degree == 3:
            kernel = kernel*kernel*kernel
        elif degree == 4:
            kernel = kernel*kernel*kernel*kernel
        else:
            print('Degree higher than expected')
            exit(1)
        return array(normalize_unit_diag(center(kernel)))
    
    def generate_perm(self, num_perm=50):
        """Generate a file with permutations of the index of examples"""
        print('Generate %d permutations of %d examples in %s'
              % (num_perm, self.num_ex, self.perm_file))
        fp = open(self.perm_file, 'w')
        for iperm in range(num_perm):
            cur_mix = permutation(self.num_ex)
            fp.write(' '.join(map(str, cur_mix.tolist())))
            fp.write('\n')
        fp.close()



def okl_name(dataname, split_idx):
    """The identifier of the classifier"""
    return 'lipids_okl_%s_%d' % (dataname, split_idx)

def pred_name(res_dir, dataname, split_idx):
    """The file name of the predictions"""
    return '%s/prediction_%s_%d.pkl' % (res_dir, dataname, split_idx)
        
def perf_name(res_dir, dataname):
    """The file name of the performance results"""
    return  '%s/performance_%s.pkl' % (res_dir, dataname)

def kern_mach_name(res_dir, dataname, split_idx):
    """The file name of the classifier"""
    return '%s/%s.pkl' % (res_dir, okl_name(dataname, split_idx))

def export_ky_name(res_dir, tar_name, dataname, split_idx, reg_idx):
    """The json file name for the label kernel matrix"""
    return '%s/%s/%s_%d.json.gz' % (res_dir, tar_name, okl_name(dataname, split_idx), reg_idx)

def export_pred_name(res_dir, tar_name, dataname, split_idx):
    """The json file anme for predictions"""
    return '%s/%s/prediction_%s_%d.json.gz' % (res_dir, tar_name, dataname, split_idx)

def export_perf_name(res_dir, tar_name, dataname):
    """The json file name for performance"""
    return '%s/%s/performance_%s.json.gz' % (res_dir, tar_name, dataname)







def _is_found(res_file):
    if isfile(res_file):
        print('%s found' % res_file)
        return True
    else:
        return False

def is_trained(data_dir=None, pred_dir=None, split_idx=None, dataname='wholegenome',
               labelname='lipids', all_reg_param=None, ktype=None, lipid_kernel=None):
    res_file = kern_mach_name('%s/%s/' % (pred_dir, ktype), dataname, split_idx)
    return _is_found(res_file)

def is_predicted(data_dir=None, pred_dir=None, split_idx=None, dataname='wholegenome',
                 labelname='lipids', ktype=None):
    res_file = pred_name('%s/%s/' % (pred_dir, ktype), dataname, split_idx)
    return _is_found(res_file)

def is_scored(data_dir=None, pred_dir=None, split_idx=None, dataname='wholegenome', 
              labelname='lipids', ktype=None, num_split=None):
    res_file = perf_name('%s/%s/' % (pred_dir, ktype), dataname)
    return _is_found(res_file)

def is_ex_ky(res_dir=None, chrom=None, split_idx=None, dir_name=None, num_reg_param=0):
    for reg_idx in range(num_reg_param):
        ky_file = export_ky_name(res_dir, dir_name, chrom, split_idx, reg_idx)
        have_file = _is_found(ky_file)
        if not have_file:
            return False
    return True
    
def is_ex_pred(res_dir=None, chrom=None, split_idx=None, dir_name=None):
    json_file = export_pred_name(res_dir, dir_name, chrom, split_idx)
    return _is_found(json_file)

def is_ex_perf(res_dir=None, chrom=None, dir_name=None):
    json_file = export_perf_name(res_dir, dir_name, chrom)
    return _is_found(json_file)




def configure_from_file(filename):
    """Configure the experiment"""
    config = dict()
    execfile(filename, config)
    del config['__builtins__']
    return config

