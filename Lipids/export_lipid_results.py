
import os
import sys
import ujson
import gzip
import time
from pprint import pprint
from optwok.io_pickle import load
from optwok.kernel import normalize_unit_diag
import safhs_data
import expttools

from optwok.expttools import apply_to_combination as atc


def export_Ky(res_dir=None, chrom=None, split_idx=None, dir_name=None, num_reg_param=0):
    """Export the learned lipid-lipid similarity
    for each chromosome, split, and regularization parameter.
    """
    kern_mach = load(safhs_data.kern_mach_name(res_dir, chrom, split_idx))
    if kern_mach is None:
        return
    for reg_idx in range(num_reg_param):
        Ky = normalize_unit_diag(kern_mach.all_Ky[:,:,reg_idx])
        ky_file = safhs_data.export_ky_name(res_dir, dir_name, chrom, split_idx, reg_idx)
        print('Exporting %s' % ky_file)
        ujson.dump(Ky.tolist(), gzip.open(ky_file, 'w'))

def export_pred(res_dir=None, chrom=None, split_idx=None, dir_name=None):
    """Export the prediction for the best regularization parameter.
    for each chromosome and split.
    """
    pred_file = safhs_data.pred_name(res_dir, chrom, split_idx)
    json_file = safhs_data.export_pred_name(res_dir, dir_name, chrom, split_idx)
    preds = load(pred_file)
    if preds is None:
        return
    json_pred = {'Ky_pred': preds['Ky_pred'].tolist(), 'eye_pred': preds['eye_pred'].tolist(),
                 'all_reg_param': preds['all_reg_param'].tolist(),
                 'Ky_reg_idx': preds['Ky_reg_idx'], 'eye_reg_idx': preds['eye_reg_idx']}
    ujson.dump(json_pred, gzip.open(json_file, 'w'))
    
def export_perf(res_dir=None, chrom=None, dir_name=None):
    """Export the performance for each chromosome"""
    perf_file = safhs_data.perf_name(res_dir, chrom)
    all_perf = load(perf_file)
    if all_perf is None:
        return
    json_file = safhs_data.export_perf_name(res_dir, dir_name, chrom)
    print('Exporting %s' % json_file)
    ujson.dump(all_perf.tolist(), gzip.open(json_file, 'w'))

def get_tar_name():
    return time.strftime("%Y%m%d-%H%M")
    
def export_all(conf, result_dir, dir_name):
    """Export the kernel matrix on lipid similarity"""
    expttools.makedir('%s/%s' % (result_dir, dir_name))
    fparam = {'res_dir': result_dir, 'dir_name': dir_name}
    param = {'chrom': conf['datasets']}
    atc(export_perf, param, fixed=fparam, check_result=safhs_data.is_ex_perf, multiproc=True, blocking=True)
    param = {'chrom': conf['datasets'], 'split_idx': range(conf['num_split'])}
    atc(export_pred, param, fixed=fparam, check_result=safhs_data.is_ex_pred, multiproc=True, blocking=True)
    fparam = {'res_dir': result_dir, 'dir_name': dir_name, 'num_reg_param': len(conf['all_reg_param'])}
    atc(export_Ky, param, fixed=fparam, check_result=safhs_data.is_ex_ky, multiproc=True, blocking=True)
    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    conf = safhs_data.configure_from_file(filename)
    print('Running experiments with the following settings')
    pprint(conf)

    for ktype in conf['ktypes']:
        export_all(conf, '%s/%s/' % (conf['result_dir'], ktype), 'json')
    
