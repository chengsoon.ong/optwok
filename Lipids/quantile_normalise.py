"""Implementation of quantile normalisation"""

from numpy import argsort, zeros
from numpy import mean

def quantile_normalise(data, avg=mean):
    """Perform quantile normalisation on the data (assumed to be a 2D array).
    Assume each column is an example, and quantile normalise the rows.

    Replace value with the mean of the values of the same rank.
    """
    sort_idx = argsort(data, axis=1)
    new_data = zeros(data.shape)
    for idx in range(data.shape[1]):
        new_val = avg(data[sort_idx==idx])
        new_data[sort_idx == idx] = new_val
    return new_data
