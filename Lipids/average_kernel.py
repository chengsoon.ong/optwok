"""Compute the average output kernel"""

import numpy as np
import matplotlib.pyplot as plt
import safhs_data as sd
import view_lipid_similarity as vl
from optwok.io_pickle import load, save
from optwok.kernel import normalize_unit_diag

def show_kernel(Ky):
    fig = plt.figure(figsize=(20,12))
    Ky = normalize_unit_diag(Ky)
    leaf_id = vl.show_dendrogram(fig, Ky)
    vl.show_matrix(fig, Ky, leaf_id)
    vl.show_colorbar(fig)

def show_diag(self_sim):
    fig = plt.figure(figsize=(30,5))
    ax = fig.add_subplot(111)
    xval = self_sim.shape[1]
    ax.boxplot(self_sim)
    ax.plot([-1, xval+1], [1,1], 'k:')
    ax.set_xticklabels([])

def collect(dir_name, chrom, min_num_good, min_score):
    reg_idx = 14
    mach = load(sd.kern_mach_name(dir_name, str(chrom), 0))
    Ky = np.zeros(mach.Ky.shape)
    #self_sim = np.zeros((20, mach.Ky.shape[0]))
    num_matrix = 0

    perf = load(sd.perf_name(dir_name, str(chrom)))
    for split_idx in range(20):
        num_good = len(np.flatnonzero(perf[split_idx,:,0] > min_score))
        print num_good
        if num_good > min_num_good:
            print('Chrom %d, split %d' % (chrom, split_idx))
            mach = load(sd.kern_mach_name(dir_name, str(chrom), split_idx))
            Ky += mach.all_Ky[:,:,reg_idx]
            #self_sim[split_idx, :] = np.diag(mach.all_Ky[:,:,reg_idx])
            num_matrix += 1
    Ky /= float(num_matrix)
    return Ky


if __name__ == '__main__':
    for chrom in range(1,23):
        Ky = collect('/Volumes/scratch/4Tim/Prediction/genotype/p2i', chrom, 10, 0.1)
        save('average_lipid_kernel_%d.pkl' % chrom, Ky)

    Ky = load('average_lipid_kernel_1.pkl')
    Ky = np.zeros(Ky.shape)
    for chrom in range(1,23):
        Ky += load('average_lipid_kernel_%d.pkl' % chrom)
    Ky /= 22.0
    save('average_lipid_kernel.pkl', Ky)

