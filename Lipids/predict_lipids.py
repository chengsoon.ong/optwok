"""Learning the kernel on labels.
Use coordinate descent to optimize least squares SVM.

San Antonio Family Heart Study
"""

import sys
from pprint import pprint
from os.path import isfile
from numpy import zeros, logspace, eye
from numpy import mean, std
from numpy.random import permutation
import scipy.stats

from matplotlib.pyplot import matshow, colorbar, show

from optwok.kernel import CustomKernel
from optwok.evaluation import rmse, r2, spearman
from optwok.io_pickle import load, save
from optwok.expttools import apply_to_combination as atc

import expttools
import safhs_data
from safhs_data import pred_name, perf_name, kern_mach_name
from boxcox import boxcox_auto


def predict_with_best_param(kern_mach, test_ex, reg_idx_Ky, reg_idx_eye):
    """Return the predictions for the learned Ky and identity Ky"""
    print('Learned Ky')
    reg = kern_mach.all_reg_param[reg_idx_Ky]
    if kern_mach.all_Ky is not None:
        Ky = kern_mach.all_Ky[:,:,reg_idx_Ky]
    else:
        Ky = kern_mach.Ky
    print('Regularisation paramter = %e' % reg)
    kern_mach.reg_param = reg
    kern_mach.infer_sv(Ky)
    all_preds = kern_mach.predict(test_ex)

    print('Identity Ky')
    reg = kern_mach.all_reg_param[reg_idx_eye]
    print('Regularisation paramter = %e' % reg)
    kern_mach.reg_param = reg
    kern_mach.infer_sv(eye(kern_mach.num_classes))
    all_preds_eye = kern_mach.predict(test_ex)

    return all_preds, all_preds_eye

    
def predict(data_dir=None, pred_dir=None, split_idx=None, dataname='wholegenome', ktype=None,
            labelname='lipids'):
    """Predict for the best value of the regularization parameter"""
    print('Prediction %s split %d' % (dataname, split_idx))
    res_dir = '%s/%s/' % (pred_dir, ktype)
    kern_mach = load(kern_mach_name(res_dir, dataname, split_idx))
    if kern_mach is None:
        return

    data = safhs_data.DatasetKernel(data_dir, kernel_file=dataname, label_file=labelname,
                                    kernel_type=ktype)
    test_ex = data.kernel_test(split_idx)
    test_lab = data.labels_test(split_idx)

    # perf can be r2, spearman, scipy.stats.pearsonr
    reg_idx_Ky, reg_idx_eye = expttools.predict_all(test_ex, test_lab, kern_mach,
                                                    '.', 'temp', perf=r2, reg_index=True)
    Ky_pred, eye_pred = predict_with_best_param(kern_mach, test_ex, reg_idx_Ky, reg_idx_eye)
    pred_file = pred_name(res_dir, dataname, split_idx)
    tosave = {'Ky_pred': Ky_pred, 'eye_pred': eye_pred,
              'all_reg_param': kern_mach.all_reg_param,
              'Ky_reg_idx': reg_idx_Ky, 'eye_reg_idx': reg_idx_eye}
    save(pred_file, tosave)

def compute_perf(data_dir=None, pred_dir=None, num_split=None, dataname='wholegenome',
                 ktype=None, labelname='lipids'):
    """Find the best regularization parameter and compute the performance

    Return the performance of classifier for all splits.
    Dimensions:
    0 - splits
    1 - task
    2 - (Ky, eye)
    """
    res_dir = '%s/%s/' % (pred_dir, ktype)
    data = safhs_data.DatasetKernel(data_dir, kernel_file=dataname, label_file=labelname,
                                    kernel_type=ktype)
    all_perf = zeros((num_split, data.num_class, 2))
    for split_idx in range(num_split):
        print('Compute performance %s split %d' % (dataname, split_idx))
        pred_file = pred_name(res_dir, dataname, split_idx)
        pred = load(pred_file)
        if pred is None:
            all_perf[split_idx,:,:] = 0.
            continue
        data = safhs_data.DatasetKernel(data_dir, kernel_file=dataname, label_file=labelname,
                                        kernel_type=ktype)
        test_lab = data.labels_test(split_idx)
        for task_idx in range(data.num_class):
            all_perf[split_idx, task_idx, 0] = r2(pred['Ky_pred'][task_idx,:], test_lab[task_idx,:])
            all_perf[split_idx, task_idx, 1] = r2(pred['eye_pred'][task_idx,:], test_lab[task_idx,:])
    res_file = perf_name(res_dir, dataname)
    save(res_file, all_perf)
    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    conf = safhs_data.configure_from_file(filename)
    print('Running experiments with the following settings')
    pprint(conf)

    param = {'dataname': conf['datasets'], 'split_idx': range(conf['num_split']), 
             'ktype': conf['ktypes']}
    fparam = {'data_dir': conf['data_dir'], 'pred_dir': conf['result_dir'], 'labelname': conf['labels']}
    atc(predict, param, fixed=fparam, check_result=safhs_data.is_predicted, multiproc=True, blocking=True)

    
    param = {'dataname': conf['datasets'], 'ktype': conf['ktypes']}
    fparam = {'num_split': conf['num_split'], 'data_dir': conf['data_dir'], 'pred_dir': conf['result_dir'],
              'labelname': conf['labels']}
    atc(compute_perf, param, fixed=fparam, check_result=safhs_data.is_scored, multiproc=True, blocking=True)
