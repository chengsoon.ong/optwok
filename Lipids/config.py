
data_dir = '/Users/cong/Data/SAFHS/lipids'
result_dir = '/Volumes/scratch/4Tim/Prediction_old/old/nofamily/'

num_split = 20
#ktypes = ['linear','p2h','p2i','p3h','p3i','p4h','p4i']
ktypes = ['linear']

datasets = ['wholegenome_nofamilymembership']
#for chrom in range(1,23):
#    datasets.append('%d_nofamilymembership' % chrom)
labels = 'lipids'

import numpy
all_reg_param = numpy.logspace(-1,1,15)
del numpy
