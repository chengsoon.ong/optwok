"""Box Cox transform to convert data into a normal distribution"""

from numpy import array, min, std, sum, log, exp
from scipy import optimize

def boxcox_auto(x):
    """
    Automatically determines the lambda needed to perform a boxcox
    transform of the given vector of data points.  Note that it will
    also automatically offset the datapoints so that the minimum value
    is just above 0 to satisfy the criteria for the boxcox transform.
    
    The object returned by this function contains the transformed data
    ('bc_data'), the lambda ('lamb'), and the offset used on the data
    points ('dataOffset').  This object can be fed easily into
    boxcox_inverse() to retrieve the original data values like so:
    
    EXAMPLE:
    >>> bc_results = boxcox_auto(data_vector)
    >>> print bc_results
    {'lamb': array([ 0.313]), 'bc_data': array([ 0.47712018,  1.33916353,  6.66393874, ...,  3.80242394,
    3.79166974,  0.47712018]), 'data_offset': 2.2204460492503131e-16}
    >>> reconstit = boxcox_inverse(**bc_results)
    >>> print numpy.mean((dataVector - reconstit) ** 2)
    5.6965875916e-29
    """
    const_offset = -min(x) + 1e-8
    tempX = x + const_offset
    bclambda = optimize.fmin(boxcox_opt, 0.0, args=(tempX), maxiter=2000, disp=0)
    bclambda = bclambda[0]

    # Generate the transformed data using the optimal lambda.
    return({'bc_data': boxcox(tempX, bclambda), 'lamb': bclambda, 'data_offset': const_offset})

def boxcox(x, lamb):
    """
    boxcox() performs the boxcox transformation upon the data vector 'x',
    using the supplied lambda value 'lamb'.
    Note that this function does not check for minimum value of the data,
    and it will not correct for values being below 0.
 
    The function returns a vector the same size of x containing the
    the transformed values.
    """
    if (lamb != 0.0) :
        return(((x ** lamb) - 1) / lamb)
    else:
        return(log(x))
    
def boxcox_inverse(bc_data, lamb, data_offset = 0.0) :
    """
    Performs the inverse operation of the boxcox transform.
    Note that one can use the output of boxcox_auto() to easily
    run boxcox_inverse:
    
    >>> bc_results = boxcox_auto(data)
    >>> reconstit_data = boxcox_inverse(**bc_results)
    
    Also can be used directly like so:
    >>> trans_data = boxcox(orig_data, lamb_val)
    >>> trans_data = do_processing(trans_data)
    >>> reconstit_data = boxcox_inverse(trans_data, lamb_val)
    """
    if (lamb != 0.0) :
        return((((bc_data * lamb) + 1) ** (1.0/lamb)) - data_offset)
    else :
        return(exp(bc_data) - data_offset)


def boxcox_opt(lamb, *pargs):
    """
    Don't call this function, it is meant to be
    used by boxcox_auto().
    """
    x = array(pargs)
    
    # Transform data using a particular lambda.
    xhat = boxcox(x, lamb[0])
    
    # The algorithm calls for maximizing the LLF; however, since we have
    # only functions that minimize, the LLF is negated so that we can 
    # minimize the function instead of maximixing it to find the optimum lambda.
    return(-(-(len(x)/2.0) * log(std(xhat.T)**2) + (lamb[0] - 1.0)*(sum(log(x)))))


