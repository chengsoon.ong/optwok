"""Some code for running experiments"""

import os
import numpy
from numpy import zeros, argmax, argmin, savetxt
from numpy import vstack, mean, sum
from pylab import semilogx, xlabel, ylabel, legend, loglog
from pylab import figure, title, show, savefig, subplot
import scipy.stats

from optwok.evaluation import accuracy, rmse, r2, spearman
from optwok.evaluation import differential_entropy, relative_entropy
from optwok.io_pickle import save, load

def makedir(dir_name):
    """Create a directory if it does not exist. Not threadsafe!"""
    try: 
        os.makedirs(dir_name)
    except OSError:
        if not os.path.isdir(dir_name):
            raise

def plot_corr(dataname, all_reg_param, all_acc, diag_acc, filename):
    figure()
    subplot(211)
    assert(all_acc.shape == diag_acc.shape)
    if len(all_acc.shape) == 2:
        semilogx(all_reg_param, mean(all_acc, axis=0), 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc.T, 'b+')
        semilogx(all_reg_param, mean(diag_acc, axis=0), 'g-', label='identity')
        semilogx(all_reg_param, diag_acc.T, 'gx')
    else:
        semilogx(all_reg_param, all_acc, 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc, 'b+')
        semilogx(all_reg_param, diag_acc, 'g-', label='identity')
        semilogx(all_reg_param, diag_acc, 'gx')
        
    ylabel('Corr. (higher is better)')
    legend(loc='lower right')
    title(dataname)
    subplot(212)
    max_diag_acc = numpy.max(diag_acc, axis=1)
    diff_acc = zeros(all_acc.shape)
    for ix in range(len(all_reg_param)):
        diff_acc[:,ix] = all_acc[:,ix]-max_diag_acc
    semilogx(all_reg_param, diff_acc.T, 'r+')
    semilogx([all_reg_param[0],all_reg_param[-1]], [0,0], 'k-')
    ylabel('improvement from learning L')
    xlabel('regularization parameter')
    savefig(filename)

def _multi_argbest(all_acc, order):
    """Heuristic rule to deal with equal best"""
    num_param = len(all_acc)
    max_idx_right = num_param - order(all_acc[::-1]) - 1
    max_idx_left = order(all_acc)
    if max_idx_left == 0:
        max_idx = max_idx_right
    elif max_idx_right == num_param:
        max_idx = max_idx_left
    elif max_idx_right >= max_idx_left:
        max_idx = max_idx_right
    else:
        print "Unknown case!"
        print max_idx_left, max_idx_right
    return max_idx

def find_best_idx_avg(labels, all_preds, all_reg_param, perf):
    """Find the maximum correlation"""
    print('Finding best average index using %s' % perf.__name__)
    num_param = len(all_reg_param)
    all_acc = zeros(num_param)
    for ix in range(num_param):
        acc = 0.0
        for task in range(all_preds.shape[0]):
            res = perf(all_preds[task,:,ix], labels[task,:])
            if type(res) == numpy.float64:
                acc += res
            else:
                # pearsonr returns 2 tuple (corr, pval)
                acc += res[0]
        all_acc[ix] = acc/float(all_preds.shape[0])
    max_idx = _multi_argbest(all_acc, argmax)
    print 'Best average correlation (at lambda = %e): %f' % (all_reg_param[max_idx], all_acc[max_idx])
    return (all_acc, max_idx)

def find_best_idx_top(labels, all_preds, all_reg_param, perf):
    """Find the maximum correlation"""
    print('Finding best top index using %s' % perf.__name__)
    num_param = len(all_reg_param)
    all_acc = zeros(num_param)
    for ix in range(num_param):
        acc = -1.0
        for task in range(all_preds.shape[0]):
            res = perf(all_preds[task,:,ix], labels[task,:])
            if type(res) == numpy.float64:
                acc = max(acc,res)
            else:
                # pearsonr returns 2 tuple (corr, pval)
                acc = max(acc,res[0])
        all_acc[ix] = acc
    max_idx = _multi_argbest(all_acc, argmax)
    print 'Best top correlation (at lambda = %e): %f' % (all_reg_param[max_idx], all_acc[max_idx])
    return (all_acc, max_idx)

def predict_all(test_ex, test_lab, kern_mach, outdir, dataname,
                perf=accuracy, reg_index=False):
    """Find the best support vectors and predict for all values
    of the regularization parameter"""
    print 'Results for learned L'
    all_preds = kern_mach.predict_reg_path(test_ex)
    (all_acc, max_idx) = find_best_idx_top(test_lab, all_preds, kern_mach.all_reg_param, perf)
    if reg_index:
        all_acc = max_idx

    print 'Results for identity L'
    diag_preds = kern_mach.predict_reg_path(test_ex, ident_Ky=True)
    #save('%s/%s_preds_diag.pkl' % (outdir, dataname), diag_preds)
    (diag_acc, max_idx) = find_best_idx_top(test_lab, diag_preds, kern_mach.all_reg_param, perf)
    if reg_index:
        diag_acc = max_idx

    return all_acc, diag_acc
