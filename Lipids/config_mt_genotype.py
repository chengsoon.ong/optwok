
data_dir = '/mnt/safhs/Data/lipids/'
result_dir = '/mnt/safhs/Work/mtlin_genotype/'

num_split = 20
ktypes = ['linear','p2h','p2i','p3h','p3i','p4h','p4i']

datasets = ['wholegenome']
for chrom in range(1,23):
    datasets.append('%d' % chrom)
labels = 'lipids'
#lipid_kernel = 'average_lipid_kernel.pkl'
lipid_kernel = 'output_kernel_linear.pkl'
#lipid_kernel = 'output_kernel_rbf.pkl'

import numpy
all_reg_param = numpy.logspace(-1,1,15)
del numpy
