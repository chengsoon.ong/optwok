"""Learning the kernel on labels.
Use coordinate descent to optimize least squares SVM.

San Antonio Family Heart Study
"""

import sys
import os
from os.path import isfile
from pprint import pprint
from optwok.kernel import CustomKernel
from optwok.kernelopt import KernelOptLS, MultiTask
from optwok.io_pickle import load, save
from optwok.expttools import apply_to_combination as atc

import expttools
import safhs_data
from safhs_data import kern_mach_name

def learn_Ky(data_dir=None, pred_dir=None, split_idx=None, all_reg_param=None, 
             dataname='wholegenome', ktype=None, labelname='lipids'):
    """Learn the kernel on labels"""
    print('Training %s split %d' % (dataname, split_idx))
    data = safhs_data.DatasetKernel(data_dir, kernel_file=dataname, 
                                    label_file=labelname, kernel_type=ktype)
    kern_mach = KernelOptLS(reg_param=10.0, kernel=CustomKernel(), verbose=False)
    kern_mach.all_reg_param = all_reg_param
    kern_mach.max_iter = 100
    kern_mach.get_max = False
    kernel = data.kernel_train(split_idx)
    labels = data.labels_train(split_idx)
    kern_mach.Kx.compute(kernel)
    kern_mach.train_reg_path(kernel, labels)

    # The learned kernel
    res_dir = '%s/%s/' % (pred_dir, ktype)
    print('Saving the learned kernel to %s' % res_dir)
    save(kern_mach_name(res_dir, dataname, split_idx), kern_mach)
    return


def main(conf):
    """SAFHS experiment"""
    for ktype in conf['ktypes']:
        expttools.makedir('%s/%s' % (conf['result_dir'], ktype))
    
    param = {'dataname': conf['datasets'], 'split_idx': range(conf['num_split']), 
             'ktype': conf['ktypes']}
    fparam = {'data_dir': conf['data_dir'], 'pred_dir': conf['result_dir'], 
              'labelname': conf['labels'], 'all_reg_param': conf['all_reg_param']}
    atc(learn_Ky, param, fixed=fparam, check_result=safhs_data.is_trained, multiproc=True, blocking=True)

    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    conf = safhs_data.configure_from_file(filename)
    print('Running experiments with the following settings')
    pprint(conf)
    main(conf)
