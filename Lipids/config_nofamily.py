
data_dir = '/mnt/safhs/Data/lipids/'
result_dir = '/mnt/safhs/Work/nofamily/'

num_split = 20
ktypes = ['linear','p2h','p2i','p3h','p3i','p4h','p4i']

datasets = ['wholegenome_nofamilymembership']
for chrom in range(1,23):
    datasets.append('%d_nofamilymembership' % chrom)
labels = 'lipids'

import numpy
all_reg_param = numpy.logspace(-1,1,15)
del numpy
