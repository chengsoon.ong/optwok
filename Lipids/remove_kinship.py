"""Train a predictor on the kinship matrix, and predict lipids from it.
Compute the residual error to use as a new set of labels."""

import sys
import ujson
from numpy import logspace, eye, array
from optwok.evaluation import spearman
from optwok.kernelopt import KernelOptLS
from optwok.kernel import CustomKernel
from optwok.kernel import normalize_unit_diag, center
import safhs_data

def subtract_pred(data_dir, work_dir, remove='familymembership'):
    """Create labels by subtracting the prediction using 'remove',
    from lipids.
    """
    print('Removing %s/%s.json' % (data_dir, remove))
    data = safhs_data.DatasetKernel(data_dir, kernel_file=remove)
    kern_mach = KernelOptLS(reg_param=1.0, kernel=CustomKernel())
    kern_mach.max_iter = 2
    kern_mach.get_max = False
    kern_mach.Kx.compute(data.kernel)
    kern_mach.train(data.kernel, data.labels)
    kern_mach.infer_sv(eye(data.num_class))
    preds = kern_mach.predict(data.kernel)
    perf = 0.0
    for task in range(data.num_class):
        perf += spearman(preds[task,:], data.labels[task,:])
    print('Average performance : %1.5f' % (perf/float(data.num_class)))

    new_label = data.labels - preds
    outfile = '%s/lipids_no%s.json' % (data_dir, remove)
    print('New label file %s' % outfile)
    ujson.dump(new_label.tolist(), open(outfile, 'w'))

def _multiply_kernel(cur_file, res_file, remove):
    """Inner loop of multiply_kernel"""
    print('Multiplying kernel %s' % cur_file)
    data = ujson.load(open(cur_file, 'r'))
    if type(data) == type({}):
        K = array(data['K'])
    elif type(data) == type([]):
        K = array(data)
    K = array(normalize_unit_diag(center(K)))
    family = ujson.load(open('%s/%s.json' % (data_dir, remove)))
    R = array(family)
    kernel = K*R
    ujson.dump(kernel.tolist(), open(res_file, 'w'))
    
def multiply_kernel(data_dir, remove='familymembership'):
    """Create labels by pointwise multiplying the kernel using 'remove'"""
    cur_file = '%s/wholegenome.json' % data_dir
    res_file = '%s/wholegenome_no%s.json' % (data_dir, remove)
    _multiply_kernel(cur_file, res_file, remove)
    for chrom in range(1,23):
        cur_file = '%s/%d.json' % (data_dir, chrom)
        res_file = '%s/%d_no%s.json' % (data_dir, chrom, remove)
        _multiply_kernel(cur_file, res_file, remove)
        
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python %s data_dir result_dir' % sys.argv[0])
        exit(1)
    # data_dir = '/Users/cong/Data/SAFHS/kernels/'
    # res_dir = '/Users/cong/temp/work_LSSVM/large_inhom/'
    data_dir = sys.argv[1]
    res_dir = sys.argv[2]
    subtract_pred(data_dir, res_dir)
    multiply_kernel(data_dir)
    
