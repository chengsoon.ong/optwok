"""Visualise the kernel matrix"""

import sys
from numpy import ix_
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from optwok.kernel import normalize_unit_diag, kernel2distance
from optwok.io_pickle import load


def show_dendrogram(fig, Ky):
    Dy = kernel2distance(Ky)
    [axd_x, axd_y, axd_w, axd_h] = [0.05, 0.05, 0.2, 0.9]
    axd = fig.add_axes([axd_x, axd_y, axd_w, axd_h], frame_on=True)
    clusters = linkage(Dy, 'complete')
    tree = dendrogram(clusters, color_threshold=2.0, orientation='right')
    #clus_idx = fcluster(clusters, 0.7*max(clusters[:,2]), 'distance')
    xlabels = axd.get_xticklabels()
    for label in xlabels: 
        label.set_rotation(30) 
    axd.set_xlabel('Distance')
    return tree['leaves']

def show_matrix(fig, Ky, leaf_id):
    [axh_x, axh_y, axh_w, axh_h] = [0.15, 0.05, 0.9, 0.9]
    axh = fig.add_axes([axh_x, axh_y, axh_w, axh_h])
    Ky = Ky[ix_(leaf_id, leaf_id)]
    axh.matshow(Ky, origin='lower', cmap=plt.cm.RdBu_r)
    axh.set_xticks([])
    axh.set_yticks([])

def show_colorbar(fig):
    [axc_x, axc_y, axc_w, axc_h] = [0.9, 0.05, 0.05, 0.9]
    axc = fig.add_axes([axc_x, axc_y, axc_w, axc_h], frame_on=False)
    cb = mpl.colorbar.ColorbarBase(axc, cmap=plt.cm.RdBu_r, orientation='vertical')
    axc.set_title('colorkey')


def show_kernel(okl_file, reg_idx):
    kern_mach = load(okl_file)
    if kern_mach is None:
        return
    print('Regularisation paramter = %e' % kern_mach.all_reg_param[reg_idx])
    Ky = normalize_unit_diag(kern_mach.all_Ky[:,:,reg_idx])
    fig = plt.figure(figsize=(10,6))
    leaf_id = show_dendrogram(fig, Ky)
    show_matrix(fig, Ky, leaf_id)
    show_colorbar(fig)
    plt.show()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python %s okl_file reg_idx' % sys.argv[0])
        exit(1)
    okl_file = sys.argv[1]
    reg_idx = int(sys.argv[2])
    show_kernel(okl_file, reg_idx)

