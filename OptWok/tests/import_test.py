"""Checking whether all the external libraries are installed"""

try:
    import cvxopt
    print cvxopt.__file__
except ImportError:
    print 'cvxopt not found'

try:
    import pythongrid
    print pythongrid.__file__
except ImportError:
    print 'pythongrid not found'

try:
    import cython
    print cython.__file__
except ImportError:
    print 'cython not found'

try:
    import slycot
    print slycot.__file__
except ImportError:
    print 'slycot not found'

