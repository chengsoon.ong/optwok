# cython: profile=True

"""Test the kernel module, including speedups due to cython"""
import pstats, cProfile
from numpy import sum
from numpy.random import rand, randint, seed
from optwok.testtools import list2matrix



def test_kernel(kfunc, X):
    print '=== Test Kernel ==='
    kg = kfunc()
    print kg
    kg.compute(X)
    print kg.kmat
    for ix in range(X.shape[1]):
        for iy in range(X.shape[1]):
            print '(%d,%d) %f' % (ix, iy, kg[ix,iy])
            
def test_joint_kernel(kfunc, X):
    print '=== Test Joint Kernel ==='
    joint = JointKernel(kfunc(), num_classes=2)
    print joint
    print joint.Kx
    joint.compute(X)
    print joint.kmat
    for xi in range(X.shape[1]):
        for yi in range(2):
            for xj in range(X.shape[1]):
                for yj in range(2):
                    print 'Kx(%d,%d),Ky(%d,%d): %f' % (xi,xj,yi,yj, joint[xi,xj,yi,yj])
    
def test_prediction(kfunc, Xtrain, Xtest):
    print '=== Test prediction ==='
    kg = kfunc()
    print kg
    kg.compute(Xtrain)
    print kg.compute_test(Xtest)
    joint = JointKernel(kfunc(), num_classes=2)
    joint.compute(Xtrain)
    print joint.compute_test(Xtest)

def demo_context_kernel(kfunc, X, Z):
    print '=== Test Context Kernel ==='
    from optwok.testtools import list2matrix
    ck = ContextKernel(kfunc(), list2matrix([[0],[1,2]], noise=0.2))
    print ck
    print ck.Kx
    ck.compute(X,Z)
    print ck.kmat

def _test_linear_kernel(kernel):
    X1 = rand(45,5000)
    X2 = rand(45,12000)
    kernel.compute(X1)
    kernel.kmat
    kernel.compute_test(X1)
    kernel.compute_test(X2)

def test_linear_kernel():
    from optwok.kernelbase import LinearKernel
    kernel = LinearKernel()
    _test_linear_kernel(kernel)
        
def test_linear_kernel_pp():
    from optwok.kernel_pp import LinearKernel
    kernel = LinearKernel()
    _test_linear_kernel(kernel)

def profile_linear_kernel():
    cProfile.runctx('test_linear_kernel_pp()', globals(), locals(), 'kernel_pp.prof')
    cProfile.runctx('test_linear_kernel()', globals(), locals(), 'kernelbase.prof')

    s = pstats.Stats('kernel_pp.prof')
    s.strip_dirs().sort_stats('cumulative').print_stats(10)

    f = pstats.Stats('kernelbase.prof')
    f.strip_dirs().sort_stats('cumulative').print_stats(10)

def _test_context_kernel(kernel):
    num_feat = 45
    num1 = 1000
    num2 = 2000
    seed(0)
    X1 = rand(num_feat, num1)
    c1 = randint(3, size=num1)
    X2 = rand(num_feat, num2)
    c2 = randint(3, size=num2)
    kernel.compute(X1, c1)
    kernel.kmat
    kernel.compute_test(X1, c1)
    kernel.compute_test(X2, c2)

def test_context_kernel():
    from optwok.kernelbase import LinearKernel, ContextKernel
    ck = ContextKernel(LinearKernel(), list2matrix([[0],[1,2]], noise=0.1))
    _test_context_kernel(ck)
        
def test_context_kernel_pp():
    from optwok.kernel_pp import LinearKernel, ContextKernel
    ck = ContextKernel(LinearKernel(), list2matrix([[0],[1,2]], noise=0.1))
    _test_context_kernel(ck)

def profile_context_kernel():
    cProfile.runctx('test_context_kernel_pp()', globals(), locals(), 'kernel_pp.prof')
    cProfile.runctx('test_context_kernel()', globals(), locals(), 'kernelbase.prof')

    s = pstats.Stats('kernel_pp.prof')
    s.strip_dirs().sort_stats('cumulative').print_stats(10)

    f = pstats.Stats('kernelbase.prof')
    f.strip_dirs().sort_stats('cumulative').print_stats(10)

def test_all():
    """Execute all the functions"""
    X = rand(2,3)
    print X
    demo_context_kernel(LinearKernel, X, [0,1,2])
    test_kernel(LinearKernel, X)
    test_kernel(GaussKernel, X)
    test_joint_kernel(LinearKernel, X)
    test_joint_kernel(GaussKernel, X)
    Xtest = rand(2,1)
    test_prediction(LinearKernel, X, Xtest)
    test_prediction(GaussKernel, X, Xtest)

if __name__ == '__main__':
    from optwok.kernel_pp import LinearKernel, ContextKernel
    kernel = ContextKernel(LinearKernel(), list2matrix([[0],[1,2]], noise=0.9))

    num_feat = 45
    num1 = 1000
    num2 = 2000
    seed(0)
    X1 = rand(num_feat, num1)
    c1 = randint(2, size=num1)
    X2 = rand(num_feat, num2)
    c2 = randint(3, size=num2)
    kernel.compute(X1, c1)

    print sum(abs(kernel.kmat - kernel.kmat_forloop))
    print sum(abs(kernel.compute_test(X2,c2) - kernel.compute_test_forloop(X2,c2)))

    cProfile.runctx('kernel.kmat;kernel.compute_test(X2,c2)',
                    globals(), locals(), 'kmat.prof')
    cProfile.runctx('kernel.kmat_forloop;kernel.compute_test_forloop(X2,c2)',
                    globals(), locals(), 'kmat_forloop.prof')

    s = pstats.Stats('kmat.prof')
    s.strip_dirs().sort_stats('cumulative').print_stats(10)

    f = pstats.Stats('kmat_forloop.prof')
    f.strip_dirs().sort_stats('cumulative').print_stats(10)

