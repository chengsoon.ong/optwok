import unittest
from examples.classifier_demo import example_toy, example_cloud_2d

class Demo(unittest.TestCase):
    """Run the demo for classifier"""
    def test_demo(self):
        example_toy()

if __name__ == '__main__':
    unittest.main()

