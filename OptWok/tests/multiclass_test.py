import unittest
from examples.multiclass_demo import demo_identity_Ky, demo_merged_classes, demo_split_classes

class Demo(unittest.TestCase):
    """Run the demo for multiclass SVM"""
    def test_identity(self):
        demo_identity_Ky()

    def test_merged(self):
        demo_merged_classes()

    def test_split(self):
        demo_split_classes()

if __name__ == '__main__':
    unittest.main()
