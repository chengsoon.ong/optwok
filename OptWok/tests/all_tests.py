"""
Run all tests
"""
import glob
import unittest

def create_test_suite():
    test_file_strings = glob.glob('optwok/test/*_test.py')
    module_strings = ['optwok.test.'+str[12:len(str)-3] for str in test_file_strings]
    suites = [unittest.defaultTestLoader.loadTestsFromName(name) for name in module_strings]
    testSuite = unittest.TestSuite(suites)
    return testSuite
