"""
Use unittest to run all tests in optwok/test
"""
import unittest
import optwok.test.all_tests
testSuite = optwok.test.all_tests.create_test_suite()
text_runner = unittest.TextTestRunner().run(testSuite)
