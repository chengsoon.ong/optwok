#!/usr/bin/env python

import sys
import numpy
from numpy import array, vstack
from optwok.hmm import HMMMulti


### Weather Model ###
weather = {
    # Hidden variables
    'hidden_symbols' : ('sunny','rainy','foggy'),

    # Observed variables
    'observed_symbols' : ('no','yes'),

    # prior probability on weather states
    # P(sunny) = 0.5  P(rainy) = 0.25  P(foggy) = 0.25
    'prob_hidden' : [0.5, 0.25, 0.25],

    # transition probabilities
    #                    tomorrrow
    #    today     sunny  rainy  foggy
    #    sunny      0.8    0.05   0.15
    #    rainy      0.2    0.6    0.2 
    #    foggy      0.2    0.3    0.5
    'prob_transition' : [ [0.8, 0.05, 0.15], [0.2, 0.6, 0.2], [0.2, 0.3, 0.5] ],

    # conditional probabilities of evidence (observations) given weather
    #                          sunny  rainy  foggy 
    # P(umbrella=no|weather)    0.9    0.2    0.7
    # P(umbrella=yes|weather)   0.1    0.8    0.3
    'prob_emission' : [ [0.9, 0.2, 0.7], [0.1, 0.8, 0.3] ],
    }




def load_data(filename):
    """Read the dataset e.g. weather-test1-1000.txt"""
    input = open(filename, 'r')
    input.readline()
    data = []
    for i in input.readlines():
        x = i.split()
        y = x[0].split(",")
        data.append(y)
    return data

def accuracy(a,b):
    """Sum of the correct predictions along the sequence"""
    total = float(max(len(a),len(b)))
    c = 0
    for i in range(min(len(a),len(b))):
        if a[i] == b[i]:
            c = c + 1          
    return c/total

def test(model, data):
    print('Testing model on data')
    observations = {'umbrella': []}
    hiddens = []
    for h,o in data:
        observations['umbrella'].append(o)
        hiddens.append(h)
    n_obs_short = 10
    obs_short = {'umbrella': observations['umbrella'][0:n_obs_short]}
    hiddens_short = hiddens[0:n_obs_short]

    # Set up HMM
    hmm = HMMMulti(model['hidden_symbols'], {'umbrella': model['observed_symbols']})
    hmm.prior = array(model['prob_hidden'])
    hmm.A = array(model['prob_transition'])
    hmm.B['umbrella'] = array(model['prob_emission']).T
    print(hmm)
    hmm.check_parameters()
    hmm.set_observed(obs_short)
    
    # Compute the distribution of current hidden variable
    for t in range(5,8):
        print('Marginal distribution of hidden variable at time=%d' % t)
        dist = hmm.posterior(t)
        print(dist)

    # Compute the probability of observing the current sequence
    print('Log likelihood of observing:')
    print(obs_short)
    prob = hmm.log_likelihood()
    print(prob)

    # Estimate the maximum marginal probabilities of the hidden variables
    print('The marginally most probable hidden states, true state (bottom)')
    marginal = hmm.max_marginal()
    print(vstack([marginal, array(hiddens_short)]))

    # Compute the most likely sequence of states
    #result_viterbi_full = hmm.max_aposteriori()
    #print(accuracy(hiddens, result_viterbi_full))



def test_interface():
    print('Check the multi output interface')
    observed = {'cage':(4,5,6),'his':('low','high')}
    hmm = HMMMulti(('normal','TSR'), observed)
    hmm.prior = array([0.7, 0.3])
    hmm.A = array([[0.7,0.3], [0.1,0.9]])
    hmm.B = {'cage': array([[0.9, 0.05, 0.05], [0.05, 0.25, 0.7]]),
             'his': array([[0.8,0.2], [0.1,0.9]]),
             }
    print(hmm)
    print('Slice of output probabilities for %s state %d' % ('cage', 5))
    print(hmm.B['cage'][:,hmm.observed2index['cage'][5]])

    observations = {'cage':array([6,5,4,5]),
                    'his':array(['high','high','low','high'])}
    hmm.check_parameters()
    hmm.set_observed(observations)
    print('Num variables=%d' % hmm.num_variables)
    print('%d hidden states, observed states' % hmm.num_hidden_states)
    print(hmm.num_observed_states)
    print('Log likelihood')
    print(hmm.log_likelihood())
    print('Marginals:')
    for t in range(hmm.num_variables):
        print('Time=%d,' % t)
        print(hmm.posterior(t))
        print('Total probability mass = %f' % numpy.sum(hmm.posterior(t)))
    print('Maximum marginals')
    print(hmm.max_marginal())
    print(hmm.scale)
    print(hmm.alpha)
    print(hmm.beta)
    print('\n\n')


if __name__ == '__main__':
    test_interface()
    
    if len(sys.argv) != 2:
        print('Usage: python %s data.txt')
        exit(1)
    file_name = sys.argv[1]
    data = load_data(file_name)
    test(weather, data)

