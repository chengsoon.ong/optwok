from numpy import ones, array
from numpy.linalg import norm
from optwok.perceptron import Perceptron
from optwok.sparse_perceptron import SparsePerceptron
from optwok.simdata import create_data, linear_separation
from optwok.evaluation import accuracy
from optwok.io_pickle import load, save
from pylab import figure, hist, bar, xlabel, ylabel
from pylab import legend, plot, show, subplot, title


def plot_results(true_w, perc, expt_title):
    num_feat = len(true_w)
    d = perc.w.shape[0]
    threshold = 1.0/d
    figure()
    subplot(311)
    long_true_w = 1e-4*ones(d)
    long_true_w[:num_feat] = true_w.flatten()
    bar(range(d), long_true_w)
    title(expt_title)
    subplot(312)
    bar(range(d), perc.q, hold=True)
    #(counts, bins, patches) = hist(perc.col_idx_all, d, hold=True)
    plot([0, d], [threshold, threshold],'r-')
    title('Average dual sampling distribution')
    subplot(313)
    bar(range(d), perc.w)
    title('Learned weights')

def plot_objective(objective, num_steps, keys):
    """Plot the progress of training"""
    figure()
    for key in keys:
        plot(range(999, num_steps, 1000), objective[key], hold=True)
    legend(keys)
    xlabel('iterations')
    ylabel('objective value')


def init_perceptron(is_l1):
    if is_l1:
        perc = SparsePerceptron()
    else:
        perc = Perceptron()
    return perc

def test_gaussians(is_l1=True):
    """Two classes, each a Gaussian"""
    (train_ex, train_labels, test_ex, test_labels) = create_data(num_train=2000,
                                                                 num_test=10000, num_feat=50,
                                                                 num_noise_feat = 50,
                                                                 width = 8.0)
    perc = init_perceptron(is_l1)
    perc.train(train_ex, train_labels)
    print('Train time:\t\t\t %5.1f' % perc.train_time)
    preds = perc.predict(test_ex)
    acc = accuracy(preds, test_labels)
    print('Accuracy:\t\t\t %1.2f' % acc)

def _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title):
    print('Train time:\t\t\t %5.1f' % perc.train_time)
    param, w, b = metadata
    plot_results(w, perc, plot_title)
    preds = perc.predict(test_ex)
    acc = accuracy(preds, test_labels)
    print('Accuracy:\t\t\t %1.2f' % acc)

    return perc.cost, perc.stoc_cost

def test_exp(num_feat=10,num_noise=90,margin = 0.1,num_train=1000):
    """Debug the sparse perceptron"""
    metadata, data, label = linear_separation(min(10*num_train,10000), num_feat, num_noise, margin, homogeneous=True, in_simplex=True)
    for ix in range(data.shape[1]):
        data[:,ix] /= norm(data[:,ix])
    train_ex = data[:,:num_train]
    train_labels = label[:num_train]
    test_ex = data[:,num_train:]
    test_labels = label[num_train:]

    perc_margin = 0.5
    perc = SparsePerceptron()
    perc.train_exp(train_ex, train_labels, perc_margin)
    costs = _test_linsep_results(perc, metadata, test_ex, test_labels, 'Exponentiated update')
    figure()
    plot(range(999, perc.num_steps, 1000), costs[0], hold=True)
    plot(range(999, perc.num_steps, 1000), costs[1])
    show()

def test_linsep(is_l1=False):
    """Two classes in box, separated by margin"""
    num_feat = 10
    num_noise = 90
    margin = 0.1
    num_train = 1000
    metadata, data, label = linear_separation(min(10*num_train,10000), num_feat, num_noise, margin, homogeneous=True, in_simplex=is_l1)
    for ix in range(data.shape[1]):
        data[:,ix] /= norm(data[:,ix])
    train_ex = data[:,:num_train]
    train_labels = label[:num_train]
    test_ex = data[:,num_train:]
    test_labels = label[num_train:]

    objective = {}
    perc_margin = 1.0

    plot_title = 'Clarkson et. al.'
    print('\n=== %s ===' % plot_title)
    perc = init_perceptron(is_l1=False)
    perc.train_orig(train_ex, train_labels, perc_margin)
    costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
    (objective['orig'], objective['orig stoc']) = costs

    plot_title = 'New algorithm'
    print('\n=== %s ===' % plot_title)
    perc = init_perceptron(is_l1)
    perc.train(train_ex, train_labels, perc_margin)
    costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
    (objective['new'], objective['new stoc']) = costs

    plot_title = 'Assume knowledge of true features'
    print('\n=== %s ===' % plot_title)
    perc = init_perceptron(is_l1=False)
    perc.train_cheat(train_ex, train_labels, perc_margin)
    costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
    (objective['prior'], objective['prior stoc']) = costs

    if is_l1:
        plot_title = 'Full exponential update'
        print('\n=== %s ===' % plot_title)
        perc = init_perceptron(is_l1)
        perc.train_exp(train_ex, train_labels, perc_margin)
        costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
        (objective['exp'], objective['exp stoc']) = costs

        plot_title = 'Multiplicative update'
        print('\n=== %s ===' % plot_title)
        perc = init_perceptron(is_l1)
        perc.train_mult(train_ex, train_labels, perc_margin)
        costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
        (objective['mult'], objective['mult stoc']) = costs
    else:
        plot_title = 'High memory'
        print('\n=== %s ===' % plot_title)
        perc = init_perceptron(is_l1)
        perc.train_himem(train_ex, train_labels, perc_margin)
        costs = _test_linsep_results(perc, metadata, test_ex, test_labels, plot_title)
        (objective['himem'], objective['himem stoc']) = costs

        
        
    if is_l1:
        plot_objective(objective, perc.num_steps,
                       ['new', 'prior', 'orig', 'mult', 'exp'])
        plot_objective(objective, perc.num_steps,
                       ['new stoc', 'prior stoc', 'orig stoc', 'mult stoc', 'exp stoc'])
    else:
        plot_objective(objective, perc.num_steps,
                       ['new', 'prior', 'orig', 'himem'])
        plot_objective(objective, perc.num_steps,
                       ['new stoc', 'prior stoc', 'orig stoc', 'himem stoc'])
    show()

def test_linsep_devel():
    """Some experimental developments"""

    print('\n=== New algorithm k samples ===')
    perc.train_k(train_ex, train_labels, perc_margin)
    print('Train time:\t\t\t %5.1f' % perc.train_time)
    param, w, b = metadata
    plot_results(w, perc)
    preds = perc.predict(test_ex)
    acc = accuracy(preds, test_labels)
    print('Accuracy:\t\t\t %1.2f' % acc)
    objective['new k'] = perc.cost
    objective['new k stoc'] = perc.stoc_cost

    

def linsep_expt(num_feat, num_noise, margin, num_train, num_test,
                work_dir, num_expt=10, is_l1=True):
    """Check the feature selection properties of the dual variable"""
    for expt_id in range(num_expt):
        print('Experiment %d of %d' % (expt_id, num_expt-1))
        metadata, data, label = linear_separation(num_train + num_test,
                                                  num_feat, num_noise, margin, homogeneous=True)
        train_ex = data[:,:num_train]
        train_labels = label[:num_train]
        test_ex = data[:,num_train:]
        test_labels = label[num_train:]
        perc = init_perceptron(is_l1)
        perc.train(train_ex, train_labels, margin)
        savefile = '%s/perceptron_feat=%d_noise=%d_train=%d_rep=%02d.pkl' %\
                   (work_dir, num_feat, num_noise, num_train, expt_id)
        save(savefile, (perc, train_ex, train_labels, test_ex, test_labels))
    
def linsep_collect(num_feat, num_noise, num_train, work_dir, num_expt=10):
    """Collect the results"""
    for expt_id in range(num_expt):
        savefile = '%s/perceptron_feat=%d_noise=%d_train=%d_rep=%02d.pkl' %\
                   (work_dir, num_feat, num_noise, num_train, expt_id)
        blob = load(savefile)
        perc = blob[0]
        d = perc.w.shape[0]
        threshold = 1.0/d
        figure()
        bar(range(d), perc.q, hold=True)
        plot([0, d], [threshold, threshold],'r-')
        test_ex = blob[3]
        test_labels = blob[4]
        preds = perc.predict(test_ex)
        acc = accuracy(preds, test_labels)
        print('Accuracy:\t\t\t %1.2f' % acc)
        print('False negative features:\t %d' % sum(perc.q[:num_feat] < threshold))
        print('False positive features:\t %d' % sum(perc.q[num_feat:] > threshold))
    show()

def repeat_expt():
    work_dir = '/local/cong/work_perceptron'
    num_feat = 50
    num_noise = 50
    margin = 0.2
    num_expt = 5
    num_train = 1000
    num_test = 10000

    linsep_expt(num_feat, num_noise, margin, num_train, num_test, work_dir, num_expt)
    linsep_collect(num_feat, num_noise, num_train, work_dir, num_expt)

if __name__ == '__main__':
    #test_gaussians(is_l1=True)
    test_linsep(is_l1=True)
    #repeat_expt()

