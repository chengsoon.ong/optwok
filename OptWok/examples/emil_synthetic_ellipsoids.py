'''
Created on 05.09.2012

@author: kgabriel
'''
from __future__ import division

from scipy import random
from numpy import diag, ones, dot, pi, cos, sin, array
from numpy.random import rand


def random_ellipsoids(num_pos, num_neg, n, var_pos=1, var_neg=1, mean_radius=2, axisp=False):
    ''' generate num_pos positive and num_neg negative random 
         ellipsoids of dimensionality n '''
    
    offset_p = random.randn(n) + 1
    offset_n = random.randn(n) - 1
    
    qp = random.randn(num_pos, n) * var_pos + offset_p
    qn = random.randn(num_neg, n) * var_neg + offset_n
    
    S = []
    for _ in range(num_pos + num_neg):
        #print 'next random covariance...'
        if not axisp:
            S.append(random_covariance_matrix(n, mean_radius))
        else:
            S.append(diag(rand(n))*mean_radius)
    
    return array(list(qp) + list(qn)), array(S), array(list(ones(num_pos))+ list(-1*ones(num_neg)))
    
    

def random_covariance_matrix(n, scale=1):
    ''' generate random covariance matrix of dimension n '''

#    Use Householder reflections to build orthonormal matrix and then C = QSQ'
#    I = eye(n)
#    Q = I
#    K = 2
#    for i in range(K*n*n):
#        v = random.rand(n)
#        v = v / linalg.norm(v)
#        P = I - outer(v,v)
#        Q = dot(P, Q)
#    
#    S = diag(random.rand(n))
#    
#    C = dot(Q,dot(S, Q.transpose()))
    
#   Use C=Q'Q
    Q = random.rand(n,n)
    C = dot(Q, Q.transpose())
    if n==2:
        if rand(1) > 0.5:
            theta = 0
        else:
            theta = pi/2
        C = dot(dot(rotate(theta).T,C),rotate(theta)) 
        
    return scale * C

def rotate(theta):
    return array([[cos(theta), -sin(theta)], [sin(theta), cos(theta)]])
