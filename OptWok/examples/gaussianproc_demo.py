"""
Some code for testing Gaussian process regression, also as a demo
"""

import os
import matplotlib
#matplotlib.use('PDF')
#from matplotlib.pyplot import savefig
from matplotlib.pyplot import plot, fill, show, axis, figure, gca
from numpy import concatenate, arange, ones, sqrt
from numpy import flatnonzero

from optwok.testtools import pair2vec, flatten_list, list2matrix
from optwok.kernel import GaussKernel
from optwok.io_pickle import load

from optwok.gaussian_proc import GaussianProcess, GaussianProcessScalar

def _plot_1D_gp(data_ex, data_lab, test_range, preds, pred_var, colour):
    plot(test_range.flatten(), preds, colour, hold=True)
    sd = 2.0*sqrt(pred_var)
    var_x = concatenate((test_range.flatten(), test_range.flatten()[::-1]))
    var_y = concatenate((preds+sd, (preds-sd)[::-1]))
    fill(var_x, var_y, edgecolor='w', facecolor=colour, alpha=0.3)
    plot(data_ex, data_lab, colour+'o')


def plot_prediction(X, Z, Y, gp):
    """Plot the data points, the predictions and the predictive variance"""
    colour_list = ['b','g','r','c','m','k']
    min_val = min(X)
    max_val = max(X)
    test_range = arange(min_val-1, max_val+1, 0.1)
    num_test = len(test_range)
    test_range.shape = (1,num_test)
    num_context = int(max(Z))+1
    
    for cur_cont in range(num_context):
        (preds, pred_var) = gp.predict((test_range, cur_cont*ones(num_test)))
        data_ex = X[flatnonzero(Z==cur_cont)]
        data_lab = Y[flatnonzero(Z==cur_cont)]
        _plot_1D_gp(data_ex, data_lab, test_range, preds, pred_var, colour_list[cur_cont])
    ax = gca()
    matplotlib.rcParams.update({'font.size': 18})
    ax.set_xlabel('experimental conditions')
    ax.set_ylabel('value of outcomes')
    #ax.set_xlim([0.2,5.1])
    #ax.set_ylim([-2.5,1])
    #ax.text(0.8,0.5, 'exploit')
    #ax.text(3.3,0.5, 'explore')
    #savefig('figure_gp.pdf')
    

def generate_toy_data(num_ex=10, domain=5, noise_level=0.1, mix_list=[[0,1],[2]]):
    """Generate a multi-output Gaussian process dataset with dependencies between outputs."""
    from optwok.simdata import gp_gen_pair, save_gp_context_data
    num_class = len(flatten_list(mix_list))
    dataname = 'gaussproc_%02d_%d_%1.1f' % (num_class, num_ex, noise_level)
    print('Generating %s' % dataname)
    (X,Z,Y) = gp_gen_pair(num_ex, 1, domain, noise_level, mix_list)
    save_gp_context_data(X, Z, Y, dataname, data_dir='tests')

def toy_data_gp(num_ex=10, noise_level=0.1, mix_list=[[0,1],[2]]):
    """Estimate and plot the 1D Gaussian process"""
    num_class = len(flatten_list(mix_list))
    data = load('tests/gaussproc_%02d_%d_%1.1f.pkl' % (num_class, num_ex, noise_level))
    #Ky = list2matrix(mix_list, noise=0.1) # so that can observe difference bewteen 0 and 1
    Ky = list2matrix(mix_list)
    print('Estimating mean and variance of Gaussian Process')
    gp = GaussianProcess(GaussKernel, Ky, reg_param=0.1)
    gp.train(data.examples, data.context, data.labels)
    plot_prediction(data.examples.flatten(), data.context, data.labels, gp)

def toy_data_gp_scalar(num_ex=10, num_class=3, noise_level=0.1):
    """Estimate and plot the 1D scalar output GP"""
    colour_list = ['b','g','r','c','m','k']

    test_ex = arange(-1, 6, 0.1)
    num_test = len(test_ex)
    test_ex.shape = (1,num_test)
    data = load('tests/gaussproc_%02d_%d_%1.1f.pkl' % (num_class, num_ex, noise_level))
    for context in range(num_class):
        gp = GaussianProcessScalar(GaussKernel, reg_param=0.1)
        idx = flatnonzero(data.context == context)
        gp.train(data.examples[:,idx], data.labels[idx])
        (preds, pred_var) = gp.predict(test_ex)
        _plot_1D_gp(data.examples[:,idx].flatten(), data.labels[idx],
                    test_ex, preds, pred_var, colour_list[context])

if __name__ == '__main__':
    num_ex = 20
    #generate_toy_data(num_ex, mix_list=[[0,1],[2],[3,4,5]])
    toy_data_gp(num_ex, mix_list=[[0,1],[2],[3,4,5]])
    print('mix_list=[[0,1],[2],[3,4,5]]')
    figure()
    toy_data_gp(num_ex, mix_list=[[0,1,2,3,4,5]])
    print('mix_list=[[0,1,2,3,4,5]]')
    figure()
    toy_data_gp(num_ex, mix_list=[[0],[1],[2],[3],[4],[5]])
    print('mix_list=[[0],[1],[2],[3],[4],[5]]')

    figure()
    toy_data_gp_scalar(num_ex, 6)
    print('scalar GP')
    show()
