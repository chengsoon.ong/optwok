"""Example to demonstrate the difference between a standard
dual SVM and a SVM+ (learning with teacher)
"""
from optwok.evaluation import accuracy
from optwok.simdata import teacher_data
from optwok.kernel import GaussKernel
from optwok.teacher import SVMplus
from optwok.classifier import SVMDual

if __name__ == '__main__':
    data = teacher_data(num_train=100, num_test=1000,
                        num_feat=10, num_feat_teach=50, width=4.)
    (train_ex, teach_ex, train_lab, test_ex, test_lab) = data

    print('Standard SVM')
    kdict = {'name':'gauss', 'param': 1.0}
    svm = SVMDual(1.0, kdict)
    svm.train(train_ex, train_lab)
    svmout = svm.predict(train_ex)
    acc = accuracy(svmout, train_lab)
    print('Train acc:\t\t\t %1.2f' % acc)
    svmout = svm.predict(test_ex)
    acc = accuracy(svmout, test_lab)
    print('Accuracy:\t\t\t %1.2f' % acc)
    
    print('Learning with teacher')
    kdict = [{'name':'gauss', 'param': 1.0},
             {'name':'gauss', 'param': 1.0}]
    svmp = SVMplus([1.0, 10.], kdict)
    svmp.train(train_ex, teach_ex, train_lab)
    svmout = svmp.predict(train_ex)
    acc = accuracy(svmout, train_lab)
    print('Train acc:\t\t\t %1.2f' % acc)
    svmout = svmp.predict(test_ex)
    acc = accuracy(svmout, test_lab)
    print('Accuracy:\t\t\t %1.2f' % acc)
    
