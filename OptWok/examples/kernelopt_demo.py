"""Demo for learning the output kernel"""
from numpy import vstack, dot
from matplotlib.pyplot import matshow, gray, colorbar, savefig, show, title
from optwok.kernelopt import KernelOptLS, KernelOptLSGP, KernelOptLin, KernelOptLR, MultiTask
from optwok.kernel import LinearKernel
    

def show_kernel(kmat):
    """Use matshow with gray colormap"""
    matshow(kmat)
    gray()
    colorbar()


def show_kernel_dev(kmat):
    """Use matshow with a nice colormap"""
    minval = min(kmat.flatten())
    maxval = max(kmat.flatten())
    absval = max([abs(minval),abs(maxval)])
    near_zero = 0.1*absval
    if minval < 0.0:
        zeroval = -minval/(2.0*absval)
    else:
        zeroval = 0.0
    small_neg = zeroval-near_zero
    small_pos = min([1.0, zeroval+near_zero])
    if small_neg >= 0.0:
        blueval = (small_neg-minval)/(small_neg + absval)
        assert(blueval>=0.0 and blueval<=1.0)
        print(blueval)
        cdict = {'red':  ((0.0, blueval, blueval),
                          (small_neg, 1.0, 1.0),
                          (zeroval, 1.0, 1.0),
                          (small_pos, 1.0, 1.0),
                          (1.0, 1.0, 1.0)),

                 'green': ((0.0, 0.8-blueval, 0.8-blueval),
                           (small_neg, 1.0, 1.0),
                           (zeroval, 1.0, 1.0),
                           (small_pos, 1.0, 1.0),
                           (1.0, 0.2, 0.2)),

                 'blue':  ((0.0, 1-blueval, 1-blueval),
                           (small_neg, 1.0, 1.0),
                           (zeroval, 1.0, 1.0),
                           (small_pos, 1.0, 1.0),
                           (1.0, 0.0, 0.0))
                 }
    else:
        small_neg = 0.0
        cdict = {'red':  ((0.0, 1.0, 1.0),
                          (small_neg, 1.0, 1.0),
                          (zeroval, 1.0, 1.0),
                          (small_pos, 1.0, 1.0),
                          (1.0, 1.0, 1.0)),

                 'green': ((0.0, 1.0, 1.0),
                           (small_neg, 1.0, 1.0),
                           (zeroval, 1.0, 1.0),
                           (small_pos, 1.0, 1.0),
                           (1.0, 0.2, 0.2)),

                 'blue':  ((0.0, 1.0, 1.0),
                           (small_neg, 1.0, 1.0),
                           (zeroval, 1.0, 1.0),
                           (small_pos, 1.0, 1.0),
                           (1.0, 0.2, 0.2))
                 }
    blue_red = LinearSegmentedColormap('BlueRedZero', cdict)
    matshow(kmat, cmap=blue_red)
    colorbar()


def test_kernelopt():
    """Small test for class KernelOpt"""
    from numpy.random import randn
    X = randn(10,50)
    Kx = array(matrix(X).T*matrix(X))
    alpha = randn(50,4)
    print(normalize_unit_diag(matrix(alpha).T*matrix(Kx)*matrix(alpha)))
    kern_mach = KernelOpt()
    kern_mach.classifier.Kx = Kx
    print(kern_mach.solve_opt(alpha))

def _create_test_data(mix_list=[[0,1],[2],[3],[4]],
                      num_point=[100, 50, 50, 50],
                      num_feat=20, edge_length=3):
    """Create an multiclass dataset with mixed classes"""
    from optwok.simdata import multi_cloudgen_mix
    
    num_blobs = len(mix_list)
    train_data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, examples, labels) = train_data
    return (metadata, examples, labels)

def test_kerneloptls():
    """Small test for class KernelOptLS"""
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    svm = KernelOptLS(reg_param=1000.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    print(vstack([train_out, labels]))
    show_kernel(svm.Ky)
    savefig('testKy.pdf')

def test_gp(mix_list=[[0,1],[2]]):
    """Small test for class KernelOptLS,
    for Gaussian process regression"""
    from optwok.simdata import gp_gen_pair
    from optwok.evaluation import rmse

    (examples, context, labels) = gp_gen_pair(10, 3, 5, 0.1, mix_list)
    svm = KernelOptLSGP(reg_param=10.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels)
    (train_out, pred_var) = svm.predict(examples)
    print(vstack([train_out, labels]))
    print('Error:')
    print(rmse(train_out, labels))
    
def test_reg_path_Ky():
    """The regularization path"""
    from optwok.kernel import normalize_unit_diag
    
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    svm = KernelOptLS(kernel=LinearKernel(), verbose=True)
    svm.train_reg_path(examples, labels)
    for ix in range(0,svm.all_Ky.shape[2],5):
        show_kernel(normalize_unit_diag(svm.all_Ky[:,:,ix]))
        title(ix)

def test_reg_path_acc():
    """Check accuracy for whole regularization path"""
    from optwok.evaluation import accuracy
    from pylab import figure, semilogx, xlabel, ylabel, legend
    from numpy import zeros, eye
    
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    svm = KernelOptLS(kernel=LinearKernel(), verbose=True)
    svm.train_reg_path(examples, labels)
    (metadata, examples, labels) = _create_test_data()
    all_preds = svm.predict_reg_path(examples)
    
    all_acc = zeros(len(svm.all_reg_param))
    for ix in range(all_preds.shape[1]):
        all_acc[ix] = accuracy(all_preds[:,ix], labels)
    svm.reg_param = 1.0
    svm.infer_sv(eye(svm.num_classes))
    diag_acc = accuracy(svm.predict(examples), labels)

    figure()
    semilogx(svm.all_reg_param, all_acc, hold=True, label='learn Ky')
    semilogx(svm.all_reg_param, all_acc, 'rx')
    semilogx([svm.all_reg_param[0],svm.all_reg_param[-1]], [diag_acc, diag_acc], 'g-', label='identity')
    xlabel('regularization parameter')
    ylabel('accuracy (higher is better)')
    legend()
    
def test_kerneloptlin():
    """Compare KernelOptLS with KernelOptLin"""
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    svm = KernelOptLS(reg_param=1000.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    print(train_out.shape)
    print(vstack([train_out, labels]))
    show_kernel(svm.Ky)
    savefig('testKy.pdf')

    svm = KernelOptLin(reg_param=1000.0, verbose=True)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    print(train_out.shape)
    print(vstack([train_out, labels]))
    show_kernel(svm.Ky)
    savefig('testLin.pdf')

def test_kernelopt_lowrank():
    """Compare KernelOptLS with KernelOptLR"""
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    svm = KernelOptLS(reg_param=1000.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    print(vstack([train_out, labels]))
    show_kernel(svm.Ky)
    savefig('testLS.pdf')

    svm = KernelOptLR(output_rank=4, reg_param=10.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    print(vstack([train_out, labels]))
    show_kernel(dot(svm.Ky,svm.Ky.T))
    savefig('testLR.pdf')

def test_multitask():
    """Check that multitask learning is sane"""
    from optwok.evaluation import accuracy_mix
    from optwok.testtools import list2matrix
    
    (metadata, examples, labels) = _create_test_data()
    print(metadata)
    mix_list = [[0,1],[2],[3],[4]]
    Ky = list2matrix(mix_list)
    svm = MultiTask(reg_param=1000.0, kernel=LinearKernel(), verbose=True)
    svm.train(examples, labels, init_Ky=Ky)
    train_out = svm.predict(examples)
    print(vstack([train_out, labels]))
    acc = accuracy_mix(train_out, labels, mix_list)
    print('Accuracy = %1.3f' % acc)
    
if __name__ == '__main__':
    test_kerneloptls()
    test_reg_path_Ky()
    test_reg_path_acc()
    test_gp()
    test_kerneloptlin()
    test_kernelopt_lowrank()
    test_multitask()
    show()
