"""Try to reproduce the phase transition of feature selection"""
from numpy import array, zeros, arange, meshgrid
from optwok.perceptron import Perceptron
from optwok.sparse_perceptron import SparsePerceptron
from optwok.simdata import linear_separation
from optwok.io_pickle import load, save

from matplotlib.pyplot import figure, show, axis, plot
from matplotlib.pyplot import title, xlabel, ylabel, colorbar
from matplotlib.pyplot import pcolormesh
from matplotlib.mlab import griddata



def _get_range_d(n, num_d):
    return array(range(n/(2*num_d),n,n/num_d))

def _get_range_k(cur_d, num_k):
    #return array(range(cur_d/num_k,cur_d,cur_d/num_k))
    return array(range(1,cur_d,cur_d/num_k))

def expt_perc(n, d, k, margin=0.5, perc_margin=1.0):
    """Train perceptron and check for features found"""
    print('Training with n=%d, d=%d, k=%d' % (n,d,k))
    (md, data, label) = linear_separation(n, k, d-k, margin,
                                          homogeneous=True, in_simplex=True)

    #perc = Perceptron()
    perc = SparsePerceptron()
    perc.train(data, label, perc_margin)
    print('Train time:\t\t\t %5.1f' % perc.train_time)
    
    threshold = 1.0/d
    fn = 1.0*sum(perc.q[:k] < threshold)/k
    fp = 1.0*sum(perc.q[k:] > threshold)/(d-k)
    print('Performance: fp=%f, fn=%f' % (fp,fn))
    return (fp,fn)

def test_grid(n,num_d,num_k):
    """Check whether the grid is good enough"""
    x = []
    y = []
    max_k = 0
    d = _get_range_d(n, num_d)
    for (idx_d, cur_d) in enumerate(d):
        k = _get_range_k(cur_d, num_k)
        max_k = max([max_k,len(k)])
        for (idx_k, cur_k) in enumerate(k):
            x.append((n-cur_d)/(1.0*n))
            y.append(cur_k/(1.0*cur_d))
    assert(min(x) > 0.0)
    assert(min(y) > 0.0)
    assert(max(x) < 1.0)
    assert(max(y) < 1.0)
    print(max_k)
    plot(x,y,'kx')
    axis([0,1,0,1])
    return max_k

def expt_phase_transition(n, num_d, num_k, max_k):
    """For various values of examples (n), features(d) and meaningful features (k)
    train a perceprton"""
    fp = zeros((num_d, max_k))
    fn = zeros((num_d, max_k))
    d = _get_range_d(n, num_d)
    for (idx_d, cur_d) in enumerate(d):
        k = _get_range_k(cur_d, num_k)
        for (idx_k, cur_k) in enumerate(k):
            (fp[idx_d, idx_k],fn[idx_d, idx_k]) = expt_perc(n,cur_d,cur_k)
    save('phase_transition.pkl', (n,d,num_k,fp,fn))

def collect_phase_transition():
    """Load and plot the results"""
    
    blob = load('phase_transition.pkl')
    (n,d,num_k,fp,fn) = blob
    x = []
    y = []
    z_fp = []
    z_fn = []
    for (idx_d, cur_d) in enumerate(d):
        k = _get_range_k(cur_d, num_k)
        for (idx_k, cur_k) in enumerate(k):
            x.append((n-cur_d)/(1.0*n))
            y.append(cur_k/(1.0*cur_d))
            z_fp.append(fp[idx_d, idx_k])
            z_fn.append(fn[idx_d, idx_k])
    x = array(x)
    y = array(y)
    z_fp = array(z_fp)
    z_fn = array(z_fn)

    plot_griddata(x,y,z_fp,z_fn)
    #plot_pcolormesh(x,y,z_fp,z_fn)

def plot_griddata(x,y,z_fp,z_fn):
    x_steps = arange(0.0,1.0,0.01)
    y_steps = arange(0.0,1.0,0.01)
    (x_grid, y_grid) = meshgrid(x_steps, y_steps)

    figure()
    print('False positive')
    fp = griddata(x, y, z_fp, x_grid, y_grid)
    print(fp)
    pcolormesh(x_grid, y_grid, fp, hold=True)
    plot(x, y, 'ko', alpha=0.2)
    title('False positive')
    xlabel('(n-d)/n')
    ylabel('k/d')
    axis([0,1,0,1])
    colorbar()
    
    figure()
    print('False negative')
    fn = griddata(x, y, z_fn, x_grid, y_grid)
    print(fn)
    pcolormesh(x_grid, y_grid, fn, hold=True)
    plot(x, y, 'ko', alpha=0.2)
    title('False negative')
    xlabel('(n-d)/n')
    ylabel('k/d')
    axis([0,1,0,1])
    colorbar()
    
def plot_pcolormesh(x,y,z_fp,z_fn):
    x.shape = (len(d),len(k))
    y.shape = (len(d),len(k))
    z_fp.shape = (len(d),len(k))
    z_fn.shape = (len(d),len(k))

    figure()
    print('False positive')
    pcolormesh(x,y,z_fp)
    title('False positive')
    xlabel('(n-d)/n')
    ylabel('k/d')
    axis([0,1,0,1])
    colorbar()
    
    figure()
    print('False negative')
    pcolormesh(x,y,z_fn)
    title('False negative')
    xlabel('(n-d)/n')
    ylabel('k/d')
    axis([0,1,0,1])
    colorbar()
    
if __name__ == '__main__':
    n = 1000
    num_d = 20
    num_k = 20

    max_k = test_grid(n, num_d, num_k)
    expt_phase_transition(n, num_d, num_k, max_k)
    collect_phase_transition()
    show()
