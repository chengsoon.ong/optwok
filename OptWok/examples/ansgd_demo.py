"""Demo and testing of accelerated nonsmooth stochastic gradient descent"""

from numpy import eye
from optwok.evaluation import accuracy
from optwok.simdata import create_data
from optwok.ansgd import SVM, ElasticNet, GraphNet, KernelElasticNet

def train_and_test(classifier):
    data = create_data(num_train=200, num_test=10000, num_feat=5,
                       num_noise_feat = 5, width=1.5)
    (train_ex, train_labels, test_ex, test_labels) = data 
    classifier.train(train_ex, train_labels)
    svmout = classifier.predict(test_ex)
    acc = accuracy(svmout, test_labels)
    print('Accuracy: %1.2f' % acc)
    print(classifier.w.T)

def demo_independent_features():
    """Check that all 4 classifiers are working"""
    reg_param = 1.2
    l1_reg_param = 0.7
    print('SVM')
    svm = SVM(reg_param)
    train_and_test(svm)

    print('ElasticNet')
    elastic_net = ElasticNet(reg_param, l1_reg_param)
    train_and_test(elastic_net)

    M = eye(10)
    M[:5,:5] += 0.3
    print('GraphNet')
    graph_net = GraphNet(reg_param, l1_reg_param, M)
    train_and_test(graph_net)

    print('KernelElasticNet')
    kernel_elastic_net = KernelElasticNet(reg_param, l1_reg_param, M)
    train_and_test(kernel_elastic_net)

    
if __name__ == '__main__':
    demo_independent_features()
    
