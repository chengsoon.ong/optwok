from optwok.named_array import NArray
import subprocess

if __name__ == '__main__':
    res = NArray(['CV','Task','Algorithm'])
    print('Initial conditions')
    print(res.ndim)
    print(res.dim_names)
    print(res.shape)
    res.add_keys(range(5), 'CV')
    print('Added 5 CV elements')
    print(res.shape)
    for task in ['A','B','A']:
        res.add_key(task, 'Task')
    for alg in ['svm','mtl','ky']:
        res.add_key(alg, 'Algorithm')
    print('Added tasks and methods')
    print(res.shape)
    print('Still nothing inside')
    print(res.content)

    # Note that you can fill in values in any order
    values = range(5,100)
    val_ix = 0
    for cv in range(3):
        for task in ['A','B']:
            for alg in ['ky','svm','mtl']:
                res.fill_value(values[val_ix], [task, cv, alg],
                             ['Task','CV','Algorithm'])
                val_ix += 1
        print('After CV=%d' % cv)
        print(res.content)

    res2 = NArray(['CV','Task','Algorithm'])
    res2.add_keys(range(5), 'CV')
    res2.add_keys(['A','B'], 'Task')
    res2.add_keys(['svm','mtl','ky'], 'Algorithm')
    for cv in [2,3,4]:
        for task in ['A','B']:
            for alg in ['ky','svm','mtl']:
                res.fill_value(values[val_ix], [task, cv, alg],
                             ['Task','CV','Algorithm'])
                val_ix += 1
    print(res2.content)
    print('Merging')
    res.merge(res2)
    del res2
    print(res.content)
    
    print('Get some entries')
    print(res.get_value(['B','mtl',2], ['Task','Algorithm','CV']))
    print(res.get_value(['mtl',2,'B'], ['Algorithm','CV','Task']))
    print(res.get_value([0,'A','ky'], ['CV', 'Task', 'Algorithm']))
    
    print('Get all keys')
    print(res.keys('Task'))
    print(res.keys('Algorithm'))
    
    print('Get a slice')
    print(res.slice('Task','B'))
    print('A thinner slice')
    print(res.slice(['Task','Algorithm'],['B','svm']))

    print('Bad slices')
    try:
        print(res.slice('Task','Z'))
    except IndexError as msg:
        print(msg)
    try:
        print(res.slice('Tas','B'))
    except IndexError as msg:
        print(msg)
    try:
        print(res.slice(['Task','Alg'],['B','svm']))
    except IndexError as msg:
        print(msg)
    try:
        print(res.slice(['Task','Algorithm'],['Z','svm']))
    except IndexError as msg:
        print(msg)

    print('Saving and loading')
    res.save('na_demo.json')
    new_res = NArray()
    new_res.load('na_demo.json')
    print('Get all keys')
    print(new_res.keys('Task'))
    print(new_res.keys('Algorithm'))
    
    print('Get a slice')
    print(new_res.slice('Task','B'))
    print('A thinner slice')
    print(new_res.slice(['Task','Algorithm'],['B','svm']))

    print('Save in matlab format')
    new_res.savemat('na_demo.mat')
    
    subprocess.check_call('rm na_demo.json na_demo.mat', shell=True)
