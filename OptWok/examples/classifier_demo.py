"""
Some code for testing and also as a demo
"""
from optwok.classifier import SVMPrimal, OneNormSVM, CapOneNormSVM, ZeroNormSVM
from optwok.classifier import RLSVM2, RLSVM1, RLSVMC1, RLSVM0
from optwok.classifier import OneNormSVM_dense
from optwok.evaluation import accuracy
from optwok.simdata import create_data
from optwok.testtools import plot_2d_pred, plot_2d

import pylab
import numpy
from pprint import pprint
from numpy import dot, eye, array, sign, sum, arange
from numpy.linalg import norm, cholesky, LinAlgError, eig, svd

def example_cloud_2d():
    """Use 2d gaussian clouds for training, plot the resulting classifier"""
    numpy.random.seed(1)
    (train_ex, train_lab, test_ex, test_labels) = create_data(num_train=200, num_test=10, num_feat=2,
                                                              num_noise_feat=0, width=0.7, frac_flip=0.2)
    mu = 1.0
    reg_param = 1.0
    verbose = False

    kdict = {'name':'linear', 'param': 0}
    #kdict = {'name':'poly', 'param': 3, 'inhomog': True}

    # Train
    svm = SVMPrimal(reg_param, kdict, verbose=verbose)
    svm.train(train_ex, train_lab)
    rlsvm = RLSVM2(reg_param, kdict, mu, rand_init=False, verbose=verbose)
    rlsvm.train(train_ex, train_lab)

    # Predict on grid
    plot_limit = 4.0
    axis_limit = [-plot_limit, plot_limit, -plot_limit, plot_limit]
    x1 = numpy.linspace(-plot_limit, plot_limit, 100)
    x2 = numpy.linspace(-plot_limit, plot_limit, 100)
    x,y = numpy.meshgrid(x1,x2)
    pred_ex = numpy.array((numpy.ravel(x), numpy.ravel(y)))

    svmout = svm.predict(pred_ex)
    svmout.shape = (100,100)
    rlsvmout = rlsvm.predict(pred_ex)
    rlsvmout.shape = (100,100)

    print('=====================================================================')
    print('SVM Primal objective: %f' % svm.primal_obj)
    print('SVM weights: ')
    print(svm.w)
    print('SVM bias: %f' % svm.b)
    print('=====================================================================')
    print('RLSVM Primal objective: %f' % rlsvm.primal_obj)
    print('RLSVM weights: ')
    print(rlsvm.w)
    print('RLSVM bias: %f' % rlsvm.b)
    print('Number of passes through data: %d' % rlsvm.num_iter)

    plot_2d_pred(x, y, svmout, train_ex, train_lab, axis_limit, 'svm')
    plot_2d_pred(x, y, rlsvmout, train_ex, train_lab, axis_limit, 'rlsvm')
    pylab.show()
    
def demo_helper(classifier, train_ex, train_labels, test_ex, test_labels):
    """Helper script to train, predict and report performance"""
    classifier.train(train_ex, train_labels)
    svmout = classifier.predict(test_ex)
    acc = accuracy(svmout, test_labels)
    print('Accuracy:\t\t\t %1.2f' % acc)
    #print('Margin:\t\t\t\t %f' % classifier.margin)
    #print('Empirical Risk:\t\t\t %f' % sum(classifier.emp_risk))
    #print('Primal Objective:\t\t %f' % classifier.primal_obj)
    #print(numpy.where(abs(classifier.w)>classifier.epsilon)[0])
    #print('Fraction of features used:\t %1.4f' % classifier.frac_feat)
    #print('Fraction support vectors:\t %1.4f' % classifier.frac_sv)
    #print('Total fraction:\t\t\t %1.4f' % classifier.frac_total)
    pprint(classifier.perf)

def demo_svm(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create a SVM, train and predict"""
    print('=== Running SVM ===')
    svm = SVMPrimal(reg_param, kdict, verbose=verbose)
    demo_helper(svm, train_ex, train_labels, test_ex, test_labels)

def demo_onenormsvm(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create a one norm SVM, train and predict"""
    print('=== Running one norm SVM ===')
    svm = OneNormSVM(reg_param, kdict, verbose=verbose)
    demo_helper(svm, train_ex, train_labels, test_ex, test_labels)

def demo_onenormsvm_dense(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create a one norm SVM, train and predict"""
    print('=== Running one norm SVM with dense matrices ===')
    svm = OneNormSVM_dense(reg_param, kdict, verbose=verbose)
    demo_helper(svm, train_ex, train_labels, test_ex, test_labels)

def demo_caponenormsvm(reg_param, kdict, a, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create a capped one norm SVM, train and predict"""
    print('=== Running cap one norm SVM ===')
    svm = CapOneNormSVM(reg_param, kdict, a, rand_init=False, verbose=verbose)
    demo_helper(svm, train_ex, train_labels, test_ex, test_labels)

def demo_zeronormsvm(reg_param, kdict, a, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create a one norm SVM, train and predict"""
    print('=== Running zero norm SVM ===')
    svm = ZeroNormSVM(reg_param, kdict, a, rand_init=False, verbose=verbose)
    demo_helper(svm, train_ex, train_labels, test_ex, test_labels)

def demo_rlsvm2(reg_param, kdict, mu, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create an RLSVM, train and predict"""
    print('=== Running RLSVM2 ===')
    rlsvm = RLSVM2(reg_param, kdict, mu, rand_init=False, verbose=verbose)
    demo_helper(rlsvm, train_ex, train_labels, test_ex, test_labels)

def demo_rlsvm1(reg_param, kdict, mu, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create an RLSVM, train and predict"""
    print('=== Running RLSVM1 ===')
    rlsvm = RLSVM1(reg_param, kdict, mu, rand_init=False, verbose=verbose)
    demo_helper(rlsvm, train_ex, train_labels, test_ex, test_labels)

def demo_rlsvmc1(reg_param, kdict, mu, a, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create an RLSVM, train and predict"""
    print('=== Running RLSVMC1 ===')
    rlsvm = RLSVMC1(reg_param, kdict, mu, a, rand_init=False, verbose=verbose)
    demo_helper(rlsvm, train_ex, train_labels, test_ex, test_labels)

def demo_rlsvm0(reg_param, kdict, mu, a, verbose, train_ex, train_labels, test_ex, test_labels):
    """Create an RLSVM, train and predict"""
    print('=== Running RLSVM0 ===')
    rlsvm = RLSVM0(reg_param, kdict, mu, a, rand_init=False, verbose=verbose)
    demo_helper(rlsvm, train_ex, train_labels, test_ex, test_labels)

def example_toy():
    """Training and classification using two gaussian clouds"""
    #numpy.random.seed(1)
    (train_ex, train_labels, test_ex, test_labels) = create_data(num_train=200,
                                                                 num_test=1000, num_feat=5,
                                                                 num_noise_feat = 45,
                                                                 width=1.5, frac_flip=0.1,
                                                                 symm_flip=True)
    mu = 1.0
    a = 4.0   # the exponent in zero norm
    cap = 0.5 # the cap in capped one norm
    reg_param = 1.0
    verbose = False

    #kdict = {'name':'poly', 'param': 2, 'inhomog': True}
    kdict = {'name':'linear', 'param': 0}
    
    demo_svm(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_onenormsvm(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_caponenormsvm(reg_param, kdict, cap, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_zeronormsvm(reg_param, kdict, a, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_rlsvm2(reg_param, kdict, mu, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_rlsvm1(reg_param, kdict, mu, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_rlsvmc1(reg_param, kdict, mu, cap, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_rlsvm0(reg_param, kdict, mu, a, verbose, train_ex, train_labels, test_ex, test_labels)

def example_debug():
    """Tiny example to try to check global optimality"""
    plot_limit = 4.0
    axis_limit = [-plot_limit, plot_limit, -plot_limit, plot_limit]
    train_ex = numpy.array([[-1.0, -1.0, -1.0, -1.0, -1.0, 1.0,  1.0, 1.0, 1.0, 1.0],
                            [-1.0, 0.5,  0.0, -0.5, 1.0,  0.5, -0.5, 0.0, 1.0, -1.0]])
    train_lab = numpy.array([-1., -1., -1., -1., 1., 1., 1., 1.,1.,1.])
    x1 = numpy.linspace(-plot_limit, plot_limit, 100)
    x2 = numpy.linspace(-plot_limit, plot_limit, 100)
    x,y = numpy.meshgrid(x1,x2)
    pred_ex = numpy.array((numpy.ravel(x), numpy.ravel(y)))

    verbose = False
    mu = 1.0
    C = 1.0       
    kdict = {'name':'linear', 'param': 0}
    svm = OneNormSVM(C, kdict, verbose=verbose)
    svm.train(train_ex, train_lab)
    print('=====================================================================')
    print('OneNormSVM objective: %f' % svm.primal_obj)
    print('SVM weights: ')
    print(svm.w)
    print('SVM bias: %f' % svm.b)
    print('=====================================================================')
    svmout = svm.predict(pred_ex)
    svmout.shape = (100,100)
    del svm

    svm = RLSVM1(C, kdict, mu, verbose=verbose)
    svm.train(train_ex, train_lab)
    print('=====================================================================')
    print('RLSVM1 Primal objective: %f' % svm.primal_obj)
    print('RLSVM1 weights: ')
    print(svm.w)
    print('RLSVM1 bias: %f' % svm.b)
    print('Number of passes through data: %d' % svm.num_iter)
    print('=====================================================================')
    rlsvmout = svm.predict(pred_ex)
    rlsvmout.shape = (100,100)
    del svm

    plot_2d_pred(x, y, svmout, train_ex, train_lab, axis_limit, 'svm')
    plot_2d_pred(x, y, rlsvmout, train_ex, train_lab, axis_limit, 'dcsvm')
    pylab.show()



def example_sparse():
    """Training and classification using two gaussian clouds,
    use sparse classifiers
    """
    #numpy.random.seed(1)
    (train_ex, train_labels, test_ex, test_labels) = create_data(num_train=200,
                                                                 num_test=1000, num_feat=5,
                                                                 num_noise_feat = 45,
                                                                 width=1.5, frac_flip=0.1,
                                                                 symm_flip=True)
    mu = 1.0
    a = 4.0   # the exponent in zero norm
    cap = 0.5 # the cap in capped one norm
    reg_param = 1.0
    verbose = False

    #kdict = {'name':'poly', 'param': 2, 'inhomog': True}
    kdict = {'name':'linear', 'param': 0}
    
    demo_onenormsvm_dense(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels)
    demo_onenormsvm(reg_param, kdict, verbose, train_ex, train_labels, test_ex, test_labels)
    
    
if __name__ == '__main__':
    example_debug()
    example_toy()
    example_cloud_2d()

    #example_sparse()
    
