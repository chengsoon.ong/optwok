"""
Demo to show how to use the contextual bandits optimization problem
using the upper confidence bound Gaussian process
"""
from pylab import show
from numpy import ones, eye

from optwok.kernel import GaussKernel
from optwok.testtools import list2matrix
from optwok.io_pickle import load
from optwok.gaussian_proc import GaussianProcess
from optwok import datatools
from optwok.context_ucb import expt_cucb, expt_cucb_batch
from optwok.context_ucb import plot_min_regret, plot_average_regret

from examples.gaussianproc_demo import plot_prediction

def demo_expt_cucb():
    raw_data = load('tests/gaussproc_06_20_0.1.pkl')
    data = datatools.Data(raw_data.examples, raw_data.context, raw_data.labels)
    print(data.examples(1))
    num_context = 6
    reg_param = 0.1
    width = 0.5
    num_epoch = 10
    
    Ky = list2matrix([[0,1],[2],[3,4,5]], neg_corr=True)
    print(Ky)
    gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(raw_data.examples[:,:1], raw_data.context[:1], raw_data.labels[:1])
    expt_cucb(data, gp, 'expt_cucb.pkl', num_context, num_epoch)

    Ky = ones((num_context, num_context))
    gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(raw_data.examples[:,:1], raw_data.context[:1], raw_data.labels[:1])
    expt_cucb(data, gp, 'expt_cucb_cat.pkl', num_context, num_epoch)
    
    Ky = eye(num_context)
    gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(raw_data.examples[:,:1], raw_data.context[:1], raw_data.labels[:1])
    expt_cucb(data, gp, 'expt_cucb_ind.pkl', num_context, num_epoch)
    
    plot_min_regret('expt_cucb.pkl')
    plot_min_regret('expt_cucb_cat.pkl')
    plot_min_regret('expt_cucb_ind.pkl')
    plot_average_regret(['expt_cucb.pkl','expt_cucb_cat.pkl','expt_cucb_ind.pkl'],
                        ['CGP-UCB','merge','ignore'])

def demo_expt_cucb_batch():
    raw_data = load('tests/gaussproc_06_20_0.1.pkl')
    data = datatools.Data(raw_data.examples, raw_data.context, raw_data.labels)
    num_context = 6
    reg_param = 0.1
    width = 0.5
    num_epoch = 3
    batch_size = 3
    
    Ky = list2matrix([[0,1],[2],[3,4,5]], neg_corr=True)
    print(Ky)

    gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(raw_data.examples[:,:1], raw_data.context[:1], raw_data.labels[:1])
    expt_cucb_batch(data, gp, 'expt_cucb_kbest.pkl', num_context, num_epoch,
                    batch_size=batch_size, mode='kbest')
    del gp
    
    gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(raw_data.examples[:,:1], raw_data.context[:1], raw_data.labels[:1])
    expt_cucb_batch(data, gp, 'expt_cucb_batch.pkl', num_context, num_epoch,
                    batch_size=batch_size)
    del gp
    
    plot_min_regret('expt_cucb_batch.pkl')
    plot_min_regret('expt_cucb_kbest.pkl')
    plot_average_regret(['expt_cucb_batch.pkl','expt_cucb_kbest.pkl'], ['greedy','kbest'])

    
if __name__ == '__main__':
    demo_expt_cucb()
    demo_expt_cucb_batch()
    show()

