def example_test_pu_classifier():
    """Debug the PU classifier"""
    
    print("example_test_pu_classifier")
    numpy.random.seed(1)
    
    cov = 0.4
    cov_arr = (1+arange(10))/10.0
    kernel_width = 1
    kernel_type = 'Linear'
    dim_arr = 2+arange(8)
    dim = 2
    Cp = 100
    C_arr = [1,10,100,1000]
    Cu = 10
    mu_arr = (1+arange(10))/10.0
    num_positive = 100
    num_mixed = 150
    positive_ratio_in_mix = 0.333
    num_test = 500
    verbose = True
    
    
    results = {}
    for mu in mu_arr:
        for Cp in C_arr:
            for Cu in C_arr: 
            
                model = (mu,Cp,Cu)
                print("parameters mu, Cp, Cu ", model)
                
                (train_examples, train_labels, real_train_labels, test_examples, test_labels) = \
                create_datasets_for_positive_and_mixed_experiment(dim, cov, num_positive, num_mixed, positive_ratio_in_mix, num_test)
                
                plot_2d(train_examples, train_labels)
                pylab.show()
                plot_2d(train_examples, real_train_labels)
                pylab.show()
                plot_2d(test_examples, test_labels)
                pylab.show()
                
                exit()
                pu_classifier = PUClassifier(Cp, Cu, mu, kernel_type, verbose = verbose, rand_init = False)
                pu_classifier.train(train_examples, train_labels)
                (predictions, cost_sensitive_svm_predictions) = pu_classifier.predict(train_examples)
                
                acc1 =  100.0*sum(sign(predictions) == train_labels)/len(train_labels)
                acc2 =  100.0*sum(sign(predictions) == real_train_labels)/len(real_train_labels)
                print("accuracy ",acc1,acc2)
                
                #compare with the cost sensitive svm
                cs_acc1 =  100.0*sum(sign(cost_sensitive_svm_predictions) == train_labels)/len(train_labels)
                cs_acc2 =  100.0*sum(sign(cost_sensitive_svm_predictions) == real_train_labels)/len(real_train_labels)
                print("cost sensitive svm accuracy ",cs_acc1, cs_acc2)
                
                results[model] = (acc1,acc2, cs_acc1, cs_acc2)
    
    print(results)
            
           
     #sanity check
#    kernel_mat = dot(train_examples.T, train_examples)
#    alphas = array([1.2,3,5,2,4,-2,-3,-1.2,-0.1,-5])
#    alphas.shape = (10,1)
#    num_p = 5
#    P = kernel_mat.copy()
#    P[num_p:, :num_p] *= -1
#    P[:num_p, num_p:] *= -1
#    sum1 =  0.5*dot(dot(alphas.T,P),alphas)
#    print(sum1)
#    sum2 = 0
#    for i in range(5):
#        for j in range(5):
#            sum2 += 0.5*alphas[i]*alphas[j]*kernel_mat[i,j]
#            sum2 += 0.5*alphas[5+i]*alphas[5+j]*kernel_mat[5+i,5+j]
#    print(sum2)
#    for i in range(5):
#        for j in range(5,10):
#            sum2 -= alphas[i]*alphas[j]*kernel_mat[i,j]
#    
#    print(sum2)


if __name__ == '__main__':
    
    example_test_pu_classifier()
