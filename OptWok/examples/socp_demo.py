'''
Created on 16.12.2011

@author: kgabriel
'''

from optwok.socp import SOCP
from numpy import array

if __name__ == '__main__':
    socp = SOCP(3, verbose = True)
    socp.set_objective(array([-2, 1, 5.]))

    A = array([[-13., 3, 5], [-12, 12, -6]])
    b = array([-3, -2.])
    c = array([-12., -6, 5])
    d = -12

    socp.add_ineq_constraints(A, b, c, d)

    A = array([[-3., 6, 2], [1, 9, 2], [-1, -19, 3]])
    b = array([0, 3, -42.])
    c = array([-3, 6, -10.])
    d = 27

    socp.add_ineq_constraints(A, b, c, d)

    socp.solve()
    print(socp.solution)
