"""Show how to create and use a pytables dataset"""

import os
from tables import openFile
from optwok.simdata import linear_separation
from optwok.evaluation import accuracy
from optwok.ansgd import SVM
from optwok.mldata_pytables import Dataset

def create_hdf5_data(num_examples=1000, num_feat=5, num_noise_feat=5):
    """Create some data and save as HDF5 file"""
    dataname = 'pytables_demo'
    data = linear_separation(num_examples, num_feat, num_noise_feat)
    (metadata, examples, labels) = data
    h5file = openFile(dataname+'_mat.h5', mode='w', title='Test file')
    h5file.createArray(h5file.root, 'examples', examples)
    h5file.createArray(h5file.root, 'labels', labels)
    h5file.close()
    return dataname
    
if __name__ == '__main__':
    dataname = create_hdf5_data()
    data = Dataset(dataname, data_dir=os.getcwd())
    print('Number of examples = %d' % data.num_examples)
    print('Number of features = %d' % data.num_features)

    reg_param = 1.2
    svm = SVM(reg_param)
    (idx_train, idx_pred, id_str) = data.get_perm(3, 'test')
    svm.train(data.examples[:,idx_train], data.labels[idx_train])
    svmout = svm.predict(data.examples[:,idx_pred])
    labels = data.labels[idx_pred]
    acc = accuracy(svmout, labels)
    print('Accuracy: %1.2f' % acc)

    data.close()
    os.remove(dataname+'_mat.h5')
    os.remove(dataname+'_perm.txt')
    
