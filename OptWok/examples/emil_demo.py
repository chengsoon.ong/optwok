"""
Some code for testing and also as a usage demo
See bioweb.me/emil for benchmark experiments
"""

from numpy import r_
from numpy.random import seed
from optwok.mil.mi_data import EllipsoidData, MIMeanData
from optwok.utility import create_perm, permfile_name
from emil_synthetic_ellipsoids import random_ellipsoids
from emil_plotting import plot_ellipsoids, plot_hyperplane
from optwok.mil.mi_classifier import MeanSVM
from optwok.mil.mi_classifier_emil import eMILDcaSocp, eMILDcaBfgs, eMILBfgs
from matplotlib.pyplot import show, figure
from optwok.performance_measures import get_conf_matrix, accuracy

def example_ellipsoid_2d():
    """Create some random ellipsoids, train and plot the resulting classifier"""
    reg_param = 1.5
    num_pos = 13
    num_neg = 13
    num_ex = num_pos + num_neg
    num_feat = 2
    
    seed(1)

    # Create some data
    perm_desc = 'example'
    perm_dataset = 'synthetic_ellipsoid_2d'
    create_perm('.', perm_dataset, perm_desc, 10, num_ex)
    means, covariances, labels = random_ellipsoids(num_neg=num_neg, num_pos=num_pos, n=num_feat, mean_radius=3)
    data = EllipsoidData(means, covariances, labels)
    meandata = MIMeanData(means, labels)
    
    train_data, test_data = data.get_train_pred_data(permfile=permfile_name('.', perm_dataset, perm_desc), perm_idx=1,
                                                     model_selection=False, frac_ms=0.5, fold_cv=None, num_cv=None)

    print('Training mean SVM for initialization')
    meanclassifier = MeanSVM()
    mean_train_data, _ = meandata.get_train_pred_data(permfile=permfile_name('.', perm_dataset, perm_desc), perm_idx=1,
                                                     model_selection=False, frac_ms=0.5, fold_cv=None, num_cv=None)
    meanclassifier.train(mean_train_data, reg_param)

    classifier = eMILDcaSocp()
    print('Training eMIL')
    classifier.train(train_data, reg_param, v0=r_[meanclassifier.w, meanclassifier.b], huber_delta=0.1)
    
    print('Predicting')
    train_pred = classifier.predict(train_data)
    conf = get_conf_matrix(train_pred, train_data.labels)
    acc = accuracy(conf)
    print('Accuracy = %1.3f' % acc)

    # Plot training
    fig = figure()
    plot_ellipsoids(train_data.means, train_data.covariances, train_data.labels, color=True)
    plot_hyperplane(classifier.w, classifier.b)
    fig.savefig('%s-reg_param_%d' % (classifier.__class__.__name__, reg_param))
    show()

if __name__ == '__main__':
    example_ellipsoid_2d()
