"""
Some code for testing and also as a demo
"""
import pylab
from numpy import eye, vstack, ones, matrix
from numpy import nonzero, abs, zeros, sum, sqrt, unique
from optwok.simdata import multi_cloudgen, multi_cloudgen_mix
from optwok.testtools import plot_confusion
from optwok.multiclass import StructPredictor
from optwok.evaluation import accuracy, accuracy_mix
from optwok.kernelopt import KernelOpt
from optwok.kernel import JointKernel, LinearKernel, GaussKernel
from optwok.kernel import normalize_unit_diag

def demo_identity_Ky():
    print('demo_identity_Ky')
    num_point = 100
    num_feat = 10
    num_class = 3
    edge_length = 3

    train_data = multi_cloudgen(num_point, num_feat, num_class, edge_length)
    (metadata, examples, labels) = train_data
    print(metadata)
    kernel = JointKernel(LinearKernel(), num_classes=num_class)
    svm = StructPredictor(1.0, kernel)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    train_acc = accuracy(train_out, labels)
    print('Training accuracy:\t %1.2f' % train_acc)

    test_data = multi_cloudgen(num_point, num_feat, num_class, edge_length)
    (metadata, test_ex, test_lab) = test_data
    svmout = svm.predict(test_ex)
    acc = accuracy(svmout, test_lab)
    print('Test accuracy:\t\t %1.2f' % acc)

def demo_merged_classes():
    """The examples come from different Gaussians, but have the same label."""
    print('demo_merged_classes')
    num_point = 100
    num_feat = 10
    num_class = 4
    edge_length = 3
    mix_list = [[0,1],[2],[3]]
    
    train_data = multi_cloudgen_mix(num_point, num_feat, num_class, edge_length, mix_list)
    (metadata, examples, labels) = train_data
    print(metadata)
    kernel = JointKernel(GaussKernel(), num_classes=3)
    svm = StructPredictor(1.0, kernel)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    train_acc = accuracy(train_out, labels)
    print('Training accuracy:\t %1.2f' % train_acc)

    test_data = multi_cloudgen_mix(num_point, num_feat, num_class, edge_length, mix_list)
    (metadata, test_ex, test_lab) = test_data
    svmout = svm.predict(test_ex)
    acc = accuracy(svmout, test_lab)
    print('Test accuracy:\t\t %1.2f' % acc)


def demo_split_classes():
    """The examples from the same Gaussians may have different labels."""
    print('demo_split_classes')
    num_point = 100
    num_feat = 6
    num_class = 3
    edge_length = 2
    mix_list = [[0,1],[2],[3]]
    Ky = eye(4)
    Ky[0,1] = 0.99
    Ky[1,0] = 0.99
    print(Ky)
    
    train_data = multi_cloudgen_mix(num_point, num_feat, num_class, edge_length, mix_list)
    (metadata, examples, labels) = train_data
    print(metadata)
    test_data = multi_cloudgen_mix(num_point, num_feat, num_class, edge_length, mix_list)
    (metadata, test_ex, test_lab) = test_data
    kernel = JointKernel(LinearKernel(), num_classes=4)

    print('============== With identity Ky ==============')
    svm = StructPredictor(1.0, kernel)
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    #print(vstack([train_out, labels]))
    print('Training accuracy:\t\t %1.2f' % accuracy(train_out, labels))
    print('Training accuracy (mix):\t %1.2f' % accuracy_mix(train_out, labels, mix_list))

    svmout = svm.predict(test_ex)
    #print(vstack([svmout, test_lab]))
    print('Test accuracy:\t\t\t %1.2f' % accuracy(svmout, test_lab))
    print('Test accuracy (mix):\t\t %1.2f' % accuracy_mix(svmout, test_lab, mix_list))

    Ktarget = normalize_unit_diag(matrix(svm.alpha).T*matrix(svm.kernel.Kx.kmat)*matrix(svm.alpha))
    print(Ktarget)

    print('============== With mix Ky ==============')
    svm.kernel.Ky = Ky
    svm.train(examples, labels)
    train_out = svm.predict(examples)
    #print(vstack([train_out, labels]))
    print('Training accuracy:\t\t %1.2f' % accuracy(train_out, labels))
    print('Training accuracy (mix):\t %1.2f' % accuracy_mix(train_out, labels, mix_list))

    svmout = svm.predict(test_ex)
    #print(vstack([svmout, test_lab]))
    print('Test accuracy:\t\t\t %1.2f' % accuracy(svmout, test_lab))
    print('Test accuracy (mix):\t\t %1.2f' % accuracy_mix(svmout, test_lab, mix_list))

    print(normalize_unit_diag(matrix(svm.alpha).T*matrix(svm.kernel.Kx.kmat)*matrix(svm.alpha)))


if __name__ == '__main__':
    demo_identity_Ky()
    demo_merged_classes()
    demo_split_classes()
