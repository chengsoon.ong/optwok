from numpy import array
from optwok.evaluation import accuracy, balanced_accuracy
from optwok.evaluation import stats_empirical, stats_binormal

def _demo_show(tpr, fpr, ppv, auc, apr):
    from pylab import plot, figure, axis, title, xlabel, ylabel

    print('AUC=%f, APR=%f' % (auc, apr))

    figure()
    if len(tpr) < 100:
        plot(fpr, tpr, 'bo', hold=True)
    plot(fpr, tpr, 'b-')
    axis([0,1,0,1])
    xlabel('False positive rate')
    ylabel('True positive rate')
    title('ROC')

    figure()
    if len(ppv) < 100:
        plot(tpr, ppv, 'bo', hold=True)
    plot(tpr, ppv, 'b-')
    axis([0,1,0,1])
    ylabel('Positive predictive value')
    xlabel('True positive rate')
    title('PRC')
    

def demo_stats_binary():
    from numpy.random import rand
    from pylab import show
    labels = array([-1, 1., -1., 1., -1., -1., -1., 1., -1., -1.])
    output = array([-0.4, 0.9, 0.4, 0.3, -0.1, -0.6, -0.7, -0.2, -1., -0.9,])

    acc = accuracy(output, labels)
    balacc = balanced_accuracy(output, labels)
    print('ACC=%f, BAL=%f' % (acc, balacc))
    tpr, fpr, ppv, auc, apr = stats_empirical(output, labels)
    _demo_show(tpr, fpr, ppv, auc, apr)
    tpr, fpr, ppv, auc, apr = stats_binormal(output, labels)
    _demo_show(tpr, fpr, ppv, auc, apr)
    show()

if __name__ == '__main__':
    demo_stats_binary()
