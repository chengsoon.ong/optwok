'''
Created on 05.09.2012

@author: kgabriel
'''

from numpy import linalg
from sys import float_info

from math import atan2
from matplotlib.patches import Ellipse
from numpy import array, logical_not, sqrt, pi
from numpy.linalg import norm
from matplotlib.pyplot import plot, xlim, gca, Polygon


def plot_ellipsoids(q, S, y, color=False):
    ''' plot 2d-ellipsoids '''
    
    for qi, Si, yi in zip(q, S, y):
        plot_ellipse(qi, Si, label=yi, color=color)

def plot_ellipse(q, Q, edge = 'k', face = 'none', scale = 1, label=None, color=False, alpha=0.7):
    # Plots the ellipse defined by: 
    U, s , _ = linalg.svd(Q)
#    print 's0: ', s[0]
#    print 's1: ', s[1]
    orient = atan2(U[1, 0], U[0, 0]) * 180 / pi
    ellipsePlot = Ellipse(xy = q, width = 2 * scale * sqrt(s[0]),
                          height = 2 * scale * sqrt(s[1]), angle = orient, facecolor = face,
                          edgecolor = edge)
    ax = gca()
    
    ax.set_aspect('equal')
    if label is None:
        l = '.'
    elif label > 0:
        l = '+'
        c = 'g'
    else:
        l = 'o'
        c = 'r'
                
    if color:
        ellipsePlot.set_facecolor(c)
        ellipsePlot.set_alpha(alpha)
        
    ax.add_patch(ellipsePlot);
    
    plot(q[0], q[1], 'k' + l)
    return ellipsePlot

def plot_vector(origin, vector, color='k'):
    ''' plot vector from origin '''
    
    dest = origin + vector
    
    plot([origin[0], dest[0]], [origin[1], dest[1]], '-', color = color)
    plot(origin[0], origin[1], 'o', color = color)
    plot(dest[0], dest[1], 'x', color = color)

def plot_hyperplane(w,b,lcolor='black',pcolor='green',lstyle='solid',alpha=0.5):
    ''' plot hyperplane defined by w and b, plus margin 1/|w| '''
    xl = array(xlim())
    if w[1] == 0:
        w[1] = float_info.min
    y = (-w[0] * xl - b) / w[1]
    plot(xl, y, color=lcolor, linestyle=lstyle)
    
    margin = 1 / norm(w)
    
    patch = Polygon([[xl[0], y[0] - margin], [xl[0], y[0] + margin], [xl[1], y[1] + margin], [xl[1], y[1]-margin]], 
                    fill=True, closed=True, color=pcolor,alpha=alpha)
    gca().add_patch(patch)

def plot_hyperplane_nomargin(w, b):
    # plot hyperplane w'x + b = 0 in 2d

    xl = array(xlim())
    plot(xl, (-w[0] * xl - b) / w[1])
    
def plot_points(p, y):
    p, y = array(p), array(y)
    pos = y > 0
    plot(p[pos,0], p[pos,1], 'go')
    plot(p[logical_not(pos),0], p[logical_not(pos),1], 'ro')
