"""Demonstrate attribute learning code"""

from numpy import array, matrix
from numpy import vstack, ones, argmax
from numpy.random import rand
from optwok.simdata import multi_cloudgen_mix
from optwok.attributeopt import AttributeOpt
from optwok.multiclass import idx2binvec
from optwok.kernel import GaussKernel

from matplotlib.pyplot import matshow, gray, colorbar, show

def show_kernel(kmat):
    """Use matshow with gray colormap"""
    matshow(kmat)
    gray()
    colorbar()

if __name__ == '__main__':
    num_point = 100
    num_feat = 10
    num_class = 3
    num_att = 5
    edge_len = 3
    mix_list = [[0,1],[2,3],[4]]
    att_mat = matrix("1,1,0,0,0; 0,0,1,1,0; 0,0,0,0,1")

    train_data = multi_cloudgen_mix(num_point, num_feat, num_class, edge_len, mix_list)
    (metadata, examples, attributes) = train_data
    labels = array(matrix(idx2binvec(attributes))*att_mat.T).T

    svm = AttributeOpt(kernel=GaussKernel(), verbose=True)
    svm.train(examples, labels, rand(num_class, num_att))
    train_out = svm.predict(examples)
    print(vstack([train_out, argmax(labels, axis=0)]))
    print(svm.Ky)
    print(svm.A)
    show_kernel(svm.Ky)
    show_kernel(svm.A)
    show()
    
