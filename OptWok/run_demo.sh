#!/bin/bash

echo '===== Classifier Demo ====='
python examples/classifier_demo.py

echo '===== Context UCB Demo ====='
python examples/contextucb_demo.py
rm expt_cucb.pkl expt_cucb_cat.pkl expt_cucb_kbest.pkl
rm expt_cucb_batch.pkl expt_cucb_ind.pkl
rm expt_cucb_avg.pdf expt_cucb_cat_min.pdf expt_cucb_min.pdf
rm expt_cucb_batch_avg.pdf expt_cucb_ind_min.pdf
rm expt_cucb_batch_min.pdf expt_cucb_kbest_min.pdf

echo '===== Ellipsoidal MIL Demo ====='
python examples/emil_demo.py
rm example_synthetic_ellipsoid_2d.json

echo '===== Gaussian Process Regression Demo ====='
python examples/gaussianproc_demo.py

echo '===== Learning Output Kernel Demo ====='
python examples/kernelopt_demo.py
rm testKy.pdf testLin.pdf

echo '===== Multiclass Demo ====='
python examples/multiclass_demo.py

echo '===== Named Array Demo ====='
python examples/named_array_demo.py

echo '===== Score Demo ====='
python examples/score_demo.py

echo '===== SOCP Demo ====='
python examples/socp_demo.py

