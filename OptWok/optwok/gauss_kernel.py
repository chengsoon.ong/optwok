
class Kernel(object):
    """A wrapper to define the interfaces for kernels"""
    def __init__(self, normalize=True):
        self.train_ex = None
        self.test_ex = None
        self.km = None
        self.normalize = normalize   # not implemented yet
        
    def init_data(self, train_ex, test_ex):
        """Copy data to internal variables"""
        self.train_ex = train_ex.copy()
        self.test_ex = test_ex.copy()

    def get_kernel_matrix(self, split='train'):
        pass

class LinearKernel(Kernel):
    """Standard scalar product"""
    def __init__(self, normalize=True):
        Kernel.__init__(self, normalize)

    def get_kernel_matrix(self, split='train'):
        if split == 'train':
            return array(matrix(self.train_ex).T*matrix(self.train_ex))
        elif split == 'test':
            return array(matrix(self.train_ex).T*matrix(self.test_ex))

class GaussianKernel(Kernel):
    """Gaussian Kernel with scalar width
    k(x,y) = exp(-width * ||x-y||^2)
    """
    def __init__(self, width=1.0, normalize=True):
        Kernel.__init__(self, normalize)
        self.width = width

    def get_kernel_matrix(self, split='train'):
        if split == 'train':
            m = self.train_ex.shape[1]
            a = self.train_ex
            unitvec = ones(m,1)
            dot_a = sum(self.width*a*a,1)
            K = matrix(sqrt(self.width)*a).T * matrix(sqrt(self.width)*a)
            for ix in range(m):
                K[:,ix] = exp(2*K[:,ix] - transpose(dot_a) - dot_a[ix]*unitvec)

        elif split == 'test':
            a = self.train_ex
            m = a.shape[1]
            b = self.test_ex
            n = b.shape[1]

            unitvec = ones(m,1)
            dot_a = sum(self.width*a*a,1)
            dot_a = sum(self.width*a*a,1)
            K = matrix(sqrt(self.width)*a).T * matrix(sqrt(self.width)*b)
            for ix in range(n):
                K[:,ix] = exp(2*K[:,ix] - transpose(dot_a) - dot_b[ix]*unitvec)

        return K

    def get_kernel_matrix_slow(self, split='train'):
        """Implement the double for loop"""
        if split == 'train':
            m = self.train_ex.shape[1]
            K = zeros(m,m)
            for ix in range(m):
                for iy in range(ix,m):
                    K[ix,iy] = exp(-self.width * norm(self.train_ex[:,ix]
                                                      - self.train_ex[:,iy])**2)
                    K[iy,ix] = K[ix,iy]                    
        elif split == 'test':
            m = self.train_ex.shape[1]
            n = self.test_ex.shape[1]
            K = zeros(m,n)
            for ix in range(m):
                for iy in range(n):
                    K[ix,iy] = exp(-self.width * norm(self.train_ex[:,ix]
                                                      - self.test_ex[:,iy])**2)
        return K

class StudentKernel(Kernel):
    """Kernel based on the unnormalized Student T distribution.
    k(x,y) = (1+(||x-y||^2)/nu)^(-(nu+1)/2)
    """
    pass

class IntegratedGaussianKernel(Kernel):
    """Integral of the Gaussian kernel with respect to the width.
    k(x,y) = sqrt(2)*sigma*exp(-1/2*sigma^2 ||x-y||^2)
             - sqrt(pi)*||x-y||*erfc(-1/sqrt(2)*sigma ||x-y||)
    """
    pass

class GaussianCovKernel(Kernel):
    """Gaussian kernel with the full covariance function.
    k(x,y) = exp(- (x-y).T * Cov * (x-y))

    Consider also different shrinkage approaches.
    """

class MixGaussianKernel(Kernel):
    """A finite sum of Gaussian Kernels"""
    pass

class TaylorGaussianKernel(Kernel):
    """A infinite sum of Gaussian Kernels"""
    pass

