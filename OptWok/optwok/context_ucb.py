"""Contextual Gaussian Process Upper Confidence Bound"""
import os
import copy
from numpy import delete, arange, kron, unique, array
from numpy import argmax, argsort
from numpy import log, log2, log10, zeros, ones, matrix, eye, sqrt
from numpy import max, min, mean
from numpy import flatnonzero
from numpy.random import randint, permutation, randn
from matplotlib.pyplot import figure, plot, legend, title, show
from matplotlib.pyplot import xlabel, ylabel, axis, savefig
from optwok import datatools
from optwok.io_pickle import load, save

class BanditResults(object):
    """A container to collect the data for a
    contextual bandit experiment"""
    def __init__(self, verbose=False):
        """Does nothing except set verbosity"""
        self.verbose = verbose
        pass
    
    def init_data(self, data, num_context, num_epoch):
        """Make a copy of the data and initialize results"""
        self.data = copy.deepcopy(data)
        self.r_sum = zeros(num_context*(num_epoch-1)+1)
        self.r_sum[0] = data.max_labels[0] - data.min_label
        self.r_max = zeros((num_context, num_epoch))
        for context in range(num_context):
            self.r_max[context,0] = self.data.max_labels[context]
        self.selection = zeros((num_context, num_epoch))
        self.observed = zeros((num_context, num_epoch))
        self.num_expt = 0

    def do_experiment(self, context, epoch, pred_idx, pred_val=None):
        """Simulate an experiment"""
        y = self.data.observe(pred_idx, context)
        regret = self.data.max_labels[context] - y
        self.r_sum[self.num_expt+1] = self.r_sum[self.num_expt] + regret
        self.num_expt += 1
        self.r_max[context, epoch] = min([self.r_max[context, epoch-1], regret])
        self.selection[context, epoch] = pred_idx
        self.observed[context, epoch] = y
        if pred_val:
            if self.verbose:
                print('chose = %d, UCB = %f, observation = %f' % (pred_idx, pred_val, y))
            if pred_val < y:
                print('=== Observation %f higher than UCB %f!' % (y, pred_val))
        return y

    def save_result(self, filename):
        """Save the results to file with pickle"""
        save(filename, (self.r_sum, self.r_max, self.selection, self.observed))

    def load_result(self, filename):
        """Initialize results from file"""
        (self.r_sum, self.r_max, self.selection, self.observed) = load(filename)

def compute_upper_bound(preds, pred_var, epoch, num_data):
    """Compute the upper confidence bound"""
    beta = 2.0
    upper_bound = preds + beta*log(num_data*(epoch+1.0))*sqrt(pred_var)
    
    diff = max([(max(upper_bound) - min(upper_bound))/abs(mean(upper_bound)),0.01])
    # add some random noise
    upper_bound += 0.01*diff*randn(num_data)
    diff = (max(upper_bound) - min(upper_bound))/abs(mean(upper_bound))

    if diff < 1e-5:
        print('==== UCB very flat!')
        print('Context=%d, epoch=%d' % (context, epoch))
        print(max(upper_bound), min(upper_bound), mean(upper_bound), diff)

    return upper_bound

def ucb(examples, context, gp, epoch, verbose):
    """Upper confidence bound algorithm with context"""
    num_data = examples.shape[1]
    (preds, pred_var) = gp.predict((examples, context*ones(num_data)))
    upper_bound = compute_upper_bound(preds, pred_var, epoch, num_data)
    max_possible = argmax(upper_bound)    

    if verbose:
        print('Preds(max)=%f, pred_sd(max)=%f' % (preds[max_possible], sqrt(pred_var[max_possible])))
    #plot(examples.flatten(), preds, 'b.')
    #plot(examples.flatten(), upper_bound, 'r.')
    #plot(examples[:,max_possible], preds[max_possible], 'go')
    #show()

    return max_possible, upper_bound[max_possible]

def ucb_kbest(examples, context, gp, epoch, batch_size, verbose):
    """Predict the k best experiments from the GP"""
    num_data = examples.shape[1]
    (preds, pred_var) = gp.predict((examples, context*ones(num_data)))
    upper_bound = compute_upper_bound(preds, pred_var, epoch, num_data)
    idx = argsort(upper_bound)
    kbest = idx[-batch_size:]

    return kbest

def ucb_batch(examples, context, gp, epoch, batch_size, verbose):
    """Predict small batches, using ucb, of examples to test"""
    local_gp = copy.deepcopy(gp)
    local_gp.reg_param /= float(batch_size)
    all_pred_idx = []
    for batch_idx in range(batch_size):
        (pred_idx, pred_val) = ucb(examples, context, local_gp,
                                   epoch, verbose)
        all_pred_idx.append(pred_idx)
        local_gp.update(examples[:,pred_idx], context, pred_val)
    return all_pred_idx

def expt_cucb(data, gp, savefile, num_context=3, num_epoch=15, verbose=False):
    """Estimate the learning curves for ucb"""
    print(savefile)
    results = BanditResults()
    results.init_data(data, num_context, num_epoch)
    for context in range(num_context):
        for epoch in range(1,num_epoch):
            if verbose:
                print('Context=%d, epoch=%d' % (context, epoch))
            (pred_idx, pred_val) = ucb(data.examples(context), context, gp, epoch, verbose)
            y = results.do_experiment(context, epoch, pred_idx, pred_val)
            gp.update(data.examples(context)[:,pred_idx], context, y)
    results.save_result(savefile)

def expt_cucb_batch(data, gp, savefile, num_context=3, num_epoch=15,
                    batch_size=5, mode='greedy', verbose=False):
    """Estimate the learning curves for batch ucb"""
    print(savefile)
    results = BanditResults()
    results.init_data(data, num_context, (num_epoch-1)*batch_size+1)
    for context in range(num_context):
        for epoch in range(1,num_epoch):
            if verbose:
                print('Context=%d, epoch=%d' % (context, epoch))
            if mode == 'greedy':
                all_pred_idx = ucb_batch(data.examples(context),
                                         context, gp, epoch*batch_size, batch_size, verbose)
            elif mode == 'kbest':
                all_pred_idx = ucb_kbest(data.examples(context),
                                         context, gp, epoch*batch_size, batch_size, verbose)
            elif mode == 'random':
                idx = permutation(len(data.examples(context)))
                all_pred_idx = idx[:batch_size]
            for pred_idx in all_pred_idx:
                y = results.do_experiment(context, epoch, pred_idx)
                gp.update(data.examples(context)[:,pred_idx], context, y)
    results.save_result(savefile)

def plot_min_regret(filename, leg_list=None):
    print(filename)
    (r_sum, r_max, selection, observed) = load(filename)
    (num_context, num_epoch) = r_max.shape
    if leg_list is None:
        leg_list = map(str,range(num_context))
    for ix, task in enumerate(leg_list):
        pos = flatnonzero(observed[ix,:]>0)
        print('%s:%d unique positives from %d' % (task, len(unique(selection[ix,pos])), num_epoch))

    figure()
    plot(range(0,num_epoch),r_max.T, hold=True)
    #plot(range(0,num_epoch),r_max.T, 'k.')
    plot([0,num_epoch],[0,0], 'k--')
    legend(leg_list, loc=4)
    title('minimum regret')
    xlabel('number of rounds')
    #axis([0,num_epoch-1,-3,3])
    savefig('%s_min.pdf' % filename[:-4], dpi=300, bbox_inches='tight')

    
def plot_average_regret(file_list, leg_list):
    """Plot all average regrets,
    assuming that the file and legend order is the same.
    """
    colours = ['b','r','g','c','m','k','y']
    assert len(file_list) == len(leg_list)
    figure()
    for (fidx, file_name) in enumerate(file_list):
        (r_sum, r_max, selection, observed) = load(file_name)
        (num_context, num_epoch) = r_max.shape
        plot(arange(1,len(r_sum)+1), r_sum/(arange(1,len(r_sum)+1)), colours[fidx])
    #for ix in range(1,num_context):
    #    plot([(num_epoch-1)*ix,(num_epoch-1)*ix], [0,5], 'k:')
    legend(leg_list, loc=1)
    title('average regret')
    xlabel('number of rounds')
    #axis([1,num_context*(num_epoch-1), 1, 4])
    
    savefig('%s_avg.pdf' % file_list[0][:-4], dpi=300, bbox_inches='tight')


def plot_average_regret_old(f_learn, f_cat, f_ind):
    figure()
    (r_sum, r_max, selection, observed) = load(f_learn)
    (num_context, num_epoch) = r_max.shape
    plot(arange(1,len(r_sum)+1), r_sum/(arange(1,len(r_sum)+1)), 'b')
    (r_sum, r_max, selection, observed) = load(f_cat)
    plot(arange(1,len(r_sum)+1), r_sum/(arange(1,len(r_sum)+1)), 'r')
    (r_sum, r_max, selection, observed) = load(f_ind)
    plot(arange(1,len(r_sum)+1), r_sum/(arange(1,len(r_sum)+1)), 'g')
    #for ix in range(1,num_context):
    #    plot([(num_epoch-1)*ix,(num_epoch-1)*ix], [0,5], 'k:')
    legend(['CGP-UCB','merge','ignore'], loc=1)
    title('average regret')
    xlabel('number of rounds')
    #axis([1,num_context*(num_epoch-1), 1, 4])
    
    savefig('%s_avg.pdf' % f_learn[:-4], dpi=300, bbox_inches='tight')


