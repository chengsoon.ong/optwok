"""Class for second order cone programming"""

from __future__ import print_function
from numpy.core.numeric import newaxis
from optwok.sip import SIP

import cvxopt
import cvxopt.solvers
from cvxopt import matrix as cm, sparse as cs

from numpy import matrix, array, dot, vstack, hstack, kron
from numpy import zeros, ones, mean, std, sign, eye, nonzero

have_mosek = True
import sys
try:
    import mosek
    print('using mosek', end='\n', file=sys.stderr)
except:
    print('not using mosek', end='\n', file=sys.stderr)
    have_mosek = False


class SOCP(SIP):
    ''' A wrapper to cvxopt SOCP solver, converting the boyd notation into cvxopt matrices
    
    boyd + component wise constraints:
    
        min_x   f'x
        st      ||A_i x + b_i|| <= c_i'x + d_i,    i = 1 ... m
                0 <= b_0 - A_0 x
                Fx = g
                
    cvxopt:
        
        min_x   c'x
        st      G_i x + s_i = h_i,    i = 0 ... M
                Ax = b
                s_0 >= 0
                s_i0 >= ||s_i1||    i = 1 ... M
    
    '''

    def __init__(self, num_vars, verbose = False, use_mosek=False):
        SIP.__init__(self, num_vars, verbose, use_mosek=use_mosek)
        self.G0 = None
        self.h0 = None
        self.G1 = []
        self.h1 = []

    def set_objective(self, f):
        """Set f"""
        assert f.shape[0] == self.num_vars, 'Size mismatch in linear objective term'
        self.c = cm(f)

    def set_component_ineq_constraints(self, A_0, b_0):
        ''' Set the non-cone constraints
            0 <= b_0 - A_0 x '''
        assert A_0.shape == (b_0.shape[0], self.num_vars), 'Size mismatch in inequality constraint (lhs)'
        assert b_0.shape[0] == self.num_vars
        
        self.G0 = cm(A_0)
        self.h0 = cm(b_0)

    def add_ineq_constraints(self, A_i, b_i, c_i, d_i):
        """Add the new inequality constraints to the list"""
        assert c_i.shape[0] == self.num_vars, 'Size mismatch in inequality constraint (rhs)'
        assert A_i.shape == (b_i.shape[0], self.num_vars), 'Size mismatch in inequality constraint (lhs)'

        G = cm(vstack((-c_i.T, -A_i)))
        h = cm(vstack((d_i, b_i[:, newaxis])))

        self.G1.append(G)
        self.h1.append(h)


    def solve_cvxopt(self):
        """Check for empty constraints then invoke solver"""
        if have_mosek and self.use_mosek:
            if not self.verbose:
                cvxopt.solvers.options['MOSEK'] = {mosek.iparam.log:0}
            res = cvxopt.solvers.socp(self.c,
                                    self.G0, self.h0, self.G1, self.h1, solver = 'mosek')
        else:
            if not self.verbose:
                cvxopt.solvers.options['show_progress'] = False
            res = cvxopt.solvers.socp(self.c, self.G0, self.h0, self.G1, self.h1)
        return res
    
class SOCPS(SOCP):
    ''' SOCP for sparse constraints
        converts matrices/numpy arrays into sparse matrices'''
    def set_component_ineq_constraints(self, A_0, b_0):
        ''' Set the non-cone constraints
            0 <= b_0 - A_0 x '''
        assert A_0.shape == (b_0.shape[0], self.num_vars), 'Size mismatch in inequality constraint (lhs)'
        assert b_0.shape[0] == self.num_vars
        
        self.G0 = cs(cm(A_0))
        self.h0 = cm(b_0)

    def add_ineq_constraints(self, A_i, b_i, c_i, d_i):
        """Add the new inequality constraints to the list"""
        assert c_i.shape[0] == self.num_vars, 'Size mismatch in inequality constraint (rhs)'
        assert A_i.shape == (b_i.shape[0], self.num_vars), 'Size mismatch in inequality constraint (lhs)'

        G = cs(cm(vstack((-c_i.T, -A_i))))
        h = cm(vstack((d_i, b_i[:, newaxis])))


        self.G1.append(G)
        self.h1.append(h)
