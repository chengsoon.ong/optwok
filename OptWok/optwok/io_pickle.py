"""A Matlab like interface for loading and saving objects.

Has an additional benefit of collecting all instances of pickle code here."""

import sys
import bz2
import subprocess
import os

python_version = sys.version_info[:2]
if python_version >= (3, 0):
    from pickle import load as pload
    from pickle import dump as pdump
else:
    from cPickle import load as pload
    from cPickle import dump as pdump

def save(filename, myobj):
    """save the myobj to filename using pickle"""
    try:
        f = bz2.BZ2File(filename, 'wb')
    except IOError:
        sys.stderr.write('Trying local path...')
        try:
            f = bz2.BZ2File(os.path.join(os.getcwd(), filename))
        except IOError:
            sys.stderr.write('File ' + filename + ' cannot be written\n')
            return

    pdump(myobj, f, protocol = 2)
    f.close()


def load(filename, raise_error=False):
    """load the myobj from filename using pickle"""
    try:
        f = bz2.BZ2File(filename, 'rb')
    except IOError:
        if raise_error:
            raise
        else:
            sys.stderr.write('File ' + filename + ' cannot be read\n')
            return

    try:
        myobj = pload(f)
    except UnicodeDecodeError as e:
        f.seek(0)
        myobj = pload(f, encoding='latin1')
    except Exception as e:
        print('Unable to load data ', pickle_file, ':', e)
        raise
    f.close()
    return myobj

def convert_v6(input_filename, output_filename):
   """ starts matlab and converts a structure to a matlab v6 structure """

   cmd = "matlab -nojvm -nodisplay -r \"load %s; save -v6 '%s'; exit\"" % (input_filename, output_filename)
   obj = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
   out, err = obj.communicate()
   assert err == '', 'An error occured!\n%s' % err
