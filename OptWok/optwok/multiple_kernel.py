"""Implement a direct Nonsmooth Newton approach for solving multiple kernel learning.

TODO: Update to getter and setter
"""

from numpy import zeros, ones, eye
from numpy import dot, diag
from numpy.random import rand
from numpy.linalg import norm, lstsq

class MKLNewton(object):
    """Multiple kernel learning binary classifier.
    Training using nonsmooth Newton method.

    z = (alpha, beta, xi, lamb, mu, eta)

    kernels are stored as a 3 dimensional array,
    with (p,i,j) being the entry for kernel p, example (i,j).
    """

    def __init__(self, reg_param=1.0, smooth_param=0.2, newton_tol=1e-6):
        """
        C = reg_param
        epsilon = smooth_param
        tol = newton_tol
        """
        self.reg_param = reg_param
        self.smooth_param = smooth_param
        self.newton_tol = newton_tol

    @property
    def num_ex(self):
        """The number of examples in the training data"""
        return len(self.labels)

    @property
    def num_kernel(self):
        """The number of kernels"""
        return self.kernels.shape[0]
        
    @property
    def num_var(self):
        """The total number of variables in the optimisation"""
        return 2*(self.num_ex+self.num_kernel+1)

    @property
    def idx_alpha(self):
        return slice(0, self.num_ex)

    @property
    def idx_beta(self):
        return slice(self.num_ex, self.num_ex+self.num_kernel)

    @property
    def idx_xi(self):
        return self.num_ex+self.num_kernel

    @property
    def idx_lamb(self):
        return slice(self.num_ex+self.num_kernel+1, self.num_ex+2*self.num_kernel+1)

    @property
    def idx_mu(self):
        return slice(self.num_ex+2*self.num_kernel+1, 2*self.num_ex+2*self.num_kernel+1)

    @property
    def idx_eta(self):
        return 2*self.num_ex+2*self.num_kernel+1
        
    def get_var(self):
        """Get the value of z by concatenating variables appropriately"""
        z = zeros(self.num_var)
        z[self.idx_alpha] = self.alpha
        z[self.idx_beta] = self.beta
        z[self.idx_xi] = self.xi
        z[self.idx_lamb] = self.lamb
        z[self.idx_mu] = self.mu
        z[self.idx_eta] = self.eta
        return z
    
    def set_var(self, z):
        """Set the variables to the value in z"""
        self.alpha = z[self.idx_alpha]
        self.beta = z[self.idx_beta]
        self.xi = z[self.idx_xi]
        self.lamb = z[self.idx_lamb]
        self.mu = z[self.idx_mu]
        self.eta = z[self.idx_eta]
        
    def add_var(self, z):
        """Add z to the variables"""
        self.alpha += z[self.idx_alpha]
        self.beta += z[self.idx_beta]
        self.xi += z[self.idx_xi]
        self.lamb += z[self.idx_lamb]
        self.mu += z[self.idx_mu]
        self.eta += z[self.idx_eta]

    def init_var(self):
        """Initialise to a feasible value"""
        self.alpha = 0.5*self.reg_vec
        self.beta = zeros(self.num_kernel)
        self.beta[0] = 0.99
        self.xi = 0.1
        self.lamb = 0.1*ones(self.num_kernel)
        self.mu = zeros(self.num_ex)
        self.eta = dot(self.labels, self.alpha)
        self.z_old = zeros(self.num_var)
        
    def active_sets(self):
        """Compute the vectors identifying the active sets
        chi1: betas sum to one
        chi2: betas positive
        chi3: alphas less than reg_param
        chi4: alphas positive
        """
        c = 0.00001*ones(self.num_kernel+1)
        d = 0.00001*ones(self.num_ex)

        if self.xi - c[0]*(1.-sum(self.beta)) > 0:
            chi1 = 1.
        else:
            chi1 = 0.

        chi2 = zeros(self.num_kernel)
        cond = self.lamb - c[1:]*self.beta > 0
        chi2[cond] = 1.

        chi3 = zeros(self.num_ex)
        cond = self.mu + d*(self.alpha-self.reg_vec) > 0
        chi3[cond] = 1.

        chi4 = zeros(self.num_ex)
        cond = self.mu + d*self.alpha < 0
        chi4[cond] = 1.

        return (chi1,chi2,chi3,chi4)

    def newton_update(self, ac_set):
        """Solve the linear system to find the Newton step"""
        chi1, chi2, chi3, chi4 = ac_set
        lhs = zeros((self.num_var, self.num_var))
        K_opt = zeros((self.num_ex, self.num_ex))
        for p in range(self.num_kernel):
            K_opt += self.beta[p]*self.kernels[p,:,:]
        lhs[self.idx_alpha, self.idx_alpha] = self.smooth_param*eye(self.num_ex) + 2.*K_opt
        Kalpha = zeros((self.num_ex, self.num_kernel))
        for p in range(self.num_kernel):
            Kalpha[:,p] = dot(self.kernels[p,:,:], self.alpha)
        lhs[self.idx_alpha, self.idx_beta] = 2.*Kalpha
        lhs[self.idx_beta, self.idx_alpha] = 2.*Kalpha.T
        lhs[self.idx_alpha, self.idx_mu] = -eye(self.num_ex)
        lhs[self.idx_alpha, self.idx_eta] = self.labels
        lhs[self.idx_eta, self.idx_alpha] = self.labels.T
        lhs[self.idx_beta, self.idx_xi] = -ones(self.num_kernel)
        lhs[self.idx_beta, self.idx_lamb] = -eye(self.num_kernel)
        lhs[self.idx_xi, self.idx_beta] = chi1
        lhs[self.idx_xi, self.idx_xi] = 1.-chi1
        lhs[self.idx_lamb, self.idx_beta] = diag(chi2)
        lhs[self.idx_lamb, self.idx_lamb] = diag(1.-chi2)
        lhs[self.idx_mu, self.idx_alpha] = diag(chi3+chi4)
        lhs[self.idx_mu, self.idx_mu] = diag(1.-chi3-chi4)

        rhs = zeros(self.num_var)
        rhs[self.idx_alpha] = 1
        rhs[self.idx_xi] = chi1
        rhs[self.idx_mu] = chi3*self.reg_vec

        (zopt, residuals, rank, sing) = lstsq(lhs, rhs)
        return zopt

    def optimal(self, z_diff):
        """Compute the vector measuring the optimality conditions
        ||z_{new} - z_{now}|| / ||z_{now} - z_{old}|| < tol
        """
        numerator = norm(z_diff)
        denominator = norm(self.get_var() - self.z_old)
        return numerator/denominator < self.newton_tol

    def optimize(self, max_iter=100):
        """Find solution using nonsmooth Newton"""
        self.init_var()
        is_opt = False
        idx = 0
        while not is_opt and idx < max_iter:
            ac_set = self.active_sets()
            z = self.newton_update(ac_set)
            is_opt = self.optimal(z)
            self.z_old = self.get_var()
            self.add_var(z)
            print(self.beta)
            print(self.xi, self.lamb)
            idx += 1
        
    def train(self, kernels, labels):
        """Train an SVM with MKL on examples and labels"""
        self.kernels = kernels
        self.labels = labels
        self.reg_vec = self.reg_param*ones(self.num_ex)
        self.optimize()
        
    def predict(self, kernels):
        """Predict the output for the given kernels"""
        K_opt = zeros(kernels[0,:,:].shape)
        for p in range(self.num_kernel):
            K_opt += self.beta[p]*kernels[p,:,:]
        pred = dot(K_opt, self.alpha)
        return pred

if __name__ == '__main__':
    from optwok.simdata import cloudgen
    from optwok.kernel_pp import GaussKernel
    from optwok.testtools import compare_pred

    #from matplotlib.pyplot import figure, pcolor, show, colorbar
    
    num_ex = 300
    num_kernel = 2
    meta, data, label = cloudgen(num_ex,2,0,0.5,0.5)
    k1 = GaussKernel(10.0)
    k1.compute(data)
    k2 = GaussKernel(0.1)
    k2.compute(data)
    kernels = zeros((2,num_ex,num_ex))
    kernels[0,:,:] = k1.kmat
    kernels[1,:,:] = k2.kmat
    #figure()
    #pcolor(k1.kmat)
    #colorbar()
    #figure()
    #pcolor(k2.kmat)
    #colorbar()
    #show()

    nnmkl = MKLNewton()
    nnmkl.train(kernels, label)
    pred = nnmkl.predict(kernels)
    compare_pred(pred, label, show='rand', num_show=5)
    
    
