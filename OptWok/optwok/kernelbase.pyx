# cython: profile=True

"""Implement a Cython interface."""


from numpy import array
import numpy as np
cimport numpy as np
np.import_array()
cimport cython


DTYPE = np.float
ctypedef np.float_t DTYPE_t


@cython.boundscheck(False) # turn off bounds-checking for entire function
cdef getitem2(np.ndarray[DTYPE_t, ndim=2, negative_indices=False] kmat, Py_ssize_t xi, Py_ssize_t xj):
    """Fast version of __getitem__ with 2 arguments"""
    return <float> kmat[xi, xj]

@cython.boundscheck(False)
cdef getitem4(np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Kx,
              np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Ky,
              Py_ssize_t xi, Py_ssize_t xj, Py_ssize_t yi, Py_ssize_t yj):
    """Fast version of __getitem__ with 4 arguments"""
    return <float> Kx[xi, xj]*Ky[yi, yj]

@cython.boundscheck(False)
cdef DTYPE_t getprod(np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Kx,
                     np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Ky,
                     np.ndarray[Py_ssize_t, ndim=1, negative_indices=False] context,
                     Py_ssize_t ix, Py_ssize_t iy):
    """product of input and output kernels"""
    cdef DTYPE_t val = Kx[ix,iy]*Ky[context[ix], context[iy]]
    return val

@cython.boundscheck(False)
cdef DTYPE_t getprod_cross(np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Kx,
                           np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Ky,
                           np.ndarray[Py_ssize_t, ndim=1, negative_indices=False] context,
                           np.ndarray[Py_ssize_t, ndim=1, negative_indices=False] context_cross,
                           Py_ssize_t ix, Py_ssize_t iy):
    """product of input and output kernels"""
    cdef DTYPE_t val = Kx[ix,iy]*Ky[context[ix], context_cross[iy]]
    return val

class Kernel(object):
    """A Mercer kernel, with Cython interface for getitem"""
    def __init__(self):
        self.kmat = array([], dtype=DTYPE)
        self.examples = None
        
    def __getitem__(self, idx):
        assert(len(idx)==2)
        (xi,xj) = idx
        return getitem2(self.kmat, xi, xj)

    def __repr__(self):
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def dump(self) :
        """
        returns a string that can be used to construct an equivalent object
        """
        kstr = self.__module__ + '.' + self.__class__.__name__ + '(' + \
               self.construction_params() + ')'
        return kstr

    def construction_params(self):
        raise NotImplementedError

    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        raise NotImplementedError

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        raise NotImplementedError


class JointKernel(object):
    """A kernel on input and output pairs."""
    def __init__(self, input_kernel, output_kmat=None, num_classes=-1):
        self.Kx = input_kernel
        if output_kmat is not None:
            self.Ky = output_kmat
        else:
            assert(num_classes > 1)
            self.Ky = np.eye(num_classes)
        self.examples = None

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def __getitem__(self, idx):
        """Simulate matrix element.
        Order is assumed to be examples first then data.
        """
        assert(len(idx)==4)
        (xi, xj, yi, yj) = idx
        return getitem4(self.Kx.kmat, self.Ky, xi, xj, yi, yj)
    
    @property
    def kmat(self):
        """Simulate the whole kernel matrix"""
        return np.kron(self.Kx.kmat, self.Ky)

    def compute(self, examples):
        self.examples = examples
        self.Kx.compute(examples)

    def compute_test(self, test_ex):
        """Returns a non-square matrix, of num_train by num_test size."""
        Kx = self.Kx.compute_test(test_ex)
        return np.kron(Kx, self.Ky)

class ContextKernel(Kernel):
    """A kernel of (data, context) pairs,
    mathematically the same as the JointKernel,
    but does not evaluate all combinations"""
    def __init__(self, input_kernel, output_kmat):
        self.Kx = input_kernel
        self.num_classes = output_kmat.shape[0]
        self.Ky = output_kmat
        self.examples = None
        self.num_examples = 0
        self.context = None

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def __getitem__(self, idx):
        """Simulate matrix element.
        Order is assumed to be (example,context,example,context)
        """
        assert(len(idx)==4)
        (xi, yi, xj, yj) = idx
        return getitem4(self.Kx.kmat, self.Ky, xi, xj, yi, yj)

    @property
    def kmat(self):
        """Simulate the whole kernel matrix"""
        cdef Py_ssize_t ix
        cdef Py_ssize_t iy
        cdef DTYPE_t val
        cdef np.ndarray K = np.zeros((self.num_examples, self.num_examples), dtype=DTYPE)
        for ix in range(self.num_examples):
            for iy in range(ix, self.num_examples):
                #val = self.Kx[ix,iy]*self.Ky[self.context[ix], self.context[iy]]
                val = getprod(self.Kx.kmat, self.Ky, self.context, ix, iy)
                K[ix,iy] = val
                K[iy,ix] = val
        return K

    def compute(self, examples, context):
        self.examples = examples
        self.context = context
        self.num_examples = examples.shape[1]
        self.Kx.compute(examples)

    def compute_test(self, examples, context):
        """Returns a non-square matrix, of num_train by num_test size."""
        assert self.examples is not None
        assert self.context is not None
        cdef Py_ssize_t num_test = examples.shape[1]
        cdef np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Kx_cross = self.Kx.compute_test(examples)
        cdef np.ndarray[DTYPE_t, ndim=2, negative_indices=False] Kmat_cross = np.empty((self.num_examples, num_test),dtype=DTYPE)
        cdef Py_ssize_t ix
        cdef Py_ssize_t iy
        for ix in range(self.num_examples):
            for iy in range(num_test):
                Kmat_cross[ix,iy] = Kx_cross[ix,iy]*self.Ky[self.context[ix], context[iy]]
                #Kmat_cross[ix,iy] = getprod_cross(Kx_cross, self.Ky, self.context, context, ix, iy)
        return Kmat_cross


class LinearKernel(Kernel):
    """Standard dot product"""
    def __init__(self):
        Kernel.__init__(self)
        
    def construction_params(self):
        return ''
    
    def compute(self, np.ndarray[DTYPE_t, ndim=2] examples):
        """Evaluate the kernel function and store the value"""
        self.examples = examples
        self.kmat = np.dot(examples.T, examples)

    def compute_test(self, np.ndarray[DTYPE_t, ndim=2] test_ex):
        """Evaluate the kernel function on test examples"""
        cdef np.ndarray[DTYPE_t, ndim=2, negative_indices=False] k_cross = np.dot(self.examples.T, test_ex)
        return k_cross
    
class GaussKernel(Kernel):
    """Gaussian rbf kernel"""
    def __init__(self, gamma=1.0):
        Kernel.__init__(self)
        self.gamma = gamma

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>\n'
        rep += 'gamma : ' + str(self.gamma)
        return rep

    def constructionParams(self) :
        return 'gamma = ' + str(self.gamma)

    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        self.examples = examples
        self.kmat = np.exp(-self.gamma * self.sqr_dist(examples, examples))

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        if test_ex.ndim == 1:
            test_ex.shape = (len(test_ex),1)
        return np.exp(-self.gamma * self.sqr_dist(self.examples, test_ex))
    
    def sqr_dist(a,b):
        """Compute the square distance between vectors"""
        assert(a.shape[0] == b.shape[0])
        if len(a.shape)>1:
            # a is a matrix
            dot_a = np.sum(a*a, axis=0).T
            dot_b = np.sum(b*b ,axis=0).T
            unitvec = np.ones(dot_a.shape)
            D = np.array(2.0*np.matrix(a).T*np.matrix(b))
            for ix,bval in enumerate(dot_b):
                D[:,ix] = dot_a - D[:,ix] + np.kron(bval,unitvec)
        else:
            D = np.zeros((len(a),len(b)))
            for (ixa, cur_a) in enumerate(a):
                for (ixb, cur_b) in enumerate(b):
                    D[ixa,ixb] = (cur_a-cur_b)*(cur_a-cur_b)

        return D


class CustomKernel(Kernel):
    """Just a positive semidefinite matrix"""
    def __init__(self):
        Kernel.__init__(self)

    def compute(self, examples):
        """Assume input is a kernel matrix.
        Copy the training kernel matrix"""
        self.kmat = examples

    def compute_test(self, test_ex):
        """Assume input is a kernel matrix.
        Just return the test kernel"""
        return test_ex

