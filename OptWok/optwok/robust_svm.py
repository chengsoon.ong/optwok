'''
Created on 21.02.2013

@author: Gabriel
'''
from optwok.socp import SOCPS
from numpy import r_, zeros, sqrt, ones, eye, diag, sign, array, dot, squeeze
from optwok.mldata import ClassificationData
try:
    from sklearn.svm import SVC
except ImportError:
    print('sklearn missing')


class RobustData(ClassificationData):
    ''' Dataset that has a covariance matrix associated with every data point '''
    
    def __init__(self, examples, covariances, labels):
        self.examples = examples
        self.covariances = covariances
        self.labels = labels
    
    def split_off(self, idx, id_str):
        rbd = RobustData(self.examples[idx], self.covariances[idx], self.labels[idx])
        rbd.id_str = id_str
        rbd.idx = idx
        
        return rbd
    
    def ith_data(self, i):
        return {'example': self.examples[i], 'covariance': self.covariances[i], 'label':self.labels[i]}
    
    def iter(self):                     # TODO: this should be in common MIData ancestor
        for i in range(self.num_ex):
            yield self.ith_data(i)
    
    @property
    def num_feat(self):
        return self.examples[0].shape[0]
    


class VanillaSVM():
    def __init__(self, C, verbose=False):
        self.verbose = verbose
        self.C = C
    
    def one_predict(self, data):
        ''' predict one datapoint '''
        return dot(self.w,data['example']) + self.b
    
    def predict(self, testset):         # Could also be in common MIClassifier ancestor
        ''' implement classifier '''
        y = []
        for pd in testset.iter():
            yi = self.one_predict(pd)
            y.append(yi)
        
        return sign(array(y))
    
    def train(self, trainset):
        self.train_ex = trainset.examples
        self.train_lab = trainset.labels
        
        self.num_feat = trainset.num_feat
        self.num_ex = trainset.num_ex
        
        self.solve_opt()
        
    def solve_opt(self): 
        svm = SVC(C = self.C, kernel='linear')
        svm.fit(self.train_ex, self.train_lab)
        self.w, self.b, self.primal_obj = squeeze(svm.coef_), svm.intercept_[0], None

class RobustSVM(VanillaSVM):
    def train(self, trainset):
        self.train_ex = trainset.examples
        self.train_cov = trainset.covariances
        self.train_lab = trainset.labels
        
        self.num_feat = trainset.num_feat
        self.num_ex = trainset.num_ex
        
        self.solve_opt()

    def solve_opt(self):
        '''
            x = [w, b, xi, theta],
        '''
    
        ndim = self.num_feat
        nobj = self.num_ex
    
        num_vars = ndim + 1 + nobj + 1
        socp = SOCPS(num_vars= num_vars, verbose=self.verbose, use_mosek=True)
        
        f = r_[zeros(num_vars-1),1]     # only theta
        
        socp.set_objective(f)
        
        
        # Constraints for original objective + regularization 
        # (see http://www-personal.umich.edu/~mepelman/teaching/IOE511/Handouts/511notes07-16.pdf for QCQP -> LPQP,
        #  http://retis.sssup.it/~bini/teaching/optim2010/cone_programming.pdf for LPQP -> SOCP)
        c = r_[zeros(ndim+1),-ones(nobj)*self.C, 1]/2
        A = zeros((ndim+1,num_vars))
        #A[:ndim,:ndim] = eye(ndim,ndim) * sqrt(1/2)
        A[0,:] = -c
        A[1:,:ndim] = diag(ones(ndim) * sqrt(1/2))
        d = 1/2
        b = zeros(ndim+1)
        b[0] = 1/2
        socp.add_ineq_constraints(A, b, c, d)
        
        for i, e in enumerate(zip(self.train_ex, self.train_lab, self.train_cov)):
            qi, yi, Pi = e               
            A_i = zeros((ndim, num_vars))
            A_i[:,:ndim] = Pi
            b_i = zeros(ndim)
            c_i = r_[yi*qi,yi,zeros(nobj+1)]
            c_i[ndim+1+i] = 1    # xi
            d_i = -1
            socp.add_ineq_constraints(A_i, b_i, c_i, d_i)
            
        # Constraint for positive slack
        A_0 = zeros((num_vars, num_vars))
        A_0[ndim+1:-1,ndim+1:-1] = -1*eye(nobj)
        socp.set_component_ineq_constraints(A_0, zeros(num_vars))
         
        socp.solve(warn=self.verbose)
    
        self.w = socp.solution[:ndim]
        self.b = socp.solution[ndim+1]
        self.primal_obj = socp.cur_obj