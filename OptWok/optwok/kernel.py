"""Implement some positive semidefinite kernels."""

from numpy import array, matrix
from numpy import zeros, ones, eye, diag
from numpy import average, exp, kron, sum
from numpy import sqrt, divide, mean, multiply

#try:
#    from optwok.kernelbase import Kernel, JointKernel, ContextKernel
#except ImportError:
#    from optwok.kernel_pp import Kernel, JointKernel, ContextKernel
from optwok.kernel_pp import *

#####################################################################
# Some miscellaneaous functions for kernel matrix operations
#####################################################################

def sqr_dist(a,b):
    """Compute the square distance between vectors"""
    assert(a.shape[0] == b.shape[0])
    if len(a.shape)>1:
        # a is a matrix
        dot_a = sum(a*a, axis=0).T
        dot_b = sum(b*b ,axis=0).T
        unitvec = ones(dot_a.shape)
        D = array(2.0*matrix(a).T*matrix(b))
        for ix,bval in enumerate(dot_b):
            D[:,ix] = dot_a - D[:,ix] + kron(bval,unitvec)
    else:
        D = zeros((len(a),len(b)))
        for (ixa, cur_a) in enumerate(a):
            for (ixb, cur_b) in enumerate(b):
                D[ixa,ixb] = (cur_a-cur_b)*(cur_a-cur_b)

    return D

def center(K):
    """Center the kernel matrix, such that the mean (in feature space) is zero."""
    one_mat = matrix(ones(K.shape))
    one_vec = matrix(ones((K.shape[0],1)))

    row_sum = matrix(mean(K,axis=0)).T
    R = K - row_sum * one_vec.T - one_vec * row_sum.T +\
        mean(row_sum.A)*one_mat
    return R

def normalize_variance(K):
    """Normalize the kernel matrix, such that the variance (in feature space) is 1"""
    one_vec = matrix(ones((K.shape[0],1)))
    inv_sqrt_diag = divide(one_vec, matrix(sqrt(diag(K))).T)
    KN = multiply(kron(one_vec.T,inv_sqrt_diag),K)
    KN = multiply(kron(one_vec,inv_sqrt_diag.T),K)
    return KN

def normalize_unit_diag(K):
    """Normalize the kernel matrix, such that all diagonal entries are 1."""
    Kii = diag(K)
    Kii.shape = (len(Kii),1)
    return divide(K, sqrt(matrix(Kii)*matrix(Kii).T))

def kernel2distance(K):
    """Convert the kernel matrix into the corresponding Euclidean distance."""
    # TODO: Vectorize
    D = zeros(K.shape)
    for ix in range(K.shape[0]):
        for iy in range(K.shape[1]):
            sqr_dist = K[ix,ix] + K[iy,iy] - 2*K[ix,iy]
            if sqr_dist > 0.0:
                D[ix,iy] = sqrt(sqr_dist)
    return D

