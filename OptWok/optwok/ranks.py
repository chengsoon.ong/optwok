"""Tools for dealing with ranks and scores"""

import numpy
from numpy import array, zeros
from numpy import argsort, arange
from numpy import unique

def infer_ranks(scores):
    """impute the ranks of the scores, taking care of ties."""
    num_ex = len(scores)
    unique_scores = unique(scores)
    sc = scores.copy()
    sc = sc[::-1]
    left = num_ex - sc.searchsorted(unique_scores, side='right')
    right = num_ex - sc.searchsorted(unique_scores)
    ranks = zeros(num_ex)
    for idx in range(len(unique_scores)):
        cur_rank = 0.5*(left[idx]+right[idx]+1)
        ranks[left[idx]:right[idx]] = cur_rank
    return ranks

def rank2score(ranks, uniform=False):
    """Convert an array of normalised ranks into an array of scores,
    such that the highest rank (smallest number) has the highest score."""
    if uniform:
        idx_sort = argsort(ranks)[::-1]
        unsort = argsort(idx_sort)
        scores = arange(len(ranks))[unsort]
    else:
        scores = 1.-ranks
    return scores

def score2rank(orig_scores):
    """Convert an array of scores into an array of normalised ranks,
    such that the highest score has the highest rank (1/num_ex)."""
    scores = orig_scores.copy()
    idx_sort = argsort(scores)[::-1]
    unsort = argsort(idx_sort)
    scores = scores[idx_sort]

    ranks = infer_ranks(scores)
    
    assert(len(ranks) == len(scores))
    ranks = ranks/(len(scores)+1.0)
    ranks = ranks[unsort]
    return ranks



