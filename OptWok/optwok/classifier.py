"""Implementation of the optimization methods for the
empirical loss + regularizer
framework, for binary clssification.

The algorithms are primal variable algorithms.

We consider the following losses:
- hinge loss
- ramp loss

We consider the following regularizers:
- l2
- l1
- capped l1
- l0 (approximated by exponent)
"""

from time import time
import numpy
from numpy.random import randn
from numpy import matrix, array, vstack, hstack
from numpy import zeros, ones, transpose, nonzero
from numpy import sign, eye, where, dot, diag
from numpy.linalg import norm
from numpy import newaxis, outer, inner, mean, dot

from optwok.sip import SILP, SILPS, SIQP
from optwok.poly_feature import get_poly_features
from optwok.kernel import KernelFactory
from optwok.classifier_base import Classifier, ConvexClassifier
from optwok.classifier_base import Loss, HingeLoss, HingeLossKernel
from optwok.classifier_base import HuberHingeLoss, RampLoss, SlantLoss
from optwok.classifier_base import Regularizer
from optwok.classifier_base import Reg_l2, Reg_l1, Reg_l1cap, Reg_l0
from optwok.classifier_base import Reg_elastic_net, Reg_graph_net

from cvxopt import sparse
from cvxopt import matrix as cm

class SVMPrimal(ConvexClassifier):
    """
    Standard l2 regularized SVM, in the primal
    """
    def __init__(self, reg_param, kdict, verbose=False):
        ConvexClassifier.__init__(self, reg_param, kdict, verbose)
        self.loss = HingeLoss()
        self.regularizer = Reg_l2()

    def solve_opt(self):
        """Solve the SVM QP
        Use cvxopt qp solver which expects the following format:
        min_x   1/2 x'Px + q'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving QP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+d+1
        svm = SIQP(m, self.verbose)
        P = zeros((m, m))
        P[:d,:d] = 2.0*self.reg_param*eye(d)
        q = vstack([zeros((d+1,1)), ones((n,1))])
        svm.set_objective(P,q)
        svm.add_ineq_constraints(hstack([zeros((n,d+1)), -eye(n)]), zeros((n,1)))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n)])
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

class SVMDual(ConvexClassifier):
    """
    Standard l2 regularized SVM, in the dual, not finished yet. TODO: test time, arbitrary kernel
    """

    def __init__(self, reg_param, kdict, verbose=False):
        ConvexClassifier.__init__(self, reg_param, kdict, verbose)
        self.loss = HingeLoss()
        self.regularizer = Reg_l2()
        self.kernel = KernelFactory(kdict)
        self.alpha = array([])

    def train(self, train_ex, train_lab):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""

        self.train_ex = train_ex
        self.kernel.compute(train_ex)
        self.y = train_lab

        t_start = time()
        self.solve_opt()
        t_end = time()

        # Collect statistics about optimization
        self.train_time = t_end-t_start

    def predict(self, pred_ex):
        """Wrapper to do prediction"""
        Kpred = self.kernel.compute_test(pred_ex)
        Ymat = diag(self.y)
        scores = dot(dot(Kpred.T,Ymat),self.alpha) + self.b
        return scores

    def solve_opt(self):
        """Solve the SVM QP
        Use cvxopt qp solver which expects the following format:
        min_x   1/2 x'Px + q'x
        st      Gx <= h
                Ax = b
        """
        n = len(self.y)
        qp = SIQP(n, use_mosek = True)
        q = -ones((n, 1), dtype = 'double')
        ll = outer(self.y, self.y)
        #K = inner(self.train_ex, self.train_ex)
        K = self.kernel.kmat
        P = ll * K

        qp.set_objective(P, q)
        qp.add_eq_constraints(array([self.y]), array([[0]]))
        qp.add_ineq_constraints(eye(n), (self.reg_param * ones(n))[..., newaxis])
        qp.add_ineq_constraints(-1. * eye(n), zeros((n, 1)))

        qp.solve(warn = True)
        self.alpha = qp.solution
        sel = (qp.solution > self.epsilon) & (qp.solution < self.reg_param-self.epsilon)
        pred = dot(dot(K[sel,:],diag(self.y)),self.alpha)
        b = self.y[sel] - pred
        self.b = mean(b)
        

class OneNormSVM_dense(ConvexClassifier):
    """
    One norm SVM
    """    
    def __init__(self, reg_param, kdict, verbose=False):
        ConvexClassifier.__init__(self, reg_param, kdict, verbose)
        self.loss = HingeLoss()
        self.regularizer = Reg_l1()

    def solve_opt(self):
        """Solve the linear program corresponding to the one norm SVM.
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([zeros((d+1,1)), ones((n,1)),
                                  self.reg_param*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

class OneNormSVM(ConvexClassifier):
    """
    One norm SVM, with sparse matrices as constraints
    Should behave exactly the same as the non sparse version
    """
    def __init__(self, reg_param, kdict, verbose=False):
        ConvexClassifier.__init__(self, reg_param, kdict, verbose)
        self.loss = HingeLoss()
        self.regularizer = Reg_l1()

    def solve_opt(self):
        """Solve the linear program corresponding to the one norm SVM.
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([zeros((d+1,1)), ones((n,1)),
                                  self.reg_param*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]
    
class DiffConvexClassifier(Classifier):
    """
    Classifiers which have difference of convex training methods
    """
    def __init__(self, reg_param, kdict, verbose=False, rand_init=False):
        Classifier.__init__(self, reg_param, kdict, verbose)
        self.rand_init = rand_init  # Use SVM or random initialization
        self.num_iter = 0
        self.max_iter = 100         # Maximum number of DC iterations
        self.min_diff = 1e-2        # Stopping criteria, change in parameters
        self.all_objective = []     # The objective value for all iterations

    @property
    def perf(self):
        """Summary of results"""
        perf = super(DiffConvexClassifier, self).perf
        perf['num_iter'] = self.num_iter
        perf['all_objective'] = self.all_objective
        return perf

    def _init_param(self, norm):
        """Initialize values of w and b.
        The parameter denotes whether to use l1 or l2 norm.
        """
        assert(norm == 1 or norm==2)
        if self.rand_init:
            self.num_iter = 0
            if self.verbose: print('Random initialization')
            w = self.reg_param*randn(self.train_ex.shape[0],1)
            b = -randn()
        else:
            self.num_iter = 1
            if self.verbose: print('SVM initialization')
            if norm == 1:
                svm = OneNormSVM(self.reg_param, self.kdict, self.verbose)
            elif norm == 2:
                svm = SVMPrimal(self.reg_param, self.kdict, self.verbose)
            # train computes the feature mapping explicitly
            svm.train_ex = self.train_ex
            svm.y = self.y
            svm.solve_opt()
            (w, b) = (svm.w, svm.b)

        return (w, b)

    def _subgradient_concave_ramp(self, mu):
        """Compute the subgradient of the concave part of the ramp loss"""
        (clipped, sloped, correct) = self.loss.get_loss_case(self.y*self.train_out)
        #sw = zeros(self.train_ex.shape[0])
        #sb = zeros((1,1))
        #for idx in clipped:
        #    sw += -self.y[idx]*self.train_ex[:,idx]
        #    sb += -self.y[idx]
        sw = numpy.sum(-self.y[clipped]*self.train_ex[:,clipped], axis=1)
        sw.shape = (self.train_ex.shape[0], 1)
        sb = sum(-self.y[clipped])
        sw /= mu
        sb /= mu
        
        return (sw, sb)

    def _subgradient_concave_cap_l1(self, a):
        """Compute the subgradient of the concave part of the capped l1 norm"""
        grad = sign(self.w)
        grad[nonzero(abs(self.w) < a)[0]] = 0.0
        return grad

    def _subgradient_concave_l0(self, a):
        """Compute the subgradient of the concave part of the l0 approximation"""
        return a*sign(self.w)*(1.0 - numpy.exp(-a*abs(self.w)))

class CapOneNormSVM(DiffConvexClassifier):
    """
    SVM with capped one norm regularizer.
    """
    def __init__(self, reg_param, kdict, a=1.0, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.a = a                      # The value of the cap
        self.loss = HingeLoss()
        self.regularizer = Reg_l1cap(a)
        
    #@property
    #def margin(self):
    #    """The value of the regularizer, specialized because of parameter a"""
    #    return self.regularizer(self.w, self.a)

    def _solve_linprog(self, u):
        """Solve the linear program corresponding to the convex
        part of the capped one norm SVM.
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([-self.reg_param*u, zeros((1,1)), ones((n,1)),
                                  self.reg_param*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def _solve_linprog_dense(self, u):
        """Solve the linear program corresponding to the convex
        part of the capped one norm SVM.

        Uses dense matrices.
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([-self.reg_param*u, zeros((1,1)), ones((n,1)),
                                  self.reg_param*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(1)
        self.all_objective.append(self.primal_obj)
        u = self._subgradient_concave_cap_l1(self.a)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_linprog(u)
            old_u = u
            u = self._subgradient_concave_cap_l1(self.a)
            diff_param = sum(abs(old_u-u))
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)


class ZeroNormSVM(DiffConvexClassifier):
    """
    SVM with zero norm regularizer. Uses exponential approximation.
    """
    def __init__(self, reg_param, kdict, a=4.0, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.a = a                      # The exponent in approximation
        self.loss = HingeLoss()
        self.regularizer = Reg_l0(a)
        
    #@property
    #def margin(self):
    #    """The value of the regularizer, specialized because of parameter a"""
    #    return self.regularizer(self.w, self.a)

    def _solve_linprog(self, u):
        """Solve the linear program corresponding to the convex
        part of the zero norm SVM.
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([-self.reg_param*u, zeros((1,1)), ones((n,1)),
                                  self.reg_param*self.a*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def _solve_linprog_dense(self, u):
        """Solve the linear program corresponding to the convex
        part of the zero norm SVM.

        Uses dense matrices.
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([-self.reg_param*u, zeros((1,1)), ones((n,1)),
                                  self.reg_param*self.a*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(1)
        self.all_objective.append(self.primal_obj)
        u = self._subgradient_concave_l0(self.a)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_linprog(u)
            old_u = u
            u = self._subgradient_concave_l0(self.a)
            diff_param = sum(abs(old_u-u))
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)

class RLSVM2(DiffConvexClassifier):
    """
    Ramp Loss SVM with l2 regularizer.
    """
    def __init__(self, reg_param, kdict, mu, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.mu = mu                   # the slope of the ramp
        self.loss = RampLoss(mu)
        self.regularizer = Reg_l2()
        
    def _solve_quadprog(self, sw, sb):
        """Solve the quadratic program given by
        linearizing the concave part

        Use cvxopt qp solver which expects the following format:
        min_x   1/2 x'Px + q'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving QP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+d+1
        svm = SIQP(m, self.verbose)
        P = zeros((m,m))
        P[:d,:d] = 2.0*self.reg_param*eye(d)
        q = vstack([-sw, -sb, ones((n,1))/self.mu])
        svm.set_objective(P, q)
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n)]), zeros((n,1)))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n)])
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(2)
        self.all_objective.append(self.primal_obj)
        (sw, sb) = self._subgradient_concave_ramp(self.mu)
        #print(sw, sb)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_quadprog(sw, sb)
            #print('weights')
            #print(self.w, self.b)
            (old_sw, old_sb) = (sw, sb)
            (sw, sb) = self._subgradient_concave_ramp(self.mu)
            #print(sw, sb)
            diff_param = sum(abs(old_sw-sw)) + abs(old_sb-sb)
            #print(diff_param)
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)

class RLSVM1(DiffConvexClassifier):
    """
    Ramp Loss SVM with l1 regularizer.
    """
    def __init__(self, reg_param, kdict, mu, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.mu = mu                   # the slope of the ramp
        self.loss = RampLoss(mu)
        self.regularizer = Reg_l1()
        
    def _solve_linprog(self, sw, sb):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with l1 penalty.
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([-sw, -sb, ones((n,1))/self.mu,
                                  self.reg_param*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def _solve_linprog_dense(self, sw, sb):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with l1 penalty.

        Uses dense matrices
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([-sw, -sb, ones((n,1))/self.mu,
                                  self.reg_param*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM with one norm penalty"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(1)
        self.all_objective.append(self.primal_obj)
        (sw, sb) = self._subgradient_concave_ramp(self.mu)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_linprog(sw, sb)
            (old_sw, old_sb) = (sw, sb)
            (sw, sb) = self._subgradient_concave_ramp(self.mu)
            diff_param = sum(abs(old_sw-sw)) + abs(old_sb-sb)
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)

class RLSVMC1(DiffConvexClassifier):
    """
    Ramp Loss SVM with capped l1 regularizer.
    """
    def __init__(self, reg_param, kdict, mu, a=1.0, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.mu = mu                   # the slope of the ramp
        self.a = a                     # The value of the cap
        self.loss = RampLoss(mu)
        self.regularizer = Reg_l1cap(a)
        
    #@property
    #def margin(self):
    #    """The value of the regularizer, specialized because of parameter a"""
    #    return self.regularizer(self.w, self.a)

    def _solve_linprog(self, sw, sb, u):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with capped l1 penalty.
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([-sw-self.reg_param*u, -sb, ones((n,1))/self.mu,
                                  self.reg_param*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def _solve_linprog_dense(self, sw, sb, u):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with capped l1 penalty.

        Uses dense matrices
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([-sw-self.reg_param*u, -sb, ones((n,1))/self.mu,
                                  self.reg_param*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM with capped l1 regularizer"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(1)
        self.all_objective.append(self.primal_obj)
        (sw, sb) = self._subgradient_concave_ramp(self.mu)
        u = self._subgradient_concave_cap_l1(self.a)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_linprog(sw, sb, u)
            (old_sw, old_sb, old_u) = (sw, sb, u)
            (sw, sb) = self._subgradient_concave_ramp(self.mu)
            u = self._subgradient_concave_cap_l1(self.a)
            diff_param = sum(abs(old_sw-sw)) + abs(old_sb-sb) + sum(abs(old_u-u))
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)


class RLSVM0(DiffConvexClassifier):
    """
    Ramp Loss SVM with l0 regularizer.
    """
    def __init__(self, reg_param, kdict, mu, a=4.0, verbose=False, rand_init=False):
        DiffConvexClassifier.__init__(self, reg_param, kdict, verbose, rand_init)
        self.mu = mu                   # the slope of the ramp
        self.a = a                     # The exponent in approximation
        self.loss = RampLoss(mu)
        self.regularizer = Reg_l0(a)
        
    #@property
    #def margin(self):
    #    """The value of the regularizer, specialized because of parameter a"""
    #    return self.regularizer(self.w, self.a)

    def _solve_linprog(self, sw, sb, u):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with l0 penalty.
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILPS(m, self.verbose)
        svm.set_objective(vstack([-sw-self.reg_param*u, -sb, ones((n,1))/self.mu,
                                  self.reg_param*self.a*ones((d,1))]))
        G = sparse(cm(hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])))
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]))), zeros((n,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.add_ineq_constraints(sparse(cm(hstack([-eye(d), zeros((d,n+1)), -eye(d)]))), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def _solve_linprog_dense(self, sw, sb, u):
        """Solve the linear program corresponding to the convex part
        of the ramp loss SVM with l0 penalty.

        Uses dense matrices
        
        Use cvxopt lp solver which expects the following format:
        min_x   c'x
        st      Gx <= h
                Ax = b

        We organise the variables of the SVM as (w,b,xi,eta).
        """
        yvec = self.y.copy()
        yvec.shape = (len(self.y), 1)

        if self.verbose: print('Solving LP')
        n = len(self.y)
        d = self.train_ex.shape[0]
        m = n+2*d+1
        svm = SILP(m, self.verbose)
        svm.set_objective(vstack([-sw-self.reg_param*u, -sb, ones((n,1))/self.mu,
                                  self.reg_param*self.a*ones((d,1))]))
        G = hstack([-yvec*transpose(self.train_ex), -yvec, -eye(n), zeros((n,d))])
        svm.add_ineq_constraints(G, -self.mu*ones((n,1)))
        svm.add_ineq_constraints(hstack([zeros((n,d+1)),-eye(n),zeros((n,d))]), zeros((n,1)))
        svm.add_ineq_constraints(hstack([zeros((d,d+1)),zeros((d,n)),-eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.add_ineq_constraints(hstack([-eye(d), zeros((d,n+1)), -eye(d)]), zeros((d,1)))
        svm.solve()

        if svm.optimal:
            self.w = svm.solution[:d]
            self.w.shape = (d,1)
            self.b = svm.solution[d]

    def solve_opt(self):
        """Use a DCA to solve the ramp loss SVM with zero norm (approx) regularizer"""
        if self.verbose: print('Solving DCA')
        (self.w, self.b) = self._init_param(1)
        self.all_objective.append(self.primal_obj)
        (sw, sb) = self._subgradient_concave_ramp(self.mu)
        u = self._subgradient_concave_l0(self.a)
        diff_param = numpy.inf
        while (self.num_iter < self.max_iter)\
              and (diff_param > self.min_diff):
            self._solve_linprog(sw, sb, u)
            (old_sw, old_sb, old_u) = (sw, sb, u)
            (sw, sb) = self._subgradient_concave_ramp(self.mu)
            u = self._subgradient_concave_l0(self.a)
            diff_param = sum(abs(old_sw-sw)) + abs(old_sb-sb) + sum(abs(old_u-u))
            self.num_iter += 1
            self.all_objective.append(self.primal_obj)


