'''
Created on 21.02.2013

@author: Gabriel
'''
from __future__ import division

from numpy import dot, sign, array, squeeze, repeat
from sklearn.svm import SVC


class MIClassifier(object):
    ''' Multiple Instance Learning Classifier '''
    def train(self, traindata, reg_param, v0 = None, verbose = False, *args, **kwargs):
        self.traindata = traindata
        self.reg_param = reg_param
        self.solve_opt(v0 = v0, verbose = verbose, *args, **kwargs)
    
    def predict(self, preddata):
        ''' implement classifier '''
        y = []
        for pd in preddata.iter():
            if preddata.sparse:
                yi = self.one_predict_sparse(pd)
            else:
                yi = self.one_predict(pd)
            y.append(yi)
        
        return sign(array(y))

    def read_data(self, data):
        ''' Return midata object in corresponding format, by calling .to_$ on data '''
        raise NotImplementedError
    
class MeanSVM(MIClassifier):
    ''' SVM on the means for MIL '''
    
    def one_predict(self, data):
        ''' predict one datapoint '''
        return dot(self.w,data['mean']) + self.b

    def read_data(self, data):
        ''' Read data and convert to EllipsoidData '''
        return data.to_means()
    
    def solve_opt(self, v0, verbose, *args, **kwargs):
        svm = SVC(C=1/self.reg_param, kernel='linear')
        q, y = array(self.traindata.means), array(self.traindata.labels)
        svm.fit(q, y)
        self.w, self.b, self.primal_obj = squeeze(svm.coef_), svm.intercept_[0], None

class InstanceSVM(MIClassifier):
    ''' Naive instance level SVM for MIL '''
    
    def one_predict(self, data):
        ''' predict one datapoint '''
        return reduce(max,[dot(self.w,inst)+self.b for inst in data['bag']])
    
    def read_data(self, data):
        ''' Read data and convert to EllipsoidData '''
        return data.to_instances()
    
    def solve_opt(self, v0, verbose, *args, **kwargs):
        svm = SVC(C=1/self.reg_param, kernel='linear')
        q = array([inst for qi in self.traindata.bags for inst in qi])                # flatten out all instances
        y = array([repeat(yi,len(qi)) for yi, qi in zip(self.traindata.labels,self.traindata.bags)]).flatten() # repeat labels for all instances    
        svm.fit(q, y)
        self.w, self.b, self.primal_obj = squeeze(svm.coef_), svm.intercept_[0], None



