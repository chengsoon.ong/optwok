'''
Created on 04.03.2013

@author: Gabriel
'''

from scipy.optimize.lbfgsb import fmin_l_bfgs_b
from scipy.misc import logsumexp
from numpy.linalg import norm, cholesky
from sys import float_info
from optwok.socp import SOCP
from numpy.random import rand
from scipy.sparse import issparse
from numpy import sqrt, dot, zeros, array, squeeze, ones, r_, amax, eye, exp, diag
from optwok.mil.mi_classifier import MIClassifier
from optwok.mil.mi_loss import HingeLoss, HuberizedHingeLoss
from scipy.sparse import csc_matrix

class eMIL(MIClassifier):
    ''' Ellipsoidal Multiple Instance Learning Classifier '''
    
    def one_predict_sparse(self, sparsedata):
        ''' Ellipsoidal MI classifier for one sparse ellipsoid '''
        w = csc_matrix(self.w)
        return (w*sparsedata['mean'].transpose()).A[0][0] + self.b \
                + sqrt((w * sparsedata['covariance'] * w.transpose()).A[0][0])
    
    def one_predict(self, data):
        ''' Ellipsoidal MI classifier for one ellipsoid'''
        quad_form = dot(self.w,dot(data['covariance'],self.w))
        return dot(self.w,data['mean']) + self.b + sqrt(quad_form)

    def read_data(self, data):
        ''' Read data and convert to EllipsoidData '''
        return data.to_ellipsoids()
    
    def cost_ellipsoid(self, w, b, qi, Si):
        ''' Loss per ellipsoid'''
        if issparse(Si):
            return (w*qi.transpose()).A[0][0] + b + sqrt((w * Si * w.transpose()).A[0][0])
        else:
            quad_form = dot(w,dot(Si,w))
            return dot(w,qi) + b + sqrt(quad_form)

class eMILDca(eMIL):
    ''' Ellipsoidal Multiple Instance Learning Classifier,
        Solve minimization with DCA'''
    
    def __init__(self):
        self.set_opt_param(1e10, 100)
    
    def solve_opt(self, v0, verbose, *args, **kwargs):
        self.w, self.b, self.primal_obj = self.min_dca(v0=v0, verbose=verbose, *args, **kwargs)
    
    def set_opt_param(self, factr, maxiter):
        self.maxiter = maxiter
        self.factr = factr
    
    def check_conv(self, v_old, v_new, f_old, f_new, iteration):
        return abs((f_old-f_new)/max([1,abs(f_old),abs(f_new)])) < self.factr * float_info.epsilon \
              or iteration > self.maxiter
    
    def loss(self, w, b):
        ''' compute the loss for hyperplane (w,b) and bags with labels y and regularization parameter lamb '''
    
        lossf = HingeLoss()
        loss = 0
        if self.traindata.sparse:
            w = csc_matrix(w)
        for qi, si, yi in zip(self.traindata.means,self.traindata.covariances,self.traindata.labels):   # sum over hinge loss for all distances
            fi = yi*(self.cost_ellipsoid(w,b,qi,si))
            loss += lossf.loss(fi)
 
        if self.traindata.sparse:
            w = squeeze(w.A)
                   
        # regularize with 2-norm squared:
        ww = dot(w,w)
        
        loss = loss + self.reg_param * ww

        return loss

    
    def min_dca(self, v0=None, verbose=False, check_convergence=None, *args, **kwargs):
        ''' Minimize with the CCCP, needs solve_dca_subproblem(w_k ,v0,verbose) defined
            check_convergence(v_old, v_new, f_old, f_new, iter) returns false to continue'''
        if check_convergence is None:
            check_convergence = self.check_conv
        if v0 is None:
            v = r_[rand(self.traindata.means[0].shape[0]), rand(1)]
        else:
            v = v0    

        converged = False
        i = 0
        
        while not converged:
            v_old = v
            v, f = self.solve_dca_subproblem(v[:-1],v0=v, verbose=verbose)
            i += 1
            
            f_old = self.loss(v_old[:-1],v_old[-1])
            f_new = self.loss(v[:-1],v[-1])
            if verbose:
                print('current sub-problem loss: ', f)
                print('full loss: %f, old full loss: %f' %(f_new, f_old))
                print('v diff: ', norm(v_old-v))
            converged = check_convergence(v_old, v, f_old, f_new, i)
            
        return v[:-1], v[-1], f    


class eMILDcaBfgs(eMILDca):
    ''' Ellipsoidal Multiple Instance Learning Classifier,
        Solve minimization with DCA and subproblem with bfgs and soft_max'''
        
    def check_convergence(self, v_old, v_new, f_old, f_new, iteration ):
        return self.check_conv(v_old, v_new, f_old, f_new, iteration) \
                    or norm(v_old-v_new)< self.factr * float_info.epsilon
    
    def loss_and_grad_dca(self, v, wk):
        ''' gradient and loss at w, b for the DCA loss'''
        w = v[:-1]
        b = v[-1]
        
        beta = 18
        
        
        loss = 0
        gradw = 0
        gradb = 0
        for qi, Pi, yi in zip(self.traindata.means, self.traindata.covariances, self.traindata.labels):
            quad_form = sqrt(dot(w,dot(Pi,w)))
            pred =  quad_form + dot(w,qi) + b
            pred_grad = qi + dot(Pi,w)/quad_form
            if yi > 0: #positive
                smgp = self._soft_max_grad(-1+pred,beta)
                loss += self._soft_max(-1+pred,beta)
                gradw += smgp * pred_grad
                gradb += smgp
                # Linearization of concave part
                conc_linw = -qi - dot(Pi, wk) / sqrt(dot(wk,dot(Pi,wk)))
                loss += dot(w,conc_linw) - b
                gradw += conc_linw
                gradb += -1
            else: #negative
                smgn = self._soft_max_grad(1+pred,beta)
                loss += self._soft_max(1+pred,beta)
                gradw += smgn * pred_grad
                gradb += smgn 
                
        # Regularizer
        loss += self.reg_param * dot(w,w)
        gradw += self.reg_param * 2 * w
        
        return loss, r_[gradw, gradb]
    
    def solve_dca_subproblem(self, wk,v0,verbose):
        return self.solve_dca_bfgs(wk,v0,verbose)
    
    def solve_dca_bfgs(self, wk, v0, verbose=False):
        ''' Solve the same problem as solve_dca_socp, but instead of converting to a SOCP,
            use the soft max instead of the hinge loss '''
        
        factr = 1e7
        pgtol = 1e-05
        v, f, info = fmin_l_bfgs_b(func = self.loss_and_grad_dca, x0 = v0, approx_grad = False,
                                  m=100, fprime = None, args = (wk,), factr = factr, pgtol=pgtol)
        
        if verbose:
            print('loss: ', f)
            print('norm of gradient: ', norm(info['grad']))
            print('warnflag: ', info['warnflag'])
            print('stop reason: ', info['task'])
            print('funcalls: ', info['funcalls'])
        
    
        return v, f
    
    def _soft_max(self,x,beta):
        ''' relax max(0,x) by 1/b (e^(b*x)+1) '''
        return 1/beta * logsumexp([beta*x, 0])
        
    def _soft_max_grad(self,x, beta):
        ''' return the gradient of the soft max 
            (need to multiply this by the gradient of x...'''
    
        y = beta*x
        return exp(y-logsumexp([y,0]))

class eMILDcaSocp(eMILDca):
    ''' Ellipsoidal Multiple Instance Learning Classifier
        Solve minimization with DCA and subproblem as SOCP'''

    #@memoized
    def chol_S(self,i):
        ''' return A, where A'A = Si '''
        return cholesky(self.traindata.covariances[i]).T
    
    def solve_dca_subproblem(self, wk,v0,verbose):
        return self.solve_dca_socp(wk,verbose)
    
    def solve_dca_socp(self, wk, verbose=False):
        '''
            x = [w, b, xi, theta],
        '''
        
        num_vars = self.traindata.num_feat + 1 + self.traindata.num_ex + 1
        num_feat = self.traindata.num_feat
        num_ex = self.traindata.num_ex
        socp = SOCP(num_vars= num_vars, verbose=verbose, use_mosek=True)
        
        pb_ind = array(self.traindata.labels) > 0
        q = array(self.traindata.means)
        P = array(self.traindata.covariances)
        
        # Linearization of concave part
        conc_linw = 0
        for qi, Pi in zip(q[pb_ind], P[pb_ind]):
            conc_linw += -qi - dot(Pi, wk) / sqrt(dot(wk,dot(Pi,wk)))
        
        conc_linb = -sum(pb_ind)
        
    #    f = r_[conc_linw, conc_linb, ones(mibag.m), mibag.lamb]
        f = r_[zeros(num_vars-1),1]     # only z
        
        socp.set_objective(f)
        
        
        # Constraints for original objective + regularization 
        q = r_[-conc_linw, -conc_linb, -ones(num_ex), 1]/2
        A = zeros((num_feat + 1,num_vars))
        A[0,:] = -q
        A[1:,:num_feat] = diag(ones(num_feat) * sqrt(self.reg_param/2))
        d = 1/2
        b = zeros(num_feat+1)
        b[0] = 1/2
        socp.add_ineq_constraints(A, b, q, d)
        
        for i, e in enumerate(zip(self.traindata.means, self.traindata.labels)):
            qi, yi = e               
            A_i = zeros((num_feat, num_vars))
            A_i[:,:num_feat] = self.chol_S(i) # Ai
            b_i = zeros(num_feat)
            c_i = r_[-qi,-1,zeros(num_ex+1)]
            c_i[num_feat+1+i] = 1    # xi
            d_i = yi
            socp.add_ineq_constraints(A_i, b_i, c_i, d_i)
    
        # Constraint for positive slack
        A_0 = zeros((num_vars, num_vars))
        A_0[num_feat+1:-1,num_feat+1:-1] = -1*eye(num_ex)
        socp.set_component_ineq_constraints(A_0, zeros(num_vars))
    
        socp.solve(warn=verbose)
    
        return socp.solution[:num_feat+1], socp.cur_obj


class eMILBfgs(eMIL):
    ''' Ellipsoidal Multiple Instance Learning Classifier,
        Minimize with bfgs'''
    
    def solve_opt(self, v0, verbose, huber_delta, *args, **kwargs):
        self.lossf = HuberizedHingeLoss(huber_delta)
        self.w, self.b, self.primal_obj = self.min_bfgs(v0=v0, verbose=verbose, *args, **kwargs)
    
    def loss_and_grad_onepm(self, v):
        return self.loss_and_grad(v[:-1],v[-1])
    
    def loss_and_grad(self, w,b):
        ''' compute the loss for hyperplane (w,b) and bag  with labels y and regularization parameter lamb
            also compute the gradient with respect to (w,b)'''
        loss = 0
        gradw, gradb = 0, 0
        nw = norm(w)
        if self.traindata.sparse:
            w = csc_matrix(w)
        for qi, si, yi in zip(self.traindata.means,self.traindata.covariances,self.traindata.labels):   # sum over hinge loss for all distances
            fi = yi*(self.cost_ellipsoid(w,b,qi,si))
            loss += self.lossf.loss(fi)
            gradwi, gradbi = self.lossf.grad(fi, qi, si, yi, w, nw)
            gradb += gradbi
            gradw += gradwi
 
 
        if self.traindata.sparse:
            w = squeeze(w.A)
                   
        # regularize with 2-norm squared:
        ww = dot(w,w)
        gradw = gradw + self.reg_param * 2* w
        
        loss = loss + self.reg_param * ww

        return loss, r_[gradw, gradb]

    def min_bfgs(self,v0=None, verbose=False):
        ''' Find optimal hyperplane for the balls (q,s) with labels y and the regularizer parameter lamb '''
        
        if v0 is None:
            w0 = ones(amax(self.traindata.num_feat))
            w0 = w0 / norm(w0)
            v0 = r_[w0, 0]
        
        if verbose:
            disp = 1
        else:
            disp = 0
            
        if self.traindata.sparse:
            factr = 1e7
            pgtol = 1e-04
        else:
            factr = 1e7
            pgtol = 1e-05
            
        v, f, info = fmin_l_bfgs_b(func = self.loss_and_grad_onepm, x0 = v0, approx_grad = False,
                                  m=100, fprime = None, args = (), factr = factr, pgtol=pgtol)
        
        if verbose:
            print('loss: ', f)
            print('norm of gradient: ', norm(info['grad']))
            print('warnflag: ', info['warnflag'])
            print('stop reason: ', info['task'])
            print('funcalls: ', info['funcalls'])
        
    
        return v[:-1], v[-1], f
