'''
Created on 28.02.2013

@author: Gabriel
'''
from __future__ import division
from optwok.mldata import ClassificationData
from scipy.sparse import issparse
from numpy import squeeze, array

class MIData(ClassificationData):
    def iter(self):
        for i in range(self.num_ex):
            yield self.ith_data(i)




class EllipsoidData(MIData):
    ''' Class for ellipsoids for multiple instance learning '''
    def __init__(self, means, covariances, labels):
        self.means = means
        self.covariances = covariances
        self.labels = labels
        
        if issparse(self.means[0]):
            self.sparse = True
        else:
            self.sparse = False
    
    def split_off(self, idx, id_str):
        rbd = EllipsoidData(self.means[idx], self.covariances[idx], self.labels[idx])
        rbd.id_str = id_str
        rbd.idx = idx
        
        return rbd
    
    def ith_data(self, i):
        return {'mean': self.means[i], 'covariance': self.covariances[i], 'label':self.labels[i]}
    
    @property
    def num_feat(self):
        return max(self.means[0].shape)

class MultipleInstanceData(MIData):
    ''' Class for multiple instance bags: bags is a list of lists of instances
        label is on a bag level '''
    def __init__(self, bags, labels):
        self.labels = labels
        if issparse(bags[0][0]):
            self.bags = [[squeeze(inst.A) for inst in qi]  for qi in bags]
        else:
            self.bags = bags
        
        self.sparse = False
    
    def split_off(self, idx, id_str):
        rbd = MultipleInstanceData(self.bags[idx], self.labels[idx])
        rbd.id_str = id_str
        rbd.idx = idx
        
        return rbd

    def ith_data(self, i):
        return {'bag': self.bags[i], 'label':self.labels[i]}

    @property
    def num_feat(self):
        return max(self.bags[0][0].shape)

class MIMeanData(MIData):
    ''' Class for means of multiple instance bags '''
    
    def __init__(self, means, labels):
        self.labels = labels
        
        if issparse(means[0]):
            self.means = [squeeze(qi.A) for qi in means]
        else:
            self.means = means
            
        self.sparse = False
    
    def split_off(self, idx, id_str):
        rbd = MIMeanData(self.means[idx], self.labels[idx])
        rbd.id_str = id_str
        rbd.idx = idx
        
        return rbd
    
    def ith_data(self, i):
        return {'mean': self.means[i], 'label':self.labels[i]}

    @property
    def num_feat(self):
        return max(self.means[0].shape)