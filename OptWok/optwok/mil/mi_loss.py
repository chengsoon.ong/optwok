'''
Created on 04.03.2013

@author: Gabriel
'''
from __future__ import division

from numpy import squeeze, sqrt, dot
from scipy.sparse import issparse

class eMILLoss(object):
    def gradf(self, qi, Si, yi, w, nw):
        if issparse(Si):
            return -yi*(squeeze((qi+ (Si*w.transpose()).transpose()/sqrt((w*Si*w.transpose()).A[0,0])).A))
        else:
            quad_form = dot(w,dot(Si,w))
            return -yi*(qi + dot(Si,w)/sqrt(quad_form))

class HingeLoss(eMILLoss):
    
    def grad(self, fi, qi, Si, yi, w, nw):
        if 1 <= fi:
            gradbi, gradwi = 0, 0
        else:
            gradbi, gradwi = -yi, self.gradf(qi,Si,yi,w,nw)
            
        return gradwi, gradbi
    
    def loss(self, x):
        return max(0,1-x)
    
class HuberizedHingeLoss(eMILLoss):
    ''' hinge without the kink, delta >= 0 parametrizes how "squared" the loss is 
        if delta is zero the original hinge loss is recovered
    '''
    
    def __init__(self, delta):
        self.delta = delta
    
    def grad(self, fi, qi, Si, yi, w, nw):
        if fi <= 1:
                
            if fi > 1 - self.delta:
                fid = (1-fi) / self.delta
                gradbi = fid * -yi
                gradwi = fid * self.gradf(qi, Si, yi, w, nw)
            else:
                gradbi = -yi
                gradwi = self.gradf(qi, Si, yi, w, nw)
        else:
            gradbi, gradwi = 0, 0
            
        return gradwi, gradbi
    
    def loss(self, x):
        if x <= 1:
            if x > 1 - self.delta:                   # 1 - delta < x <= 1
                return ((1-x) **2) / (2*self.delta)
            else:                               # x <= 1 - delta
                return 1 - x - self.delta/2 
        else:                                   # x > 1        
            return 0