"""Data management"""

import numpy as np
import pandas as pd
from numpy import array, transpose, mean
from numpy import arange, hstack, kron, ones, eye
from numpy import sqrt, concatenate, zeros, log10
from numpy.random import permutation, seed
from numpy import flatnonzero
from numpy.linalg import norm


class Data(object):
    """A container for contextual bandit data"""
    def __init__(self, examples, contexts, labels):
        self.num_context = int(max(contexts))+1
        self._examples = []
        self._contexts = []
        self._labels = []
        self.min_label = 10000
        self.max_labels = -10000*ones(self.num_context)
        for con in range(self.num_context):
            idx = flatnonzero(contexts==con)
            self._examples.append(examples[:,idx])
            self._contexts.append(contexts[idx])
            self._labels.append(labels[idx])
            self.min_label = min(self.min_label, np.min(labels[idx]))
            self.max_labels[con] = np.max(labels[idx])

    def __repr__(self):
        return str((self._examples, self._contexts, self._labels))

    @property
    def num_features(self):
        """The number of features in the examples"""
        return self._examples[0].shape[0]

    @property
    def num_contexts(self):
        """The number of contexts"""
        return len(self._labels)
        
    def remove(self, idx, context):
        """Delete data item indexed by idx"""
        self._examples[context] = delete(self._examples[context], idx, 1)
        self._contexts[context] = delete(self._contexts[context], idx)
        self._labels[context] = delete(self._labels[context], idx)

    def examples(self, context):
        """Return all active data for given context"""
        return self._examples[context]

    def observe(self, idx, context):
        """Return the label for idx"""
        return self._labels[context][idx]
        
    def set_label(self, idx, context, val):
        """Set the value of label"""
        self._labels[context][idx] = val

    def _create_dataframe(self, context):
        """Helper function for saving,
        create a pd.DataFrame for context"""
        cols = ['label','context']
        cols.extend(range(self.num_features))
        df = pd.DataFrame(columns=cols)
        df['label'] = self._labels[context]
        df['context'] = self._contexts[context]
        df[list(range(self.num_features))] = self._examples[context].T
        return df
    
    def save(self, file_name):
        """Save to a bzip2 CSV file using pandas"""
        cols = ['label','context']
        cols.extend(range(self.num_features))
        df = pd.DataFrame(columns=cols)
        for con in range(self.num_contexts):
            df = df.append(self._create_dataframe(con), ignore_index=True)
        df.to_csv(file_name, index=False, compression='bz2')
        
def data_stats(all_data, task_list=None, num_cv=5):
    """Print some statistics about each dataset"""
    if task_list:
        all_tasks = task_list
    else:
        all_tasks = all_data.keys()

    for task in all_tasks:
        data = all_data[task]
        print('Number of examples in %s : %d' % (task, data.nof_peptides))

def _get_data(data, splits, subsample, num_feat):
    examples = zeros((num_feat,0))
    labels = []
    for cv in splits:
        # Normalize to unit length
        new_ex = transpose(array(data.encoded_peptides[cv]))
        for ix in range(new_ex.shape[1]):
            new_ex[:,ix] /= norm(new_ex[:,ix])
        examples = hstack([examples, new_ex])
        labels.extend(-log10(data.ic50s[cv])+log10(500))
    examples = array(examples)
    labels = array(labels)
    if subsample:
        idx = flatnonzero(labels<-1.6)
        perm = permutation(len(idx))
        subidx = idx[perm[:20]]
        idx = concatenate([flatnonzero(labels>=-1.6),subidx])
        examples = examples[:,idx]
        labels = labels[idx]
    return examples, labels

def get_data(all_data, splits, all_task=None, subsample=False, num_feat=45):
    """Concatenate data from the splits,
    randomly subsample 20 examples from labels < -1.6
    """
    if all_task is None:
        return _get_data(all_data, splits, subsample, num_feat)
    else:
        examples = zeros((num_feat,0))
        labels = array([])
        contexts = array([])
        for (task_id, task) in enumerate(all_task):
            data = all_data[task]
            (new_ex, new_lab) = _get_data(data, splits, subsample, num_feat)
            examples = hstack([examples, new_ex])
            labels = hstack([labels, new_lab])
            contexts = concatenate([contexts, task_id*ones(len(new_lab))])
        return (examples, contexts, labels)

def get_split(data, split, num_cv=5, verbose=False):
    """Concatenate the data not in 'split' into the test set.
    'split' is the training set.
    """
    test_idx = range(num_cv)
    test_idx.remove(split)
    (train_ex, train_lab) = get_data(data, [split])
    (test_ex, test_lab) = get_data(data, test_idx)

    if verbose:
        print('Split %d' % split)
        print('features=%d, num_train=%d, num_test=%d'\
              % (train_ex.shape[0], train_ex.shape[1], test_ex.shape[1]))
        
    return train_ex, train_lab, test_ex, test_lab

def get_multitask(all_tasks, all_data, split):
    """Treat each task as a different label"""
    train_ex = []
    train_lab_list = []
    test_ex = []
    test_lab_list = []
    num_train = 0
    num_test = 0
    for task in all_tasks:
        data = all_data[task]
        tre, trl, tee, tel = get_split(data, split)
        num_train += len(trl)
        num_test += len(tel)
        train_ex.append(tre)
        train_lab_list.append(trl)
        test_ex.append(tee)
        test_lab_list.append(tel)
    train_ex = hstack(train_ex)
    test_ex = hstack(test_ex)

    # Construct the matrix of labels
    train_lab = zeros((len(all_tasks), num_train))
    idx = 0
    for (ix, trl) in enumerate(train_lab_list):
        train_lab[ix, idx:idx+len(trl)] = trl
        idx += len(trl)
    test_lab = zeros((len(all_tasks), num_test))
    idx = 0
    for (ix, tel) in enumerate(test_lab_list):
        test_lab[ix, idx:idx+len(tel)] = tel
        idx += len(tel)

    return train_ex, train_lab, test_ex, test_lab
    
