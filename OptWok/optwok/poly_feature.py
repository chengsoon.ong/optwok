try:
    import scipy
except ImportError:
    print('poly_feature.py: scipy not available')
import numpy

def partitions(p, d):
    """Generate integer partitions of p with maximum d terms"""
    # base case of recursion: zero is the sum of the empty list
    if p == 0:
        yield ()
        return
		
    # modify partitions of n-1 to form partitions of n
    for m in partitions(p-1, d):
        if len(m) < d:
            yield (1,) + m
            if m and (len(m) < 2 or m[1] > m[0]):
                yield (m[0] + 1,) + m[1:]

def get_subsets(d, k):
    """Get all subsets of size k from set of size d.
    Assume items in d are integers from 0 to d-1.
    """
    # check base cases
    if k == 0 or d < k:
        yield []
    elif d == k:
        yield range(d)

    else:
        # Use recursive formula based on binomial coeffecients:
        # choose(d, k) = choose(d - 1, k - 1) + choose(d - 1, k)
        for s in get_subsets(d-1, k-1):
            s.append(d-1)
            yield s
        for s in get_subsets(d-1, k):
            yield s

def compute_coeff(p,m):
    """Compute the coefficient for a polynomial kernel (of degree p)
    feature given the partition vector m"""
    denom = 1.0
    for mi in m:
        denom *= scipy.factorial(mi)
    return numpy.sqrt(scipy.factorial(p)/denom)

def get_feature_map(p,d):
    """Explicit feature map for polynomial kernel
    of degree p and original dimensions d"""
    for m in partitions(p, d):
        c = compute_coeff(p,m)
        for ss in get_subsets(d, len(m)):
            yield (c, m, ss)

def get_poly_features(X, p):
    """Return the explicit feature vector for the polynomial kernel"""
    d = X.shape[0]
    n = X.shape[1]
    Phi = []
    for ix in range(n):
        example = X[:,ix]
        cur_phi = []
        for (c, m, ss) in get_feature_map(p, d):
            value = 1.0
            for ixm in range(len(m)):
                value *= example[ss[ixm]]**m[ixm]
            cur_phi.append(c*value)
        Phi.append(cur_phi)
    Phi = (numpy.array(Phi)).T
    # How to normalize?
    for ix in range(Phi.shape[1]):
        Phi[:,ix] /= numpy.linalg.norm(Phi[:,ix])
    return Phi











#####################################################################
# Test scripts
#####################################################################
def diff_kernel_matrix(train_ex, kdict):
    """Compute the difference between the kernel matrices between shogun
    and this implementation"""

    # shogun
    from shogun.Features import RealFeatures
    from shogun.Kernel import PolyKernel
    sfeat = RealFeatures(train_ex)
    kernel = PolyKernel(sfeat, sfeat, kdict['param'], kdict['inhomog'])
    K_shogun = kernel.get_kernel_matrix()

    # explicit feature map
    if kdict['inhomog']:
        X = numpy.vstack([train_ex, numpy.ones((1,train_ex.shape[1]))])
    else:
        X = train_ex
    efeat = get_poly_features(X, kdict['param'])
    K_feat = numpy.matrix(efeat).T*numpy.matrix(efeat)
    
    # compare
    if train_ex.shape[1] < 10:
        print(K_shogun)
        print(K_feat)

    return numpy.sum(sum(numpy.abs(K_feat-K_shogun)))

def compare_with_shogun():
    """Compare the kernel matrix with shogun's"""
    from esvm.datafuncs import cloudgen

    #numpy.random.seed(1)

    num_train = 100
    num_feat = 3
    frac_pos = 0.5
    width = 1.5
    (metadata, train_ex, train_labels) = cloudgen(num_train, num_feat, frac_pos, width)

    for degree in range(1,7):
        kdict = {'name':'poly', 'param': degree, 'inhomog': False}
        print(kdict)
        print('Absolute difference: %f' % diff_kernel_matrix(train_ex, kdict))
        kdict = {'name':'poly', 'param': degree, 'inhomog': True}
        print(kdict)
        print('Absolute difference: %f' % diff_kernel_matrix(train_ex, kdict))

if __name__ == '__main__':
    compare_with_shogun()
    
