'''
Created on 04.04.2013

@author: kgabriel
'''
from __future__ import division

from numpy import sum as asum, array

def accuracy(conf):
    ''' compute the accuracy, given a confusion matrix conf
        format: [[TP, FP], [FN, TN]]'''
    
    return (conf[0,0] + conf[1,1]) / asum(conf)

def recall(conf):
    ''' compute the true positive rate (sensitivity, recall), given a confusion matrix conf
        format: [[TP, FP], [FN, TN]]'''
    try:
        rec = conf[0,0] / (conf[0,0] + conf[1,0])
    except RuntimeWarning:  # division by zero
        rec = 0
    return rec

def precision(conf):
    ''' compute the precision (or PPV), given a confusion matrix conf
        format: [[TP, FP], [FN, TN]]'''
    try:
        prec = conf[0,0] / (conf[0,0] + conf[0,1])
    except RuntimeWarning:  # division by zero
        prec = 0
    return prec 

def get_conf_matrix(ypred, ytrue):
    ''' get confusion matrix from [-1,+1] labels '''
    ypred = array(ypred)
    ytrue = array(ytrue)
    yt = (ypred*ytrue)
    tp =  asum((ytrue>0) & (yt>0))
    tn =  asum((ytrue<0)  & (yt>0))
    fp =  asum((ytrue<0)  & (yt<0))
    fn =  asum((ytrue>0)  & (yt<0))
    
    return array([[tp,fp], [fn,tn]])