"""Implementation of a multiclass SVM in the dual.
Uses the vector formulation of multiclass SVM,
and not one-vs-one or one-vs-all.
"""

from time import time
from numpy.random import randn
from numpy import matrix, array, vstack, hstack
from numpy import zeros, ones, transpose, nonzero
from numpy import sign, eye, unique, kron, argmax
from numpy import trace

from optwok.kernel import kernel2distance
from optwok.sip import SIQP

class MulticlassSVM(object):
    """Base class for multiclass support vector machine,
    allowing a kernel on labels"""
    def __init__(self, reg_param, kernel, verbose=False):
        assert(reg_param > 0.0)
        self.reg_param = reg_param     # Tradeoff regularizer vs loss
        self.kernel = kernel           # kernel
        self.train_ex = array([])      # training examples
        self.y = array([])             # labels
        self.scale = 1.0               # the trace of Ktrain
        self.train_time = 0.0          # time used for training
        
        self.alpha = array([])         # weight vector
        self.num_train = 0             # number of examples
        self.num_classes = 0           # number of classes
        
        self.epsilon = 1e-6            # how small is zero?
        self.verbose = verbose         # how much output

    @property
    def train_out(self):
        """Output of the classifier on the training set.
        return a column vector of scores for each example."""
        return array((matrix(self.kernel.Ky)*matrix(self.alpha).T*matrix(self.kernel.Kx.kmat)).T)

    def solve_opt(self):
        """To be implemented in children"""
        raise NotImplementedError

    def train(self, train_ex, train_lab):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""

        self.train_ex = train_ex
        self.y = train_lab
        self.num_train = len(self.y)
        self.num_classes = len(unique(self.y))
        self.kernel.compute(train_ex)

        t_start = time()
        self.solve_opt()
        t_end = time()

        # Collect statistics about optimization
        self.train_time = t_end-t_start
        if self.verbose: print('Trained for %f seconds' % self.train_time)

    def predict(self, pred_ex):
        """Wrapper to do prediction.
        Compute K*alpha,
        return a column vector of scores for each example
        """
        Ktest = self.kernel.Kx.compute_test(pred_ex)
        scores = array((matrix(self.kernel.Ky)*matrix(self.alpha).T*matrix(Ktest)).T)
        return argmax(scores, axis=1)
    

class StructPredictor(MulticlassSVM):
    """Structured output SVM in the dual"""
    def __init__(self, reg_param, kernel, verbose=False):
        MulticlassSVM.__init__(self, reg_param, kernel, verbose)
        
    def solve_opt(self):
        """Solve the multiclass SVM QP
        Use cvxopt qp solver which expects the following format:
        min_x   1/2 x'Px + q'x
        st      Gx <= h
                Ax = b

        alpha has num_train rows and num_classes columns
        alphavec = self.alpha.T.reshape((m,1), order='F')
        """
        m = self.num_train*self.num_classes
        # class indicator
        oneY = zeros((self.num_train, self.num_classes))
        for ix in range(self.num_train):
            oneY[ix, self.y[ix]] = 1.0
        oneY.shape = (m,1)

        # Margin term for margin rescaling
        distance = kernel2distance(self.kernel.Ky)
        distY = zeros((self.num_train, self.num_classes))
        for ix in range(self.num_train):
            distY[ix,:] = distance[self.y[ix],:]
        distY.shape = (m,1)

        # Set up the optimization problem
        svm = SIQP(m, self.verbose)
        #svm.set_objective(kron(self.Kx, self.Ky), -oneY)
        #svm.set_objective(self.kernel.kmat, -oneY)
        svm.set_objective(self.kernel.kmat, distY)
        D = eye(m)
        idx = nonzero(oneY)[0]
        D[idx,idx] = -1.0
        svm.add_ineq_constraints(D, zeros((m,1)))
        UB = zeros((m,m))
        UB[idx,idx] = 1.0
        svm.add_ineq_constraints(UB, self.reg_param*ones((m,1)))

        # sum_u alpha_iu = 0 for all i
        for ix in range(self.num_train):
            cur_const = zeros((1,m))
            cur_const[0,self.num_classes*ix:self.num_classes*(ix+1)] = 1.0
            svm.add_eq_constraints(cur_const, zeros((1,1)))
        # sum_i alpha_iu = 0 for all u
        #for ix in range(self.num_classes):
        #    class_idx = zeros((1,self.num_classes))
        #    class_idx[0,ix] = 1.0
        #    svm.add_eq_constraints(kron(ones((1,self.num_train)), class_idx), zeros((1,1)))
            
        svm.solve()

        self.alpha = svm.solution.reshape((self.num_train, self.num_classes))

def idx2binvec(y):
    """Convert a vector y with class labels 0,1,...
    to a binary matrix with corresponding 1 in column label"""
    num_class = len(unique(y))
    Y = zeros((len(y),num_class))
    for ix in range(len(y)):
        Y[ix,y[ix]] = 1.0
    return Y
