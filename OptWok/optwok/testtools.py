"""Some functions to help test the modules"""
from numpy import log, exp, nonzero
from numpy import linspace, square, median
from numpy import zeros, eye, sort, ix_
from numpy import array, vstack
from numpy.random import permutation
from optwok.kernel import sqr_dist, normalize_unit_diag

def plot_confusion(mix_list):
    """Plot the kernel matrix corresponding to a particular
    mix_list denoting the mixing of labels."""
    from matplotlib.pyplot import matshow, colorbar
    Ky = list2matrix(mix_list)
    matshow(Ky)
    colorbar()

def list2matrix(mix_list, noise=0.0, neg_corr=False):
    """Create the corresponding {0,1} matrix to the mixture list"""
    k = len(flatten_list(mix_list))
    Ky = zeros((k,k))
    off_diag = 1.0-noise
    for block in mix_list:
        Ky[ix_(block,block)] = off_diag
    for ix in range(k):
        Ky[ix,ix] = 1.0
    if neg_corr:
        num_ones = sum(sum(Ky))
        neg_val = -num_ones/(k*k)
        Ky[nonzero(Ky==0)] = neg_val
        Ky = normalize_unit_diag(Ky+1e-5*eye(k))
    return Ky

def flatten_list(general_list):
    """Convert a list of lists into one list. Works recursively."""
    if isinstance(general_list, list):
        return sum((flatten_list(x) for x in general_list), [])
    else:
        return [general_list]

def pair2vec(context, value):
    """Convert a pair of (context, value) into a vector
    with value in element context.
    Assume that context is in range()
    """
    vec = zeros((max(context)+1, len(context)))
    for (ix,cur) in enumerate(zip(context, value)):
        vec[cur[0],ix] = cur[1]
    return vec

def plot_2d(train_ex, train_lab):
    """Plot 2D data"""
    from matplotlib.pyplot import plot
    
    pos_lab = train_lab > 0.0
    neg_lab = train_lab < 0.0
    plot(train_ex[0,pos_lab], train_ex[1,pos_lab], 'r+')
    plot(train_ex[0,neg_lab], train_ex[1,neg_lab], 'bx')
    
def plot_2d_pred(x, y, svmout, train_ex, train_lab, axis_limit, title_text):
    """Plot 2D data, along with prediction contours."""
    from matplotlib.pyplot import figure, pcolor, contour, axis, title

    figure()
    pcolor(x, y, svmout)
    contour(x, y, svmout, linewidths=1, colors='black', hold=True)
    plot_2d(train_ex, train_lab)
    axis(axis_limit)
    title(title_text)

def get_gauss_widths(examples, num_widths, start_quantile=0.1, stop_quantile=0.9):
    """Return a candidate set of possible widths
    for use in the Gaussian kernel.
    """
    distance = sqr_dist(examples, examples)
    # get the sorted distances (ignoring distances to self)
    dm = sort(distance.flatten())
    dm = square(dm[examples.shape[1]:])
    if num_widths == 1:
        widths = median(dm)
    else:
        # find points uniformly spaces in log scale
        start_idx = round(start_quantile*len(dm))
        stop_idx = round(stop_quantile*len(dm))
        dm = log(dm)
        widths = exp(linspace(dm[stop_idx],dm[start_idx],num_widths))
    widths = 1/widths
    print(widths)
    return widths

def compare_pred(pred, label, show='rand', num_show=10):
    """Print a small number of predictions and labels"""
    if show == 'rand':
        idx = permutation(len(label))[:num_show]
    else:
        idx = array(range(num_show))
    print(vstack([idx,vstack([pred,label])[:,idx]]))
    
            
