"""Gaussian process regression"""

from numpy import array, matrix, kron, diag, ones, real, dot, ndarray
from numpy import zeros, eye, hstack, concatenate, isnan, sum, log, pi, inner, outer, ones_like, trace
from numpy.linalg import solve, inv, cholesky, lstsq
from numpy.linalg import eig
from scipy.linalg.matfuncs import sqrtm

from optwok.kernel import ContextKernel
from numpy.core.numeric import newaxis

#import ipdb

# TODO: general kernel function
#       gp.update to rank one updates

class GaussianProcess(object):
    """Gaussian Process regression,
    for objects which are pairs of (data, context)"""
    def __init__(self, Ks, Kz, kparam = None, reg_param = 1.0):
        if kparam is not None:
            self.Ks = Ks(kparam)
            self.Ks_pred = Ks(kparam)
        else:
            self.Ks = Ks()
            self.Ks_pred = Ks()
        self.Kz = Kz
        self.Ktrain = ContextKernel(self.Ks, self.Kz)
        self.Kpred = ContextKernel(self.Ks_pred, self.Kz)
        self.train_ex = array([])
        self.train_cont = array([], dtype = int)
        self.train_lab = array([])

        self.reg_param = reg_param
        self.num_train = 0
        self.num_context = Kz.shape[0]

        self.alpha = array([])
        self.L = array([])

    def train(self, train_ex, train_cont, train_lab):
        """Learn the parameters.
        train_ex is a tuple containing (example, context)
        """
        self.train_ex = train_ex
        self.train_cont = array(train_cont, dtype = int)
        self.train_lab = array(train_lab)
        self.num_train = self.train_ex.shape[1]
        self._update()

    def _update(self):
        """Computations to estimate GP parameters"""
        self.Ktrain.compute(self.train_ex, self.train_cont)
        #self.L = real(sqrtm(self.Ktrain.kmat + self.reg_param*eye(self.num_train)))
        self.L = cholesky(self.Ktrain.kmat + self.reg_param * eye(self.num_train))
        (x, residues, rank, sing) = lstsq(self.L, self.train_lab)
        (self.alpha, residues, rank, sing) = lstsq(self.L.T, x)

    def update(self, orig_ex, new_cont, new_lab):
        """Include the new example into training data"""
        new_ex = orig_ex.copy()
        new_ex.shape = (len(new_ex), 1)
        self.train_ex = hstack([self.train_ex, new_ex])
        self.train_cont = concatenate([self.train_cont, array([new_cont], dtype = int)])
        self.train_lab = concatenate([self.train_lab, array([new_lab])])
        self.num_train += 1
        assert len(self.train_cont) == self.num_train
        self._update()

    def predict(self, pred_data):
        """Estimate the outputs"""
        pred_ex = pred_data[0]
        pred_cont = array(pred_data[1], dtype = int)
        num_pred = pred_ex.shape[1]
        self.Kpred.compute(pred_ex, pred_cont)
        if self.num_train == 0:
            scores = zeros(num_pred)
            pred_var = diag(self.Kpred.kmat)
        else:
            Kmat_cross = self.Ktrain.compute_test(pred_ex, pred_cont)
            scores = dot(Kmat_cross.T, self.alpha).flatten()
            (v, residues, rank, sing) = lstsq(self.L, Kmat_cross)
            pred_var = diag(self.Kpred.kmat - dot(v.T, v))
        if not (pred_var > 0).all():
            print('=== Predictive variance negative!')
            #ipdb.set_trace()

        return scores, pred_var

class GaussianProcessScalar(object):
    """Gaussian Process regression"""
    def __init__(self, Ks, kparam = None, reg_param = 1.0, m=0):
        ''' Ks: Kernel Object, kparam: Kernel Parameters, reg_param: noise in the data,
            m: constant mean '''
        if kparam is not None:
            self.Ktrain = Ks(kparam)
            self.Kpred = Ks(kparam)
        else:
            self.Ktrain = Ks()
            self.Kpred = Ks()
        self.train_ex = array([])
        self.train_lab = array([])

        self.reg_param = reg_param
        self.num_train = 0

        self.inv_cov = matrix([])
        self.alpha = array([])
        self.L = matrix([])
        self.m = m
        self.kparam = kparam

    def train(self, train_ex, train_lab):
        """Learn the parameters."""
        self.train_ex = train_ex
        self.train_lab = matrix(train_lab)
        try:
            self.num_train = self.train_ex.shape[1]
        except IndexError:  # 1-dimensional
            self.num_train = self.train_ex.shape[0]
            self.train_ex = self.train_ex[newaxis, :]

        self.train_lab.shape = (self.num_train, 1)
        self.Ktrain.compute(self.train_ex)
        self.L = cholesky(self.Ktrain.kmat + self.reg_param * eye(self.num_train))
        self.alpha = solve(self.L.T, solve(self.L, self.train_lab - self.m))

    def predict(self, pred_ex):
        """Estimate the outputs"""
        try:
            num_pred = pred_ex.shape[1]
        except IndexError:  # 1-d
            pred_ex = pred_ex[newaxis, :]

        self.Kpred.compute(pred_ex)
        Kmat_cross = self.Ktrain.compute_test(pred_ex)
        scores = array(Kmat_cross.T * self.alpha)
        v = matrix(solve(self.L, Kmat_cross))
        pred_var = diag(self.Kpred.kmat - v.T * v)
        return self.m + scores.flatten(), pred_var
    
    def nl_likelihood(self):
        return array((self.train_lab - self.m).T.dot(self.alpha)/2 + sum(log(diag(self.L))) + self.num_train * log(2*pi)/2)[0][0]
    
    def grad_nl_likelihood(self):
        Q = solve(self.L, eye(self.num_train)) / self.reg_param - outer(self.alpha, self.alpha)
        dnoise = self.reg_param * trace(Q)
        dm = dot(ones_like(self.train_ex),array(self.alpha))[0][0]
        grad =[dnoise, dm]
        dkparam = self.Ktrain.compute_grad()
        for dkp in dkparam:
            grad.append(sum(sum(Q*dkp))/2)
        
        return array(grad)

