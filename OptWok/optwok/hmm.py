"""
A module to create, train, and apply Hidden Markov Models (HMMs)
with multiple observed variables per hidden state.

Only consider finite state models.
"""
import numpy
from numpy import array, zeros, ones, dot
from numpy import argmax, log, exp
from numpy.random import uniform

class HMMMulti(object):
    """
    HMM with multiple observed variables per hidden variable.

    A - hidden state transition matrix
    B - observed state distribution (there are K of them)
    """
    def __init__(self, hidden_symbols, observed_symbols):
        """
        Construct an initial HMM.
        hidden_symbols is a tuple containing the states of the hidden variable
        observed_symbols is a dictionary representing the multiple observed variables.
          Each key is a name of the observed variable
          Each value is a tuple corresponding to the events for the observed variable.
        """
        self.hidden_symbols = hidden_symbols
        num_hidden = len(self.hidden_symbols)
        self.hidden2index = dict( zip(self.hidden_symbols, range(num_hidden)) )

        raw_A = uniform( size = num_hidden*num_hidden )
        raw_A = raw_A.reshape( ( num_hidden, num_hidden ) )
        self.A = _normalise(raw_A)

        self.observed_symbols = observed_symbols
        self.B = dict()
        self.observed2index = dict()
        for obs,states in self.observed_symbols.items():
            num_states = len(states)
            raw_B = uniform( size = num_hidden*num_states )
            raw_B = raw_B.reshape( (num_hidden, num_states) )
            self.B[obs] = _normalise(raw_B)
            self.observed2index[obs] = dict( zip(states, range(num_states)) )
        self.prior = ones(self.num_hidden_states)/float(self.num_hidden_states)        

    def __repr__(self):
        out = '<HMMMulti> START\n'
        out += 'Hidden states\n'
        out += str(self.hidden_symbols)
        out += '\n'
        out += 'Prior\n'
        out += str(self.prior)
        out += '\n'
        out += str(self.A)
        out += '\n'
        out += 'Observed states\n'
        out += str(self.observed_symbols)
        out += '\n'
        for obs,states in self.observed_symbols.items():
            out += '%s (%d states)\n' % (obs, len(states))
            out += str(self.B[obs])
            out += '\n'
        out += '<HMMMulti> END\n'
        return out

    @property
    def num_hidden_states(self):
        return len(self.hidden_symbols)

    @property
    def num_observed_states(self):
        num_states = dict()
        for key,val in self.observed_symbols.items():
            num_states[key] = len(val)
        return num_states

    @property
    def num_variables(self):
        return len(self.observed.values()[0])

    def check_parameters(self):
        """Check that the dimensions are matching for:
        - prior on hidden states
        - transition probabilities
        - emission probabilities
        """
        num_prior = len(self.prior)
        num_trans, num_trans_t = self.A.shape
        assert num_trans == num_trans_t, 'Transition matrix not square'
        assert num_trans == self.num_hidden_states, 'Transition matrix does not match number of symbols'
        for emit in self.B.keys():
            num_out_h, num_out = self.B[emit].shape
            assert num_out_h == num_trans, 'Transition and emission matrix not matching'
            assert num_out == len(self.observed_symbols[emit]),\
                'Emission matrix does not match number of symbols'

    def set_observed(self, observed):
        """Copy the observed variables into the object.
        This determines the number of hidden and observed variables.
        Also pre-compute the alpha and beta messages.
        """
        self.observed = dict()
        for emit in observed.keys():
            val = []
            for item in observed[emit]:
                val.append(self.observed2index[emit][item])
            self.observed[emit] = array(val)
        self.alpha = zeros((self.num_hidden_states, self.num_variables))
        self.beta = zeros((self.num_hidden_states, self.num_variables))
        self.forward()
        self.backward()

    def log_likelihood(self):
        """Compute the log likelihood of the observed sequence"""
        return -numpy.sum(log(self.scale))

    def posterior(self, t):
        """Return the posterior distribution of the hidden variable
        at time t, given the observed sequence"""
        #return self.alpha[:,t]*self.beta[:,t]/exp(self.log_likelihood())
        return self.alpha[:,t]

    def forward(self):
        """Return the probability of the observation sequences given the model"""
        self.scale = zeros(self.num_variables)
        self.alpha[:,0] = self.prior
        for emit in self.B.keys():
            self.alpha[:,0] *= self.B[emit][:, self.observed[emit][0]]
        self.scale[0] = 1.0/numpy.sum(self.alpha[:,0])
        self.alpha[:,0] *= self.scale[0]
        for t in range(1, self.num_variables):
            self.alpha[:,t] = dot(self.alpha[:,t-1], self.A)
            for emit in self.B.keys():
                self.alpha[:,t] *= self.B[emit][:, self.observed[emit][t]]
            self.scale[t] = 1.0/numpy.sum(self.alpha[:,t])
            self.alpha[:,t] *= self.scale[t]

    def backward(self):
        """Return the probability of the partial observation sequence
        from time t+1 to the end T, given the state S at time t and the model"""
        self.beta[:,-1] = self.scale[-1]*ones(self.num_hidden_states)
        for t in reversed(range(self.num_variables-1)):
            prod = self.beta[:,t+1]
            for emit in self.B.keys():
                prod *= self.B[emit][:, self.observed[emit][t]]
            self.beta[:,t] = self.scale[t]*dot(self.A, prod)

    def max_marginal(self):
        """Return the best set of hidden variable states.
        Choose states which are individually most likely
        """
        marg = []
        for t in range(self.num_variables):
            marg.append(self.hidden_symbols[argmax(self.posterior(t))])
        return array(marg)
        
    def max_aposteriori(self):
        """Return the set of hidden variable states with highest probability.
        Uses the Viterbi algorithm.
        """
        raise NotImplementedError

def _normalise(raw):
    """Normalise a probability distribution"""
    return ( raw.T / raw.T.sum( 0 ) ).T

