"""Base classes for classifiers"""

from time import time
import numpy
from numpy import array, matrix, zeros, nonzero

class Classifier(object):
    """
    Implementation of the optimization methods for the
    empirical loss + regularizer
    framework, for binary clssification.

    We only consider linear classifiers.
    """
    def __init__(self, reg_param=1.0, kdict=None, verbose=False):
        assert(reg_param > 0.0)
        self.reg_param = reg_param     # Tradeoff regularizer vs loss
        self.regularizer = None
        self.loss = None

        self.kdict = kdict             # kernel
        self.train_ex = array([])      # training examples
        self.y = array([])             # labels
        self.train_time = 0.0          # time used for training
        
        self.w = array([])             # weight
        self.b = array([])             # bias
        self.epsilon = 1e-6            # how small is zero?
        self.verbose = verbose         # how much output

    @property
    def num_examples(self):
        return len(self.y)

    @property
    def num_feat(self):
        return self.train_ex.shape[0]
    
    @property
    def train_out(self):
        """Output of the classifier on the training set"""
        return self.get_svm_out(self.train_ex)
    
    @property
    def margin(self):
        """The value of the regularizer"""
        return self.regularizer.margin(self.w)

    @property
    def emp_risk(self):
        """A vector of the empirical loss"""
        return self.loss.get_emp_risk(self.y*self.train_out)

    @property
    def primal_obj(self):
        """Directly compute the primal objective function."""
        if self.verbose:
            print('Margin: %f, Risk: %f' % (self.margin,
                                            self.reg_param*sum(self.emp_risk)))
        obj = self.margin + self.reg_param*sum(self.emp_risk)
        return obj

    @property
    def max_weight(self):
        """The maximum absolute value of the weight."""
        return max(abs(self.w))[0]

    @property
    def frac_sv(self):
        """The fraction of examples used as support vectors."""
        return self.loss.get_frac_sv(self.y*self.train_out)

    @property
    def nonzero_feat(self):
        """The features corresponding to non-zero parameters"""
        return numpy.where(abs(self.w)>self.epsilon)[0]
    
    @property
    def frac_feat(self):
        """The fraction of features used in the final classifier,
        i.e. the number of non-zero parameters"""
        return float(len(self.nonzero_feat))/float(len(self.w))

    @property
    def frac_total(self):
        """The fraction of training data used in the final classifier,
        taking both features and support vectors into account."""
        num_sv = float(self.loss.get_num_sv(self.y*self.train_out))
        num_feat = float(len(numpy.where(abs(self.w)>self.epsilon)[0]))
        return (num_sv*num_feat) / (float(self.train_ex.shape[1])*float(len(self.w)))

    @property
    def perf(self):
        """Summary of results"""
        perf = {'max_weight': self.max_weight,
                'nonzero_feat': self.nonzero_feat,
                'frac_feat': self.frac_feat,
                'frac_sv': self.frac_sv,
                'frac_total': self.frac_total,
                'train_time': self.train_time,
                'objective': self.primal_obj,
                }
        return perf

    def get_svm_out(self, X):
        """Compute K*alpha+b"""
        return array(matrix(self.w).T*matrix(X) + self.b).flatten()

    def solve_opt(self):
        """solve_opt should the implemented in child class"""
        pass
    
    def train(self, train_ex, train_lab):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""

        self.train_ex = compute_features(train_ex, self.kdict)
        self.y = train_lab

        t_start = time()
        self.solve_opt()
        t_end = time()

        # Collect statistics about optimization
        self.train_time = t_end-t_start

        if self.verbose: print('%f support vectors' % self.frac_sv)
        if self.verbose: print('Primal Objective: %f' % self.primal_obj)

    def predict(self, pred_ex):
        """Wrapper to do prediction"""
        return self.get_svm_out(compute_features(pred_ex, self.kdict))

    
class ConvexClassifier(Classifier):
    """
    Classifiers which have convex training methods
    """
    def __init__(self, reg_param, kdict, verbose):
        Classifier.__init__(self, reg_param, kdict, verbose)
        self.num_iter = 1










class Loss(object):
    """A loss function, for classification"""
    epsilon = 0.01

    def gradient(self, w):
        """The vector of gradients"""
        raise NotImplementedError

class HingeLoss(Loss):
    """
    Standard SVM loss
    """
    def get_emp_risk(self, t):
        """Compute the empirical risk, for each example."""
        (sloped, correct) = self.get_loss_case(t)
        emp_risk = zeros(len(t))
        emp_risk[sloped] = 1.0 - t[sloped]
        return emp_risk

    def get_num_sv(self, t):
        """Estimate the number of support vectors"""
        (sloped, correct) = self.get_loss_case(t)
        return len(sloped)

    def get_frac_sv(self, t):
        """Estimate the fraction of support vectors"""
        num_sv = self.get_num_sv(t)
        frac_sv = float(num_sv)/float(len(t))
        return frac_sv

    def get_loss_case(self, t):
        """Detect whether we are on the slope,
        or classified correctly (larger than margin)."""
        sloped = numpy.where(t <= 1.0+self.epsilon)[0]
        correct = numpy.where(t > 1.0+self.epsilon)[0]
        return (sloped, correct)

    def gradient(self, x, y, t):
        """The gradient of a single example"""
        if t <= 1.:
            return -y*x
        else:
            return zeros(x.shape)

class HingeLossKernel(Loss):
    """
    Standard SVM loss, with matrix inner product
    """
    def __init__(self, M):
        self.M = M
        
class HuberHingeLoss(Loss):
    """
    Huberized hinge loss, to make things differentiable
    """
    def __init__(self, delta=0.5):
        """The loss is quadratic in window delta"""
        self.delta = delta
        
    def get_loss_case(self, t):
        """Detect the three cases,
        correct, curved, sloped
        """
        sloped = numpy.where(t <= 1.0-self.delta)[0]
        curved = numpy.where(abs(1-t)<=self.delta)[0]
        correct = numpy.where(t > 1.0+self.delta)[0]
        return (sloped, curved, correct)

    def gradient(self, x, y, t):
        """The gradient of a single example"""
        if t <= 1.0-self.delta:
            return -y*x
        elif t > 1.0+self.delta:
            return zeros(x.shape)
        else:
            return (-0.5*y/self.delta)*(1+self.delta-t)*x
        
class RampLoss(Loss):
    """
    Truncated hinge loss
    """
    def __init__(self, mu):
        """The parameter mu controls the slope between [0,2]"""
        assert((mu > 0.0) and (mu <= 1.0))
        self.mu = mu
        
    def get_emp_risk(self, t):
        """Compute the empirical risk, for each example."""
        (clipped, sloped, correct) = self.get_loss_case(t)
        emp_risk = zeros(len(t))
        emp_risk[clipped] = self.mu + 1.0
        emp_risk[sloped] = 1.0 - (t[sloped]/self.mu)
        return emp_risk

    def get_num_sv(self, t):
        """Estimate the number of support vectors"""
        (clipped, sloped, correct) = self.get_loss_case(t)
        return len(sloped)

    def get_frac_sv(self, t):
        """Estimate the fraction of support vectors"""
        num_sv = self.get_num_sv(t)
        frac_sv = float(num_sv)/float(len(t))
        return frac_sv

    def get_loss_case(self, t):
        """Detect whether we are at saturation, on the slope,
        or classified correctly (larger than margin)."""
        clipped = numpy.where(t<-(self.mu*self.mu)-self.epsilon)[0]
        lb = numpy.where(t>=-(self.mu*self.mu)-self.epsilon, 1, 0)
        ub = numpy.where(t<=self.mu+self.epsilon, 1, 0)
        sloped = numpy.where((lb+ub)>1)[0]
        correct = numpy.where(t>self.mu+self.epsilon)[0]
        return (clipped, sloped, correct)

class SlantLoss(RampLoss):
    """
    Hinge loss with double kink
    """
    def get_emp_risk(self, t):
        """Compute the empirical risk, for each example."""
        (clipped, sloped, correct) = self.get_loss_case(t)
        emp_risk = zeros(len(t))
        emp_risk[clipped] = 0.5*(3.0-t[clipped])
        emp_risk[sloped] = 1.0 - t[sloped]

        return emp_risk

class Regularizer(object):
    """
    A regularizer class
    """
    def margin(self, w):
        """The value of the regularizer, intuitively the width of the margin"""
        raise NotImplementedError

    def gradient(self, w):
        """The vector of gradients"""
        raise NotImplementedError

class Reg_l2(Regularizer):
    """The l2 norm of a vector"""
    def margin(self, w):
        """The value of the regularizer, intuitively the width of the margin"""
        return 0.5*numpy.linalg.norm(w)*numpy.linalg.norm(w)
        
    def gradient(self, w):
        """The vector of gradients"""
        return w

class Reg_l1(Regularizer):
    """The l1 norm of a vector"""
    def margin(self, w):
        """The value of the regularizer, intuitively the width of the margin"""
        return sum(abs(w))
        
    def gradient(self, w):
        """The vector of gradients"""
        return sign(w)

class Reg_l1cap(Regularizer):
    """The l1 norm of a vector"""
    def __init__(self, a=1.0):
        """'a' is the value of the cap"""
        assert(a > 0.0)
        self.a = a
    
    def margin(self, w, a=None):
        """The value of the regularizer, intuitively the width of the margin"""
        if a is not None:
            assert(a > 0.0)
            self.a = a
        cap_l1 = abs(w)
        cap_l1[nonzero(cap_l1 > self.a)[0]] = self.a
        return sum(cap_l1)
        
    def gradient(self, w, a=None):
        """The vector of gradients"""
        if a is not None:
            assert(a > 0.0)
            self.a = a
        cap_l1 = abs(w)
        grad = sign(w)
        grad[nonzero(cap_l1 > self.a)[0]] = 0.
        return grad

class Reg_l0(Regularizer):
    """The l1 norm of a vector"""
    def __init__(self, a=1.0):
        """'a' is the coefficient of the exponent"""
        assert(a > 0.0)
        self.a = a
    
    def margin(self, w, a=None):
        """The value of the regularizer, intuitively the width of the margin"""
        if a is not None:
            assert(a > 0.0)
            self.a = a
        return sum(1.0 - numpy.exp(-self.a*abs(w)))
        
    def gradient(self, w, a=None):
        """The vector of gradients"""
        if a is not None:
            assert(a > 0.0)
            self.a = a
        return -self.a*sign(w)*numpy.exp(-self.a*abs(w))

class Reg_elastic_net(Regularizer):
    """The elastic net regularizer (l1+l2)"""
    def margin(self, w):
        """The value of the regularizer"""
        return 0.5*numpy.linalg.norm(w)*numpy.linalg.norm(w) + sum(abs(w))

    def gradient(self, w):
        """The vector of gradients"""
        return w + sign(w)

class Reg_graph_net(Regularizer):
    """The elastic net regularizer, with graph laplacian in l2"""
    def __init__(self, M):
        self.M = M
        
    def margin(self, w):
        """The value of the regularizer"""
        return 0.5*dot(dot(w,self.M),w) + sum(abs(w))

    def gradient(self, w):
        """The vector of gradients"""
        return dot(self.M,w) + sign(w)

    
def compute_features(train_ex, kdict):
    """Explicitly compute the feature space of the kernel"""
    if kdict['name'] == 'linear':
        return train_ex
    elif kdict['name'] == 'poly':
        if kdict['inhomog'] == True:
            train_ex = vstack([train_ex, ones((1,train_ex.shape[1]))])
        phi = get_poly_features(train_ex, kdict['param'])
        return phi
    else:
        print('Explicit feature construction for kernel %s not implemented' % kdict['name'])

