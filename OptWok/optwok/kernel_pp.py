"""Implement some positive semidefinite kernels.

These are in pure python. For the cython implementation, see kernelbase.pyx
"""

from numpy import array, matrix, max, ix_
from numpy import zeros, ones, eye, diag, empty
from numpy import average, exp, kron, sum
from numpy import sqrt, divide, mean, multiply
from numpy import pi, sin, cos
from numpy import flatnonzero

class Kernel(object):
    """A Mercer kernel. Note that if Cython is available, then this is not used."""
    def __init__(self):
        self.kmat = array([])
        self.examples = None

    def __getitem__(self, idx):
        assert(len(idx)==2)
        return self.kmat[idx[0], idx[1]]

    def __repr__(self):
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def dump(self) :
        """
        returns a string that can be used to construct an equivalent object
        """
        kstr = self.__module__ + '.' + self.__class__.__name__ + '(' + \
               self.construction_params() + ')'
        return kstr

    def construction_params(self):
        raise NotImplementedError

    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        raise NotImplementedError

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        raise NotImplementedError


class JointKernel(object):
    """A kernel on input and output pairs."""
    def __init__(self, input_kernel, output_kmat=None, num_classes=-1):
        self.Kx = input_kernel
        if output_kmat is not None:
            self.Ky = output_kmat
        else:
            assert(num_classes > 1)
            self.Ky = eye(num_classes)
        self.examples = None

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def __getitem__(self, idx):
        """Simulate matrix element.
        Order is assumed to be examples first then data.
        """
        assert(len(idx)==4)
        return self.Kx[idx[0], idx[1]]*self.Ky[idx[2], idx[3]]

    @property
    def kmat(self):
        """Simulate the whole kernel matrix"""
        return kron(self.Kx.kmat, self.Ky)

    def compute(self, examples):
        self.examples = examples
        self.Kx.compute(examples)

    def compute_test(self, test_ex):
        """Returns a non-square matrix, of num_train by num_test size."""
        Kx = self.Kx.compute_test(test_ex)
        return kron(Kx, self.Ky)

class ContextKernel(Kernel):
    """A kernel of (data, context) pairs,
    mathematically the same as the JointKernel,
    but does not evaluate all combinations"""
    def __init__(self, input_kernel, output_kmat):
        self.Kx = input_kernel
        self.num_context = output_kmat.shape[0]
        self.Ky = output_kmat
        self.examples = None
        self.num_examples = 0
        self.context = None

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>'
        return rep

    def __getitem__(self, idx):
        """Simulate matrix element.
        Order is assumed to be (example,context,example,context)
        """
        assert(len(idx)==4)
        return self.Kx[idx[0], idx[2]]*self.Ky[idx[1], idx[3]]

    @property
    def kmat(self):
        """Simulate the whole kernel matrix"""
        K = zeros((self.num_examples, self.num_examples))
        for con_x in range(self.num_context):
            for con_y in range(self.num_context):
                ix = flatnonzero(self.context == con_x)
                iy = flatnonzero(self.context == con_y)
                if len(ix)>0 and len(iy)>0:
                    Ky_val = self.Ky[self.context[ix[0]], self.context[iy[0]]]
                    if abs(Ky_val) > 0.0:
                        K[ix_(ix,iy)] = self.Kx[ix_(ix,iy)]*Ky_val
                        K[ix_(iy,ix)] = self.Kx[ix_(iy,ix)]*Ky_val
        return K

    @property
    def kmat_forloop(self):
        """Simulate the whole kernel matrix"""
        K = zeros((self.num_examples, self.num_examples))
        for ix in range(self.num_examples):
            for iy in range(ix, self.num_examples):
                val = self.Kx[ix,iy]*self.Ky[self.context[ix], self.context[iy]]
                K[ix,iy] = val
                K[iy,ix] = val
        return K

    def compute(self, examples, context):
        self.examples = examples
        self.context = context
        self.num_examples = examples.shape[1]
        self.Kx.compute(examples)

    def compute_test(self, examples, context):
        """Returns a non-square matrix, of num_train by num_test size."""
        assert self.examples is not None
        assert self.context is not None
        num_test = examples.shape[1]
        Kx_cross = self.Kx.compute_test(examples)
        Kmat_cross = zeros((self.num_examples, num_test))
        for con_x in range(self.num_context):
            for con_y in range(self.num_context):
                ix = flatnonzero(self.context == con_x)
                iy = flatnonzero(context == con_y)
                if len(ix)>0 and len(iy)>0:
                    Ky_val = self.Ky[self.context[ix[0]], context[iy[0]]]
                    if abs(Ky_val) > 0.0:
                        Kmat_cross[ix_(ix,iy)] = Kx_cross[ix_(ix,iy)]*Ky_val
        return Kmat_cross

    def compute_test_forloop(self, examples, context):
        """Returns a non-square matrix, of num_train by num_test size."""
        assert self.examples is not None
        assert self.context is not None
        num_test = examples.shape[1]
        Kx_cross = self.Kx.compute_test(examples)
        Kmat_cross = zeros((self.num_examples, num_test))
        for ix in range(self.num_examples):
            for iy in range(num_test):
                Kmat_cross[ix,iy] = Kx_cross[ix,iy]*self.Ky[self.context[ix], context[iy]]
        return Kmat_cross

class LinearKernel(Kernel):
    """Standard dot product"""
    def __init__(self):
        Kernel.__init__(self)
        
    def construction_params(self):
        return ''
    
    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        self.examples = examples
        self.kmat = array(matrix(examples).T*matrix(examples))

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        if test_ex.ndim == 1:
            test_ex.shape = (len(test_ex),1)
        return array(matrix(self.examples).T*matrix(test_ex))

class SEKernel(Kernel):
    ''' Squared Exponential Kernel '''
    def sqr_dist(self, a, b):
        """Compute the square distance between vectors"""
        assert(a.shape[0] == b.shape[0])
        if len(a.shape) > 1:
            # a is a matrix
            dot_a = sum(a * a, axis = 0).T
            dot_b = sum(b * b , axis = 0).T
            unitvec = ones(dot_a.shape)
            D = array(2.0 * matrix(a).T * matrix(b))
            for ix, bval in enumerate(dot_b):
                D[:, ix] = dot_a - D[:, ix] + kron(bval, unitvec)
        else:
            D = zeros((len(a), len(b)))
            for (ixa, cur_a) in enumerate(a):
                for (ixb, cur_b) in enumerate(b):
                    D[ixa, ixb] = (cur_a - cur_b) * (cur_a - cur_b)

        return D

class GaussKernel(SEKernel):
    """Gaussian rbf kernel"""
    def __init__(self, gamma=1.0):
        Kernel.__init__(self)
        self.gamma = gamma

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>\n'
        rep += 'gamma : ' + str(self.gamma)
        return rep

    def constructionParams(self) :
        return 'gamma = ' + str(self.gamma)

    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        self.examples = examples
        self.kmat = exp(-self.gamma * self.sqr_dist(examples, examples))

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        if test_ex.ndim == 1:
            test_ex.shape = (len(test_ex),1)
        return exp(-self.gamma * self.sqr_dist(self.examples, test_ex))



class PeriodicKernel(SEKernel):
    """ Periodic kernel
    k(x,y) = theta * exp( -2*sin^2( pi*||x-y||/P )/lamb^2 )
    """
    def __init__(self, theta=1.0, lamb=1.1, P=pi):
        Kernel.__init__(self)
        self.theta = theta
        self.lamb = lamb
        self.P = P

    def __repr__(self) :
        rep = '<' + self.__class__.__name__ + ' instance>\n'
        rep += 'theta : ' + str(self.theta)
        rep += ', lambda : ' + str(self.lamb)
        rep += ', P : ' + str(self.P)
        return rep

    def constructionParams(self) :
        return 'theta = ' + str(self.theta) + ', lambda : ' + str(self.lamb) + ', P : ' + str(self.P)

    def compute_pk(self, ex1, ex2):
        ''' Evaluate kernel for ex1 and ex2 '''
        K = sin(pi * sqrt(self.sqr_dist(ex1, ex2)) / self.P) / self.lamb
        K = K * K
        return self.theta * exp(-2 * K)

    def compute(self, examples):
        """Evaluate the kernel function and store the value"""
        self.examples = examples
        self.kmat = self.compute_pk(examples, examples)

    def compute_test(self, test_ex):
        """Evaluate the kernel function on test examples"""
        if test_ex.ndim == 1:
            test_ex.shape = (len(test_ex), 1)
        return self.compute_pk(self.examples, test_ex)

    def compute_grad(self):
        K = pi * sqrt(self.sqr_dist(self.examples, self.examples)) / self.P
        R = sin(K) / self.lamb
        sKs = R * R
        
        dtheta = 2 * self.theta * exp(-2*sKs)
        dP = 4 * self.theta / self.lamb * exp(-2 * R*R) * R * cos(K) * K
        dlamb = 4 * self.theta * exp(-2*sKs) * sKs
        
        return dtheta, dlamb, dP


class CustomKernel(Kernel):
    """Just a positive semidefinite matrix"""
    def __init__(self):
        Kernel.__init__(self)

    def compute(self, examples):
        """Assume input is a kernel matrix.
        Copy the training kernel matrix"""
        self.kmat = matrix(examples)

    def compute_test(self, test_ex):
        """Assume input is a kernel matrix.
        Just return the test kernel"""
        return test_ex


def KernelFactory(kdict):
    """Some if then statements to map between
    the strings describing the kernels to the actual implementation
    """
    if kdict['name'] == 'gauss':
        return GaussKernel(kdict['param'])
    if kdict['name'] == 'custom':
        return CustomKernel()
    else:
        print('%s not parsed properly in KernelFactory' % kdict['name'])
