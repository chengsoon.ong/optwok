"""Helper functions for expttools"""

import sys
python_version = sys.version_info[:2]

def _wrap_kwargs(kwargs):
    """Small helper function to enable Pool"""
    function_handle = kwargs['_function_handle']
    del kwargs['_function_handle']
    return function_handle(**kwargs)



if python_version >= (3, 0):
    def _pickle_method(method):
        func_name = method.__func__.__name__
        obj = method.__self__
        cls = method.__self__.__class__
        return _unpickle_method, (func_name, obj, cls)

    def _unpickle_method(func_name, obj, cls):
        for cls in cls.mro():
            try:
                func = cls.__dict__[func_name]
            except KeyError:
                pass
            else:
                break
        return func.__get__(obj, cls)
    
else:
    def _pickle_method(method):
        """A workaround for inability to pickle instance methods"""
        func_name = method.im_func.__name__
        obj = method.im_self
        cls = method.im_class
        return _unpickle_method, (func_name, obj, cls)

    def _unpickle_method(func_name, obj, cls):
        """A workaround for inability to pickle instance methods"""
        for cls in cls.mro():
            try:
                func = cls.__dict__[func_name]
            except KeyError:
                pass
            else:
                break
        return func.__get__(obj, cls)

