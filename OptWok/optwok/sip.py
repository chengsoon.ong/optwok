"""A collection of classes for semi-infinite programming"""

from __future__ import print_function
import sys
try:
    import cvxopt.solvers
    from cvxopt import matrix as cm
    from cvxopt import sparse as cs
except ImportError:
    import sys
    print('Cannot import cvxopt', end='\n', file=sys.stderr)

from numpy import matrix, array, dot, vstack, hstack, kron
from numpy import zeros, ones, mean, std, sign, eye, nonzero

import sys
#sys.path.append('/cluster/home/infk/cong/Install/mosek/6/tools/platform/linux64x86/python/2')

have_mosek = True
try:
    import mosek
except ImportError as error:
    print(error, end='\n', file=sys.stderr)
    have_mosek = False

class SIP(object):
    """A wrapper to cvxopt solver.
    Allow an iterative addition of constraints to the original problem.

    The cvxopt solver expects the following format:

    min_x   c'x
    st      Gx <= h
            Ax = b

    min_x   1/2 x'Px + q'x
    st      Gx <= h
            Ax = b

    """
    def __init__(self, num_vars, verbose=False, use_mosek=False):
        assert num_vars > 0.0, 'Need at least one variable'

        self.num_vars = num_vars
        self.stop_eps = 1e-7

        self.old_w = None
        
        self.P = cm(0.0, (num_vars, num_vars))
        self.q = cm(0.0, (num_vars, 1))
        self.G = cm(0.0, (0, num_vars))
        self.h = cm(0.0, (0, 1))
        self.A = cm(0.0, (0, num_vars))
        self.b = cm(0.0, (0, 1))

        self.solution = None
        self.lag_eq = None
        self.cur_obj = None
        self.optimal = False

        self.verbose = verbose
        self.use_mosek = use_mosek

    def add_ineq_constraints(self, G_new, h_new):
        """Concatenate the new inequality constraints"""
        assert G_new.shape[1] == self.num_vars,  'Size mismatch in inequality constraint (lhs)'
        assert h_new.shape[1] == 1,              'Size mismatch in inequality constraint (rhs)'
        assert G_new.shape[0] == h_new.shape[0], 'Size mismatch in inequality constraint'
        self.G = cm(vstack([array(self.G), G_new]))
        self.h = cm(vstack([array(self.h, dtype = 'd'), array(h_new, dtype = 'd')]))

    def add_eq_constraints(self, A_new, b_new):
        """Concatenate the new equality constraints"""
        assert A_new.shape[1] == self.num_vars,  'Size mismatch in equality constraint (lhs)'
        assert b_new.shape[1] == 1,              'Size mismatch in equality constraint (rhs)'
        assert A_new.shape[0] == b_new.shape[0], 'Size mismatch in equality constraint'
        self.A = cm(vstack([array(self.A), A_new]))
        self.b = cm(vstack([array(self.b), b_new]))

    def solve(self, warn=True):
        """Check for empty constraints then invoke solver"""
        res = self.solve_cvxopt()
        if self.verbose: print(res)
        self.solution = array(res['x']).copy().flatten()
        self.lag_eq = array(res['y']).copy().flatten()
        self.cur_obj = res['primal objective']
        if res['status'] == 'optimal':
            self.optimal = True
        else:
            self.optimal = False
            if warn:
                print('sip.py: cvxopt did not converge to optimality')
                print(res)
        del res

class SIQP(SIP):
    """A wrapper to cvxopt QP solver."""
    def __init__(self, num_vars, verbose=False, use_mosek=True):
        SIP.__init__(self, num_vars, verbose, use_mosek)

    def set_objective(self, P, q):
        """Set P and q"""
        assert P.shape == (self.num_vars, self.num_vars), 'Size mismatch in quadratic objective term'
        assert q.shape == (self.num_vars, 1), 'Size mismatch in linear objective term'
        self.P = cm(P)
        self.q = cm(q)

    def solve_cvxopt(self):
        """Check for empty constraints then invoke solver"""
        if self.verbose:
            print('have mosek %s' % str(have_mosek))
            print('use mosek %s' % str(self.use_mosek))
        if have_mosek and self.use_mosek:
            if not self.verbose:
                cvxopt.solvers.options['MOSEK'] = {mosek.iparam.log:0}
            res = cvxopt.solvers.qp(self.P, self.q,
                                    self.G, self.h, self.A, self.b, 'mosek')
        else:
            if not self.verbose:
                cvxopt.solvers.options['show_progress'] = False
            res = cvxopt.solvers.qp(self.P, self.q, self.G, self.h, self.A, self.b)
        return res

class SILP(SIP):
    """A wrapper to cvxopt LP solver."""
    def __init__(self, num_vars, verbose=False, use_mosek=True):
        SIP.__init__(self, num_vars, verbose, use_mosek)

    def set_objective(self, q):
        """Set c"""
        assert q.shape == (self.num_vars, 1), 'Size mismatch in linear objective term'
        self.q = cm(q)

    def solve_cvxopt(self):
        """Check for empty constraints then invoke solver"""
        if self.verbose:
            print('have mosek %s' % str(have_mosek))
            print('use mosek %s' % str(self.use_mosek))
        if have_mosek and self.use_mosek:
            if not self.verbose:
                cvxopt.solvers.options['MOSEK'] = {mosek.iparam.log:0}
            res = cvxopt.solvers.lp(self.q,
                                    self.G, self.h, self.A, self.b, 'mosek')
        else:
            if not self.verbose:
                cvxopt.solvers.options['show_progress'] = False
            res = cvxopt.solvers.lp(self.q, self.G, self.h, self.A, self.b)
        return res


class SILPS(SILP):
    """A wrapper to cvxopt LP solver with sparse matrices for the constraints."""
    def __init__(self, num_vars, verbose=False, use_mosek=True):
        SILP.__init__(self, num_vars, verbose, use_mosek)
    
    def add_ineq_constraints(self, G_new, h_new):
        """Concatenate the new inequality constraints"""
        assert h_new.shape[1] == 1,              'Size mismatch in inequality constraint (rhs)'
        assert G_new.size[1] == self.num_vars,  'Size mismatch in inequality constraint (lhs)'
        assert G_new.size[0] == h_new.shape[0], 'Size mismatch in inequality constraint'
        
        self.G = cs([self.G,G_new])
        self.h = cm(vstack([array(self.h,dtype='d'), array(h_new,dtype='d')]))
        
    def add_eq_constraints(self, A_new, b_new):
        """Concatenate the new equality constraints"""
        assert A_new.size[1] == self.num_vars,  'Size mismatch in equality constraint (lhs)'
        assert b_new.shape[1] == 1,              'Size mismatch in equality constraint (rhs)'
        assert A_new.size[0] == b_new.shape[0], 'Size mismatch in equality constraint'
        
        self.A = cs([self.A, A_new])
        self.b = cm(vstack([array(self.b,dtype='d'), array(b_new,dtype='d')]))
