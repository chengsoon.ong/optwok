"""Collection of routines for solving matrix linear equations"""

from numpy.linalg import eigh
from numpy import dot, outer, ones

def solve_sylvester(A,B,C,symm=False):
    """Solve the Sylvester equation:
    AXB + X = C
    where A and B are symmetric real valued,
    by using the eigenvalue decomposition
    """
    LA, U = eigh(A)
    LB, V = eigh(B)
    frac = dot(dot(U.T,C),V)/(outer(LA,LB)+ones((len(LA),len(LB))))
    sol = dot(dot(U,frac),V.T)
    if symm:
        return 0.5*(sol+sol.T)
    else:
        return sol
