'''
Created on 06.03.2013

@author: Gabriel
'''

import os
from numpy import ndarray, array
from numpy.random import rand, permutation

have_json = True
try:
    import jsonpickle.handlers
    from jsonpickle.handlers import BaseHandler
    from jsonpickle import encode, decode, set_encoder_options
except ImportError as imperr:
    have_json = False
    print(imperr, ": json dump and load won't work")

from types import FunctionType
import string
import datetime as dt
import inspect
from os.path import join as pjoin, isfile
import numpy

dtformat = '%Y-%m-%d_%H-%M-%S'
dformat = '%Y-%m-%d'

if have_json:
    class NumpyFloatHandler(jsonpickle.handlers.BaseHandler):
        def flatten(self, obj, data):
            """
            Converts and round to float and encode
            """
            return round(obj,12)
      
    jsonpickle.handlers.registry.register(numpy.float, NumpyFloatHandler)
    jsonpickle.handlers.registry.register(numpy.float32, NumpyFloatHandler)
    jsonpickle.handlers.registry.register(numpy.float64, NumpyFloatHandler)
    
    class NumpyHandler(BaseHandler):
        def flatten(self,obj,data):
            data['array'] = obj.__repr__()
            return data
        
        def restore(self,obj):
            return eval(obj['array'])
    
    jsonpickle.handlers.registry.register(ndarray,NumpyHandler)
    set_encoder_options('json',indent=4)

def ensure_dir(f):
    d = os.path.dirname(f)
    if d != '':
        if not os.path.exists(d):
            os.makedirs(d)

#def name_experiment(*args,**kwargs):
#    ''' Create a string for an experiment '''
#    fname = slugify('-'.join(['-'.join(map(strify,args)), '-'.join(map(strify_item,sorted(kwargs.items())))]))
#    return fname
    
def check_result(path, result_desc):
    ''' Check if result file exists:
        path: path to result folder
        result_desc: string that identifies the experiment result file
    '''
    return isfile(pjoin(path, result_desc))

def jappend(fname):
    if fname[-5:] != '.json':
        fname = fname + '.json'
        
    return fname

def jexists(fname):
    fnamej = jappend(fname)
    return isfile(fnamej)

def jdump(fname, obj):
    ensure_dir(fname)
    fname = jappend(fname)
    with open(fname, 'w') as f:
        f.write(encode(obj))
    
def jload(fname):
    fname = jappend(fname)
    with open(fname, 'r') as f:
        ftxt = f.read()
        return(decode(ftxt))

def create_perm(path, dataset, description, n_perm, num_ex):
    ''' create n_perm permutations for dataset with num_ex examples and save to file '''
    p = []
    for i in range(n_perm):
        p += [permutation(num_ex).tolist()]
        
    fname = permfile_name(path, dataset, description)
    jdump(fname, p)


def permfile_name(path, dataset, description):
    return pjoin(path, description + '_' + dataset)
    
def slugify(filename):
    ''' convert a string into a valid filename '''
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in filename if c in valid_chars)

def strify_item(item):
    ''' strify dict items '''
    return "%s(%s)" %(strify(item[0]),strify(item[1]))

def strify(anything):
    ''' Recursive application of str to anything s.t. it qualifies as a filename '''
    if isinstance(anything, list) or isinstance(anything, tuple):
        return '_'.join(map(strify, anything))
    elif isinstance(anything, dict):
        return '_'.join(map(strify_item, list(anything.items())))
    elif isinstance(anything, ndarray):
        raise TypeError('Can not convert numpy array to string')
    elif isinstance(anything, FunctionType):
        if anything.__name__ == '<lambda>':
            return str(rand(1)[0])
        else:
            return anything.__name__
    elif isinstance(anything, dt.datetime):
        return anything.strftime(dtformat)
    elif isinstance(anything, dt.date):
        return anything.strftime(dformat)
    elif inspect.isclass(anything):
        return anything.__name__
    else:
        return str(anything)



def _test_jdump():
    a = array([[1,2],[3,4]])
    obj = {'a': a, 'b': 4.3}
    objs = encode(obj)
    print(objs)
    objl = decode(objs)
    print(objl)
    
    jdump('test.txt', obj)
    print(jload('test.txt'))

def _test_strify():
    class FooBar:
        pass
    
    a=[FooBar, [1,2,3]]
    print(FooBar.__name__)
    print(strify(a))

if __name__ == '__main__':
    _test_strify()