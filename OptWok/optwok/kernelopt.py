"""Optimize over the choice of kernel.
For learning the kernel over labels.
"""

from __future__ import print_function
import sys
from time import time
from numpy import zeros, ones, eye, sign, dot
from numpy import matrix, array, diag, trace, outer
from numpy import vstack, unique, maximum, argmax
from numpy.linalg import eigh, norm, solve, lstsq, inv
from numpy import diag, sqrt, divide, logspace, kron
from numpy import min, max, transpose, real
from scipy.linalg.matfuncs import sqrtm
from matplotlib.pyplot import matshow, title, colorbar, savefig, show, gray
from matplotlib.colors import LinearSegmentedColormap
try:
    from cvxopt import matrix as cm
    from cvxopt.solvers import sdp
except ImportError:
    print('Cannot import cvxopt', end='\n', file=sys.stderr)

from optwok.multiclass import StructPredictor
from optwok.multiclass import idx2binvec
from optwok.kernel import normalize_unit_diag, JointKernel
from optwok.io_pickle import load, save
from optwok.mat_blas import solve_sylvester

class KernelOptBase(object):
    """Base class for learning the kernel on labels"""
    def __init__(self, reg_param=1.0, verbose=False):
        assert(reg_param > 0.0)
        self.reg_param = reg_param     # Tradeoff regularizer vs loss
        self.all_reg_param = logspace(-4,4,30)
        self.verbose = verbose         # how much output
        self.Ky = array([])            # kernel on labels
        self.all_Ky = None             # label kernel for whole reg path
        self.get_max = True            # at prediction return the argmax

        self.train_ex = array([])      # training examples
        self.train_lab = array([])     # labels in original format
        self.y = array([])             # labels
        self.num_feat = 0              # number of features
        self.num_classes = 0           # number of classes
        self.num_train = 0             # number of training examples

        #self.objective = []            # objective value at each iteration
        self.alpha = array([])         # support vector coefficients

    def train(self, train_ex, train_lab):
        raise NotImplementedError

    def predict(self, pred_ex):
        raise NotImplementedError

    def save_Ky(self, dataname, work_dir='.'):
        """Save the learned label kernel,
        as pickle and pdf image"""
        if self.all_Ky is None:
            save('%s/%s_Ky.pkl' % (work_dir, dataname), self.Ky)
        else:
            save('%s/%s_Ky.pkl' % (work_dir, dataname), self.all_Ky)
            
    def normalize_unit_diag(self):
        """Normalize Ky to unit diagonal, and appropriately scale alpha"""
        diag_Ky = diag(self.Ky)
        diag_Ky.shape = (self.num_classes, 1)        
        self.Ky = divide(self.Ky, sqrt(matrix(diag_Ky)*matrix(diag_Ky).T))
        diag_Ky.shape = (1, self.num_classes)
        diag_Ky = sqrt(diag_Ky)
        self.alpha = self.alpha*diag_Ky

    def _set_dimensions(self, train_lab):
        """Infer the number of examples and classes"""
        if len(train_lab.shape) == 1:
            self.y = idx2binvec(train_lab)
            self.num_train = len(train_lab)
            self.num_classes = len(unique(train_lab))
            assert(self.y.shape == (self.num_train, self.num_classes))
        else:
            self.y = train_lab.T
            (self.num_classes, self.num_train) = train_lab.shape
        
    def check_dim(self):
        """Check that the training data is of the right shape"""
        print('Training with %d examples and %d tasks' % (self.num_train, self.num_classes))
        assert(self.y.shape == (self.num_train, self.num_classes))
        assert(self.Kx.kmat.shape == (self.num_train, self.num_train))

class KernelOptLS(KernelOptBase):
    """Optimize the kernel on the labels,
    with the least squares loss, and also Frobenious norm regularizer.
    Uses numpy.linalg.eigh for solving the sylvester equation"""
    def __init__(self, reg_param=1.0, kernel=None, verbose=False):
        KernelOptBase.__init__(self, reg_param, verbose)
        self.max_iter = 1000           # maximum number of iterations
        self.rel_tol = 0.001           # relative stopping tolerance
        self.near_zero = 1e-5          # initialize a positive definite matrix

        self.Kx = kernel               # kernel on examples

    def train(self, train_ex, train_lab, init_Ky=None):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""
        self.train_ex = train_ex
        self.train_lab = train_lab
        self._set_dimensions(train_lab)
        self.Kx.compute(train_ex)
        if init_Ky is not None:
            self.Ky = init_Ky
        else:
            #self.Ky = zeros((self.num_classes,self.num_classes))
            self.Ky = self.near_zero*eye(self.num_classes)
        # shrink Ky towards this matrix
        #bias = matrix(ones((self.num_classes,self.num_classes)))
        #bias = matrix(eye(self.num_classes))

        self.optimize()
        
        #self.normalize_unit_diag()
        if self.verbose:
            print('Kernel on labels')
            print(self.Ky)

    def predict(self, pred_ex):
        """Wrapper to do prediction.
        returns argmax of scores"""
        Ktest = self.Kx.compute_test(pred_ex)
        scores = array((matrix(self.Ky)*matrix(self.alpha).T*matrix(Ktest)).T)
        if self.get_max:
            return argmax(scores, axis=1)
        else:
            return transpose(scores)

    def train_reg_path(self, train_ex, train_lab, all_reg_param=None):
        """Solve for the whole regularization path"""
        if all_reg_param is not None:
            self.all_reg_param = all_reg_param
        self.train_ex = train_ex
        self.train_lab = train_lab
        self._set_dimensions(train_lab)
        self.Kx.compute(train_ex)
        self._set_dimensions(train_lab)
        self.check_dim()
        self.all_Ky = zeros((self.num_classes, self.num_classes, len(self.all_reg_param)))
        self.Ky = zeros((self.num_classes, self.num_classes))
        for (idx_reg, reg) in reversed(list(enumerate(self.all_reg_param))):
            self.reg_param = reg
            self.train(train_ex, train_lab, self.Ky)
            self.all_Ky[:,:,idx_reg] = self.Ky.copy()
        # Use the middle of the regularization range as default
        self.Ky = self.all_Ky[:,:,round(self.all_Ky.shape[2]/2)]

    def infer_sv(self, Ky=None):
        """Compute the support vector coefficients for a given Ky"""
        if Ky is not None:
            self.Ky = Ky
        self.alpha = solve_sylvester(self.Kx.kmat, self.Ky/self.reg_param,
                           self.y/self.reg_param)

    def predict_reg_path(self, pred_ex, ident_Ky=False):
        """Compute prediction for each value in regularization path.
        Use learned Ky and retrain to get the corresponding
        support vector coefficients.
        """
        if self.get_max:
            all_preds = zeros((pred_ex.shape[1], len(self.all_reg_param)))
        else:
            all_preds = zeros((self.num_classes, pred_ex.shape[1], len(self.all_reg_param)))
            
        for (idx_reg, reg) in enumerate(self.all_reg_param):
            if self.verbose:
                print('Predicting with reg param = %e' % reg)
            self.reg_param = reg
            if ident_Ky:
                self.infer_sv(eye(self.num_classes))
            else:
                self.infer_sv(self.all_Ky[:,:,idx_reg])
            if self.get_max:
                all_preds[:,idx_reg] = self.predict(pred_ex)
            else:
                all_preds[:,:,idx_reg] = self.predict(pred_ex)                
        return all_preds

    def optimize(self):
        """Solve the optimization problem with block coordinate descent"""
        residual = norm(self.y)
        min_tol = self.rel_tol * residual
        num_iter = 0

        print('Learning the kernel by coordinate descent')
        while residual > min_tol and num_iter < self.max_iter:
            num_iter += 1
            self.alpha = solve_sylvester(self.Kx.kmat, self.Ky/self.reg_param,
                          self.y/self.reg_param)
            #self.objective.append((trace(matrix(self.alpha).T*matrix(self.y))+
            #                            norm(self.Ky)**2)/2.0)
            X = matrix(self.Kx.kmat)*matrix(self.alpha)
            #P = 0.5*X.T*matrix(self.alpha)-matrix(self.Ky)+bias
            P = 0.5*X.T*matrix(self.alpha)-matrix(self.Ky)
            P = (P+P.T)/2.0
            Q = matrix(solve(X.T*X+self.reg_param*eye(self.num_classes), P))
            #(Q, res, xrank, s) = lstsq(X.T*X+self.reg_param*eye(self.num_classes), P)
            Q = (Q+Q.T)/2.0
            self.Ky += self.reg_param*Q
            #yhat = X*matrix(self.Ky)
            #residual = norm(yhat+self.reg_param*self.alpha-self.y)
            residual = norm(self.reg_param*Q)
            if self.verbose:
                print('Iteration: %d: residual = %f' % (num_iter, residual))
        print('Number of iterations: %d' % num_iter)


class KernelOptLR(KernelOptLS):
    """Optimize the kernel on the labels, with least squares loss,
    and Frobenious norm regularizer.

    Dinuzzo and Fukumizu "Learning low-rank output kernels", ACML 2011
    
    Users numpy.linalg.eigh for solving the sylvester equation.
    """
    def __init__(self, output_rank=10, reg_param=1.0, kernel=None, verbose=False):
        KernelOptLS.__init__(self, reg_param, kernel, verbose)
        self.output_rank = output_rank

    def optimize(self):
        """Solve the optimization problem with block coordinate descent"""
        residual = norm(self.y)
        min_tol = self.rel_tol * residual
        num_iter = 0

        print('Learning the kernel by coordinate descent')
        LX, UX = eigh(self.Kx.kmat)
        B = eye(self.num_classes, self.output_rank)
        Yhat = dot(UX.T, self.y)
        while residual > min_tol and num_iter < self.max_iter:
            num_iter += 1
            LY, UY = eigh(dot(B.T,B))
            Q = dot(Yhat, dot(B, UY))
            V = Q/(outer(LX,LY) + self.reg_param*ones((len(LX),len(LY))))
            Bp = B.copy()
            E = dot(diag(LX), V)
            P = solve(dot(E.T,E)+self.reg_param*eye(self.output_rank), dot(E.T, Yhat))
            B = dot(P.T, UY.T)
            residual = norm(B-Bp)
            if self.verbose:
                print('Iteration: %d: residual = %f' % (num_iter, residual))
        self.Ky = B
        print('Number of iterations: %d' % num_iter)
        self.alpha = dot(UX, dot(V, UY.T))
        
    
class KernelOptLin(KernelOptBase):
    """Optimize the kernel on the labels,
    with the least squares loss, and also Frobenious norm regularizer.
    Uses slycot for solving the sylvester equation

    Use linear kernel on the inputs
    """
    def __init__(self, reg_param=1.0, verbose=False):
        KernelOptBase.__init__(self, reg_param, verbose)
        self.max_iter = 1000           # maximum number of iterations
        self.rel_tol = 0.01            # relative stopping tolerance
        self.near_zero = 1e-5          # initialize a positive definite matrix
        
    def train(self, train_ex, train_lab, init_Ky=None):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""
        self.train_ex = train_ex
        self.train_lab = train_lab
        self.num_feat = train_ex.shape[0]
        self._set_dimensions(train_lab)
        if init_Ky is not None:
            self.Ky = init_Ky
        else:
            self.Ky = self.near_zero*eye(self.num_classes)

        residual = norm(self.y)
        min_tol = self.rel_tol * residual
        num_iter = 0

        print('Learning the kernel by coordinate descent')
        while residual > min_tol and num_iter < self.max_iter:
            num_iter += 1
            K = array(matrix(self.train_ex).T*matrix(self.train_ex))
            self.alpha = solve_sylvester(K, self.Ky/self.reg_param,
                          self.y/self.reg_param)
            X = K*matrix(self.alpha)
            P = 0.5*X.T*matrix(self.alpha)-matrix(self.Ky)
            P = (P+P.T)/2.0
            Q = matrix(solve(X.T*X+self.reg_param*eye(self.num_classes), P))
            Q = (Q+Q.T)/2.0
            self.Ky += self.reg_param*Q
            yhat = X*matrix(self.Ky)
            residual = norm(yhat+self.reg_param*self.alpha-self.y)
            if self.verbose:
                print('Iteration: %d: residual = %f' % (num_iter, residual))

        print('Number of iterations: %d' % num_iter)
        if self.verbose:
            print('Kernel on labels')
            print(self.Ky)

    def predict(self, pred_ex):
        """Wrapper to do prediction.
        returns argmax of scores"""
        Ktest = matrix(self.train_ex.T)*matrix(pred_ex)
        scores = array((matrix(self.Ky)*matrix(self.alpha).T*Ktest).T)
        if self.get_max:
            return argmax(scores, axis=1)
        else:
            return transpose(scores)

class MultiTask(KernelOptLS):
    """Use a fixed kernel on the labels.
    Multi task learning where the task similarity is assumed to be known.
    Use init_Ky in the train function to specify the kernel on labels.
    """
    def __init__(self, reg_param=1.0, kernel=None, verbose=False):
        KernelOptLS.__init__(self, reg_param, kernel, verbose)

    def optimize(self):
        """Solve for alphas"""
        self.infer_sv()

    def train_reg_path(self, train_ex, train_lab, all_reg_param=None):
        """Actually nothing to do because no need to learn Ky.
        Fill up self.alpha with the solution for the middle regularization paramter.
        """
        if all_reg_param is not None:
            self.all_reg_param = all_reg_param
        self.train_ex = train_ex
        self.train_lab = train_lab
        self._set_dimensions(train_lab)
        self.Kx.compute(train_ex)
        self._set_dimensions(train_lab)
        self.check_dim()
        middle = round(len(self.all_reg_param)/2)
        self.reg_param = self.all_reg_param[middle]
        self.train(train_ex, train_lab, self.Ky)
        assert(self.all_Ky is None)

    def predict_reg_path(self, pred_ex, ident_Ky=False):
        """Compute prediction for each value in regularization path.
        Use Ky and retrain to get the corresponding
        support vector coefficients.
        """
        if self.get_max:
            all_preds = zeros((pred_ex.shape[1], len(self.all_reg_param)))
        else:
            all_preds = zeros((self.num_classes, pred_ex.shape[1], len(self.all_reg_param)))
            
        for (idx_reg, reg) in enumerate(self.all_reg_param):
            if self.verbose:
                print('Predicting with reg param = %e' % reg)
            self.reg_param = reg
            if ident_Ky:
                self.infer_sv(eye(self.num_classes))
            else:
                self.infer_sv(self.Ky)
            if self.get_max:
                all_preds[:,idx_reg] = self.predict(pred_ex)
            else:
                all_preds[:,:,idx_reg] = self.predict(pred_ex)                
        return all_preds

class KernelOptLSGP(KernelOptLS):
    """Optimize the kernel on the labels,
    with the least squares loss, and also Frobenious norm regularizer.
    Uses slycot for solving the sylvester equation.

    Multiple output regression, also estimate predictive variance.
    """
    def __init__(self, reg_param=1.0, kernel=None, verbose=False):
        KernelOptLS.__init__(self, reg_param, kernel, verbose)
        self.test_kernel = JointKernel(kernel, self.Ky)

    def predict(self, pred_ex):
        """Wrapper to do prediction and also estimate predictive variance."""
        num_test = pred_ex.shape[1]
        Ktest = self.Kx.compute_test(pred_ex)
        scores = array((matrix(self.Ky)*matrix(self.alpha).T*matrix(Ktest)).T)

        self.test_kernel.Ky = self.Ky
        self.test_kernel.compute(pred_ex)
        A = matrix(inv(kron(self.Kx.kmat,self.Ky)\
                       +self.reg_param*eye(self.num_train*self.num_classes)))
        k_star = matrix(kron(Ktest, self.Ky))
        pred_var = diag(self.test_kernel.kmat - k_star.T*A*k_star)
        pred_var.shape = (num_test, self.num_classes)
        return scores.T, pred_var.T

    def predict_reg_path(self, pred_ex, ident_Ky=False):
        """Compute prediction for each value in regularization path.
        Use learned Ky and retrain to get the corresponding
        support vector coefficients.
        """
        all_preds = zeros((self.num_classes, pred_ex.shape[1], len(self.all_reg_param)))
        for (idx_reg, reg) in enumerate(self.all_reg_param):
            if self.verbose:
                print('Predicting with reg param = %e' % reg)
            self.reg_param = reg
            if ident_Ky:
                self.infer_sv(eye(self.num_classes))
            else:
                self.infer_sv(self.all_Ky[:,:,idx_reg])
            all_preds[:,:,idx_reg] = self.predict(pred_ex)
        return all_preds
        
class KernelOpt(object):
    """Learn the kernel on the labels
    Uses semidefinite programming with cvxopt
    """
    def __init__(self, reg_param=1.0, kernel=None, verbose=False):
        self.train_time = 0.0          # time used for training
        self.max_iter = 3              # maximum number of iterations
        self.verbose = verbose         # how much output

        # Pointer to the classifier object
        self.classifier = StructPredictor(reg_param, kernel, verbose)

    def solve_opt(self, alpha):
        """Solve the optimization problem using an SDP.
        max tr(alpha' Kx alpha Ky)
        st  diag(Ky) = I
            Ky > 0

        cvxopt takes the problem as:
        sdp(c[, Gl, hl[, Gs, hs[, A, b]]])

        minimize     c'x
        subject to   Gl x + sl = hl
                     sl >= 0
                     Ax = b
                     Gk x + vec(sk) = vec(hk)
                     sk > 0

        Gl and hl define the inequality constraints
        Gs and hs are lists, containing Gk and hk respectively, representing the LMIs.
        The inequality > mean positive semidefinite.

        We consider the dual form:
        max -hl'zl - sum_k tr(hk zk) - b'y
        st  Gl'zl + sum_k Gk'vec(zk) + A'y + c = 0
        zl >= 0
        zk > 0
        """
        n = alpha.shape[1]  # the number of classes
        m = n**2            # the size of Ky

        # TODO: How do we regularize?
        h = [cm(-array(normalize_unit_diag(matrix(alpha).T*self.classifier.Kx*matrix(alpha))))]
        #h = [cm(-array(matrix(alpha).T*self.classifier.Kx*matrix(alpha)))]
        #print(-h[0])
        A = zeros((m,n))
        for icol in range(n):
            A[(n+1)*icol,icol] = 1.0
        G = [cm(A)]
        c = cm(-ones((n,1)))

        print('Solving SDP')
        sol = sdp(c, Gs=G, hs=h, solver='dsdp')
        Ky = array(sol['zs'][0])
        Ky = Ky + Ky.T - diag(Ky.diagonal())
        #Ky = maximum(Ky, 0.0)
        #print(trace(cm(Ky)*h[0]))
        return Ky

    def solve_opt_abspos(self, alpha):
        """Solve the optimization problem using an SDP.
        Also require that entries in Ky are positive.

        cvxopt takes the problem as:
        sdp(c[, Gl, hl[, Gs, hs[, A, b]]])

        minimize     c'x
        subject to   Gl x + sl = hl
                     sl >= 0
                     Ax = b
                     Gk x + vec(sk) = vec(hk)
                     sk > 0

        Gl and hl define the inequality constraints
        Gs and hs are lists, containing Gk and hk respectively, representing the LMIs.
        The inequality > mean positive semidefinite.

        We consider the dual form:
        max -hl'zl - sum_k tr(hk zk) - b'y
        st  Gl'zl + sum_k Gk'vec(zk) + A'y + c = 0
        zl >= 0
        zk > 0
        """
        n = alpha.shape[1]  # the number of classes
        m = n**2            # the size of Ky
        nm = n+m            # The number of variables in the SDP

        # TODO: How do we regularize?
        #h = [cm(-array(normalize_unit_diag(matrix(alpha).T*self.classifier.Kx*matrix(alpha))))]
        #h = [cm(-array(matrix(alpha).T*self.classifier.Kx*matrix(alpha)))]
        F = zeros((nm, nm))
        F[:n,:n] = -array(normalize_unit_diag(matrix(alpha).T*self.classifier.Kx*matrix(alpha)))
        h = [cm(F)]
        #print(-h[0])
        Fk = zeros((nm**2,nm))
        for icol in range(n):
            Fk[(nm+1)*icol,icol] = 1.0
        for irow in range(n):
            for icol in range(n):
                Fk[nm*irow+icol,n+n*irow+icol] = 1.0
                Fk[n*nm+n+(nm+1)*(n*irow+icol),n+n*irow+icol] = -1.0
        G = [cm(Fk)]
        c = cm(vstack([-ones((n,1)), zeros((m,1))]))
        print('Solving SDP')
        sol = sdp(c, Gs=G, hs=h, solver='dsdp')
        Ky = array(sol['zs'][0][:n,:n])
        Ky = Ky + Ky.T - diag(Ky.diagonal())
        #Ky = maximum(Ky, 0.0)
        #print(trace(cm(Ky)*h[0]))
        return Ky

    def train(self, train_ex, train_lab):
        """Wrapper to convert data to feature matrix,
        and solve the optimization"""
        self.classifier.train(train_ex, train_lab)
        for ix in range(self.max_iter):
            print('Iteration: %d' % ix)
            self.classifier.Ky = self.solve_opt(self.classifier.alpha)
            self.classifier.solve_opt()
            #print(self.classifier.Ky)
            #print(eigh(self.classifier.Ky)[0])

    def predict(self, pred_ex):
        """Wrapper to do prediction."""
        return self.classifier.predict(pred_ex)

