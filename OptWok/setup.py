#!/usr/bin/env python
import os
import numpy
# Use numpy version of distutils to get f2py
#from numpy.distutils.core import setup, Extension
from distutils.core import setup, Extension
from distutils.command.sdist import sdist
from glob import glob
try:
    #from Cython.Build import cythonize
    from Cython.Distutils import build_ext
    have_cython = True
except ImportError:
    from distutils.command import build_ext
    have_cython = False

opt_modules = []


if have_cython:
    # cython code is used to speed up kronecker product kernel
    kron = Extension('optwok.kernelbase',
                     sources = ['optwok/kernelbase.pyx'],
                     include_dirs = [numpy.get_include(),'.'],
                     )
    opt_modules.append(kron)

class sdist_hg(sdist):
    """Append Mercurial repository version if 'dev' option is used with sdist"""
    user_options = sdist.user_options + [
            ('dev', None, "Add a dev marker")
            ]

    def initialize_options(self):
        sdist.initialize_options(self)
        self.dev = 0

    def run(self):
        if self.dev:
            suffix = '.dev%d' % self.get_tip_revision()
            self.distribution.metadata.version += suffix
        sdist.run(self)

    def get_tip_revision(self, path=os.path.expanduser('~')+'/Mercurial/OptWok'):
        from mercurial.hg import repository
        from mercurial.ui import ui
        from mercurial import node
        repo = repository(ui(), path)
        tip = repo.changelog.tip()
        return repo.changelog.rev(tip)

setup(name = 'OptWok',
      author = 'Cheng Soon Ong',
      author_email = 'chengsoon.ong@anu.edu.au',
      version = '0.5',
      description = 'A collection of tools for prototyping optimization methods in machine learning.',
      long_description = open('README').read(),
      url = 'http://www.ong-home.my/optwok.html',
      packages = ['optwok', 'optwok.mil'],
      cmdclass = {'sdist': sdist_hg, 'build_ext': build_ext},
      #ext_modules = cythonize(opt_modules),
      ext_modules = opt_modules,
      license = 'GNU GPL version 3 or later'
    )

