"""Experiments using Gaussian Processes for MHC data"""

import cPickle
import os
from numpy import array, transpose, mean
from numpy import arange, hstack, kron, ones, eye
from numpy import sqrt, concatenate, zeros, log10
from numpy.random import permutation, seed
from optwok.simdata import gp_gen
from optwok.kernelopt import KernelOptLSGP
from optwok.kernel import GaussKernel, LinearKernel
from optwok.kernel import normalize_unit_diag
from optwok.io_pickle import load
from optwok.gaussian_proc import GaussianProcess
from optwok import context_ucb
from optwok.testtools import list2matrix
from optwok.datatools import get_data, Data
from matplotlib.pyplot import plot, show, figure, legend, fill
import settings as s

#####################################################################
# Toy Data
#####################################################################

def independent_gp(X, Y, support):
    """Three independent gaussian processes,
    sanity check with other implementation"""
    import infpy
    import infpy.gp
    from infpy.gp import GaussianProcess, gp_learn_hyperparameters
    import infpy.gp.kernel_short_names as kernels

    X0 = transpose(X)

    Y0 = Y[0,:]
    kernel0 = kernels.SE([1.])
    gp = GaussianProcess(X0, Y0, kernel0)
    gp_learn_hyperparameters(gp)
    infpy.gp.gp_1D_predict(gp, 100, 0, support)

    Y1 = Y[1,:]
    kernel1 = kernels.SE([1.])
    gp = GaussianProcess(X0, Y1, kernel1)
    gp_learn_hyperparameters(gp)
    infpy.gp.gp_1D_predict(gp, 100, 0, support)

    Y2 = Y[2,:]
    kernel2 = kernels.SE([1.])
    gp = GaussianProcess(X0, Y2, kernel2)
    gp_learn_hyperparameters(gp)
    infpy.gp.gp_1D_predict(gp, 100, 0, support)

def multioutput_gp(X, Y, support):
    """Multiple output regression with learning Ky"""
    svm = KernelOptLSGP(reg_param=0.05, kernel=GaussKernel())
    svm.train(X,Y)
    Xtest = arange(0,support,0.1)
    Xtest.shape = (1,len(Xtest))
    (preds, pred_var) = svm.predict(Xtest)
    plot_prediction(X, Y, Xtest, preds, pred_var)

def plot_prediction(data_ex, data_lab, test_ex, preds, pred_var):
    """Plot the data points, the predictions and the predictive variance"""
    colour_list = ['b','g','r','c','m','k','y']
    test_ex = test_ex.flatten()
    (num_out, num_ex) = preds.shape
    for cur_out in range(num_out):
        plot(test_ex, preds[cur_out,:], colour_list[cur_out], hold=True)
        plot(data_ex.flatten(), data_lab[cur_out,:], colour_list[cur_out]+'o')
        sd = 2.0*sqrt(pred_var[cur_out,:])
        var_x = concatenate((test_ex, test_ex[::-1]))
        var_y = concatenate((preds[cur_out,:]+sd, (preds[cur_out,:]-sd)[::-1]))
        fill(var_x, var_y, edgecolor='w', facecolor=colour_list[cur_out], alpha=0.3)

def toy_data():
    support = 5
    (X,Y) = gp_gen(10, 1, support, 0.1)
    multioutput_gp(X,Y,support)
    independent_gp(X,Y,support)
    show()

def export2matlab():
    from scipy.io.matlab import savemat
    all_data = cPickle.load(open(s.datafile,'r'))
    MHC_alleles = s.all_tasks
    (examples, contexts, labels) = get_data(all_data, range(5), s.all_tasks)
    learned_context_kernel = load(s.sim_name)
    sequence_context_kernel = s.Kseq7.copy()
    output = {'MHC_alleles':MHC_alleles,
              'learned_context_kernel':learned_context_kernel,
              'sequence_context_kernel':sequence_context_kernel,
              'examples':examples,
              'contexts':contexts,
              'labels':labels,
              }
    savemat('iedb_benchmark.mat',output)

def expt_cucb_mhc(all_tasks, Ky, outfile, reg_param, width):
    all_data = cPickle.load(open(s.datafile,'r'))
    #data_stats(all_data, all_tasks)

    (b_examples, b_contexts, b_labels) = get_data(all_data, [0], all_tasks)
    num_base = 1
    #perm = permutation(len(b_contexts))
    #b_examples = b_examples[:,perm[:num_base]]
    #b_contexts = b_contexts[perm[:num_base]]
    #b_labels = b_labels[perm[:num_base]]
    b_examples = b_examples[:,:num_base]
    b_contexts = b_contexts[:num_base]
    b_labels = b_labels[:num_base]
    print b_contexts
    print 'Number of examples in base GP = %d' % len(b_labels)
    if width is None:
        gp = GaussianProcess(LinearKernel, Ky, reg_param=reg_param)
    else:
        gp = GaussianProcess(GaussKernel, Ky, kparam=width, reg_param=reg_param)
    gp.train(b_examples, b_contexts, b_labels)
    
    (examples, contexts, labels) = get_data(all_data, [2,3,4], all_tasks)
    print 'Number of examples in search set = %d' % len(labels)
    data = Data(examples, contexts, labels)
    context_ucb.expt_cucb(data, gp, outfile,
                          num_context=len(all_tasks), num_epoch=50)



def expt_mhc(reg_param=0.3, width=None, cucb=True, plot_res=True):
    if width is None:
        ind_name = 'expt_cucb_ind_%s_%2.2f.pkl' % (s.expt_name, reg_param)
        cat_name = 'expt_cucb_cat_%s_%2.2f.pkl' % (s.expt_name, reg_param)
        res_name = 'expt_cucb_%s_%2.2f.pkl' % (s.expt_name, reg_param)
    else:
        ind_name = 'expt_cucb_ind_%s_%2.2f_%2.2f.pkl' % (s.expt_name, reg_param, width)
        cat_name = 'expt_cucb_cat_%s_%2.2f_%2.2f.pkl' % (s.expt_name, reg_param, width)
        res_name = 'expt_cucb_%s_%2.2f_%2.2f.pkl' % (s.expt_name, reg_param, width)

    if cucb:
        # Use learned task similarity
        #Ky = normalize_unit_diag(load(s.sim_name))
        #Ky = normalize_unit_diag(s.KMKL4a)
        Ky = normalize_unit_diag(s.Kseq7rev)
        print Ky
        expt_cucb_mhc(s.all_tasks, Ky, res_name, reg_param, width)
        # Tasks are all the same (concatenated)
        expt_cucb_mhc(s.all_tasks, ones((s.num_task, s.num_task)), cat_name, reg_param, width)
        # Tasks are independent
        expt_cucb_mhc(s.all_tasks, eye(s.num_task), ind_name, reg_param, width)

    if plot_res:
        context_ucb.plot_average_regret([res_name, cat_name, ind_name],
                                        ['CGP-UCB','merge','ignore'])
        #show()
    
if __name__ == '__main__':
    #for reg_param in [0.2, 0.25, 0.3, 0.35, 0.4]:
    #    for width in [2.2, 2.3, 2.4, 2.5]:
    #        expt_mhc(reg_param, width)
    #for reg_param in [0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37]:
    #    for width in [2.2, 2.5, 2.7, 3.0]:
    #        expt_mhc(reg_param, width)
    for reg_param in [0.32]:
        for width in [2.5]:
            expt_mhc(reg_param, width)

    
    #for reg_param in [0.2, 0.25, 0.3, 0.35, 0.4]:
    #    expt_mhc(reg_param)
    
