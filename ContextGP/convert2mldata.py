"""Script to convert data from Christian Widmer to mldata.org format"""

import pickle
import os
import numpy as np
from optwok.datatools import data_stats, get_data, Data


def convert_nips(orig_datafile, csvfile):
    print('Creating NIPS dataset')
    all_tasks = ['A_2403', 'A_2402', 'A_2301', 'A_0201', 'A_0203', 'A_0202', 'A_6901']

    f = open(orig_datafile, 'rb')
    all_data = pickle.load(f, fix_imports=True)
    data_stats(all_data, all_tasks)

    (b_examples, b_contexts, b_labels) = get_data(all_data, [0], all_tasks)
    num_base = 1
    b_examples = b_examples[:,:num_base]
    b_contexts = b_contexts[:num_base]
    b_labels = b_labels[:num_base]
    print(b_contexts)
    print('Number of examples in base GP = %d' % len(b_labels))

    (examples, contexts, labels) = get_data(all_data, [2,3,4], all_tasks)
    print('Number of examples in search set = %d' % len(labels))
    examples = np.hstack([b_examples, examples])
    contexts = np.concatenate([b_contexts, contexts])
    labels = np.concatenate([b_labels, labels])
    print(examples.shape, contexts.shape, labels.shape)
    data = Data(examples, contexts, labels)
    data.save(csvfile)

def convert_all(orig_datafile, csvfile):
    print('Converting whole dataset')
    f = open(orig_datafile, 'rb')
    all_data = pickle.load(f, fix_imports=True)
    all_tasks = list(all_data.keys())
    data_stats(all_data, all_tasks)
    (examples, contexts, labels) = get_data(all_data, [0,1,2,3,4], all_tasks)
    print('Number of examples in all splits = %d' % len(labels))
    print(examples.shape, contexts.shape, labels.shape)
    data = Data(examples, contexts, labels)
    data.save(csvfile)

if __name__ == '__main__':
    orig_datafile = os.path.expanduser('~')+'/Data/iedb_benchmark/iedb_data_pca.pickle'
    csvfile_nips = os.path.expanduser('~')+'/Data/iedb_benchmark/mhc_nips2011.csv.bz2'
    csvfile = os.path.expanduser('~')+'/Data/iedb_benchmark/mhc_data.csv.bz2'
    convert_nips(orig_datafile, csvfile_nips)
    convert_all(orig_datafile, csvfile)
    
