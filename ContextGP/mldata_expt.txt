

# Output kernel matrices (task similarity) obtained from
# C. Widmer, N. Toussaint, Y. Altun, and G. Raetsch.
# Inferring latent task structure for multitask learning by multiple kernel learning.
# BMC Bioinformatics, 11(Suppl 8:S5), 2010.


all_tasks = ['A_2403', 'A_2402', 'A_2301', 'A_0201', 'A_0203', 'A_0202', 'A_6901']
Ky = array([[1., 0.9, 0.85, 0.25, 0., 0.3, 0.],
                  [0.9, 1., 0.9, 0., 0., 0.25, 0.],
                  [0.85, 0.9, 1., 0.25, 0., 0.25, 0.],
                  [0.25, 0., 0.25, 1., 0.85, 0.85, 0.55],
                  [0., 0., 0., 0.85, 1., 0.85, 0.45],
                  [0.3, 0.25, 0.25, 0.85, 0.85, 1., 0.45],
                  [0., 0., 0., 0.55, 0.45, 0.45, 1.]])

# Data conversion
- 1st data point from split 0, and all data from split 2,3,4.
- Features are the 45 PCA values of encoded peptides (divided by the vector norm)
- Labels are -log10(ic50s)+log10(500)
- First column contains the label, second column the context, the rest the features

# Experimental notes
- A base GP is trained on the first example
- GP UCB run on all the rest


