"""Find a good regularization parameter using validation"""
import cPickle
import mhc_data_gp
from optwok.evaluation import balanced_accuracy, rmse
from optwok.io_pickle import load
from optwok.gaussian_proc import GaussianProcess
from optwok.kernel import LinearKernel
from optwok.testtools import list2matrix
from optwok.datatools import get_data
from numpy import sign, logspace
import settings as s

def expt_gp(reg_param, examples, contexts, labels, t_examples, t_contexts, t_labels):
    gp = GaussianProcess(LinearKernel, Ky, reg_param=reg_param)
    gp.train(examples, contexts, labels)
    (preds, pred_var) = gp.predict((t_examples, t_contexts))
    error = rmse(preds, t_labels)

    return error

if __name__ == '__main__':
    all_data = cPickle.load(open(s.datafile,'r'))
    Ky = load(s.sim_name)

    (examples, contexts, labels) = get_data(all_data, [0,1,2], s.all_tasks)
    (t_examples, t_contexts, t_labels) = get_data(all_data, [3,4], s.all_tasks)
    for reg_param in logspace(-3,3,10):
        acc = expt_gp(reg_param, examples, contexts, labels, t_examples, t_contexts, t_labels)
        print 'Reg=%f, error=%f' % (reg_param, acc)
