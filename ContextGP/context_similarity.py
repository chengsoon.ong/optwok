"""Use split 0 to learn the similarity between tasks"""

import cPickle
import os
from numpy import array, zeros, ones, kron, concatenate
from numpy import hstack, transpose, unique, vstack, matrix
from numpy import mean, argsort, array, arange, log10
from numpy.random import permutation

from matplotlib.pyplot import figure, plot, show, axis, savefig
from matplotlib.pyplot import title, ylabel, boxplot
from matplotlib.pyplot import matshow, colorbar, xticks, yticks
from matplotlib.mlab import find

from optwok.evaluation import rmse_multitask
from optwok.io_pickle import load, save
from optwok.kernelopt import KernelOptLS
from optwok.kernel import LinearKernel, normalize_unit_diag
from optwok.named_array import NArray
from optwok.gaussian_proc import GaussianProcessScalar
from optwok.datatools import get_data, get_multitask
import settings as s

#####################################################################
# The learning methods
#####################################################################

def learn_output(all_data, results, reg_param, name='multitask_learn'):
    """Learn the output kernel between tasks"""
    #from pylab import hist, show
    split = 0

    train_ex, train_lab, test_ex, test_lab = get_multitask(s.all_tasks, all_data, split)
    svm = KernelOptLS(kernel=LinearKernel(), reg_param=reg_param)
    print 'Split %d: SVM with %d features and %d examples'\
          % (split, train_ex.shape[0], train_ex.shape[1])
    svm.train(train_ex, train_lab)
    print svm.Ky
    svm.get_max = False
    svmout = svm.predict(test_ex)
    error = rmse_multitask(svmout, test_lab)
    for (ix, task) in enumerate(results.keys('Task')):
        print 'RMSE (%s): %f' % (task, error[ix])
        results.fill_value(error[ix], [task, 'rmse'],
                           ['Task','Perf'])
    return svm.Ky

def expt_learn_output():
    """Learn the similarity between MHC alleles"""
    all_data = cPickle.load(open(s.datafile,'r'))
    data_stats(all_data, s.all_tasks)

    # Initialize the results storage
    results = NArray(['Task','Perf'])
    results.add_keys(s.all_tasks, 'Task')
    results.add_keys(['rmse'], 'Perf')

    # All the computational work is here
    Ky = learn_output(all_data, results, 7.0)
    save(s.consim_name, results)
    save(s.sim_name, Ky)

    matshow(Ky)
    xticks(range(s.num_task), s.all_tasks)
    yticks(range(s.num_task), s.all_tasks)
    colorbar()
    show()

def compute_prediction_correlation():
    """Use the correlation between the predictions of different contexts
    to determine the context similarity"""
    all_data = cPickle.load(open(s.datafile,'r'))

    # create test set
    split = [2,3,4]
    test_ex = zeros((45,0))
    for (context, task) in enumerate(s.all_tasks):
        ex, lab = get_data(all_data[task], split, subsample=True)
        test_ex = hstack([test_ex, ex])
    num_test = test_ex.shape[1]
    print 'Number of test examples = %d' % num_test

    # The predictions of the different GPs
    split = [0,1]
    preds = zeros((s.num_task, num_test))
    for (context, task) in enumerate(s.all_tasks):
        train_ex, train_lab = get_data(all_data[task], split, subsample=True)
        num_ex = len(train_lab)
        gp = GaussianProcessScalar(LinearKernel, reg_param=0.3)
        gp.train(train_ex, train_lab)
        (preds[context,:], pred_var) = gp.predict(test_ex)
    corr = normalize_unit_diag(matrix(preds)*matrix(preds).T)
    print corr
    save(s.sim_name, corr)


if __name__ == '__main__':
    compute_prediction_correlation()
    Ky = load(s.sim_name)
    matshow(Ky)
    colorbar()
    xticks(range(s.num_task), s.all_tasks, rotation=20)
    yticks(range(s.num_task), s.all_tasks)
    savefig('%s.pdf' % s.sim_name[:-4], dpi=300, bbox_inches='tight')
