"""Experiments for batch CGP-UCB for MHC data"""



import cPickle
from optwok.kernel import GaussKernel, LinearKernel
from optwok.kernel import normalize_unit_diag
from optwok.datatools import get_data, Data
from optwok.gaussian_proc import GaussianProcess
from optwok.context_ucb import plot_average_regret
from optwok import context_ucb


import settings as s


def cgp_ucb_batch(all_tasks, Ky, outfile_batch, outfile_kbest, outfile_rand, reg_param):
    all_data = cPickle.load(open(s.datafile,'r'))
    #data_stats(all_data, all_tasks)

    (b_examples, b_contexts, b_labels) = get_data(all_data, [0], all_tasks)
    num_base = 1
    b_examples = b_examples[:,:num_base]
    b_contexts = b_contexts[:num_base]
    b_labels = b_labels[:num_base]
    print 'Number of examples in base GP = %d' % len(b_labels)

    gp = GaussianProcess(LinearKernel, Ky, reg_param=reg_param)
    gp.train(b_examples, b_contexts, b_labels)
    (examples, contexts, labels) = get_data(all_data, [2,3,4], all_tasks)
    print 'Number of examples in search set = %d' % len(labels)
    data = Data(examples, contexts, labels)

    num_context = len(all_tasks)
    num_epoch = 5
    batch_size = 10
    context_ucb.expt_cucb_batch(data, gp, outfile_batch,
                                num_context, num_epoch, batch_size)
    
    context_ucb.expt_cucb_batch(data, gp, outfile_kbest,
                                num_context, num_epoch, batch_size, mode='kbest')
    
    context_ucb.expt_cucb_batch(data, gp, outfile_rand,
                                num_context, num_epoch, batch_size, mode='random')
    
    plot_average_regret([outfile_batch, outfile_kbest, outfile_rand],
                        ['greedy','kbest','random'])
    



def expt_mhc_batch(reg_param):
    batch_name = 'expt_cucb_batch_%s_%2.2f.pkl' % (s.expt_name, reg_param)
    kbest_name = 'expt_cucb_kbest_%s_%2.2f.pkl' % (s.expt_name, reg_param)
    rand_name =  'expt_cucb_rand_%s_%2.2f.pkl' % (s.expt_name, reg_param)
    Ky = normalize_unit_diag(s.Kseq7rev)
    print Ky
    cgp_ucb_batch(s.all_tasks, Ky, batch_name, kbest_name, rand_name, reg_param)


if __name__ == '__main__':
    for reg_param in [0.35]:
        expt_mhc_batch(reg_param)
