from os.path import isfile
import pickle
import numpy
from numpy import array, mean
from numpy.random import permutation
import optwok.io_pickle as iop
from optwok.named_array import NArray
from optwok.evaluation import auc
from optwok.ansgd import SVM, ElasticNet, GraphNet, KernelElasticNet

have_pytables=True
try:
    import tables
except ImportError:
    print('pytables not available')
    have_pytables=False
if have_pytables:
    from optwok.mldata_pytables import Dataset, param2string
else:
    from optwok.mldata import Dataset, param2string


class GWASData(Dataset):
    """Container to store genome wide association study (GWAS)
    data, with genotype (0,1,2) and phenotype (-1,+1).
    Additionally, store the graph Laplacian.
    """
    def __init__(self, name, frac_train):
        Dataset.__init__(self, name, frac_train=frac_train)
        self.examples = array([])
        self.labels = array([])
        self.laplacian = array([])

    def load_data(self):
        """Default behaviour, use instance name as filename for loading data"""
        self.load_laplacian('%s.pkl' % self.name)
        if have_pytables:
            self.load_examples()
        else:
            self.load_examples_csv('%s.csv' % self.name)
        assert(self.num_features == self.laplacian.shape[0])

    def load_examples(self):
        """Take a subset of the data from the HDF5 file"""
        data = self.data.root
        self.labels = data.labels[0]
        self.examples = data.examples[self.probe_idx,:]
        
    def load_examples_csv(self, filename, normalize=False):
        """Load the data from a CSV file"""
        data = numpy.loadtxt(filename, delimiter=',')
        # TODO: check type
        self.labels = data[:,0]
        self.examples = data[:,1:].T
        if normalize:
            self.examples /= float(self.num_features)

    def load_laplacian(self, filename):
        data = pickle.load(open(filename, 'rb'))
        self.probe_idx = data['snp_idx'] - 1
        self.laplacian = data['laplacian'].todense().A


class MLExptResults(object):
    """Container for experimental results"""
    def __init__(self, dataname, preds, labels, params):
        self.dataname = dataname
        if type(params) == type(1.0):
            self.reg_param = params
            self.l1_reg_param = 0
        else:
            self.reg_param = params[0]
            self.l1_reg_param = params[1]
        self.preds = preds
        self.labels = labels
        
class MLExptBase(object):
    """A wrapper around a classifier to each computation of test error.
    The experiment is a two layer design:
    - The inner layer uses cross validation to find the best hyperparameters
    - The outer layer uses random splits with the best hyperparameters
      to compute the performance

    This is an abstract base class which defines the minimal interface.
    """
    def __init__(self, config):
        self.id_str = ''
        self.config = config

    def train_and_predict(self):
        """Train a classifier and apply it to the test set."""
        raise NotImplementedError

    def find_best_param(self, datasets, perm_idx, num_cv, search_param):
        """Find the best hyperparameter settings"""
        best_param = NArray(['Data','perm'], max_elem=max(len(datasets), len(perm_idx)),
                            is_object=True)
        best_param.add_keys(datasets, 'Data')
        best_param.add_keys(perm_idx, 'perm')
        for dataname in datasets:
            for perm in perm_idx:
                best_val = self._search_param(dataname, perm, num_cv, search_param)
                best_param.fill_value(best_val, [dataname, perm], ['Data','perm'])
        return best_param

    def _search_param(self, dataname, perm, num_cv, search_param, best_perf=0.5):
        """Search either l2 only or l2 and l1 regularization"""
        if len(list(search_param.keys())) == 2:
            for l2 in search_param['reg_param']:
                for l1 in search_param['l1_reg_param']:
                    perf = self._compute_perf_cv(dataname, perm, num_cv, (l2, l1))
                    if perf > best_perf:
                        best_perf = perf
                        best_val = (l2,l1)
        else:
            for l2 in search_param['reg_param']:
                perf = self._compute_perf_cv(dataname, perm, num_cv, l2)
                if perf > best_perf:
                    best_perf = perf
                    best_val = l2
        return best_val

    def result_exists(self, dataname=None, perm_idx=1, split_type='test', fold=None,
                       num_cv=None, params=None, best_param=None, verbose=True):
        """Check whether file with results exists"""
        id_str = param2string(perm_idx, split_type, fold, num_cv, self.config['frac_train'])
        if best_param:
            params = best_param.get_value([dataname, perm_idx], ['Data','perm'])
        filename = self._get_filename(dataname, id_str, params)
        found = isfile(filename)
        return found

    def compute_perf(self, datasets, perm_idx, best_param):
        """For each dataset in the array datasets,
        collect the accuracy over the permutations"""
        perf = dict()
        for dataname in datasets:
            cur_perf = []
            for perm in perm_idx:
                id_str = param2string(perm, 'test', 1, 5, self.config['frac_train'])
                filename = self._get_filename(dataname, id_str,
                                              best_param.get_value([dataname, perm],
                                                                   ['Data','perm']))
                results = iop.load(filename)
                cur_perf.append(auc(results.preds, results.labels))
            perf[dataname] = cur_perf
        return perf


    def _train_and_predict(self, dataname, svm, dataset,
                           perm_idx, split_type, fold, num_cv, params):
        """Common code after instantiating predictor"""
        self._print_progress(split_type, dataname, perm_idx, fold, num_cv, params)

        (idx_train, idx_pred, id_str) = dataset.get_perm(perm_idx, split_type, fold, num_cv)
        self.id_str = id_str

        #svm.check_dimensions(dataset.examples[:,idx_train], dataset.labels[idx_train])
        svm.train(dataset.examples[:,idx_train], dataset.labels[idx_train])
        preds = svm.predict(dataset.examples[:,idx_pred])
        labels = dataset.labels[idx_pred]

        results = MLExptResults(dataname, preds, labels, params)
        iop.save(self._get_filename(dataname, id_str, params), results)

    def _compute_perf_cv(self, dataname, perm, num_cv, params):
        """Compute the average accuracy over the cross validation folds"""
        cur_perf = []
        for fold in range(1, num_cv+1):
            id_str = param2string(perm, 'val', fold, num_cv, self.config['frac_train'])
            filename = self._get_filename(dataname, id_str, params)
            results = iop.load(filename)
            cur_perf.append(auc(results.preds, results.labels))
        return mean(cur_perf)
    
    def _param_str(self, params):
        """Convert the parameters into a string"""
        if type(params) == type(1.0):
            return 'l2reg=%3.3f' % params
        else:
            return 'l2reg=%3.3f_l1reg=%3.3f' % params
            
    def _get_filename(self, dataname, id_str, params):
        """Filename of results
        self.algo needs to be defined by derived class
        """
        filename = '%s/results_%s_%s_%s_%s.pkl'\
            % (self.config['work_dir'], self.algo, dataname, self._param_str(params), id_str)
        return filename

    def _print_progress(self, split_type, dataname, perm_idx, fold, num_cv, params):
        """Which classifier is running now?
        self.algo needs to be defined by derived class
        """
        if split_type == 'val':
            print('Solving %s perm=%d cv %d:%d %s(%s)'\
                % (dataname, perm_idx, fold, num_cv, self.algo, self._param_str(params)))
        else:
            print('Solving %s perm=%d %s(%s)'\
                % (dataname, perm_idx, self.algo, self._param_str(params)))

class MLExptSVM(MLExptBase):
    """A wrapper around SVM with ansgd to compute test error"""

    def __init__(self, config):
        MLExptBase.__init__(self, config)
        self.algo = 'SVM'

    def train_and_predict(self, dataname=None, perm_idx=1, split_type='test',
                          fold=None, num_cv=None, reg_param=1.0, l1_reg_param=1.0,
                          best_param=None, verbose=False):
        """Train SVM and use to predict"""
        if verbose: print('train_and_predict')
        if self.result_exists(dataname, perm_idx, split_type, fold, num_cv,
                              reg_param, best_param, verbose):
            return
        dataset = GWASData(self.config['data_dir'] + dataname, self.config['frac_train'])
        dataset.load_data()
        if best_param:
            reg_param = best_param.get_value([dataname, perm_idx], ['Data','perm'])
        svm = SVM(reg_param)
        self._train_and_predict(dataname, svm, dataset, perm_idx, split_type, fold, num_cv,
                                reg_param)

class MLExptElasticNet(MLExptBase):
    """A wrapper around elastic net to compute test error"""

    def __init__(self, config):
        MLExptBase.__init__(self, config)
        self.algo = 'ElasticNet'

    def train_and_predict(self, dataname=None, perm_idx=1, split_type='test',
                          fold=None, num_cv=None, reg_param=1.0, l1_reg_param=1.0,
                          best_param=None, verbose=False):
        """Train ElasticNet and use to predict"""
        if verbose: print('train_and_predict')
        if self.result_exists(dataname, perm_idx, split_type, fold, num_cv,
                              (reg_param, l1_reg_param), best_param, verbose):
            return
        if best_param:
            (reg_param, l1_reg_param) = best_param.get_value([dataname, perm_idx], ['Data','perm'])
        dataset = GWASData(self.config['data_dir'] + dataname, self.config['frac_train'])
        dataset.load_data()
        svm = ElasticNet(reg_param, l1_reg_param)
        self._train_and_predict(dataname, svm, dataset, perm_idx, split_type, fold, num_cv,
                                (reg_param, l1_reg_param))

class MLExptGraphNet(MLExptBase):
    """A wrapper around graph net to compute test error"""

    def __init__(self, config):
        MLExptBase.__init__(self, config)
        self.algo = 'GraphNet'

    def train_and_predict(self, dataname=None, perm_idx=1, split_type='test',
                          fold=None, num_cv=None, reg_param=1.0, l1_reg_param=1.0,
                          best_param=None, verbose=False):
        """Train GraphNet and use to predict"""
        if verbose: print('train_and_predict')
        if self.result_exists(dataname, perm_idx, split_type, fold, num_cv,
                              (reg_param, l1_reg_param), best_param, verbose):
            return
        if best_param:
            (reg_param, l1_reg_param) = best_param.get_value([dataname, perm_idx], ['Data','perm'])
        dataset = GWASData(self.config['data_dir'] + dataname, self.config['frac_train'])
        dataset.load_data()
        svm = GraphNet(reg_param, l1_reg_param, dataset.laplacian.copy())
        self._train_and_predict(dataname, svm, dataset, perm_idx, split_type, fold, num_cv,
                                (reg_param, l1_reg_param))

class MLExptKernelElasticNet(MLExptBase):
    """A wrapper around kernel elastic net to compute test error"""

    def __init__(self, config):
        MLExptBase.__init__(self, config)
        self.algo = 'KernelElasticNet'

    def train_and_predict(self, dataname=None, perm_idx=1, split_type='test',
                          fold=None, num_cv=None, reg_param=1.0, l1_reg_param=1.0,
                          best_param=None, verbose=False):
        """Train KernelElasticNet and use to predict"""
        if verbose: print('train_and_predict')
        if self.result_exists(dataname, perm_idx, split_type, fold, num_cv,
                              (reg_param, l1_reg_param), best_param, verbose):
            return
        if best_param:
            (reg_param, l1_reg_param) = best_param.get_value([dataname, perm_idx], ['Data','perm'])
        dataset = GWASData(self.config['data_dir'] + dataname, self.config['frac_train'])
        dataset.load_data()
        svm = KernelElasticNet(reg_param, l1_reg_param, dataset.laplacian.copy())
        self._train_and_predict(dataname, svm, dataset, perm_idx, split_type, fold, num_cv,
                                (reg_param, l1_reg_param))

