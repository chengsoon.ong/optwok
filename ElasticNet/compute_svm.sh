#!/bin/bash
#PBS -N ansgd_svm
#PBS -A pMelb0105
#PBS -q serial
#PBS -l nodes=1:ppn=16
#PBS -l walltime=11:59:00
#PBS -l pmem=2000mb
python3 /home/cong/Optwok/ElasticNet/experiment.py /home/cong/Optwok/ElasticNet/conf_edward.py SVM

