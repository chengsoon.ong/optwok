"""A config file to set paths, and experimental parameters"""

#####################################################################
# Location of data
#####################################################################
import os
# Base directory
if os.path.isdir('/home/ubuntu/'):
    base_dir = '/home/ubuntu/'
elif os.path.isdir('/Users/cheng.ong/'):
    base_dir = '/Users/cheng.ong/'
else:
    print 'No base directory found'
    exit()
del os

# Source of data
data_dir = base_dir + 'Data/WTCCC/'
# Save trained SVMs and predictions here
work_dir = base_dir + 'temp/work/'
# Save the results and figures here
results_dir = base_dir + 'temp/results/'

#####################################################################
# Model selection
#####################################################################

datasets = ['bdWTC']
#datasets = ['bdWTC','cadWTC','cdWTC','htWTC','raWTC','t1dWTC','t2dWTC']
reg_param = [0.001, 0.01, 0.1, 1.]
l1_reg_param = [0.001, 0.01, 0.1, 1.]

#####################################################################
# Experimental setup
#####################################################################

# Use the following permutations for current experiment
perm_idx = [0,1,2,3,4,5,6,7,8,9]
# The data splits [frac_train,frac_test]
frac_train = 0.9
frac_test = 1-frac_train
# Data split settings for validation
validation_conf = {'split_type':'val', 'num_cv':5}
# Data split settings for the test set
test_conf = {'split_type':'test'}
