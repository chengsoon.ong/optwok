"""Apply elastic net classifier to WTCCC data
algo = ['SVM', 'ElasticNet', 'GraphNet', 'KernelElasticNet']
"""

import sys, os
import pickle, time
from pprint import pprint
from numpy import array, arange
from optwok.named_array import NArray
from optwok.expttools import apply_to_combination as atc
from optwok.mldata import generate_perm
from mlexpt import MLExptSVM, MLExptElasticNet, MLExptGraphNet, MLExptKernelElasticNet
try:
    import matplotlib.pyplot as plt
except ImportError:
    print('matplotlib not found')

def main(config):
    expt = eval('MLExpt'+config['algo']+'(config)')
    if config['algo'] == 'SVM':
        expt_param = {'dataname': config['datasets'],
                      'perm_idx': config['perm_idx'],
                      'reg_param': config['reg_param'],
                      'fold': list(range(1, config['validation_conf']['num_cv']+1)),
                      }
        search_param = {'reg_param': config['reg_param'],
                        }

    else:
        expt_param = {'dataname': config['datasets'],
                      'perm_idx': config['perm_idx'],
                      'reg_param': config['reg_param'],
                      'l1_reg_param': config['l1_reg_param'],
                      'fold': list(range(1, config['validation_conf']['num_cv']+1)),
                      }
        search_param = {'reg_param': config['reg_param'],
                        'l1_reg_param': config['l1_reg_param'],
                        }


        
    fparam = config['validation_conf'].copy()
    print('================')
    print('Cross validation')
    print('================')
    atc(expt.train_and_predict, expt_param, fixed=fparam, multiproc=True,
        check_result=expt.result_exists)

    print('===============')
    print('Find best param')
    print('===============')
    best_param = expt.find_best_param(config['datasets'], config['perm_idx'],
                                      config['validation_conf']['num_cv'],
                                      search_param)
    pprint(best_param.content)
    expt_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                  }
    fparam = config['test_conf'].copy()
    fparam['best_param'] = best_param
    print('===============')
    print('Test prediction')
    print('===============')    
    atc(expt.train_and_predict, expt_param, fixed=fparam, multiproc=True,
        check_result=expt.result_exists)
    auc_svm = expt.compute_perf(config['datasets'], config['perm_idx'], best_param)
    pprint(auc_svm)
    output = {'config':config,
              'auc': auc_svm,
              'best_param': best_param,
              }
    pickle.dump(output, open(config['results_dir']+config['algo']+'-'+
                 time.strftime('%Y-%m-%d-%H-%M-%S')+'.pkl', 'wb'))

def generate_all_perm(config):
    """Generate the permutation indices of the examples"""
    for data in config['datasets']:
        dataname = config['data_dir'] + data + '.csv'
        permfile = config['data_dir'] + data + '_perm.txt'
        print('Generating permutations for %s in %s' % (dataname, permfile))
        generate_perm(dataname, permfile)

def configure_from_file(filename, algo=None):
    """Configure the experiment"""
    config = dict()
    exec(compile(open(filename).read(), filename, 'exec'),config)
    config.pop('__builtins__')
    if algo:
        config['algo'] = algo
    return config

def collect_results(config):
    """Load all the results files and display"""
    all_algo = ['SVM', 'ElasticNet', 'GraphNet', 'KernelElasticNet']
    all_results = NArray(['Data','algo'], is_object=True)
    all_results.add_keys(all_algo, 'algo')
    all_results.add_keys(config['datasets'], 'Data')
    filelist = os.listdir(config['results_dir'])
    #newest = max(filelist, key=lambda x: os.stat(x).st_mtime)
    for dataname in config['datasets']:
        for filename in filelist:
            print('Reading %s%s' % (config['results_dir'], filename))
            result = pickle.load(open(config['results_dir']+filename,'rb'))
            algo = filename.split('-')[0]
            all_results.fill_value(array(result['auc'][dataname]),
                                   [dataname, algo],
                                   ['Data','algo'])
        show_results(all_results, dataname)
    plt.show()

def show_results(auc, dataname):
    results = auc.slice('Data', dataname)
    pprint(results)
    plt.figure()
    plt.boxplot(results)
    plt.ylabel('Area under ROC')
    plt.xticks(arange(4)+1, auc.keys('algo'))
    plt.title(dataname)
    curax = list(plt.axis())
    curax[2] = 0.5
    curax[3] = 0.9
    plt.axis(curax)
    plt.savefig('%s.pdf' % dataname, dpi=400, orientation='landscape',
                pad_inches=0)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python %s configure.py algorithm' % sys.argv[0])
        exit(1)
    filename = sys.argv[1]
    algo = sys.argv[2]
    if algo == 'perm':
        settings = configure_from_file(filename)
        generate_all_perm(settings)
        exit(0)
    elif algo == 'plot':
        settings = configure_from_file(filename)
        collect_results(settings)
        exit(0)        
    else:
        settings = configure_from_file(filename, algo)
        print('Running experiments with the following settings')
        pprint(settings)
        main(settings)

