import zipfile
import numpy
import shutil
from optwok.mldata import DatasetFileCSV
from numpy.random import permutation

def zip2txt(dataname):
    """Extract relevant files from zip archive"""
    filename = '%s/%s.zip' % (data_dir, dataname.upper())
    datazip = zipfile.ZipFile(filename)
    datazip.extract('%s/%s_train.data' % (dataname.upper(), dataname), data_dir)
    datazip.extract('%s/%s_valid.data' % (dataname.upper(), dataname), data_dir)
    datazip.extract('%s/%s_train.labels' % (dataname.upper(), dataname), data_dir)

    validzip = zipfile.ZipFile('%s/valid.zip' % data_dir)
    validzip.extract('%s_valid.labels' % dataname, '%s/%s' % (data_dir, dataname.upper()))


def read_matrix_dense(dataname):
    """Read data in the form of a dense matrix"""
    cur_data_dir = '%s/%s' % (data_dir, dataname.upper())
    data_tr = numpy.loadtxt('%s/%s_train.data' % (cur_data_dir, dataname))
    data_val = numpy.loadtxt('%s/%s_valid.data' % (cur_data_dir, dataname))
    examples = numpy.transpose(numpy.vstack([data_tr, data_val]))
    label_tr = numpy.loadtxt('%s/%s_train.labels' % (cur_data_dir, dataname))
    label_val = numpy.loadtxt('%s/%s_valid.labels' % (cur_data_dir, dataname))
    labels = numpy.concatenate([label_tr, label_val])
    return examples, labels


def merge_files(dataname, num_perms=20):
    """Merge four files into one dataset"""
    data_filename = '%s/%s.csv' % (data_dir, dataname)
    perm_filename = '%s/%s_perm.txt' % (data_dir, dataname)
    outfile = DatasetFileCSV(data_filename, 'vec', verbose=True)
    (examples, labels) = read_matrix_dense(dataname)
    (num_feat, num_ex) = examples.shape
    print 'Number of examples: %d' % num_ex
    print 'Number of features: %d' % num_feat
    outfile.writelines(examples, labels)

    handle = open(perm_filename, 'w')
    for iperm in range(num_perms):
        cur_mix = permutation(num_ex)
        handle.write(' '.join(map(str, cur_mix.tolist())))
        handle.write('\n')
    handle.close()

def fc2csv(data_dir, dataname):
    """Convert the feature selection challenge data into standard dataset"""
    print 'Converting %s' % dataname
    zip2txt(dataname)
    merge_files(dataname)
    shutil.rmtree('%s/%s' % (data_dir, dataname.upper()))

if __name__ == '__main__':
    data_dir = '/local/cong/Data/binclass_large'
    all_dataname = ['arcene', 'gisette', 'madelon']
    for dataname in all_dataname:
        fc2csv(data_dir, dataname)
    
