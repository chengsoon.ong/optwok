import numpy
import pylab

def hinge_loss(t, mu=1.0):
    """Standard SVM hinge loss"""
    return (1.0/mu)*max(0.0, mu-t)

def rev_hinge_loss(t, mu=1.0):
    """Try to fix hinge loss such that the resulting
    loss matches the 0-1 loss closely, but is non-convex.
    """
    return (1.0/mu)*max(0.0,-mu*mu+(mu*mu-1)*t)

def flat_rev_hinge_loss(t, mu=1.0):
    """Try to fix hinge loss such that the resulting
    loss matches the 0-1 loss closely, but is non-convex.
    """
    return (1.0/mu)*max(0.0,-mu*mu-t)

def symm_rev_hinge_loss(t, mu=1.0):
    """Try to fix hinge loss such that the resulting
    loss matches the 0-1 loss closely, but is non-convex.
    """
    return (1.0/mu)*max(0.0,-mu-t)

def ramp_loss(t, mu=1.0):
    """Difference of convex losses.
    Resulting loss upper bounds the 0-1 loss.
    """
    return hinge_loss(t, mu)-rev_hinge_loss(t, mu)

def flat_ramp_loss(t, mu=1.0):
    """Difference of convex losses.
    Resulting loss upper bounds the 0-1 loss.
    """
    return hinge_loss(t, mu)-flat_rev_hinge_loss(t, mu)

def symm_ramp_loss(t, mu=1.0):
    """Difference of convex losses.
    Resulting loss is symmetric around the 0-1 loss.
    """
    return 0.5*(hinge_loss(t, mu)-symm_rev_hinge_loss(t, mu))

def inv_hinge_loss(t, mu=1.0):
    """Express ramp loss as a ratio of two convex losses"""
    return (1.0/(mu+1.0))*max(mu+1.0, 1.0-t/mu)

def ramp_loss_ratio(t, mu=1.0):
    """Express ramp loss as a ratio of two convex losses"""
    return hinge_loss(t, mu)/inv_hinge_loss(t, mu)

def sloped_ramp_loss(t, mu=1.0):
    """Ramp loss with a slope instead of a truncation.
    Strictly quasiconvex.
    """
    if t > mu:
        loss = 0.0
    elif t <= mu and t >= -mu*mu:
        loss = 1.0 - t/mu
    else:
        loss = -mu*t/5.0 + 1 + mu - 0.2*mu**3
    return loss

def sloped_ramp_loss_nomu(t, mu=1.0):
    """Ramp loss with a slope instead of a truncation.
    Strictly quasiconvex.
    mu == 1
    """
    if t > 1.0:
        loss = 0.0
    elif t <= 1.0 and t >= -1.0:
        loss = 1.0 - t
    else:
        loss = (3.0-t)/2.0
    return loss

def plot_loss(loss):
    """Plot the different truncated losses"""
    t = numpy.arange(-2.0,2.0,0.01)
    all_mu = numpy.array([1.0, 0.7, 0.5, 0.3, 0.1])
    f = numpy.zeros((len(t),len(all_mu)))
    for (im, mu) in enumerate(all_mu):
        for ix in range(len(t)):
            f[ix,im] = loss(t[ix], mu)

    pylab.figure()
    pylab.plot(t,f)
    pylab.grid()
    pylab.axis([-2.0,2.0,-0.2,2.2])
    pylab.legend(all_mu)
    pylab.xlabel('$t$')
    pylab.ylabel('$\ell_\mu(t)$')
    pylab.show()


def feasibility_constraint(t, mu=1.0, s=1.0):
    """Convert the quasiconvex objective into its convex
    sublevel set by choosing a cutoff level s"""
    return hinge_loss(t, mu) - s*inv_hinge_loss(t, mu)


def plot_feasibility_constraint():
    all_s = numpy.arange(0.0, 10.0, 0.8)
    g = numpy.zeros((len(t),len(all_s)))
    for (ixs, s) in enumerate(all_s):
        for ix in range(len(t)):
            g[ix,ixs] = feasibility_constraint(t[ix], mu=0.5, s=s)

    pylab.figure()
    pylab.plot(t,g)
    pylab.grid()
    #pylab.axis([-2.0,2.0,-0.2,2.2])
    pylab.legend(all_s)
    pylab.show()


def approx_l0(t, a):
    """Approximate the l0 norm by exponential curves"""
    if t >= 0.0:
        return 1.0 - numpy.exp(-a*t)
    else:
        return 1.0 - numpy.exp(a*t)

def approx_l0_grad(t, a):
    """The gradient of the approximate l0 norm"""
    if t >= 0.0:
        return a*numpy.exp(-a*t)
    else:
        return -a*numpy.exp(a*t)

def cap_l1(t, a):
    """The capped l1 regularizer"""
    return min(a, abs(t))

def cap_l1_dc(t, a):
    """The capped l1 regularizer"""
    t_abs = abs(t)
    vex = t_abs
    if t_abs <= a:
        cave = 0.0
    else:
        cave = t_abs - a
    return vex - cave

def plot_reg(reg):
    t = numpy.arange(-2.0,2.0,0.01)
    all_a = numpy.array([3., 4., 5., 7.])
    #all_a = numpy.array([0.5, 1., 1.5])
    f = numpy.zeros((len(t),len(all_a)))
    for (ia, a) in enumerate(all_a):
        for ix in range(len(t)):
            f[ix,ia] = reg(t[ix], a)
            #f[ix,ia] = approx_l0(t[ix], a)
            #f[ix,ia] = approx_l0_grad(t[ix], a)
            #f[ix,ia] = cap_l1(t[ix], a)
            #f[ix,ia] = cap_l1_dc(t[ix], a)

    pylab.figure()
    pylab.plot(t,f)
    pylab.grid()
    pylab.axis([-2.0,2.0,-0.2,1.2])
    pylab.legend(all_a,loc='lower right')
    pylab.xlabel('$w$')
    pylab.ylabel('approx $||w||_0$')
    pylab.show()

def lp(t, p):
    """Compute the norm pth power of the l_p norm: ||t||_p^p"""
    return abs(t)**p

def plot_lp():
    t = numpy.arange(-2.0,2.0,0.01)
    all_p = numpy.array([1.0, 0.5, 0.3, 0.1])
    f = numpy.zeros((len(t),len(all_p)))
    for (ip, p) in enumerate(all_p):
        for ix in range(len(t)):
            f[ix,ip] = lp(t[ix], p)

    pylab.figure()
    pylab.plot(t,f)
    pylab.grid()
    pylab.axis([-2.0,2.0,-0.2,1.5])
    pylab.legend(all_p,loc='lower right')
    pylab.show()

def ratio_reg(t,p,q):
    """Ratio regularizer"""
    if type(t) != type(numpy.array([])):
        return (abs(t)**p)/(abs(t)**q)
    tot_p = 0.0
    tot_q = 0.0
    for ix in range(len(t)):
        tot_p += abs(t[ix])**p
        tot_q += abs(t[ix])**q
    return tot_p/tot_q

def plot_ratio_reg():
    t = numpy.arange(-2.0,2.0,0.01)
    p = numpy.arange(1.1,4,0.3)
    q = numpy.arange(1,2,0.1)
    f = numpy.zeros((len(t),len(p)))
    for ixp in range(len(p)):
        for ix in range(len(t)):
            f[ix,ixp] = ratio_reg(t[ix],p[ixp],q[ixp])
    pylab.figure()
    pylab.plot(t,f)
    pylab.grid()
    pylab.show()

def plot_ratio_reg_2d():
    a1 = numpy.arange(-2.0,2.0,0.01)
    a2 = numpy.arange(-2.0,2.0,0.01)
    x,y = numpy.meshgrid(a1,a2)
    t = numpy.array((numpy.ravel(x),numpy.ravel(y)))
    reg = numpy.zeros(t.shape[1])
    for ix in range(t.shape[1]):
        reg[ix] = ratio_reg(t[:,ix], 2, 1)
    reg.shape = (len(a1),len(a2))
    pylab.figure()
    pylab.pcolor(x,y,reg)
    pylab.contour(x,y,reg)
    pylab.show()

def cap_l1_dc_vex(t, a):
    """The capped l1 regularizer, convex part"""
    return abs(t-a)

def cap_l1_dc_cave(t, a):
    """The capped l1 regularizer, concave part"""
    t_abs = abs(t-a)
    if t_abs <= 1:
        cave = 0.0
    else:
        cave = t_abs - 1
    return cave

def plot_reg_dc():
    t = numpy.arange(-2.0,3.0,0.01)
    f_vex = numpy.zeros(len(t))
    f_cave = numpy.zeros(len(t))
    f = numpy.zeros(len(t))
    a = 0.7
    for ix in range(len(t)):
        f_vex[ix] = cap_l1_dc_vex(t[ix], a)
        f_cave[ix] = cap_l1_dc_cave(t[ix], a)
        f[ix] = f_vex[ix]-f_cave[ix]

    pylab.figure()
    pylab.subplot(311)
    pylab.plot(t,f_vex)
    pylab.title(r'Convex part: $| t- \nu |$')
    pylab.grid()
    pylab.axis([-2.0,3.0,-0.2,1.2])
    pylab.subplot(312)
    pylab.plot(t,f_cave)
    pylab.title(r'Concave part: $\max(0, |t-\nu|-1)$')
    pylab.grid()
    pylab.axis([-2.0,3.0,-0.2,1.2])
    pylab.subplot(313)
    pylab.plot(t,f)
    pylab.title('difference of convex')
    pylab.grid()
    pylab.axis([-2.0,3.0,-0.2,1.2])
    pylab.show()

def mil_loss_pos(t, sigma):
    """Multiple instance learning loss, positive class
    double kink hinge loss
    """
    wrong = 1.5*sigma - t
    margin = sigma - 0.5*t
    return max(max(0, margin), wrong)

def mil_loss_pos_diff(t, sigma):
    """Check that the double hinge is the same as
    the sum of two single hinges
    """
    hinge1 = max(0.0, sigma - 0.5*t)
    hinge2 = max(0.0, 0.5*(sigma - t))
    return hinge1 + hinge2

def mil_loss_neg(t, sigma):
    """Multiple instance learning loss, negative class
    scaled hinge loss
    """
    return max(0.0, 2.0*sigma-t)    

def mil_loss(t, sigma, label):
    if label > 0.0:
        return mil_loss_pos(t, sigma)
    else:
        return mil_loss_neg(t, sigma)

def plot_mil_loss():
    """Plot the different truncated losses"""
    t = numpy.arange(-2.0,5.0,0.01)
    all_sigma = numpy.array([2.0, 1.3, 0.5])
    f_pos = numpy.zeros((len(t),len(all_sigma)))
    for (im, mu) in enumerate(all_sigma):
        for ix in range(len(t)):
            f_pos[ix,im] = mil_loss(t[ix], mu, 1)
    f_neg = numpy.zeros((len(t),len(all_sigma)))
    for (im, mu) in enumerate(all_sigma):
        for ix in range(len(t)):
            f_neg[ix,im] = mil_loss(t[ix], mu, -1)
            #f_neg[ix,im] = mil_loss_pos_diff(t[ix], mu)

    pylab.figure()
    pylab.plot(t,f_pos)
    pylab.grid()
    pylab.axis([-2.0,5.0,-0.2,5.2])
    pylab.legend(all_sigma)
    pylab.xlabel('$t$')
    pylab.ylabel('$\ell_{\sigma+}(t)$')
    pylab.figure()
    pylab.plot(t,f_neg)
    pylab.grid()
    pylab.axis([-2.0,5.0,-0.2,5.2])
    pylab.legend(all_sigma)
    pylab.xlabel('$t$')
    pylab.ylabel('$\ell_{\sigma-}(t)$')
    pylab.show()

def dc_losses():
    """Investigating losses and regularizers
    for difference of convex functions"""
    pylab.rcParams['font.size'] = 16.0
    plot_loss(flat_ramp_loss)
    #plot_ratio_reg()
    #plot_ratio_reg_2d()
    plot_reg(approx_l0)
    #plot_lp()
    #plot_reg_dc()

if __name__ == '__main__':
    pylab.rcParams['font.size'] = 16.0
    plot_mil_loss()
