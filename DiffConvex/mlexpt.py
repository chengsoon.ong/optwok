from os.path import isfile
import cPickle
from numpy import mean, std, sort
from numpy import arange, setdiff1d
from numpy.random import permutation
from numpy import mean, std, sort, argmax, max
import optwok.classifier
from optwok.evaluation import accuracy, balanced_accuracy
from optwok.mldata import Dataset

class MLExpt(object):
    """A wrapper around a classifier to
    ease model selection and computation of test error
    """
    def __init__(self, config):
        self.data_dir = config['data_dir']                 # Where to load the data
        self.frac_train = config['frac_train']             # fraction used for training
        self.work_dir = config['work_dir']                 # The results of computation placed here
        self.results_file = config['results_file']         # Save the summary here
        self.config = config                               # Remember the configuration
        self.algo = eval('optwok.classifier.'+config['algo'])   # The classifier goes here
        if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
               or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
            self.has_ramp_loss = True
        else:
            self.has_ramp_loss = False
        self.kname = config['kernels'].keys()[0]           # Assume only have one kernel type

    def _get_kstr(self, kdict):
        """String identifier for kernel"""
        if kdict['name'] == 'poly':
            kstr = 'poly_deg=%d' % kdict['param']
        elif kdict['name'] == 'linear':
            kstr = 'linear'
        else:
            print('Unsupported kernel %s',str(kdict))
        return kstr

    def _get_filename(self, dataname, id_str, kdict, reg_param, mu=1.0):
        """Filename of results"""
        kstr = self._get_kstr(kdict)
        if self.has_ramp_loss:
            filename = '%s/results_%s_reg=%3.3f_mu=%1.1f_%s_%s.pkl'\
                       % (self.work_dir, dataname, reg_param, mu, kstr, id_str)
        else:
            filename = '%s/results_%s_reg=%3.3f_%s_%s.pkl'\
                       % (self.work_dir, dataname, reg_param, kstr, id_str)
        return filename

    def _print_progress(self, split_type, dataname, perm_idx, fold, num_cv, reg_param, mu, kdict):
        """Which classifier is running now?"""
        if self.has_ramp_loss:
            if split_type == 'val':
                print 'Solving %s:%d cv %d:%d %s(mu=%1.1f, reg=%3.3f, %s)'\
                      % (dataname, perm_idx, fold, num_cv, self.config['algo'], mu, reg_param, self._get_kstr(kdict))
            else:
                print 'Solving %s:%d %s(mu=%1.1f, reg=%3.3f, %s)'\
                      % (dataname, perm_idx, self.config['algo'], mu, reg_param, self._get_kstr(kdict))
        else:
            if split_type == 'val':
                print 'Solving %s:%d cv %d:%d %s(reg=%3.3f, %s)'\
                      % (dataname, perm_idx, fold, num_cv, self.config['algo'], reg_param, self._get_kstr(kdict))
            else:
                print 'Solving %s:%d %s(reg=%3.3f, %s)'\
                      % (dataname, perm_idx, self.config['algo'], reg_param, self._get_kstr(kdict))

    def result_exists(self, dataname=None, perm_idx=1, split_type='test', fold=None, num_cv=None,
                          reg_param=1.0, mu=1.0, kparams={}, kparam_idx=0, hyperparam=None,
                          algo=None, verbose=False):
        """Train Classifier. Intended for use for cross validation."""
        if verbose: print 'result_exists'
        if hyperparam:
            reg_param = hyperparam[dataname][perm_idx]['reg_param']
            kdict = {'name': self.kname, 'param': hyperparam[dataname][perm_idx]['kparam']}
        else:
            if self.kname == 'poly':
                kdict = {'name': self.kname, 'param': kparams[kparam_idx]}
            elif self.kname == 'linear':
                kdict = {'name': self.kname, 'param': 0}
        dataset = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train,
                          read_data=False)
        id_str = dataset.get_id_str(perm_idx, split_type, fold=fold, num_cv=num_cv)
        filename = self._get_filename(dataname, id_str, kdict, reg_param, mu)
        if isfile(filename):
            print 'Found %s' % filename
            return True
        else:
            return False
        
    def train_and_predict(self, dataname=None, perm_idx=1, split_type='test', fold=None, num_cv=None,
                          reg_param=1.0, mu=1.0, kparams={}, kparam_idx=0, hyperparam=None,
                          algo=None, verbose=False):
        """Train Classifier. Intended for use for cross validation."""
        if verbose: print 'train_and_predict'
        if hyperparam:
            reg_param = hyperparam[dataname][perm_idx]['reg_param']
            kdict = {'name': self.kname, 'param': hyperparam[dataname][perm_idx]['kparam']}
        else:
            if self.kname == 'poly':
                kdict = {'name': self.kname, 'param': kparams[kparam_idx]}
            elif self.kname == 'linear':
                kdict = {'name': self.kname, 'param': 0}
        dataset = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
        (idx_train, idx_pred, id_str) = dataset.get_perm(perm_idx, split_type,
                                                         fold=fold, num_cv=num_cv)
        filename = self._get_filename(dataname, id_str, kdict, reg_param, mu)
        if isfile(filename):
            return
        
        self._print_progress(split_type, dataname, perm_idx, fold, num_cv, reg_param, mu, kdict)
        if self.has_ramp_loss:
            svm = self.algo(reg_param, kdict, mu, verbose=verbose)
        else:
            svm = self.algo(reg_param, kdict, verbose=verbose)

        if 'rcn' in dataname:
            rcn = float(dataname[-3:])
            if rcn>0.0:
                num_ex = len(idx_train)
                flip_idx = idx_train[permutation(num_ex)[:round(rcn*num_ex)]]
                dataset.labels[flip_idx] = -1.0*dataset.labels[flip_idx]

        svm.train(dataset.examples[:,idx_train], dataset.labels[idx_train])
        preds = svm.pred(dataset.examples[:,idx_pred])
        labels = dataset.labels[idx_pred]

        results = {'dataname':dataname,
                   'reg_param': reg_param,
                   'kernel': kdict,
                   'preds': preds,
                   'labels': labels,
                   'perf': svm.perf,
                   }
        del svm
        output = open(filename, 'wb')
        # Pickle the list using the highest protocol available.
        cPickle.dump(results, output, -1)
        output.close()

    def train_cv(self, dataname=None, perm_idx=1, split_type='test', num_cv=None,
                 reg_param=1.0, mu=1.0, kparams={}, kparam_idx=0, hyperparam=None,
                 algo=None, verbose=False):
        """A for loop to perform cross validation"""
        for fold in range(1,num_cv+1):
            self.train_and_predict(dataname, perm_idx, split_type, fold, num_cv, reg_param,
                                   mu, kparams, kparam_idx, hyperparam, algo, verbose)

    def find_best_param(self, datasets, perm_idx, num_cv, search_param):
        """Find the best hyperparameter settings"""
        if self.kname == 'poly':
            ksearch = search_param['kparams']
        elif self.kname == 'linear':
            ksearch = [0]
        best_param = dict()
        for dataname in datasets:
            print 'Dataset: %s' % dataname
            best_param[dataname] = dict()
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for perm in perm_idx:
                best_acc = 0.5
                for reg_param in search_param['reg_param']:
                    for kparam in ksearch:
                        if self.has_ramp_loss:
                            for mu in search_param['mu']:
                                cur_param = {'reg_param':reg_param, 'mu':mu, 'kparam':kparam}
                                acc = self.compute_acc_cv(cur_data, perm, num_cv, **cur_param)
                                if acc > best_acc:
                                    best_acc = acc
                                    best_val = cur_param
                        else:
                            cur_param = {'reg_param':reg_param, 'kparam':kparam}
                            acc = self.compute_acc_cv(cur_data, perm, num_cv, **cur_param)
                            if acc > best_acc:
                                best_acc = acc
                                best_val = cur_param
                #print 'Permutation %d' % perm,
                #print best_val
                best_param[dataname][perm] = best_val.copy()
        return best_param

    def _get_acc_from_file(self, filename):
        """Helper function to load file and compute accuracy"""
        handle = open(filename, 'rb')
        try:
            results = cPickle.load(handle)
        except EOFError, error:
            print error
            print filename
        handle.close()
        cur_acc = balanced_accuracy(results['preds'],results['labels'])
        return cur_acc

    def _get_perf_from_file(self, filename):
        """Helper function to load file and compute performance"""
        handle = open(filename, 'rb')
        results = cPickle.load(handle)
        handle.close()
        return results['perf']

    def compute_acc_cv(self, dataset, perm_idx, num_cv, reg_param=1.0, mu=1.0, kparam=0):
        """Compute the average accuracy over the
        cross validation folds"""
        kdict = {'name':self.kname, 'param': kparam}
        cur_acc = []
        for fold in range(1,num_cv+1):
            (idx_train, idx_pred, id_str) = dataset.get_perm(perm_idx, 'val',
                                                             fold=fold, num_cv=num_cv)
            filename = self._get_filename(dataset.name, id_str, kdict, reg_param, mu)
            ca = self._get_acc_from_file(filename)
            cur_acc.append(ca)
        return mean(cur_acc)

    def compute_perf(self, datasets, perm_idx, best_param):
        """Compute the average accuracy over the
        permutations"""

        acc = dict()
        for dataname in datasets:
            cur_perf = {'acc':[]}
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for perm in perm_idx:
                (idx_train, idx_pred, id_str) = cur_data.get_perm(perm, 'test')
                kdict = {'name': self.kname, 'param': best_param[dataname][perm]['kparam']}
                if self.has_ramp_loss:
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  best_param[dataname][perm]['reg_param'],
                                                  best_param[dataname][perm]['mu'])
                else:
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  best_param[dataname][perm]['reg_param'])
                ca = self._get_acc_from_file(filename)
                cur_perf['acc'].append(ca)
                perf = self._get_perf_from_file(filename)
                for key in perf.keys():
                    if cur_perf.has_key(key):
                        cur_perf[key].append(perf[key])
                    else:
                        cur_perf[key] = [perf[key]]
            acc[dataname] = {}
            acc[dataname+'_all'] = {}
            for key in cur_perf.keys():
                if key == 'nonzero_feat': continue
                acc[dataname+'_all'][key] = cur_perf[key]
                if key == 'all_objective': continue
                acc[dataname][key] = (mean(cur_perf[key]), std(cur_perf[key]))
        return acc

    def compute_perf_cheat(self, datasets, perm_idx, best_param, cheat_param):
        """Compute the average accuracy over the
        permutations.
        Cheat by choosing best mu on test set.
        """
        acc = dict()
        for dataname in datasets:
            cur_perf = {'acc':[]}
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for perm in perm_idx:
                (idx_train, idx_pred, id_str) = cur_data.get_perm(perm, 'test')
                kdict = {'name': self.kname, 'param': best_param[dataname][perm]['kparam']}
                acc_mu = []
                for cparam in cheat_param:
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  best_param[dataname][perm]['reg_param'],
                                                  cparam)
                    acc_mu.append(self._get_acc_from_file(filename))
                best_mu = cheat_param[argmax(acc_mu)]
                filename = self._get_filename(cur_data.name, id_str, kdict,
                                              best_param[dataname][perm]['reg_param'],
                                              best_mu)
                ca = self._get_acc_from_file(filename)
                cur_perf['acc'].append(ca)
                perf = self._get_perf_from_file(filename)
                for key in perf.keys():
                    if cur_perf.has_key(key):
                        cur_perf[key].append(perf[key])
                    else:
                        cur_perf[key] = [perf[key]]
            acc[dataname] = {}
            acc[dataname+'_all'] = {}
            for key in cur_perf.keys():
                acc[dataname][key] = (mean(cur_perf[key]), std(cur_perf[key]))
                acc[dataname+'_all'][key] = cur_perf[key]
        return acc

    def compute_perf_diff(self, datasets, perm_idx, best_param):
        """Compute the difference between the best mu and mu=1.0.
        """
        acc = dict()
        for dataname in datasets:
            cur_perf = {'aa_acc1':[],'aa_frac_sv1':[],
                        'acc':[],'frac_sv':[],'mu':[]}
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for perm in perm_idx:
                (idx_train, idx_pred, id_str) = cur_data.get_perm(perm, 'test')
                kdict = {'name': self.kname, 'param': best_param[dataname][perm]['kparam']}
                filename = self._get_filename(cur_data.name, id_str, kdict,
                                              best_param[dataname][perm]['reg_param'],
                                              1.0)
                ca = self._get_acc_from_file(filename)
                perf = self._get_perf_from_file(filename)
                cur_perf['aa_acc1'].append(ca)
                cur_perf['aa_frac_sv1'].append(perf['frac_sv'])
                filename = self._get_filename(cur_data.name, id_str, kdict,
                                              best_param[dataname][perm]['reg_param'],
                                              best_param[dataname][perm]['mu'])
                ca = self._get_acc_from_file(filename)
                perf = self._get_perf_from_file(filename)
                cur_perf['acc'].append(ca)
                cur_perf['frac_sv'].append(perf['frac_sv'])
                cur_perf['mu'].append(best_param[dataname][perm]['mu'])

            acc[dataname] = {}
            acc[dataname+'_all'] = {}
            for key in cur_perf.keys():
                acc[dataname][key] = (mean(cur_perf[key]), std(cur_perf[key]))
                acc[dataname+'_all'][key] = cur_perf[key]
        return acc

    def compute_frac_sv(self, datasets, perm_idx, mus):
        """Compute the fraction of support vectors"""
        frac_sv = dict()
        frac_sv['mu'] = mus
        for dataname in datasets:
            frac_sv[dataname] = {'mean':[],'stderr':[]}
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for mu in mus:
                cur_frac = []
                for perm in perm_idx:
                    (idx_train, idx_pred, id_str) = cur_data.get_perm(perm, 'test')
                    kdict = {'name': self.kname, 'param': 0}
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  1.0, mu)
                    perf = self._get_perf_from_file(filename)
                    cur_frac.append(perf['frac_sv'])
                frac_sv[dataname]['mean'].append(mean(cur_frac))
                frac_sv[dataname]['stderr'].append(std(cur_frac))
        return frac_sv

    def compute_found_feat(self, datasets, perm_idx, best_param):
        """Compute the false discovery and false negatives,
        assuming that the first 10 features are the true ones."""

        acc = dict()
        for dataname in datasets:
            cur_perf = {'false_positive':[],
                        'false_negative':[]}
            cur_data = Dataset(dataname, data_dir=self.data_dir, frac_train=self.frac_train)
            for perm in perm_idx:
                (idx_train, idx_pred, id_str) = cur_data.get_perm(perm, 'test')
                kdict = {'name': self.kname, 'param': best_param[dataname][perm]['kparam']}
                if self.has_ramp_loss:
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  best_param[dataname][perm]['reg_param'],
                                                  best_param[dataname][perm]['mu'])
                else:
                    filename = self._get_filename(cur_data.name, id_str, kdict,
                                                  best_param[dataname][perm]['reg_param'])
                perf = self._get_perf_from_file(filename)
                cur_perf['false_positive'].append(len(setdiff1d(perf['nonzero_feat'],arange(10))))
                cur_perf['false_negative'].append(len(setdiff1d(arange(10),perf['nonzero_feat'])))
            acc[dataname] = {}
            acc[dataname+'_all'] = {}
            for key in cur_perf.keys():
                acc[dataname][key] = (mean(cur_perf[key]), std(cur_perf[key]))
                acc[dataname+'_all'][key] = cur_perf[key]
        return acc

    def show_results(self, acc, time_unit='ms'):
        """Pretty print the summary"""
        outstr = '\\hline\n Data '
        sorted_headings = sort(acc[acc.keys()[0]].keys())
        for heading in sorted_headings:
            outstr += ' & %s ' % heading.replace('_',' ')
        outstr +='\\\\\n\\hline\n'
        for ck in acc.keys():
            if ck.find('_all') >= 0: continue
            outstr += '%s' % ck.replace('_',' ')
            for cp in sorted_headings:
                if cp == 'all_objective': continue
                outstr += ' & %2.2f $\pm$ %2.2f ' % acc[ck][cp]
            outstr += '\\\\\n\\hline\n'

        print outstr

        # Save to file
        output = open(self.results_file+'.pkl', 'wb')
        cPickle.dump((self.config, acc), output, -1)
        output.close()
        text_file = open(self.results_file+'.txt', 'w')
        text_file.writelines(outstr)
        text_file.close()
