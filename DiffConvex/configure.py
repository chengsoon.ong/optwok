""" A config file which can be modified to set the paths root directories, etc."""

#####################################################################
### Location of data
#####################################################################

import os
# Base directory
if os.path.isdir('/local/cong/'):
    base_dir = '/local/cong/'
    temp_dir = base_dir
elif os.path.isdir('/Users/cong/'):
    base_dir = '/Users/cong/'
    temp_dir = base_dir + 'temp/'
elif os.path.isdir('/cluster/home/infk/cong/'):
    base_dir = '/cluster/home/infk/cong/'
    temp_dir = base_dir + 'temp/'
else:
    print 'No base directory found'
    exit()
del os

# Source of data and permutations
data_dir = base_dir + 'Data/binclass'

# Save trained SVMs and predictions here
work_dir = '%s/work_%s/' % (temp_dir, algo)

# Save the figures and summary tables here
results_file = temp_dir + 'results/' + algo

#####################################################################
### Model selection
#####################################################################

datasets = ['pima','bupa','ionosphere','wpbc','sonar']
#datasets = ['bupa']

kernels = {'linear':
           {'param': 0},
           }

reg_params = [0.1, 0.2, 0.3, 0.5, 1.0, 2.0, 3.0, 5.0, 10.0]
mus = [1.0]

#####################################################################
### Experimental setup
#####################################################################

# File which has permutations of example indices stored
permutation = 'permutations.txt'
# maximum number of permutations generated 
max_perm = 50

# Use the following permutations for current experiment
perm_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
#perm_idx = [0,1,2,3,4,5,6,7,8,9]

# The data splits [frac_train,frac_test]
frac_train = 0.7
frac_test = 1-frac_train

# Data split settings for validation
validation_conf = {'split_type':'val', 'num_cv':5}

# Data split settings for the test set
test_conf = {'split_type':'test'}

