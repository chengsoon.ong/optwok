import sys
from pprint import pprint
from optwok.expttools import apply_to_combination as atc
import rlsvm


def configure_from_file(filename):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{},config)
    return config


def main(config):
    """Run the experiment for rlsvm"""
    expt = rlsvm.ExptRLSVM(config)
    # The experiments, do the cross product of all these settings.
    expt_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                  'C': config['Cs'],
                  'mu': config['mus'],
                  'poly_deg': config['poly_deg'],
                  'fold': range(1,config['validation_conf']['num_cv']+1),
                 }
    gauss_widths = dict()
    for dataname in config['datasets']:
        gauss_widths[dataname] = expt.get_gauss_widths(dataname, config['kernels']['gauss']['num_width'])

    print '==============================================================='
    print 'Do cross validation on the training set to find best parameters'
    print '==============================================================='
    fparam = config['validation_conf']
    fparam['algo'] = config['train_algo']
    fparam['gauss_widths'] = gauss_widths
    atc(expt.train_and_predict, expt_param, fixed=fparam)

    search_param = {'C': config['Cs'],
                    'mu': config['mus'],
                    'widths': gauss_widths,
                    }
    best_param = expt.find_best_param(config['datasets'], config['perm_idx'],
                                 config['validation_conf']['num_cv'], search_param)

    print '==================================================='
    print 'Use the best parameters to compute test performance'
    print '==================================================='
    expt_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                 }
    fparam = config['test_conf']
    fparam['algo'] = config['train_algo']
    fparam['hyperparam'] = best_param
    atc(expt.train_and_predict, expt_param, fixed=fparam)

    print '================================='
    print 'Computing performance on test set'
    print '================================='
    acc = expt.compute_perf(config['datasets'], config['perm_idx'], best_param)

    print '==========='
    print 'The results'
    print '==========='
    pprint(best_param)
    expt.show_results(acc)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s config_file.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    settings = configure_from_file(filename)
    print 'Running experiments with the following settings'
    pprint(settings)
    main(settings)
    
