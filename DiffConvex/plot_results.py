"""
Collect and plot results for comparing
- hinge loss vs ramp loss.
- l2, l1, l0 regularization.
"""
import cPickle
from numpy import linspace, zeros, arange, transpose, sort
from pylab import show, figure, savefig
from pylab import plot, bar, boxplot, subplot
from pylab import axis, xlabel, ylabel, legend, title, xticks
import pylab

num_perm = 10

def load_results(res_dir, classifiers):
    """
    Load all the pickled results
    """
    results = {}
    for c in classifiers:
        filename = res_dir + c + '.pkl'
        handle = open(filename, 'rb')
        results[c] = cPickle.load(handle)
        handle.close()
    return results

def collect_results_toydata_both(results, classifiers, num_examples, noise_feats, rcns, toprint):
    """
    Collect and plot the results for toy data.
    - the total fraction of parameters against increasing noise dims and rcn.
    """
    print 'collect_results_toydata_both'
    # Matrix for increasing both rcn and noise feat
    assert(len(noise_feats)==len(rcns))
    feat_vs_both = zeros((len(classifiers), len(num_examples), len(noise_feats), num_perm))
    acc_vs_both = zeros((len(classifiers), len(num_examples), len(noise_feats), num_perm))
    for (cix, c) in enumerate(classifiers):
        for (nix, num_ex) in enumerate(num_examples):
            for ix in range(len(noise_feats)):
                data_name = 'twoballs_n=%d_10:%d_rcn=%1.1f_all' % (num_ex, noise_feats[ix], rcns[ix])
                #print 'Classifier: %s' % c
                #print 'Number of permutations: %d' % len(results[c][1][data_name]['frac_total'])
                feat_vs_both[cix, nix, ix, :] = results[c][1][data_name]['frac_total']
                acc_vs_both[cix, nix, ix, :] = results[c][1][data_name]['acc']


    for (nix, num_ex) in enumerate(num_examples):
        figure(figsize=(16,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,4,cix+1)
            boxplot(transpose(feat_vs_both[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1,["0;0.0","10;0.1","100;0.2","200;0.3"])
            if cix == 0 or cix == 4:
                ylabel('Fraction of parameters')
            if cix > 3:
                xlabel('noisy features ; noisy labels')
            axis([0.5, 4.5, 0.0, 1.0])
        if toprint:
            savefig('totfeat_vs_both_%d.eps' % (num_ex))

        figure(figsize=(16,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,4,cix+1)
            boxplot(transpose(acc_vs_both[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1,["0;0.0","10;0.1","100;0.2","200;0.3"])
            if cix == 0 or cix == 4:
                ylabel('Accuracy')
            if cix > 3:
                xlabel('noisy features ; noisy labels')
            axis([0.5, 4.5, 0.0, 1.0])
        if toprint:
            savefig('acc_vs_both_%d.eps' % (num_ex))
            
    if not toprint:
        show()
    
def collect_results_toydata_noisedim_feat(results, classifiers, num_examples, noise_feats, toprint):
    """
    Collect and plot the results for toy data.
    - The fraction of features against increasing noise dimensions.
    """
    print 'collect_results_toydata_noisedim_feat'
    # Matrix for increasing noise feat
    feat_vs_noise = zeros((len(classifiers), len(num_examples), len(noise_feats), num_perm))
    for (cix, c) in enumerate(classifiers):
        for (nix, num_ex) in enumerate(num_examples):
            for ix in range(len(noise_feats)):
                data_name = 'twoballs_n=%d_10:%d_rcn=0.0_all' % (num_ex, noise_feats[ix])
                feat_vs_noise[cix, nix, ix, :] = results[c][1][data_name]['frac_feat']

    for (nix, num_ex) in enumerate(num_examples):
        figure(figsize=(12,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,3,cix+1)
            boxplot(transpose(feat_vs_noise[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1, noise_feats)
            if cix == 0 or cix == 3:
                ylabel('Fraction of features used')
            if cix >= 3:
                xlabel('number of noisy features')
            axis([0.5, 4.5, 0.0, 1.1])
        if toprint:
            savefig('feat_vs_noise_%d.eps' % (num_ex))

    if not toprint:
        show()
    
def collect_results_toydata_noisedim_acc(results, classifiers, num_examples, noise_feats, toprint):
    """
    Collect and plot the results for toy data.
    - The accuracy against increasing noise dimensions.
    """
    print 'collect_results_toydata_noisedim_acc'
    # Matrix for increasing noise feat
    acc_vs_noise = zeros((len(classifiers), len(num_examples), len(noise_feats), num_perm))
    for (cix, c) in enumerate(classifiers):
        for (nix, num_ex) in enumerate(num_examples):
            for ix in range(len(noise_feats)):
                data_name = 'twoballs_n=%d_10:%d_rcn=0.0_all' % (num_ex, noise_feats[ix])
                acc_vs_noise[cix, nix, ix, :] = results[c][1][data_name]['acc']

    for (nix, num_ex) in enumerate(num_examples):
        figure(figsize=(16,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,4,cix+1)
            boxplot(transpose(acc_vs_noise[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1, noise_feats)
            if cix == 0 or cix == 4:
                ylabel('Accuracy')
            if cix >= 4:
                xlabel('number of noisy features')
            axis([0.5, 4.5, 0.5, 1.0])
        if toprint:
            savefig('acc_vs_noise_%d.eps' % (num_ex))
            
    if not toprint:
        show()
    
def collect_results_toydata_rcn(results, classifiers, num_examples, rcns, toprint):
    """
    Collect and plot the results for toy data.
    - The fraction of support vectors against increasing random classification noise.
    """
    print 'collect_results_toydata_rcn'
    # Matrix for increasing both rcn and noise feat
    sv_vs_rcn = zeros((len(classifiers), len(num_examples), len(rcns), num_perm))
    acc_vs_rcn = zeros((len(classifiers), len(num_examples), len(rcns), num_perm))
    for (cix, c) in enumerate(classifiers):
        for (nix, num_ex) in enumerate(num_examples):
            for ix in range(len(rcns)):
                data_name = 'twoballs_n=%d_10:0_rcn=%1.1f_all' % (num_ex, rcns[ix])
                sv_vs_rcn[cix, nix, ix, :] = results[c][1][data_name]['frac_sv']
                acc_vs_rcn[cix, nix, ix, :] = results[c][1][data_name]['acc']

    for (nix, num_ex) in enumerate(num_examples):
        figure(figsize=(16,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,4,cix+1)
            boxplot(transpose(sv_vs_rcn[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1, rcns)
            if cix == 0 or cix == 4:
                ylabel('Fraction of support vectors')
            if cix >= 4:
                xlabel('Proportion of noisy labels')
            axis([0.5, 4.5, 0.0, 1.0])
        if toprint:
            savefig('sv_vs_rcn_%d.eps' % (num_ex))

        figure(figsize=(16,6))
        for (cix, c) in enumerate(classifiers):
            subplot(2,4,cix+1)
            boxplot(transpose(acc_vs_rcn[cix,nix,:,:]))
            title(c)
            xticks(arange(4)+1, rcns)
            if cix == 0 or cix == 4:
                ylabel('Accuracy')
            if cix >= 4:
                xlabel('Proportion of noisy labels')
            axis([0.5, 4.5, 0.7, 1.0])
        if toprint:
            savefig('acc_vs_rcn_%d.eps' % (num_ex))
            
    if not toprint:
        show()
    
def collect_results_uci(results, classifiers, toprint):
    """
    Collect and plot results for UCI data.
    - fraction of used features vs fraction of support vectors
    - total used fraction vs bars for each classifier for each dataset
    """
    print 'collect_results_uci'
    datasets = ['pima','bupa','ionosphere','wpbc','sonar']
    marker = ['x','*','+','o','D','s','^','>']
    
    # Results for frac feat vs frac sv
    for (dix, d) in enumerate(datasets):
        figure()
        feats = zeros((1,1))
        svs = zeros((1,1))
        for (cix, c) in enumerate(classifiers):
            svs[0,0] = results[c][1][d]['frac_sv'][0]
            feats[0,0] = results[c][1][d]['frac_feat'][0]
            plot(svs, feats, 'k'+marker[cix])
        title(d)
        axis([-0.1, 1.1, -0.1, 1.1])
        xlabel('Fraction of support vectors')
        ylabel('Fraction of features used')
        if d == 'wpbc':
            legend(classifiers,loc=(0.6,0.1))
        if toprint:
            savefig('feat_vs_sv_%s.eps' % d)

    # Results for the total fraction for each dataset.
    pylab.rcParams['legend.fontsize'] = '12'
    color = ['b','g','r','c','m','y','k','b']
    figure()
    width = 0.11
    rect = zeros(len(classifiers)).tolist()
    for (dix, d) in enumerate(datasets):
        for (cix, c) in enumerate(classifiers):
            ft = results[c][1][d]['frac_total'][0]
            eb = results[c][1][d]['frac_total'][1]
            rect[cix] = bar(dix+cix*width, ft, width, color=color[cix], yerr=eb)
    for (idx, data) in enumerate(datasets):
        datasets[idx] = data[:5]
    xticks(arange(len(datasets))+0.4, datasets)
    ylabel('Fraction of parameters')
    legend(rect, classifiers)
    axis([0.0, len(datasets), 0.0, 1.0])
    if toprint:
        savefig('params_all.eps')
    if not toprint:
        show()

def _collect_numiter(results, classifiers, datasets, outfile, toprint):
    """Plot the number of iterations used in DCA"""
    print 'collect_numiter'
    #datasets = ['pima','bupa','ionosphere','wpbc','sonar']
    pylab.rcParams['font.size'] = 16.0
    if len(datasets) == 5:
        figure(figsize=(16,3))
    elif len(datasets) == 2:
        figure(figsize=(8,3))
    for (idx, d) in enumerate(datasets):
        data = d+'_all'
        num_iter = zeros((num_perm, len(classifiers)))
        for (svm_idx, svm) in enumerate(classifiers):
            if svm == 'SVMPrimal' or svm == 'OneNormSVM':
                continue
            num_iter[:,svm_idx] = results[svm][1][data]['num_iter']
        subplot(1, len(datasets), idx+1)
        boxplot(num_iter[:,2:])
        if idx == 0:
            ylabel('Number of iterations')
        axis([0.5, 6.5, 1.0, 16.0])
        title(d)
    if toprint:
        #savefig('num_iter_uci.eps')
        savefig(outfile)
    if not toprint:
        show()

def collect_numiter(results, classifiers, toprint, data='uci'):
    if data == 'uci':
        datasets = ['pima','bupa','ionosphere','wpbc','sonar']
        outfile = 'num_iter_uci.eps'
    elif data == 'fsc':
        datasets = ['arcene', 'gisette']
        outfile = 'num_iter_fsc.eps'
    _collect_numiter(results, classifiers, datasets, outfile, toprint)

def _collect_train_time(results, classifiers, datasets, outfile, toprint):
    """Plot the training time used in DCA"""
    print 'collect_train_time'
    #datasets = ['pima','bupa','ionosphere','wpbc','sonar']
    pylab.rcParams['font.size'] = 16.0
    if len(datasets) == 5:
        figure(figsize=(16,3))
    elif len(datasets) == 2:
        figure(figsize=(8,3))
    for (idx, d) in enumerate(datasets):
        data = d+'_all'
        train_time = zeros((num_perm, len(classifiers)))
        for (svm_idx, svm) in enumerate(classifiers):
            if svm == 'SVMPrimal' or svm == 'OneNormSVM':
                continue
            train_time[:,svm_idx] = results[svm][1][data]['train_time']
        subplot(1, len(datasets), idx+1)
        boxplot(train_time[:,2:])
        if idx == 0:
            ylabel('Training time (s)')
        #axis([0.5, 6.5, 1.0, 16.0])
        title(d)
    if toprint:
        #savefig('train_time_uci.eps')
        savefig(outfile)
    if not toprint:
        show()

def collect_train_time(results, classifiers, toprint, data='uci'):
    if data == 'uci':
        datasets = ['pima','bupa','ionosphere','wpbc','sonar']
        outfile = 'train_time_uci.eps'
    elif data == 'fsc':
        datasets = ['arcene', 'gisette']
        outfile = 'train_time_fsc.eps'
    _collect_train_time(results, classifiers, datasets, outfile, toprint)

def collect_objective_value(results, svm, toprint):
    """Plot the objective value for all iterations in DCA"""
    print 'collect_objective_value'
    if svm == 'SVMPrimal' or svm == 'OneNormSVM':
        return
    datasets = ['pima','bupa','ionosphere','wpbc','sonar']
    pylab.rcParams['font.size'] = 16.0
    figure(figsize=(16,3))
    for (idx, d) in enumerate(datasets):
        data = d+'_all'
        subplot(1, len(datasets), idx+1)
        all_objective = results[svm][1][data]['all_objective']
        for objective_val in all_objective:
            obj = sort(objective_val)[::-1]
            plot(obj/obj[0],'b-')
            plot(obj/obj[0],'bx')
        if idx == 0:
            ylabel('Objective value')
        #axis([0.5, 6.5, 1.0, 16.0])
        xlabel('Number of iterations')
        title(d)
    if toprint:
        savefig('objective_uci.eps')
    if not toprint:
        show()



if __name__ == '__main__':
    
    import os
    toprint = True
    #pylab.rcParams['font.size'] = 22.0
    #pylab.rcParams['legend.fontsize'] = 'small'
    #pylab.rcParams['lines.markersize'] = 12
    if os.path.isdir('/local/cong/'):
        basedir = '/local/cong/'
    elif os.path.isdir('/Users/cong/'):
        basedir = '/Users/cong/temp/'
    
    classifiers = ['SVMPrimal','OneNormSVM','ZeroNormSVM','CapOneNormSVM','RLSVM2','RLSVM1','RLSVM0','RLSVMC1']
    sparse_classifiers = ['OneNormSVM','ZeroNormSVM','CapOneNormSVM','RLSVM1','RLSVM0','RLSVMC1']
    num_examples = [100]
    noise_feats = [0, 10, 100, 200]
    rcns = linspace(0, 0.3, 4)
    #results = load_results(basedir+'results_toydata/', classifiers)
    #collect_results_toydata_both(results, classifiers, num_examples, noise_feats, rcns, toprint)
    #collect_results_toydata_rcn(results, classifiers, num_examples, rcns, toprint)
    #collect_results_toydata_noisedim_acc(results, classifiers, num_examples, noise_feats, toprint)

    #results = load_results(basedir+'results_toydata/', sparse_classifiers)
    #collect_results_toydata_noisedim_feat(results, sparse_classifiers, num_examples, noise_feats, toprint)
    
    results = load_results(basedir+'results/', classifiers)
    #collect_results_uci(results, classifiers, toprint)
    #collect_numiter(results, classifiers, toprint)
    #collect_train_time(results, classifiers, toprint)

    #collect_objective_value(results, 'RLSVM0', toprint)

    collect_numiter(results, classifiers, toprint, data='fsc')
    collect_train_time(results, classifiers, toprint, data='fsc')
