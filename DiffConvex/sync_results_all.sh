rsync -av cong@ice.ethz.ch:/local/cong/results/ ~/temp/results
rsync -av cong@ice.ethz.ch:/local/cong/results_toydata/ ~/temp/results_toydata
rsync -av cong@ice.ethz.ch:/local/cong/results_mu/ ~/temp/results_mu

rsync -av cong@ice.ethz.ch:/local/cong/work_SVMPrimal/ ~/temp/work_SVMPrimal
rsync -av cong@ice.ethz.ch:/local/cong/work_OneNormSVM/ ~/temp/work_OneNormSVM
rsync -av cong@ice.ethz.ch:/local/cong/work_ZeroNormSVM/ ~/temp/work_ZeroNormSVM
rsync -av cong@ice.ethz.ch:/local/cong/work_CapOneNormSVM/ ~/temp/work_CapOneNormSVM
rsync -av cong@ice.ethz.ch:/local/cong/work_RLSVM2/ ~/temp/work_RLSVM2
rsync -av cong@ice.ethz.ch:/local/cong/work_RLSVM1/ ~/temp/work_RLSVM1
rsync -av cong@ice.ethz.ch:/local/cong/work_RLSVM0/ ~/temp/work_RLSVM0
rsync -av cong@ice.ethz.ch:/local/cong/work_RLSVMC1/ ~/temp/work_RLSVMC1

