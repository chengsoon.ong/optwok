import sys
from pprint import pprint
from optwok.expttools import apply_to_combination as atc
from mlexpt import MLExpt

lsf_param = {'wall_time_h':7,
             'wall_time_m':55,
             'memory':7000,
             }

def configure_from_file(filename, algo):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{'algo':algo},config)
    config['algo'] = algo
    return config


def main(config):
    """Run the experiment for rlsvm"""
    # The experiments, do the cross product of all these settings.
    expt_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                  'reg_param': config['reg_params'],
                  'fold': range(1,config['validation_conf']['num_cv']+1),
                 }
    if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
           or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
        expt_param['mu'] = config['mus']
    expt = MLExpt(config)

    print '==============================================================='
    print 'Do cross validation on the training set to find best parameters'
    print '==============================================================='
    fparam = config['validation_conf']
    atc(expt.train_and_predict, expt_param, fixed=fparam, multiproc=True, cluster=True,
        check_result=expt.result_exists, lsf_param=lsf_param)
    #atc(expt.train_cv, expt_param, fixed=fparam, multiproc=True, cluster=True)
    #atc(expt.train_cv, expt_param, fixed=fparam, multiproc=False, cluster=False)

    search_param = {'reg_param': config['reg_params'],
                    'kparam_idx': 0,
                    }
    if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
           or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
        search_param['mu'] = config['mus']
    else:
        search_param['mu'] = [1.0]

    best_param = expt.find_best_param(config['datasets'], config['perm_idx'],
                                 config['validation_conf']['num_cv'], search_param)

    print '==================================================='
    print 'Use the best parameters to compute test performance'
    print '==================================================='
    test_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                 }
    fparam = config['test_conf']
    fparam['algo'] = config['algo']
    fparam['hyperparam'] = best_param
    atc(expt.train_and_predict, test_param, fixed=fparam, multiproc=True, cluster=True,
        check_result=expt.result_exists, lsf_param=lsf_param)

    print '================================='
    print 'Computing performance on test set'
    print '================================='
    acc = expt.compute_perf(config['datasets'], config['perm_idx'], best_param)

    print '==========='
    print 'The results'
    print '==========='
    #pprint(best_param)
    expt.show_results(acc)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: python %s configure.py algo' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    algo = sys.argv[2]
    settings = configure_from_file(filename, algo)
    print 'Running %s experiments with the following settings' % algo
    pprint(settings)
    main(settings)
