#!/bin/bash
python experiment.py configure_toydata.py SVMPrimal
python experiment.py configure_toydata.py OneNormSVM
python experiment.py configure_toydata.py CapOneNormSVM
python experiment.py configure_toydata.py RLSVM2
python experiment.py configure_toydata.py RLSVM1
python experiment.py configure_toydata.py RLSVMC1
python experiment.py configure_toydata.py ZeroNormSVM
python experiment.py configure_toydata.py RLSVM0

python experiment.py configure.py SVMPrimal
python experiment.py configure.py OneNormSVM
python experiment.py configure.py CapOneNormSVM
python experiment.py configure.py RLSVM2
python experiment.py configure.py RLSVM1
python experiment.py configure.py RLSVMC1
python experiment.py configure.py ZeroNormSVM
python experiment.py configure.py RLSVM0


#python experiment.py configure_mu.py RLSVM0
#python experiment.py configure_mu.py RLSVM2
#python experiment.py configure_mu.py RLSVM1
# Precompute everything for mu
python compute_all.py configure_mu.py RLSVM2
#python collect_results.py configure_mu.py RLSVM2

# Longer experiments on Brutus
#./sync_results.sh
#python experiment.py configure_toydata.py ZeroNormSVM
#python experiment.py configure_toydata.py RLSVM0
#python experiment.py configure.py ZeroNormSVM
#python experiment.py configure.py RLSVM0

