"""Compare the fraction of support vectors for RLSVM2"""

import sys
from pprint import pprint
from optwok.expttools import apply_to_combination as atc
from pylab import show, figure, savefig, subplot
from pylab import errorbar, xticks, yticks, grid
from pylab import axis, xlabel, ylabel, legend, title
import pylab
from mlexpt import MLExpt

def configure_from_file(filename, algo):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{'algo':algo},config)
    config['algo'] = algo
    return config

def plot_mu_sv(algo, datasets, mus, frac_sv, toprint=True):
    """
    Collect and plot the results on the effect of mu
    on the fraction of support vectors
    """
    pylab.rcParams['font.size'] = 16.0
    figure(figsize=(16,3))
    for (idx, data) in enumerate(datasets):
        print data
        subplot(1, len(datasets), idx+1)
        errorbar(mus, frac_sv[data]['mean'], yerr=frac_sv[data]['stderr'])
        title(data)
        axis([0.0, 1.1, 0.0, 0.5])
        xticks(mus[::4],mus[::4])
        grid()
        if idx==0:
            ylabel('Fraction of support vectors')
        else:
            yticks([0.1,0.2,0.3,0.4],[' ',' ',' ',' '])
    if toprint:
        savefig('mu_effect_%s.eps' % algo)
    else:
        show()

def main(config):
    """Run the experiment for rlsvm"""
    print '=================='
    print 'Collecting results'
    print '=================='
    search_param = {'reg_param': config['reg_params'],
                    'kparam_idx': 0,
                    }
    if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
           or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
        search_param['mu'] = config['mus']
    else:
        search_param['mu'] = [1.0]
    expt = MLExpt(config)

    frac_sv = expt.compute_frac_sv(config['datasets'], config['perm_idx'],
                                   config['mus'])
    plot_mu_sv(config['algo'], config['datasets'], config['mus'], frac_sv)

    best_param = expt.find_best_param(config['datasets'], config['perm_idx'],
                                 config['validation_conf']['num_cv'], search_param)
    acc = expt.compute_perf_diff(config['datasets'], config['perm_idx'], best_param)

    print '==========='
    print 'The results'
    print '==========='
    #pprint(best_param)
    expt.show_results(acc)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: python %s configure.py algo' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    algo = sys.argv[2]
    settings = configure_from_file(filename, algo)
    print 'Running %s experiments with the following settings' % algo
    pprint(settings)
    main(settings)
