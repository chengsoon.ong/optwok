"""Code used to process colorectal cancer data"""

import sys
import numpy
import csv
import cPickle
from optwok.mldata import generate_perm, DatasetFileCSV
from optwok.named_array import NArray
from optwok.evaluation import balanced_accuracy

from pylab import hist, figure, title, show
from pylab import xlabel, xticks, ylabel, savefig
from scipy.io import savemat

cancer2label = {
    'Healthy donor':0,
    'CRC_stageI':1,
    'CRC_stageII':2,
    'CRC_stageIII':3,
    'CRC_stageIV':4,
    'Lung cancer':5,
    'Pancreatic cancer':6,
    'Non-malignant gastrointestinal condition':7,
    }


def filter_data(raw_file, data_file, name_file):
    """Convert raw data into one multiclass dataset"""
    print "Converting raw data from %s to csv in %s" % (raw_file, data_file)
    print "Patient ID and protein names stored in %s" % name_file
    reader = csv.reader(open(raw_file,'r'), delimiter=';', quoting=csv.QUOTE_NONE)
    reader.next()

    data = NArray(['Patient', 'Feature'], max_elem=400)
    data.add_key('label', 'Feature')
    for item in reader:
        data.add_key(item[0], 'Patient', warn=False)
        data.add_key(item[2], 'Feature', warn=False)
        data.fill_value(cancer2label[item[1]], [item[0], 'label'], ['Patient', 'Feature'])
        if item[8] == 'NA':
            measurement = numpy.nan
        else:
            measurement = float(item[8])
            
        data.fill_value(measurement, [item[0], item[2]], ['Patient', 'Feature'])

    print '%d patients, %d proteins' % (data.shape[0], data.shape[1]-1)
    writer = csv.writer(open(data_file,'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    for row in data.content:
        writer.writerow(row)
    writer = csv.writer(open(name_file,'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    writer.writerow(data.keys('Feature'))
    writer.writerow(data.keys('Patient'))
    

def split_to_binary(pos_label, neg_label, in_file, out_file):
    """Split multiclass dataset into binary dataset"""
    print "Selecting subset of examples for binary problem"
    reader = csv.reader(open(in_file,'r'), delimiter=',', quoting=csv.QUOTE_NONE)
    writer = csv.writer(open(out_file,'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    for row in reader:
        if int(float(row[0])) in pos_label:
            row[0] = 1.0
            writer.writerow(row)
        elif int(float(row[0])) in neg_label:
            row[0] = -1.0
            writer.writerow(row)

def remove_missing(in_file, out_file, in_name_file, out_name_file):
    """Remove features with missing values"""
    print "Filtering out features with missing values"
    all_data = DatasetFileCSV(in_file, 'vec', verbose=True)
    (examples, labels) = all_data.readlines()
    num_pos = len(numpy.nonzero(labels > 0)[0])
    num_neg = len(numpy.nonzero(labels < 0)[0])
    print '%d positive, %d negative' % (num_pos, num_neg)
    idx_missing = numpy.any(numpy.isnan(examples), axis=1)
    clean_ex = examples[-idx_missing,:]
    print "Features with all measurements: %d" % clean_ex.shape[0]
    clean_data = DatasetFileCSV(out_file, 'vec')
    clean_data.writelines(clean_ex, labels)

    names = csv.reader(open(in_name_file, 'r'), delimiter=',', quoting=csv.QUOTE_NONE)
    proteins = numpy.array(names.next()[1:])
    proteins = proteins[-idx_missing].tolist()
    prot_names = csv.writer(open(out_name_file, 'w'), delimiter=',', quoting=csv.QUOTE_NONE)
    prot_names.writerow(proteins)

def get_perf_from_file(filename):
    handle = open(filename, 'rb')
    results = cPickle.load(handle)
    handle.close()
    return results['perf']

def get_acc_from_file(filename):
    """Helper function to load file and compute accuracy"""
    handle = open(filename, 'rb')
    results = cPickle.load(handle)
    handle.close()
    cur_acc = balanced_accuracy(results['preds'],results['labels'])
    return cur_acc
                                                
def found_feat(dataname, feat_names, algo, reg_param):
    """Collect the histogram of feature usage"""
    num_perm = 50
    plot_freq = True

    names_file = csv.reader(open(feat_names, 'r'), delimiter=',', quoting=csv.QUOTE_NONE)
    proteins = numpy.array(names_file.next())
    for (ix,prot) in enumerate(proteins):
        if len(prot) > 5:
            proteins[ix] = prot[:5]
    num_feat = len(proteins)

    all_features = numpy.zeros((num_feat, num_perm))
    all_acc = numpy.zeros(num_perm)
    used_feat = []
    for iperm in range(num_perm):
        if 'RLSVM' in algo:
            filename = '/local/cong/work_crc/%s/results_%s_reg=%3.3f_mu=1.0_linear_%d_r0.80.pkl' % (algo, dataname, reg_param, iperm)
        else:
            filename = '/local/cong/work_crc/%s/results_%s_reg=%3.3f_linear_%d_r0.80.pkl' % (algo, dataname, reg_param, iperm)
        perf = get_perf_from_file(filename)
        cur_feat = perf['nonzero_feat']
        all_features[cur_feat, iperm] = 1
        all_acc[iperm] = get_acc_from_file(filename)
        used_feat.extend(cur_feat.tolist())
    if plot_freq:
        figure()
        (n, bins, patches) = hist(used_feat, bins=num_feat)
        title(dataname)
        xlabel('features')
        bincenters = 0.5*(bins[1:]+bins[:-1])
        xticks(bincenters, proteins, rotation=70)
        ylabel('frequency of feature selection')
        savefig('/local/cong/work_crc/%s_%s.pdf' % (dataname, algo))

        print bincenters
        print proteins
    print 'Accuracy for each split'
    print all_acc
    savemat('/local/cong/work_crc/%s_%s.mat' % (dataname, algo),
            {'all_features': all_features, 'all_acc': all_acc})
    


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s (preproc | found_feat)' % sys.argv[0]
        exit(1)

    # [1,2,3,4]; [0]; 0.1
    dataname = 'CRC_H'

    # [1,2,3,4]; [0]; 0.1
    #dataname = 'CRC3_H'

    # [1,2,3,4]; [0,5,6]; 5.0
    #dataname = 'CRC_HLP'

    # [1,2,3,4]; [0,5]; 10.0
    #dataname = 'CRC_HL'

    # [1,2,3,4]; [0,6]; 10.0
    #dataname = 'CRC_HP'

    all_data = 'cancer_%s_missing.csv' % dataname
    clean_data = 'cancer_%s.csv' % dataname
    prot_list = 'cancer_%s.names' % dataname
    perm_file = 'cancer_%s_perm.txt' % dataname

    if sys.argv[1] == 'preproc':
        # Creating data
        #filter_data('colorectal_cancer.raw', 'colorectal_cancer.csv', 'colorectal_cancer.names')
        split_to_binary([1,2,3,4], [0], 'colorectal_cancer.csv', all_data)
        remove_missing(all_data, clean_data, 'colorectal_cancer.names', prot_list)
        generate_perm(clean_data, perm_file)

    elif sys.argv[1] == 'found_feat':
        # Analysing results
        classifiers = ['OneNormSVM','CapOneNormSVM','RLSVM1','RLSVMC1']
        for algo in classifiers:
            found_feat('cancer_%s' % dataname, prot_list, algo, 10.0)

    else:
        print 'Usage: python %s (preproc | found_feat)' % sys.argv[0]
        exit(1)
