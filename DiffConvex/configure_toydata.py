""" A config file which can be modified to set the paths root directories, etc."""

#####################################################################
### Location of data
#####################################################################

import os
# Base directory
if os.path.isdir('/local/cong/'):
    base_dir = '/local/cong/'
    temp_dir = base_dir
elif os.path.isdir('/Users/cong/'):
    base_dir = '/Users/cong/'
    temp_dir = base_dir + 'temp/'
elif os.path.isdir('/cluster/home/infk/cong/'):
    base_dir = '/cluster/home/infk/cong/'
    temp_dir = base_dir + 'temp/'
else:
    print 'No base directory found'
    exit()
del os

# Source of data and permutations
data_dir = base_dir + 'Data/binclass'

# Save trained SVMs and predictions here
work_dir = '%s/work_%s/' % (temp_dir, algo)

# Save the figures and summary tables here
results_file = temp_dir + 'results_toydata/' + algo


#####################################################################
### Model selection
#####################################################################

num_feat = 10
#num_examples = [50, 100]
num_examples = [100]
noise_feats = [0, 10, 100, 200]
rcns = [0.0, 0.1, 0.2, 0.3]

num_datasets = len(noise_feats)
assert(len(rcns)==num_datasets)

datasets = []
for num_ex in num_examples:
    for num_noise_feat in noise_feats:
        datasets.append('twoballs_n=%d_10:%d_rcn=%1.1f' % (num_ex, num_noise_feat, 0.0))
    for rcn in rcns:
        datasets.append('twoballs_n=%d_10:%d_rcn=%1.1f' % (num_ex, 0, rcn))
    for ix in range(num_datasets):
        datasets.append('twoballs_n=%d_10:%d_rcn=%1.1f' % (num_ex, noise_feats[ix], rcns[ix]))

kernels = {'linear':
           {'param': 0},
           }

reg_params = [0.1, 0.2, 0.3, 0.5, 1.0, 2.0, 3.0, 5.0, 10.0]
mus = [1.0]

#####################################################################
### Experimental setup
#####################################################################

# File which has permutations of example indices stored
permutation = 'permutations.txt'
# maximum number of permutations generated 
max_perm = 50

# Use the following permutations for current experiment
perm_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
#perm_idx = [0,1,2,3,4,5,6,7,8,9]

# The data splits [frac_train,frac_test]
frac_train = 0.7
frac_test = 1-frac_train

# Data split settings for validation
validation_conf = {'split_type':'val', 'num_cv':5}

# Data split settings for the test set
test_conf = {'split_type':'test'}

