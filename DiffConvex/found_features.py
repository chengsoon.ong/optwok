"""Compare the fraction of support vectors for RLSVM2"""

import sys
from pprint import pprint
from optwok.expttools import apply_to_combination as atc
from mlexpt import MLExpt
from matplotlib.pyplot import show, figure, savefig, subplot
from matplotlib.pyplot import boxplot, xticks, yticks, grid
from matplotlib.pyplot import axis, xlabel, ylabel, legend, title
import pylab
from numpy import zeros, arange, max

num_examples = [100]
num_noise_feats = [0, 10, 100, 200]

def configure_from_file(filename, algo):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{'algo':algo},config)
    config['algo'] = algo
    return config

def collect_found_feat(algo, datasets, num_perm, acc):
    """
    Collect the results on the
    false positive and false negative features.
    """
    fp = zeros((num_perm, len(num_noise_feats)*len(num_examples)))
    fn = zeros((num_perm, len(num_noise_feats)*len(num_examples)))
    for (idx, nnf) in enumerate(num_noise_feats):
        for (nix, num_ex) in enumerate(num_examples):
            dataname = 'twoballs_n=%d_10:%d_rcn=0.0_all' % (num_ex, nnf)
            fp[:,idx*len(num_examples)+nix] = acc[dataname]['false_positive']
            fn[:,idx*len(num_examples)+nix] = acc[dataname]['false_negative']
    return fp, fn
    
def plot_found_feat(classifiers, all_fp, all_fn, toprint=True):
    """
    Plot the results on the
    false positive and false negative features.
    """
    #pylab.rcParams['font.size'] = 22.0
    figure(figsize=(12,6))
    for (aix, algo) in enumerate(classifiers):
        subplot(2,3,aix+1)
        fp = all_fp[aix,:,:]
        boxplot(fp)
        title(algo)
        if aix==0 or aix==3:
            ylabel('false positive features')
        if aix>2:
            xlabel('number of noisy features')
        xticks(arange(len(num_noise_feats))+1, num_noise_feats)
        if max(fp) > 20:
            print 'Warning: axis truncation!'
        axis([0.5, 4.5, 0, 20])
    if toprint:
        savefig('feat_false_positive.eps')
    figure(figsize=(12,6))
    for (aix, algo) in enumerate(classifiers):
        subplot(2,3,aix+1)
        fn = all_fn[aix,:,:]
        boxplot(fn)
        title(algo)
        if aix==0 or aix==3:
            ylabel('false negative features')
        if aix>2:
            xlabel('number of noisy features')
        xticks(arange(len(num_noise_feats))+1, num_noise_feats)
        axis([0.5, 4.5, 0, 10])
    if toprint:
        savefig('feat_false_negative.eps')
    if not toprint:
        show()

def collect(config):
    """Run the experiment for rlsvm"""
    print '================================'
    print 'Collecting results: %s' % config['algo']
    print '================================'
    search_param = {'reg_param': config['reg_params'],
                    'kparam_idx': 0,
                    }
    search_param['mu'] = [1.0]
    expt = MLExpt(config)

    best_param = expt.find_best_param(config['datasets'], config['perm_idx'],
                                 config['validation_conf']['num_cv'], search_param)
    acc = expt.compute_found_feat(config['datasets'], config['perm_idx'], best_param)
    return collect_found_feat(algo, config['datasets'], len(config['perm_idx']), acc)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    classifiers = ['OneNormSVM','ZeroNormSVM','CapOneNormSVM','RLSVM1','RLSVM0','RLSVMC1']
    fp = zeros((len(classifiers), 20, len(num_noise_feats)))
    fn = zeros((len(classifiers), 20, len(num_noise_feats)))
    for (aix, algo) in enumerate(classifiers):
        filename = sys.argv[1]
        settings = configure_from_file(filename, algo)
        (fp[aix,:,:], fn[aix,:,:]) = collect(settings)
    plot_found_feat(classifiers, fp, fn)
