from scipy.io import savemat
from optwok.mldata import DatasetFileCSV
from optwok.evaluation import accuracy
from numpy.random import permutation
from pylab import hist, figure, title, show
from pylab import xlabel, ylabel, savefig
from numpy import zeros
import cPickle

def create_loo_perm(dataname):
    """Create a permutation file where the last item is always different,
    for leave one out cross validation"""
    data = DatasetFileCSV(dataname+'.csv', 'vec', verbose=True)
    (examples, labels) = data.readlines()
    (num_feat, num_examples) = examples.shape
    permfile = open(dataname+'_perm.txt','w')
    for iperm in range(num_examples):
        curperm = permutation(num_examples)
        for idx in range(num_examples):
            if curperm[idx] != iperm:
                permfile.write(str(curperm[idx])+' ')
        permfile.write(str(iperm)+'\n')
    permfile.close()

def get_perf_from_file(filename):
    handle = open(filename, 'rb')
    results = cPickle.load(handle)
    handle.close()
    return results['perf']

def get_acc_from_file(filename):
    """Helper function to load file and compute accuracy"""
    handle = open(filename, 'rb')
    results = cPickle.load(handle)
    handle.close()
    cur_acc = accuracy(results['preds'],results['labels'])
    return cur_acc
                                                
def found_feat(dataname, algo):
    """Collect the histogram of feature usage"""
    num_perm = 37
    num_feat = 22
    plot_freq = False
    used_feat = []

    all_features = zeros((num_feat, num_perm))
    all_acc = zeros(num_perm)
    for iperm in range(num_perm):
        if 'RLSVM' in algo:
            filename = '/local/cong/work_dcm/%s/results_%s_reg=0.100_mu=1.0_linear_%d_r0.97.pkl' % (algo, dataname, iperm)
        else:
            filename = '/local/cong/work_dcm/%s/results_%s_reg=0.100_linear_%d_r0.97.pkl' % (algo, dataname, iperm)
        perf = get_perf_from_file(filename)
        cur_feat = perf['nonzero_feat']
        all_features[cur_feat, iperm] = 1
        all_acc[iperm] = get_acc_from_file(filename)
        used_feat.extend(cur_feat.tolist())
    if plot_freq:
        figure()
        hist(used_feat, bins=num_feat)
        title(algo)
        xlabel('features')
        ylabel('frequency of feature selection')
        savefig('/local/cong/work_dcm/%s_%s.png' % (dataname, algo))
    print all_acc
    savemat('/local/cong/work_dcm/%s_%s.mat' % (dataname, algo),
            {'all_features': all_features, 'all_acc': all_acc})
    
    
if __name__ == '__main__':
    #found_feat('R_Z_C4_normalized', 'OneNormSVM')
    found_feat('R_Z_C4_normalized', 'CapOneNormSVM')
    #found_feat('R_Z_C4_normalized', 'ZeroNormSVM')
    #found_feat('R_Z_C4_normalized', 'RLSVM1')
    #found_feat('R_Z_C4_normalized', 'RLSVMC1')
    #found_feat('R_Z_C4_normalized', 'RLSVM0')
    show()
