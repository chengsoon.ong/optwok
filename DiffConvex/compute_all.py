import sys
from pprint import pprint
from optwok.expttools import apply_to_combination as atc

def configure_from_file(filename, algo):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{'algo':algo},config)
    config['algo'] = algo
    return config


def main(config):
    """Run the experiment for rlsvm"""
    print '======================================='
    print 'Do cross validation on the training set'
    print '======================================='
    expt_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                  'reg_param': config['reg_params'],
                  'fold': range(1,config['validation_conf']['num_cv']+1),
                 }
    if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
           or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
        expt_param['mu'] = config['mus']
        from mlexpt_mu import MLExpt
    else:
        from mlexpt import MLExpt
    expt = MLExpt(config)
    fparam = config['validation_conf']
    atc(expt.train_and_predict, expt_param, fixed=fparam, multiproc=True, cluster=True)

    print '============================'
    print 'Compute the test performance'
    print '============================'
    test_param = {'dataname': config['datasets'],
                  'perm_idx': config['perm_idx'],
                  'reg_param': config['reg_params'],
                 }
    if (config['algo'] == 'RLSVM2') or (config['algo'] == 'RLSVM1')\
           or (config['algo'] == 'RLSVMC1') or (config['algo'] == 'RLSVM0'):
        test_param['mu'] = config['mus']
    fparam = config['test_conf']
    fparam['algo'] = config['algo']
    atc(expt.train_and_predict, test_param, fixed=fparam, multiproc=True, cluster=True)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: python %s configure.py algo' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    algo = sys.argv[2]
    settings = configure_from_file(filename, algo)
    print 'Running %s experiments with the following settings' % algo
    pprint(settings)
    main(settings)
