""" A config file which can be modified to set the paths root directories, etc."""

#####################################################################
### Location of data
#####################################################################

import os
# Base directory
if os.path.isdir('/local/cong/'):
    base_dir = '/local/cong/'
    temp_dir = base_dir
elif os.path.isdir('/Users/cong/'):
    base_dir = '/Users/cong/'
    temp_dir = base_dir + 'temp/'
elif os.path.isdir('/cluster/home/infk/cong/'):
    base_dir = '/cluster/home/infk/cong/'
    temp_dir = base_dir
else:
    print 'No base directory found'
    exit()


# Source of data and permutations
data_dir = base_dir + 'Data/multiclass'

# Save trained SVMs and predictions here
work_dir = '%s/work_%s/' % (temp_dir, algo)

# Save the figures and summary tables here
results_file = temp_dir + 'results_dfr/' + algo

#####################################################################
### Model selection
#####################################################################

datasets = ['iris','wine','glass','vehicle']
width = [2.4, 16.0, 1.1, 5.0]

modelfile = '%s/svm_model' % work_dir


#####################################################################
### Experimental setup
#####################################################################

# Use the following permutations for current experiment
#perm_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
perm_idx = [20]

# The data splits [frac_train,frac_test]
#frac_train = 0.78414712841471279  # USPS split
#frac_train = 0.5  # INEX split

frac_train = 0.7

# Data split settings for the test set
test_conf = {'split_type':'test'}

