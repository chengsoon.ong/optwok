# TODO
# - Move mhc data class definition from data.py to mhc_data.py
# - Unify experimental result structure

import cPickle
import os
from numpy import array, zeros, ones, kron
from numpy import hstack, transpose, unique, vstack
from numpy import mean, argsort, array, arange, log10

from matplotlib.pyplot import figure, plot, show, axis
from matplotlib.pyplot import title, ylabel, boxplot
from matplotlib.pyplot import matshow, colorbar, xticks, yticks
        
from optwok.classifier import SVMPrimal
from optwok.evaluation import balanced_accuracy, stats_binormal
from optwok.evaluation import balanced_accuracy_multitask, stats_binormal_multitask
from optwok.io_pickle import load, save
from optwok.kernelopt import KernelOptLS
from optwok.kernel import GaussKernel, LinearKernel
from optwok.named_array import NArray

#####################################################################
# Data management
#####################################################################

def data_stats(all_data, task_list=None, num_cv=5):
    """Print some statistics about each dataset"""
    if task_list:
        all_tasks = task_list
    else:
        all_tasks = all_data.keys()

    for task in all_tasks:
        data = all_data[task]
        #num_ex = 0
        #for lab in data.class_labels:
        #    num_ex += len(lab)
        #print 'Number of examples in %s : %d' % (task, num_ex)
        print 'Number of examples in %s : %d' % (task, data.nof_peptides)

def get_split(data, split, num_cv=5, verbose=False, binclass=True):
    """Concatenate the data not in 'split' into the training set.
    'split' is the test set.
    """
    train_idx = range(num_cv)
    train_idx.remove(split)

    test_ex = transpose(array(data.encoded_peptides[split]))
    num_feat = test_ex.shape[0]
    test_lab = array(data.class_labels[split])
    #test_lab = array(data.scaled_labels[split])
    train_ex = zeros((num_feat,0))
    train_lab = []
    for cur_idx in train_idx:
        train_ex = hstack([train_ex, transpose(array(data.encoded_peptides[cur_idx]))])
        if binclass:
            train_lab.extend(data.class_labels[cur_idx])
        else:
            # ic50 score less than 500 is considered to be binding
            train_lab.extend(-log10(data.ic50s[cur_idx])+log10(500))
    train_lab = array(train_lab)

    if verbose:
        print 'Split %d' % split
        print 'features=%d, num_train=%d, num_test=%d'\
              % (train_ex.shape[0], train_ex.shape[1], test_ex.shape[1])
        
    return train_ex, train_lab, test_ex, test_lab

def get_merged(all_tasks, all_data, split):
    """Merge the data from all_tasks together"""
    train_ex = []
    train_lab = []
    test_ex = []
    test_lab = []
    for task in all_tasks:
        data = all_data[task]
        tre, trl, tee, tel = get_split(data, split)
        train_ex.append(tre)
        train_lab.append(trl)
        test_ex.append(tee)
        test_lab.append(tel)
    train_ex = hstack(train_ex)
    train_lab = hstack(train_lab)
    test_ex = hstack(test_ex)
    test_lab = hstack(test_lab)

    return train_ex, train_lab, test_ex, test_lab

def get_multitask(all_tasks, all_data, split, binclass=True):
    """Treat each task as a different label"""
    train_ex = []
    train_lab_list = []
    test_ex = []
    test_lab_list = []
    num_train = 0
    num_test = 0
    for task in all_tasks:
        data = all_data[task]
        tre, trl, tee, tel = get_split(data, split, binclass=binclass)
        num_train += len(trl)
        num_test += len(tel)
        train_ex.append(tre)
        train_lab_list.append(trl)
        test_ex.append(tee)
        test_lab_list.append(tel)
    train_ex = hstack(train_ex)
    test_ex = hstack(test_ex)

    # Construct the matrix of labels
    train_lab = zeros((len(all_tasks), num_train))
    idx = 0
    for (ix, trl) in enumerate(train_lab_list):
        train_lab[ix, idx:idx+len(trl)] = trl
        idx += len(trl)
    test_lab = zeros((len(all_tasks), num_test))
    idx = 0
    for (ix, tel) in enumerate(test_lab_list):
        test_lab[ix, idx:idx+len(tel)] = tel
        idx += len(tel)

    return train_ex, train_lab, test_ex, test_lab
    
#####################################################################
# The learning methods
#####################################################################

def single_task(all_data, results, name='single_task'):
    """Use SVMs on each task individually"""
    reg_param = 1.0
    kdict = {'name':'linear', 'param': 0}

    for task in results.keys('Task'):
        if task in ['B_4001','B_5701']:
            print 'These tasks give zero variance classifiers'
            continue
        
        data = all_data[task]
        print 'Classifying %s which has %d examples' % (task, data.nof_peptides)
        num_cv = len(results.keys('CV'))
        for split in results.keys('CV'):
            train_ex, train_lab, test_ex, test_lab = get_split(data, split, num_cv=num_cv, verbose=True)
            svm = SVMPrimal(reg_param, kdict, verbose=False)
            svm.train(train_ex, train_lab)
            svmout = svm.pred(test_ex)
            acc = balanced_accuracy(svmout, test_lab)
            print 'Accuracy: %1.2f' % acc
            results.fill_value(acc, [split, task, name, 'accuracy'],
                               ['CV','Task','Method','Perf'])
            res = stats_binormal(svmout, test_lab)
            print 'Area PR:  %1.2f' % res[4]
            results.fill_value(acc, [split, task, name, 'area PR'],
                               ['CV','Task','Method','Perf'])

def multitask_concatenate(all_data, results, name='multitask_cat'):
    """Merge data from all tasks together and train one SVM"""
    reg_param = 1.0
    kdict = {'name':'linear', 'param': 0}

    for split in results.keys('CV'):
        # Use multitask setting for test labels, such that we can evaluate properly
        train_ex, train_lab, test_ex, dummy = get_merged(all_tasks, all_data, split)
        dummy, dummy, dummy, test_lab = get_multitask(all_tasks, all_data, split)
        
        svm = SVMPrimal(reg_param, kdict, verbose=False)
        print 'Split %d: SVM with %d features and %d examples'\
              % (split, train_ex.shape[0], train_ex.shape[1])
        svm.train(train_ex, train_lab)
        svmout = svm.pred(test_ex)

        num_task = len(all_tasks)
        balacc = balanced_accuracy_multitask(kron(svmout,ones((num_task,1))), test_lab)
        res = stats_binormal_multitask(kron(svmout,ones((num_task,1))), test_lab)
        for (ix, task) in enumerate(results.keys('Task')):
            print 'Accuracy (%s): %1.2f' % (task, balacc[ix])
            print 'AUC: %1.2f, APR: %1.2f' % (res[3][ix], res[4][ix])
            results.fill_value(balacc[ix], [split, task, name, 'accuracy'],
                               ['CV','Task','Method','Perf'])
            results.fill_value(res[4][ix], [split, task, name, 'area PR'],
                               ['CV','Task','Method','Perf'])

def learn_output(all_data, results, name='multitask_learn'):
    """Learn the output kernel between tasks"""
    #from pylab import hist, show
    reg_param = 1.0
    num_tasks = results.num_keys('Task')
    num_cv = results.num_keys('CV')
    Ky = zeros((num_tasks, num_tasks, num_cv))
    for split in results.keys('CV'):
        train_ex, train_lab, test_ex, test_lab = get_multitask(all_tasks, all_data, split, binclass=False)
        svm = KernelOptLS(kernel=LinearKernel())
        print 'Split %d: SVM with %d features and %d examples'\
              % (split, train_ex.shape[0], train_ex.shape[1])
        svm.train(train_ex, train_lab)
        print svm.Ky
        Ky[:,:,split] = svm.Ky
        svm.get_max = False
        svmout = svm.predict(test_ex)
        #hist(transpose(svmout),30)
        #show()
        balacc = balanced_accuracy_multitask(svmout, test_lab)
        res = stats_binormal_multitask(svmout, test_lab)
        for (ix, task) in enumerate(results.keys('Task')):
            print 'Accuracy (%s): %1.2f' % (task, balacc[ix])
            print 'AUC: %1.2f, APR: %1.2f' % (res[3][ix], res[4][ix])
            results.fill_value(balacc[ix], [split, task, name, 'accuracy'],
                               ['CV','Task','Method','Perf'])
            results.fill_value(res[4][ix], [split, task, name, 'area PR'],
                               ['CV','Task','Method','Perf'])
    return Ky

#####################################################################
# Collect results
#####################################################################

def _plot_perf(all_perf, perf, num_cv=5):
    """Plot the results"""
    mean_perf = map(mean, all_perf.values())
    idx = argsort(mean_perf)
    sorted_task = array(all_perf.keys())[idx]
    # Tasks reported in Christian and Nora's MTL paper.
    #sorted_task = ['A_0201', 'A_0202', 'A_0203', 'A_2301', 'A_2402', 'A_2403', 'A_6901']

    figure()
    x = 0
    for task in sorted_task:
        data = all_perf[task]
        plot(x*ones(num_cv), data, 'rx', hold=True)
        x += 1
    xticks(range(x+1), sorted_task, rotation='vertical')
    cur_ax = axis()
    axis((-1, len(sorted_task), cur_ax[2], cur_ax[3]))
    ylabel(perf)

def _dict2array(all_perf):
    """Convert a dictionary of tasks to an array for plotting"""
    all_tasks = []
    all_vals = []
    for (key, val) in all_perf.iteritems():
        all_tasks.append(key)
        all_vals.append(val)
    return all_tasks, all_vals

def boxplot_group(all_perf, expt_id, base_xval, num_expt=3):
    """Assume a 2D array in all_perf.
    Each row corresponds to a cross validation split,
    and each column corresponds to a group.
    Within the group, this is the expt_id of num_expt result.
    """
    (num_cv, num_group) = all_perf.shape
    xval = array(base_xval) + expt_id - num_expt/2
    boxplot(all_perf, positions=xval, hold=True)
    

def show_results(filename):
    """Plot the results"""
    results = load(filename)
    plot_size = results.num_keys('Task')*(results.num_keys('Method')+1)+1
    xval = range(1, plot_size, 4)
    
    figure()
    for (ix, meth) in enumerate(results.keys('Method')):
        accs = results.slice(['Method','Perf'], [meth,'accuracy'])
        boxplot_group(accs, ix, xval)
    xticks(xval, results.keys('Task'))
    ylabel('Balanced Accuracy')
    axis((-2, plot_size, 0.4, 1))
    
    figure()
    for (ix, meth) in enumerate(results.keys('Method')):
        aprs = results.slice(['Method','Perf'], [meth,'area PR'])
        boxplot_group(aprs, ix, xval)
    xticks(xval, results.keys('Task'))
    ylabel('Area under Precision Recall Curve')
    axis((-2, plot_size, 0, 1))

    show()

def pickle2mat():
    """Convert the MHC data from pickle to matlab format"""
    from scipy.io import savemat
    all_data = cPickle.load(open(os.path.expanduser('~')
                                 +'/Data/iedb_benchmark/iedb_data_pca.pickle','r'))
    mat_data = {}
    for (task, data) in all_data.iteritems():
        val = {}
        val['class_labels'] = vstack(data.class_labels)
        val['encoded_peptides'] = vstack(data.encoded_peptides)
        val['encoding'] = vstack(data.encoding)
        val['ic50s'] = vstack(data.ic50s)
        val['nof_peptides'] = data.nof_peptides
        val['peptides'] = vstack(data.peptides)
        val['scaled_labels'] = vstack(data.scaled_labels)
        mat_data[task] = val
    savemat(os.path.expanduser('~')+'/Data/iedb_benchmark/iedb_data_pca.mat', mat_data)

if __name__ == '__main__':
    all_data = cPickle.load(open(os.path.expanduser('~')
                                 +'/Data/iedb_benchmark/iedb_data_pca.pickle','r'))
    #all_tasks = ['A_0201', 'A_0202', 'A_0203', 'A_2301', 'A_2402', 'A_2403', 'A_6901']
    all_tasks = ['A_2301', 'A_2402', 'A_2403','A_6901']
    #all_tasks = ['A_2301', 'A_2402', 'A_2403']
    data_stats(all_data, all_tasks)

    # Initialize the results storage
    results = NArray(['CV','Task','Method','Perf'])
    results.add_keys(range(5), 'CV')
    results.add_keys(all_tasks, 'Task')
    results.add_keys(['single_task','multitask_cat','multitask_learn'], 'Method')
    results.add_keys(['accuracy', 'area PR'], 'Perf')

    # All the computational work is here
    expt_name = '%dtasks' % len(all_tasks)
    res_name = 'multitask_mhc_%s.pkl' % expt_name
    sim_name = 'multitask_mhc_Ky_%s.pkl' % expt_name
    Ky = None
    if True:
        single_task(all_data, results)
        save(res_name, results)
        multitask_concatenate(all_data, results)
        save(res_name, results)
        Ky = learn_output(all_data, results)
        save(res_name, results)
        save(sim_name, Ky)

    if Ky is None:
        Ky = load(sim_name)
    matshow(mean(Ky, axis=2))
    xticks(range(len(all_tasks)), all_tasks)
    yticks(range(len(all_tasks)), all_tasks)
    colorbar()
    show_results(res_name)
        
