"""Some code for running experiments"""

import numpy
from numpy import zeros, argmax, argmin, savetxt
from numpy import vstack, mean, sum, max, min
from pylab import semilogx, xlabel, ylabel, legend, loglog
from pylab import figure, title, show, savefig, subplot
import scipy.stats

from optwok.evaluation import accuracy, rmse, r2, spearman
from optwok.evaluation import differential_entropy, relative_entropy
from optwok.io_pickle import save, load

def plot_acc(dataname, all_reg_param, all_acc, diag_acc, filename):
    figure()
    subplot(211)
    assert(all_acc.shape == diag_acc.shape)
    if len(all_acc.shape) == 2:
        semilogx(all_reg_param, mean(all_acc, axis=0), 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc.T, 'b+')
        semilogx(all_reg_param, mean(diag_acc, axis=0), 'g-', label='identity')
        semilogx(all_reg_param, diag_acc.T, 'gx')
    else:
        semilogx(all_reg_param, all_acc, 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc, 'b+')
        semilogx(all_reg_param, diag_acc, 'g-', label='identity')
        semilogx(all_reg_param, diag_acc, 'gx')
        
    ylabel('accuracy (%)')
    legend(loc='upper left')
    title(dataname)
    max_diag_acc = max(diag_acc, axis=1)
    diff_acc = zeros(all_acc.shape)
    for ix in range(len(all_reg_param)):
        diff_acc[:,ix] = all_acc[:,ix]-max_diag_acc
    subplot(212)
    semilogx(all_reg_param, diff_acc.T, 'r+')
    semilogx([all_reg_param[0],all_reg_param[-1]], [0,0], 'k-')
    ylabel('improvement from learning L')
    xlabel('regularization parameter')
    savefig(filename)

def plot_rmse(dataname, all_reg_param, all_acc, diag_acc, filename):
    figure()
    subplot(211)
    assert(all_acc.shape == diag_acc.shape)
    if len(all_acc.shape) == 2:
        semilogx(all_reg_param, mean(all_acc, axis=0), 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc.T, 'b+')
        semilogx(all_reg_param, mean(diag_acc, axis=0), 'g-', label='identity')
        semilogx(all_reg_param, diag_acc.T, 'gx')
    else:
        semilogx(all_reg_param, all_acc, 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc, 'b+')
        semilogx(all_reg_param, diag_acc, 'g-', label='identity')
        semilogx(all_reg_param, diag_acc, 'gx')
        
    ylabel('rmse (lower is better)')
    legend(loc='lower right')
    title(dataname)
    subplot(212)
    min_diag_acc = min(diag_acc, axis=1)
    diff_acc = zeros(all_acc.shape)
    for ix in range(len(all_reg_param)):
        diff_acc[:,ix] = min_diag_acc-all_acc[:,ix]
    semilogx(all_reg_param, diff_acc.T, 'r+')
    semilogx([all_reg_param[0],all_reg_param[-1]], [0,0], 'k-')
    ylabel('improvement from learning L')
    xlabel('regularization parameter')
    savefig(filename)

def plot_corr(dataname, all_reg_param, all_acc, diag_acc, filename):
    figure()
    subplot(211)
    assert(all_acc.shape == diag_acc.shape)
    if len(all_acc.shape) == 2:
        semilogx(all_reg_param, mean(all_acc, axis=0), 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc.T, 'b+')
        semilogx(all_reg_param, mean(diag_acc, axis=0), 'g-', label='identity')
        semilogx(all_reg_param, diag_acc.T, 'gx')
    else:
        semilogx(all_reg_param, all_acc, 'b-', hold=True, label='learn L')
        semilogx(all_reg_param, all_acc, 'b+')
        semilogx(all_reg_param, diag_acc, 'g-', label='identity')
        semilogx(all_reg_param, diag_acc, 'gx')
        
    ylabel('Corr. (higher is better)')
    legend(loc='lower right')
    title(dataname)
    subplot(212)
    max_diag_acc = max(diag_acc, axis=1)
    diff_acc = zeros(all_acc.shape)
    for ix in range(len(all_reg_param)):
        diff_acc[:,ix] = all_acc[:,ix]-max_diag_acc
    semilogx(all_reg_param, diff_acc.T, 'r+')
    semilogx([all_reg_param[0],all_reg_param[-1]], [0,0], 'k-')
    ylabel('improvement from learning L')
    xlabel('regularization parameter')
    savefig(filename)

def plot_entropy(dataname, all_reg_param, all_Ky, filename):
    num_param = len(all_reg_param)
    diff_ent = zeros(num_param)
    rel_ent = zeros(num_param)
    for ix in range(num_param):
        diff_ent[ix] = differential_entropy(all_Ky[:,:,ix])
        rel_ent[ix] = relative_entropy(all_Ky[:,:,ix])
    figure()
    subplot(211)
    loglog(all_reg_param, diff_ent, 'b-', hold=True)
    loglog(all_reg_param, diff_ent, 'b+')
    ylabel('differential entropy')
    title(dataname)
    subplot(212)
    loglog(all_reg_param, rel_ent, 'g-', hold=True)
    loglog(all_reg_param, rel_ent, 'gx')
    ylabel('relative entropy')
    xlabel('regularization parameter')
    savefig(filename)

def _multi_argbest(all_acc, order):
    """Heuristic rule to deal with equal best"""
    num_param = len(all_acc)
    max_idx_right = num_param - order(all_acc[::-1]) - 1
    max_idx_left = order(all_acc)
    if max_idx_left == 0:
        max_idx = max_idx_right
    elif max_idx_right == num_param:
        max_idx = max_idx_left
    elif max_idx_right >= max_idx_left:
        max_idx = max_idx_right
    else:
        print "Unknown case!"
        print max_idx_left, max_idx_right
    return max_idx

def find_best_idx(labels, all_preds, all_reg_param, perf):
    """Find the parameter with the best performance"""
    if perf == accuracy:
        (acc, max_idx) = find_best_idx_acc(labels, all_preds, all_reg_param)
    elif perf == rmse:
        (acc, max_idx) = find_best_idx_rmse(labels, all_preds, all_reg_param)
    elif perf in [scipy.stats.pearsonr, r2, spearman]:
        (acc, max_idx) = find_best_idx_corr(perf, labels, all_preds, all_reg_param)
    else:
        raise NotImplementedError
    return acc, max_idx

def find_best_idx_acc(labels, all_preds, all_reg_param):
    """Find the maximum accuracy"""
    num_param = len(all_reg_param)
    all_acc = zeros(num_param)
    for ix in range(num_param):
        all_acc[ix] = accuracy(all_preds[:,ix], labels)
    max_idx = _multi_argbest(all_acc, argmax)
    print 'Best accuracy (at lambda = %e): %2.2f' % (all_reg_param[max_idx], 100.0*all_acc[max_idx])
    return (all_acc, max_idx)

def find_best_idx_rmse(labels, all_preds, all_reg_param):
    """Find the minimum root mean square error"""
    num_param = len(all_reg_param)
    all_acc = zeros(num_param)
    for ix in range(num_param):
        all_acc[ix] = sum(rmse(all_preds[:,:,ix], labels))
    max_idx = _multi_argbest(all_acc, argmin)
    print 'Best squared error (at lambda = %e): %f' % (all_reg_param[max_idx], all_acc[max_idx])
    return (all_acc, max_idx)

def find_best_idx_corr(perf, labels, all_preds, all_reg_param):
    """Find the maximum correlation"""
    print('Finding best index using %s' % perf.__name__)
    num_param = len(all_reg_param)
    all_acc = zeros(num_param)
    for ix in range(num_param):
        acc = 0.0
        for task in range(all_preds.shape[0]):
            res = perf(all_preds[task,:,ix], labels[task,:])
            if type(res) == numpy.float64:
                acc += res
            else:
                # pearsonr returns 2 tuple (corr, pval)
                acc += res[0]
        all_acc[ix] = acc/float(all_preds.shape[0])
    max_idx = _multi_argbest(all_acc, argmax)
    print 'Best correlation (at lambda = %e): %f' % (all_reg_param[max_idx], all_acc[max_idx])
    return (all_acc, max_idx)

def predict_all(test_ex, test_lab, kern_mach, outdir, dataname, use_saved=False, 
                perf=accuracy, reg_index=False):
    """Find the best support vectors and predict for all values
    of the regularization parameter"""
    if use_saved:
        return predict_all_saved(test_ex, test_lab, kern_mach, outdir, dataname, perf)

    print 'Results for learned L'
    all_preds = kern_mach.predict_reg_path(test_ex)
    #save('%s/%s_preds.pkl' % (outdir, dataname), all_preds)
    (all_acc, max_idx) = find_best_idx(test_lab, all_preds, kern_mach.all_reg_param, perf)
    if reg_index:
        all_acc = max_idx

    # Update and save kern_mach that uses the best regularization parameter.
    kern_mach.Ky = kern_mach.all_Ky[:,:,max_idx]
    kern_mach.reg_param = kern_mach.all_reg_param[max_idx]
    kern_mach.infer_sv()
    #save('%s/%s_svm.pkl' % (outdir, dataname), kern_mach)
    # Double check that we have saved the right svm
    pred = kern_mach.predict(test_ex)
    #print vstack([pred, test_lab])
    #print 'Test performance: %2.2f' % (100.0*perf(pred, test_lab))
        
    #savetxt('%s/%s_preds.txt' % (outdir, dataname), pred, fmt='%d')
    #kern_mach.all_Ky = None
    #kern_mach.save_Ky(dataname, outdir)

    print 'Results for identity L'
    diag_preds = kern_mach.predict_reg_path(test_ex, ident_Ky=True)
    #save('%s/%s_preds_diag.pkl' % (outdir, dataname), diag_preds)
    (diag_acc, max_idx) = find_best_idx(test_lab, diag_preds, kern_mach.all_reg_param, perf)
    if reg_index:
        diag_acc = max_idx

    return all_acc, diag_acc

def predict_all_saved(test_ex, test_lab, kern_mach, outdir, dataname, perf):
    """Find the best support vectors and predict for all values
    of the regularization parameter. Use saved objects."""
    print 'Results for learned L'
    all_preds = load('%s/%s_preds.pkl' % (outdir, dataname))
    (all_acc, max_idx) = find_best_idx(test_lab, all_preds, kern_mach.all_reg_param, perf)
    kern_mach.Ky = kern_mach.all_Ky[:,:,max_idx]
    kern_mach.reg_param = kern_mach.all_reg_param[max_idx]
    pred = kern_mach.predict(test_ex)
    #print 'Test performance: %2.2f' % (100.0*perf(pred, test_lab))
    kern_mach.all_Ky = None
    kern_mach.save_Ky(dataname, outdir)
    print 'Results for identity L'
    diag_preds = load('%s/%s_preds_diag.pkl' % (outdir, dataname))
    (diag_acc, max_idx) = find_best_idx(test_lab, diag_preds, kern_mach.all_reg_param, perf)

    return all_acc, diag_acc

