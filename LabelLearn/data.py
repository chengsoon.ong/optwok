class Data(object):
    """major histocompatibility complex (MHC-I) proteins"""
    def __init__(self):
        self.peptides = []
	self.encoded_peptides = []
	self.ic50s = []
	self.class_labels = []
	self.scaled_labels = []
	self.encoding = ''
	self.nof_peptides = 0
