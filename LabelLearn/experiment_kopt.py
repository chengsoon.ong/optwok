"""Learning the kernel on labels."""

import sys, pylab
from pprint import pprint
from numpy import matrix, sign, sum, eye, sqrt
from optwok.expttools import apply_to_combination as atc
from optwok.mldata import Dataset
from optwok.kernel import JointKernel, GaussKernel, LinearKernel
from optwok.derfullrank import DerFullRank
from optwok.multiclass import StructPredictor
from optwok.evaluation import accuracy
from optwok.kernelopt import KernelOpt
from optwok.kernel import normalize_unit_diag
from optwok.io_pickle import load, save

def configure_from_file(filename, algo):
    """Configure the experiment"""
    config = dict()
    execfile(filename,{'algo':algo},config)
    config['algo'] = algo
    return config

def train_multiclass(expt_idx, config):
    """Train the multiclass solver"""
    dataname = config['datasets'][expt_idx]
    print 'Training: %s' % dataname
    data_dir = config['data_dir']
    dataset = Dataset(dataname, data_file='%s/%s.libsvm' % (data_dir, dataname),
                      perm_file='%s/%s_perm.txt' % (data_dir, dataname),
                      frac_train=config['frac_train'])
    (idx_train, idx_pred, id_str) = dataset.get_perm(0, 'test')
    print '%d examples, %d classes' % (dataset.num_examples, dataset.num_class)
    if dataname == 'inex':
        print 'Using Linear kernel'
        Kx = LinearKernel()
    else:
        width = 1.0/config['width'][expt_idx]
        print 'Using Gaussian kernel, width = %f' % width
        Kx = GaussKernel(width)
    kernel = JointKernel(Kx, num_classes=dataset.num_class)
    algo = eval(config['algo'])
    svm = algo(config['reg_param'][expt_idx], kernel)
    svm.train(dataset.examples[:,idx_train], dataset.labels[idx_train])
    save(config['modelfile']+'_'+dataname+'.pkl', svm.alpha)

    train_out = svm.predict(dataset.examples[:,idx_train])
    print 'Training accuracy:\t %1.2f' % accuracy(train_out, dataset.labels[:,idx_train])
    test_out = svm.predict(dataset.examples[:,idx_pred])
    print 'Test accuracy:\t\t %1.2f' % accuracy(test_out, dataset.labels[:,idx_pred])

def learn_Ky(expt_idx, config):
    """Learn the kernel on labels"""
    dataname = config['datasets'][expt_idx]
    data_dir = config['data_dir']
    dataset = Dataset(dataname, data_file='%s/%s.libsvm' % (data_dir, dataname),
                      perm_file='%s/%s_perm.txt' % (data_dir, dataname),
                      frac_train=config['frac_train'])
    (idx_train, idx_pred, id_str) = dataset.get_perm(0, 'test')
    print '%d examples, %d classes' % (dataset.num_examples, dataset.num_class)
    if dataname == 'inex':
        print 'Using Linear kernel'
        Kx = LinearKernel()
    else:
        width = 1.0/config['width'][expt_idx]
        print 'Using Gaussian kernel, width = %f' % width
        Kx = GaussKernel(width)
    Kx.compute(dataset.examples[:,idx_train])
    kern_mach = KernelOpt()
    kern_mach.classifier.Kx = Kx.kmat
    alpha = load(config['modelfile']+'_'+dataname+'.pkl')
    #print 'Row sums:'
    #print sum(alpha, axis=1)
    print 'Column sums:'
    print sum(alpha, axis=0)
    
    # The between class norm
    Ktarget = normalize_unit_diag(matrix(alpha).T*matrix(Kx.kmat)*matrix(alpha))
    print Ktarget
    pylab.gray()
    pylab.matshow(Ktarget)
    pylab.title(dataname)
    pylab.colorbar()
    pylab.savefig('%s/%s_target.pdf' % (config['work_dir'], dataname))
    
    # The learned kernel
    Ky = kern_mach.solve_opt(alpha)
    print Ky
    pylab.matshow(Ky)
    pylab.title(dataname)
    pylab.colorbar()
    pylab.savefig('%s/%s_Ky.pdf' % (config['work_dir'], dataname))

def iterate_Ky(expt_idx, config, stop_eps=0.1, max_iter=20, target='iter'):
    """Train the multiclass solver repeatedly, with Ky updated"""
    dataname = config['datasets'][expt_idx]
    print 'Training: %s' % dataname
    data_dir = config['data_dir']
    dataset = Dataset(dataname, data_file='%s/%s.libsvm' % (data_dir, dataname),
                      perm_file='%s/%s_perm.txt' % (data_dir, dataname),
                      frac_train=config['frac_train'])
    (idx_train, idx_pred, id_str) = dataset.get_perm(0, 'test')
    print '%d examples, %d classes' % (dataset.num_examples, dataset.num_class)
    if dataname == 'inex':
        print 'Using Linear kernel'
        Kx = LinearKernel()
    else:
        width = 1.0/config['width'][expt_idx]
        print 'Using Gaussian kernel, width = %f' % width
        Kx = GaussKernel(width)
    kernel = JointKernel(Kx, num_classes=dataset.num_class)
    algo = eval(config['algo'])

    tot_Ky = eye(dataset.num_class)
    prop = 2.0/(1.0+sqrt(5))
    cur_iter = 0
    change = 1.0
    while change > stop_eps and cur_iter < max_iter:
        cur_iter += 1
        svm = algo(config['reg_param'][expt_idx], kernel)
        if target == 'sdp':
            svm.kernel.compute(dataset.examples[:,idx_train])
            kern_mach = KernelOpt()
            kern_mach.classifier.Kx = svm.kernel.Kx.kmat
        svm.kernel.Ky = tot_Ky
        svm.train(dataset.examples[:,idx_train], dataset.labels[idx_train])
        save(config['modelfile']+'_'+target+'_'+dataname+'.pkl', svm.alpha)

        train_out = svm.predict(dataset.examples[:,idx_train])
        print 'Training accuracy:\t %1.2f' % accuracy(train_out, dataset.labels[:,idx_train])
        test_out = svm.predict(dataset.examples[:,idx_pred])
        print 'Test accuracy:\t\t %1.2f' % accuracy(test_out, dataset.labels[:,idx_pred])

        if target == 'iter':
            Ktarget = normalize_unit_diag(matrix(svm.alpha).T*matrix(svm.kernel.Kx.kmat)*matrix(svm.alpha))
        elif target == 'sdp':
            Ktarget = kern_mach.solve_opt(svm.alpha)

        change = sum(abs(tot_Ky - Ktarget))
        print 'change: %f' % change
        tot_Ky = prop*tot_Ky + (1.0-prop)*Ktarget
        del svm
        if target == 'sdp':
            del kern_mach

    pylab.gray()                                                                    
    pylab.matshow(tot_Ky)
    pylab.title(dataname)
    pylab.colorbar()
    pylab.savefig('%s/%s_%sKy.pdf' % (config['work_dir'], dataname, target))




def main(config):
    """Run the experiment for learning the kernel on labels (Ky)"""
    for expt_idx in range(len(config['datasets'])):
        #train_multiclass(expt_idx, config)
        #learn_Ky(expt_idx, config)
        iterate_Ky(expt_idx, config, target='iter', max_iter=config['max_iter'])
        #iterate_Ky(expt_idx, config, target='sdp', max_iter=config['max_iter'])


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: python %s configure.py algo' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    algo = sys.argv[2]
    settings = configure_from_file(filename, algo)
    print 'Running %s experiments with the following settings' % algo
    pprint(settings)
    main(settings)
