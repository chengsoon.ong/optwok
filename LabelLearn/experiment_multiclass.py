"""Learning the kernel on labels.
Uase coordinate descent to optimize least squares SVM.
"""

import sys
from pprint import pprint
from numpy import eye, zeros, logspace

from optwok.mldata import DatasetBase, Dataset
from optwok.kernel import GaussKernel, LinearKernel
from optwok.evaluation import accuracy
from optwok.kernelopt import KernelOptLS
from optwok.io_pickle import load, save

from expttools import plot_acc, predict_all, plot_entropy

def configure_from_file(filename):
    """Configure the experiment"""
    config = dict()
    execfile(filename, {'algo':'LSSVM'}, config)
    return config

def load_data(data_dir, dataname, perm_idx, frac_train):
    """Wrapper to load data"""
    dataset = Dataset(dataname, data_file='%s/%s.libsvm' % (data_dir, dataname),
                      perm_file='%s/%s_perm.txt' % (data_dir, dataname),
                      frac_train=frac_train)
    (idx_train, idx_pred, id_str) = dataset.get_perm(perm_idx, 'test')

    return (dataset, idx_train, idx_pred)

def learn_Ky(expt_idx, config, perm_idx=0,all_reg=None):
    """Learn the kernel on labels"""
    dataname = config['datasets'][expt_idx]
    print '\nlearn_Ky on %s, permutation %d' % (dataname, perm_idx)
    (dataset, idx_train, idx_pred) = load_data(config['data_dir'], dataname,
                                               perm_idx, config['frac_train'])
    print '%d examples, %d classes' % (dataset.num_examples, dataset.num_class)
    print 'Training with %d examples' % (len(idx_train))
    if 'width' in config:
        width = 1.0/config['width'][expt_idx]
        print 'Using Gaussian kernel, width = %f' % width
        Kx = GaussKernel(width)
    else:
        print 'Using Linear kernel'
        Kx = LinearKernel()
    kern_mach = KernelOptLS(kernel=Kx)
    if dataname == 'usps':
        kern_mach.verbose = True
    kern_mach.train_reg_path(dataset.examples[:,idx_train], dataset.labels[:,idx_train],
                             all_reg_param=all_reg)
    save('%s/%s_%02d_svm.pkl' % (config['work_dir'], dataname, perm_idx), kern_mach)

def collect_results(expt_idx, config, perm_idx=0, use_saved=False):
    """Present the results"""
    dataname = config['datasets'][expt_idx]
    print '\ncollect_results on %s, permutation %d' % (dataname, perm_idx)
    kern_mach = load('%s/%s_%02d_svm.pkl' % (config['work_dir'], dataname, perm_idx))
    (dataset, idx_train, idx_pred) = load_data(config['data_dir'], dataname,
                                               perm_idx, config['frac_train'])
    cur_data = '%s_%02d' % (dataname, perm_idx)
    (all_acc, diag_acc) = predict_all(dataset.examples[:,idx_pred], dataset.labels[:,idx_pred],
                                      kern_mach, config['work_dir'], cur_data,
                                      use_saved=use_saved)
    return (all_acc, diag_acc)
    
def main(config):
    """Run the experiment for learning the kernel on labels (Ky)"""
    all_reg_param = logspace(-2,4,30)
    for expt_idx in range(len(config['datasets'])):
        if len(config['perm_idx']) > 1:
            all_acc = zeros((len(config['perm_idx']),30))
            diag_acc = zeros((len(config['perm_idx']),30))
            for (ix, perm_idx) in enumerate(config['perm_idx']):
                learn_Ky(expt_idx, config, perm_idx, all_reg_param)
                (all_acc[ix,:], diag_acc[ix,:]) = collect_results(expt_idx, config, perm_idx)
        else:
            perm_idx = config['perm_idx'][0]
            #learn_Ky(expt_idx, config, perm_idx)
            (all_acc, diag_acc) = collect_results(expt_idx, config, perm_idx, use_saved=True)
        plot_acc(config['datasets'][expt_idx], all_reg_param, 100*all_acc, 100*diag_acc,
                 '%s/%s_acc.pdf' % (config['work_dir'], config['datasets'][expt_idx]))

def entropy(config):
    """Learn the kernel, then measure entropy of kernel matrix"""
    all_reg_param = logspace(-4,4,30)
    assert(len(config['perm_idx']) == 1)
    for expt_idx in range(len(config['datasets'])):
        perm_idx = config['perm_idx'][0]
        learn_Ky(expt_idx, config, perm_idx)
        kern_mach = load('%s/%s_%02d_svm.pkl' % (config['work_dir'],
                                                 config['datasets'][expt_idx], perm_idx))
        plot_entropy(config['datasets'][expt_idx], all_reg_param, kern_mach.all_Ky,
                 '%s/%s_ent.pdf' % (config['work_dir'], config['datasets'][expt_idx]))

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s configure.py' % sys.argv[0]
        exit(1)
    filename = sys.argv[1]
    settings = configure_from_file(filename)
    print 'Running experiments with the following settings'
    pprint(settings)
    main(settings)
    #entropy(settings)
