import sys
sys.path.append('/Users/cong/lib/python2.6/site-packages')
from optwok.io_pickle import load
from optwok.kernel import kernel2distance, center
from numpy import sort, triu, array
import csv

try:
    graph = ximport("graph")
except ImportError:
    graph = ximport("__init__")
    reload(graph)


caltech = 256
if caltech == 101:
    all_K = load('forSoerenCaltech101_nTrain30_Ky.pkl')
    reader = csv.reader(open('caltech_101_classes.txt','r'))
    size(800, 800)
elif caltech == 256:
    all_K = load('forCheng30_Ky.pkl')
    reader = csv.reader(open('caltech_256_classes.txt','r'))
    size(1200, 1200)
K = array(center(all_K[:,:,35]))
D = kernel2distance(K)
flatK = sort(triu(K).flatten())
threshold = flatK[-caltech]
class_id = reader.next()

# Custom style
g = graph.create(iterations=1500, distance=2.0)
g.styles.background = color(0.9)
g.styles.text = color(0)
g.styles.fontsize = 20
g.styles.textwidth = 300
g.styles.stroke = color(0.1)
g.styles.strokewidth = 2
g.styles.fill = color(1,0.5,0.5)

# create nodes
for node_id in range(len(class_id)):
    g.add_node(node_id, label=class_id[node_id])
for node in g.nodes:
    node.weight = 0.5

# create edges
for node1 in g.nodes:
    for node2 in g.nodes:
        if K[node1.id, node2.id] > threshold:
            edge_len = D[node1.id, node2.id]
            g.add_edge(node1.id, node2.id, length=edge_len)

g.layout.tweak(k=3, r=4)
g.styles.apply()
g.prune()
g.solve()
g.draw(weighted=True, directed=False, highlight=[], traffic=None)

