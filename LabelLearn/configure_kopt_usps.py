""" A config file which can be modified to set the paths root directories, etc."""

#####################################################################
### Location of data
#####################################################################

import os
# Base directory
if os.path.isdir('/local/cong/'):
    base_dir = '/local/cong/'
    temp_dir = base_dir
elif os.path.isdir('/Users/cong/'):
    base_dir = '/Users/cong/'
    temp_dir = base_dir + 'temp/'
elif os.path.isdir('/cluster/home/infk/cong/'):
    base_dir = '/cluster/home/infk/cong/'
    temp_dir = base_dir
else:
    print 'No base directory found'
    exit()


# Source of data and permutations
data_dir = base_dir + 'Data/multiclass'

# Save trained SVMs and predictions here
work_dir = '%s/work_LSSVM/' % base_dir

#####################################################################
### Model selection
#####################################################################

#datasets = ['iris','wine','glass','vehicle']
#reg_param = [1.0, 1.0, 1.0, 1.0]
#width = [2.4, 17.0, 1.6, 18.1]#

datasets = ['usps']
#reg_param = [10.0]
width = [40.0]
#max_iter = 5

#datasets = ['inex']
#reg_param = 100.0
#linear kernel!

#modelfile = '%s/svm_model' % work_dir


#####################################################################
### Experimental setup
#####################################################################

# Use the following permutations for current experiment
#perm_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
perm_idx = [0]

# The data splits [frac_train,frac_test]
frac_train = 0.78414712841471279  # USPS split
#frac_train = 0.5  # INEX split

#frac_train = 0.9

# Data split settings for validation
#validation_conf = {'split_type':'val', 'num_cv':5}

# Data split settings for the test set
test_conf = {'split_type':'test'}

