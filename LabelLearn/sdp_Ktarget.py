"""
Analyse the alpha matrix by finding the closest Ky
"""
import pylab
from numpy import eye, vstack, ones, matrix
from numpy import nonzero, abs, zeros, sum, sqrt, unique
from optwok.simdata import multi_cloudgen, multi_cloudgen_mix
from optwok.testtools import plot_confusion
from optwok.multiclass import StructPredictor
from optwok.derfullrank import DerFullRank
from optwok.evaluation import accuracy, accuracy_mix
from optwok.kernelopt import KernelOpt
from optwok.kernel import JointKernel, LinearKernel, GaussKernel
from optwok.kernel import normalize_unit_diag

def demo_learn_Ky(mix_list, id, algo_name='StructPredictor', stop_eps=0.1, max_iter=20):
    """Learn the kernel on labels"""
    print 'demo_learn_Ky'
    #mix_list = [[0,1],[2],[3]]
    num_point = 100
    num_feat = 20
    #num_class = 3
    num_blobs = len(mix_list)
    edge_length = 3

    #data = multi_cloudgen(num_point, num_feat, num_class, edge_length)
    data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, examples, labels) = data
    num_class = len(unique(labels))
    print metadata

    kernel = JointKernel(GaussKernel(), num_classes=num_class)
    algo = eval(algo_name)

    tot_Ky = eye(num_class)
    prop = 2.0/(1.0+sqrt(5))
    cur_iter = 0
    change = 1.0
    while change > stop_eps and cur_iter < max_iter:
        print 'Iteration: %d' % cur_iter
        cur_iter += 1
        svm = algo(1.0, kernel)
        svm.kernel.compute(examples)
        kern_mach = KernelOpt()
        kern_mach.classifier.Kx = svm.kernel.Kx.kmat
        svm.kernel.Ky = tot_Ky

        svm.train(examples, labels)
        #Ktarget = normalize_unit_diag(matrix(svm.alpha).T*matrix(svm.kernel.Kx.kmat)*matrix(svm.alpha))
        Ktarget = kern_mach.solve_opt(svm.alpha)
        print 'Ktarget:'
        print Ktarget
        tot_Ky = prop*tot_Ky + (1.0-prop)*Ktarget
        print 'Ky:'
        print tot_Ky
        change = sum(abs(tot_Ky - Ktarget))
        print 'change: %f' % change
        del svm
        del kern_mach

    pylab.gray()
    plot_confusion(mix_list)
    pylab.savefig('confusion%d.pdf' % id)
    pylab.matshow(tot_Ky)
    pylab.colorbar()
    pylab.savefig('sdpKy%d.pdf' % id)


if __name__ == '__main__':
    # Analyse alpha resulting from multiclass classifier
    demo_learn_Ky([[0],[1],[2],[3],[4]], 0)
    demo_learn_Ky([[0,1],[2],[3],[4]], 1)
    demo_learn_Ky([[0],[1],[2,3,4]], 2)
    demo_learn_Ky([[0],[1,2],[3,4]], 3)
