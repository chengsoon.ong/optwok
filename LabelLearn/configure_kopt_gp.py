""" A config file which can be modified to set the paths root directories, etc."""

#####################################################################
### Location of data
#####################################################################

import os
# Base directory
if os.path.isdir('/local/cong/'):
    base_dir = '/local/cong/'
    temp_dir = base_dir
elif os.path.isdir('/Users/cong/'):
    base_dir = '/Users/cong/'
    temp_dir = base_dir + 'temp/'
elif os.path.isdir('/cluster/home/infk/cong/'):
    base_dir = '/cluster/home/infk/cong/'
    temp_dir = base_dir
else:
    print 'No base directory found'
    exit()
del os

# Source of data and permutations
data_dir = base_dir + 'Data/muloutreg'

# Save trained SVMs and predictions here
work_dir = '%s/work_%s/' % (temp_dir, algo)

#####################################################################
### Model selection
#####################################################################

datasets = ['gaussproc0_1000_0.02']
width = [1.0]

#####################################################################
### Experimental setup
#####################################################################

# Use the following permutations for current experiment
perm_idx = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
frac_train = 10.0/500
# Data split settings for the test set
test_conf = {'split_type':'test'}

