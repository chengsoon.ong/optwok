"""Learning the kernel on labels.
Use coordinate descent to optimize least squares SVM.
"""

import sys
from pprint import pprint
from numpy import matrix, sign, sum, eye, sqrt
from numpy import vstack, savetxt, zeros, argmax
from scipy.io import loadmat
from pylab import semilogx, xlabel, ylabel, legend
from pylab import figure, title, show, savefig

from optwok.expttools import apply_to_combination as atc
from optwok.mldata import Dataset
from optwok.kernel import CustomKernel
from optwok.evaluation import accuracy
from optwok.kernelopt import KernelOptLS
from optwok.kernel import normalize_unit_diag
from optwok.io_pickle import load, save

from expttools import plot_acc, predict_all


#idx_train = 30*20

def learn_Ky(data_dir, dataname, work_dir='/local/cong/work_LSSVM/'):
    """Learn the kernel on labels"""
    print '\nlearn_Ky on %s' % dataname
    matlab_data = loadmat('%s/%s.mat' % (data_dir, dataname))
    if 'Ktrain' in matlab_data:
        train_ex = matlab_data['Ktrain']
    else:
        train_ex = matlab_data['K']
    num_train = train_ex.shape[0]
    print 'Training with %d examples' % (num_train)
    kern_mach = KernelOptLS(reg_param=10.0, kernel=CustomKernel(), verbose=True)
    kern_mach.Kx.compute(train_ex)
    labels = matlab_data['tr_label'].flatten()
    labels -= 1
    del matlab_data
    kern_mach.train_reg_path(train_ex, labels)

    # The learned kernel
    print 'Saving the learned kernel'
    save('%s/%s_svm.pkl' % (work_dir, dataname), kern_mach)

def load_test(data_dir, dataname, work_dir):
    """Load test data and trained kernel machine"""
    matlab_data = loadmat('%s/%s.mat' % (data_dir, dataname))
    test_ex = matlab_data['Ktest'].T
    test_lab = matlab_data['te_label'].flatten()
    test_lab -= 1

    kern_mach = load('%s/%s_svm.pkl' % (work_dir, dataname))
    return test_ex, test_lab, kern_mach

def main(dataname, data_dir, work_dir):
    """Caltech experiment"""
    #learn_Ky(data_dir, dataname)
    (test_ex, test_lab, kern_mach) = load_test(data_dir, dataname, work_dir)
    (all_acc, diag_acc) = predict_all(test_ex, test_lab, kern_mach, work_dir, dataname, use_saved=True)
    plot_acc(dataname, kern_mach.all_reg_param, all_acc, diag_acc,
             '%s/%s_acc.pdf' % (work_dir, dataname))
    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: python %s 101/256' % sys.argv[0]
        exit(1)
    #data_dir = '/local/cong/Data/multiclass'
    #work_dir = '/local/cong/work_LSSVM'
    data_dir = '/Users/cong/Data/multiclass'
    work_dir = '/Users/cong/work_LSSVM'
    if int(sys.argv[1]) == 101:
        dataname = 'caltech101_nTrain30'
    if int(sys.argv[1]) == 256:
        dataname = 'caltech256_nTrain30'
    main(dataname, data_dir, work_dir)
