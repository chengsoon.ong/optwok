"""
Generate toy data to study relationships between classes.
"""
import sys
from numpy.random import seed
from numpy import log10
from pylab import gray, savefig
from optwok.mldata import DatasetFileLibsvm, generate_perm
from optwok.simdata import multi_cloudgen_mix
from optwok.simdata import gp_gen, save_gp_data
from optwok.testtools import plot_confusion

def create_and_save(mix_list, id, output_dir='.',
                    num_point=500, num_feat=100, edge_length=3):
    """
    Generate toy data and save as csv format.
    """
    print 'create_and_save  %d: %s' % (id,str(mix_list))
    num_blobs = len(mix_list)
    #num_point_list = []
    #for ix in mix_list:
    #    num_point_list.append(len(ix)*num_point)
    #num_point = num_point_list

    data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, examples, labels) = data
    outfile = DatasetFileLibsvm('%s/multiclass_sim%d.libsvm' % (output_dir,id),
                             'vec', verbose=True)
    outfile.writelines(examples, labels)
    generate_perm('%s/multiclass_sim%d.libsvm' % (output_dir,id),
                  '%s/multiclass_sim%d_perm.txt' % (output_dir,id))
    
    gray()
    plot_confusion(mix_list)
    savefig('%s/confusion%d.pdf' % (output_dir,id))


def create_multiclass():
    """Create the toy datasets for label learning in the multiclass setting"""
    num_feat = 100
    between_class_dist = log10(num_feat)
    create_and_save([[0],[1],[2],[3],[4]], 0, output_dir,
                    num_feat=num_feat, edge_length=between_class_dist)
    create_and_save([[0,1],[2],[3],[4]], 1, output_dir,
                    num_feat=num_feat, edge_length=between_class_dist)
    create_and_save([[0],[1],[2,3,4]], 2, output_dir,
                    num_feat=num_feat, edge_length=between_class_dist)
    create_and_save([[0],[1,2],[3,4]], 3, output_dir,
                    num_feat=num_feat, edge_length=between_class_dist)

def create_gp_data():
    """Create toy data for multiple output gaussian process regression"""
    num_ex = 500
    num_feat = 1
    domain = 5
    noise_level = 0.02
    mix_list = [[0,1],[2,3,4]]
    (X,Y) = gp_gen(num_ex, num_feat, domain, noise_level, mix_list)
    dataname = 'gaussproc0_%d_%1.2f' % (num_ex, noise_level)
    save_gp_data(X, Y, dataname, data_dir='/local/cong/Data/muloutreg')

if __name__ == '__main__':
    if len(sys.argv) == 2:
        output_dir = sys.argv[1]
    elif len(sys.argv) == 1:
        output_dir = '.'
    else:
        print 'Usage: python %s [output_dir]' % sys.argv[0]
        exit(1)
    seed(1)
    create_multiclass()
    

