from numpy import linspace, zeros, meshgrid
from numpy import min, nonzero
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from pylab import savefig

def invex_objective(all_b, all_c, reg_param=1.2):
    Q = zeros((len(all_b),len(all_c)))
    for (ixb,b) in enumerate(all_b):
        for (ixc,c) in enumerate(all_c):
            Q[ixc, ixb] = ((1/reg_param)*(1-c*b)*(1-c*b) + c*c*b + b*b)/2.0
    return Q

def plot_objective(min_lim=-1, max_lim=1, num_points=150):
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    all_c = linspace(min_lim, 1+max_lim, num_points)
    all_b = linspace(1e-5, max_lim, num_points)
    (L,C) = meshgrid(all_b, all_c)
    Q = invex_objective(all_b, all_c)
    minval = min(Q.flatten())
    (minvalc, minvalb) = nonzero(Q==minval)
    ax.plot(all_b[minvalb], all_c[minvalc], Q[minvalc, minvalb],
            'ro', markersize=10)
    ax.plot_surface(L, C, Q, alpha=0.3)
    ax.contour(L, C, Q, zdir='z', offset=-2)
    ax.contour(L, C, Q, zdir='x', offset=max_lim)
    ax.contour(L, C, Q, zdir='y', offset=min_lim)

    ax.set_xlabel('L')
    #ax.set_xlim3d(0, min_lim)
    ax.set_ylabel('C')
    #ax.set_ylim3d(min_lim, max_lim)
    ax.set_zlabel('Q')
    ax.set_zlim3d(-2, 3)

    savefig('invex_objective.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()


if __name__ == '__main__':
    plot_objective()
    
