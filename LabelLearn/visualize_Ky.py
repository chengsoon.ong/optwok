"""Some tools to present the correlation between
classes in a nice fashion"""

from pylab import matshow, title, colorbar, savefig, gray
from optwok.kernel import kernel2distance, normalize_unit_diag
from optwok.io_pickle import load
from scipy.io import savemat

def plot_distance(dataname, work_dir):
    """Convert kernel into corresponding Euclidean distance"""
    print 'plot_distance(%s, %s)' % (dataname, work_dir)
    Ky = load('%s/%s_Ky.pkl' % (work_dir, dataname))
    D = kernel2distance(Ky)
    matshow(D)
    title(dataname)
    colorbar()
    savefig('%s/%s_dist.pdf' % (work_dir, dataname))

    
def plot_kernel_single(Ky, dataname, work_dir):
    """Plot kernel, normalize to unit diagonal"""
    normKy = normalize_unit_diag(Ky)
    gray()
    matshow(normKy)
    title(dataname)
    colorbar()
    savefig('%s/%s_Ky.pdf' % (work_dir, dataname))

def plot_kernel(dataname, work_dir):
    """Check how many kernels we have and call plot_kernel_single"""
    print 'plot_kernel(%s, %s)' % (dataname, work_dir)
    Ky = load('%s/%s_Ky.pkl' % (work_dir, dataname))
    savemat('%s/%s_Ky.mat' % (work_dir, dataname), {'Ky':Ky}, do_compression=True)
    if Ky.ndim == 2:
        plot_kernel_single(Ky, dataname, work_dir)
    else:
        assert(Ky.ndim == 3)
        for reg in range(Ky.shape[2]):
            plot_kernel_single(Ky[:,:,reg], '%s_%02d'%(dataname,reg), work_dir)
        
def plot_all_distance():
    plot_distance('learned0', work_dir)
    plot_distance('learned1', work_dir)
    plot_distance('learned2', work_dir)
    plot_distance('learned3', work_dir)

    plot_distance('glass', work_dir)
    plot_distance('iris', work_dir)
    plot_distance('vehicle', work_dir)
    plot_distance('wine', work_dir)
    
    plot_distance('usps', work_dir)
    plot_distance('forSoerenCaltech101_nTrain30', work_dir)

def plot_all_kernel():
    work_dir = '/local/cong/work_LSSVM'
    plot_kernel('learned0', work_dir)
    plot_kernel('learned1', work_dir)
    plot_kernel('learned2', work_dir)
    plot_kernel('learned3', work_dir)
    plot_kernel('learned4', work_dir)
    plot_kernel('learned5', work_dir)
    plot_kernel('learned6', work_dir)

    plot_kernel('glass', work_dir)
    plot_kernel('iris', work_dir)
    plot_kernel('vehicle', work_dir)
    plot_kernel('wine', work_dir)
    
    plot_kernel('usps', work_dir)
    plot_kernel('forSoerenCaltech101_nTrain30', work_dir)
    
if __name__ == '__main__':
    plot_all_kernel()
