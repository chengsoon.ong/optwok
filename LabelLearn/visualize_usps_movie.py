import sys
sys.path.append('/Users/cong/lib/python2.6/site-packages')
from optwok.io_pickle import load
from optwok.kernel import kernel2distance

try:
    graph = ximport("graph")
except ImportError:
    graph = ximport("__init__")
    reload(graph)

size(700, 700)

all_K = load('usps_Ky.pkl')
#D = kernel2distance(all_K[:,:,36])
K = all_K[:,:,36]

g = graph.create(iterations=1000, distance=2.0)

# create nodes
for node_id in range(10):
    g.add_node(node_id)

# create edges
for node1 in g.nodes:
    for node2 in g.nodes:
        g.add_edge(node1.id, node2.id, weight=K[node1.id, node2.id])

g.styles.apply()
g.solve()
g.draw(weighted=True, directed=False, highlight=[], traffic=None)

speed(5)
def setup():
    global ix, multiplier
    multiplier = 2000
    ix = -0.005*multiplier
    g.layout.tweak(r=5)

def draw():
    global ix
    if ix < 0.055*multiplier:
        ix += 1
    for node1 in g.nodes:
        for node2 in g.nodes:
            if K[node1.id, node2.id] < ix/multiplier:
                g.remove_edge(node1.id, node2.id)
    #if int(ix) % int(0.006*multiplier) == 0:
    #    g.prune(depth=0)
    g.layout.refresh()
    g.draw(weighted=True, directed=False, highlight=[], traffic=None)
