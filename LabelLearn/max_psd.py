"""Check whether a positive definite matrix can change the order of a vector"""

from numpy.random import rand
from numpy import matrix, eye
from numpy import sort
from optwok.kernel import normalize_unit_diag

num_class = 5

Z = rand(4, num_class)
K = matrix(Z).T*matrix(Z) + 1e-10*eye(num_class)
K = normalize_unit_diag(K)
preds = matrix(sort(rand(num_class,1), axis=0))
print K
print preds
print K*preds


