import sys
sys.path.append('/Users/cong/lib/python2.6/site-packages')
from optwok.io_pickle import load
from optwok.kernel import kernel2distance, center
from numpy import sort, triu, diag, array

try:
    graph = ximport("graph")
except ImportError:
    graph = ximport("__init__")
    reload(graph)

size(600, 600)

Korig = load('usps_Ky.pkl')
K = array(center(Korig))
flatK = sort(triu(K).flatten())
D = kernel2distance(K)
#flatD = sort(triu(D).flatten())
#threshold = flatD[(11*10/2)+5]
#threshold = flatD[-10]
threshold = flatK[(10*9/2)-11]

# Custom style
g = graph.create(iterations=1300, distance=2.0)
g.styles.background = color(0.9)
g.styles.text = color(0)
g.styles.fontsize = 20
g.styles.stroke = color(0.1)
g.styles.strokewidth = 2
g.styles.fill = color(1,0.5,0.5)

# create nodes
for node_id in range(10):
    g.add_node(node_id)
for node in g.nodes:
    node.weight = 0.5

# create edges
for node1 in g.nodes:
    for node2 in g.nodes:
        if K[node1.id, node2.id] > threshold:
            #edge_len = 100*(D[node1.id, node2.id]-threshold)
            edge_len = D[node1.id, node2.id]
            g.add_edge(node1.id, node2.id, length=edge_len)


g.layout.tweak(r=3)
g.styles.apply()
g.solve()
g.draw(weighted=True, directed=False, highlight=[], traffic=None)

