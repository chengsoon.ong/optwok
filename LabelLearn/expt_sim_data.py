"""
Learn the kernel between classes by iterating.
"""
import pylab
from numpy import eye, vstack, matrix
from numpy import nonzero, abs, zeros, sum, sqrt, unique
from optwok.simdata import multi_cloudgen_mix
from optwok.testtools import plot_confusion
from optwok.evaluation import accuracy, accuracy_mix
from optwok.kernel import LinearKernel
from optwok.kernel import normalize_unit_diag, kernel2distance
from optwok.kernelopt import KernelOptLS
from optwok.multiclass import idx2binvec

def demo_coord_desc(mix_list, id, num_point=100, num_feat=20, work_dir='/local/cong/work_LSSVM'):
    """The examples from the same Gaussians may have different labels."""
    print 'demo_coord_desc %d: %s' % (id,str(mix_list))
    #mix_list = [[0,1],[2],[3],[4]]
    #num_point = 100
    #num_feat = 20
    #num_class = 4
    num_blobs = len(mix_list)
    edge_length = 3

    num_point_list = []
    for ix in mix_list:
        num_point_list.append(len(ix)*num_point)
    num_point = num_point_list

    train_data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, examples, labels) = train_data
    num_class = len(unique(labels))
    print metadata
    test_data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, test_ex, test_lab) = test_data

    kern_mach = KernelOptLS(kernel=LinearKernel(), verbose=True)
    kern_mach.train_reg_path(examples, labels)
    svmout = kern_mach.predict(test_ex)
    #print vstack([svmout, test_lab])
    print 'Test accuracy:\t\t\t %1.2f' % accuracy(svmout, test_lab)
    print 'Test accuracy (mix):\t\t %1.2f' % accuracy_mix(svmout, test_lab, mix_list)

    pylab.gray()
    plot_confusion(mix_list)
    pylab.savefig('%s/confusion%d.pdf' % (work_dir, id))
    kern_mach.save_Ky('learned%d'%id, work_dir)

    #Y = idx2binvec(labels)
    #ycorr = matrix(Y).T*matrix(Y)
    #pylab.matshow(ycorr)
    #pylab.title('ycorr%d'%id)

def demo_small():
    demo_coord_desc([[0],[1],[2],[3],[4]], 0)
    demo_coord_desc([[0,1],[2],[3],[4]], 1)
    demo_coord_desc([[0],[1],[2,3,4]], 2)
    demo_coord_desc([[0],[1,2],[3,4]], 3)
    #pylab.show()

if __name__ == '__main__':
    demo_small()
    #demo_coord_desc([[0],[1,2],[3,4],[5],[6],[7],[8],[9],
    #                 [10],[11,19],[12],[13],[14],[15],[16],[17],[18],
    #                 [20,25,29],[21],[22],[23],[24],[26],[27],[28]],
    #                4, num_point = 60, num_feat=200)
    #demo_coord_desc([[0],[1,2],[3,4],[5],[6],[7],[8],[9],
    #                 [10],[11,19],[12],[13],[14],[15],[16],[17],[18],
    #                 [20,25,29],[21],[22],[23],[24],[26],[27],[28]],
    #                5, num_point = 100, num_feat=200)
    #demo_coord_desc([[0],[1],[2],[3,5],[4,9],[6],[7,9],[8]],
    #                6, num_point = 100, num_feat=200)
