"""
Learn the kernel between classes by iterating.
"""
import pylab
from numpy import eye, vstack, matrix
from numpy import nonzero, abs, zeros, sum, sqrt, unique
from optwok.testtools import plot_confusion
from optwok.simdata import multi_cloudgen_mix
from optwok.multiclass import StructPredictor
from optwok.derfullrank import DerFullRank
from optwok.evaluation import accuracy, accuracy_mix
from optwok.kernel import JointKernel, LinearKernel, GaussKernel
from optwok.kernel import normalize_unit_diag, kernel2distance


def demo_iterate_Ktarget(mix_list, id, algo_name='StructPredictor',
                         stop_eps=0.1, max_iter=100):
    """The examples from the same Gaussians may have different labels."""
    print 'demo_iterate_Ktarget %d: %s' % (id,str(mix_list))
    #mix_list = [[0,1],[2],[3],[4]]
    num_point = 100
    num_feat = 20
    #num_class = 4
    num_blobs = len(mix_list)
    edge_length = 3

    train_data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, examples, labels) = train_data
    num_class = len(unique(labels))
    print metadata
    test_data = multi_cloudgen_mix(num_point, num_feat, num_blobs, edge_length, mix_list)
    (metadata, test_ex, test_lab) = test_data
    kernel = JointKernel(LinearKernel(), num_classes=num_class)
    algo = eval(algo_name)

    tot_Ky = eye(num_class)
    prop = 2.0/(1.0+sqrt(5))
    cur_iter = 0
    change = 1.0
    while change > stop_eps and cur_iter < max_iter:
        print 'Iteration: %d' % cur_iter
        cur_iter += 1
        svm = algo(1.0, kernel)    
        svm.kernel.Ky = tot_Ky
        svm.train(examples, labels)
        #print svm.alpha
        sv = nonzero(abs(svm.alpha) > 1e-6)[0]
        print 'frac_nnz = %f' % (float(sv.size)/float(svm.num_train*svm.num_classes))
        nnz_alpha = zeros(svm.num_classes+1, dtype=int)
        for ix in range(svm.num_train):
            num_sp = len(nonzero(sv==ix)[0])
            nnz_alpha[num_sp] += 1
        print 'examples with nonzero alphas: ', nnz_alpha
        
        train_out = svm.predict(examples)
        #print vstack([train_out, labels])
        print 'Training accuracy:\t\t %1.2f' % accuracy(train_out, labels)
        print 'Training accuracy (mix):\t %1.2f' % accuracy_mix(train_out, labels, mix_list)

        svmout = svm.predict(test_ex)
        #print vstack([svmout, test_lab])
        print 'Test accuracy:\t\t\t %1.2f' % accuracy(svmout, test_lab)
        print 'Test accuracy (mix):\t\t %1.2f' % accuracy_mix(svmout, test_lab, mix_list)

        Ktarget = normalize_unit_diag(matrix(svm.alpha).T*matrix(svm.kernel.Kx.kmat)*matrix(svm.alpha))
        print 'Distance: '
        print kernel2distance(Ktarget)
        change = sum(abs(tot_Ky - Ktarget))
        print 'change: %f' % change
        tot_Ky = prop*tot_Ky + (1.0-prop)*Ktarget
        print 'Ktarget: '
        print Ktarget
        print 'Ky: '
        print tot_Ky
        del svm

    pylab.gray()
    plot_confusion(mix_list)
    pylab.savefig('confusion%d.pdf' % id)
    pylab.matshow(tot_Ky)
    pylab.colorbar()
    pylab.savefig('iterKy%d.pdf' % id)
    
if __name__ == '__main__':
    # Iterate with alpha K alpha
    demo_iterate_Ktarget([[0],[1],[2],[3],[4]], 0, algo_name='DerFullRank')
    demo_iterate_Ktarget([[0,1],[2],[3],[4]], 1, algo_name='DerFullRank')
    demo_iterate_Ktarget([[0],[1],[2,3,4]], 2, algo_name='DerFullRank')
    demo_iterate_Ktarget([[0],[1,2],[3,4]], 3, algo_name='DerFullRank')
